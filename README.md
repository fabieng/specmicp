SpecMiCP / ReactMiCP
====================

Overview
--------

SpecMiCP is a speciation solver to find the equilibrium state of a chemical
system.  The system is based on a mixed complementarity formulation of the
equilibrium condition for minerals.

For a mineral with volume fraction Sm, the equilibrium condition is :

    Sm >= 0, -log(IAPm/K) >= 0, -Sm*log(IAPm/K) = 0

where IAPm is the ion activity product and K the equilibrium constant.
This condition is reformulated using the penalized Fischer-Burmeister C-function and the system is solved
using a semismooth method.

ReactMiCP is a reactive transport solver built on top of specmicp.
It uses the operator splitting approach to couple transport and chemistry.

Modules
-------

SpecMiCP is not (yet) a program but a set of libraries that can be used to solve
specific problems.

The following modules are already available :


- **specmicp** : *core* of the library, set the system, and solve it
- **reactmicp** : the reactive transport solver
- **database** : manage the database, parse it from a file, swap the basis,
      select the correct species, ...
- **micpsolver** : Solve a Mixed Complementarity problem
- **odeint** : integration methods for solving ordinary differential
      equations (for problem involving kinetics for example)
- **dfpm** : a finite element module

The **micpsolver** and **odeint** modules can be use independantly.

Examples of use are provided in the examples directory.

Using SpecMiCP
==============

Examples
--------

To build and install SpecMiCP/ReactMiCP see the INSTALL file.
To learn how to use the API the best is to look at the examples.
The starting point should be the leaching in CO2-saturated examples :
examples/reactmicp/saturated_react/carbonation.cpp.
This file is heavily commented.
Example of use of the Python interface can be found in the tests/cython directory.

Use in your own project
-----------------------

A cmake module can be used to find SpecMiCP in your own project. It will be installed at

    <prefix>/share/specmicp/cmake/FindSpecMiCP.cmake

The python module can be used directly if it is in your python path.

Documentation
-------------

The API is documented with Doxygen and the documentation can be generated with the command :

    make doc

A (maybe outdated) version of the doc can be found [here][3].

Warning
-------

The code is in development and the API is not considered stable yet.

Python binding
--------------

A python binding is available at the following address :

https://bitbucket.org/specmicp/specmicppy

About
=====

SpecMiCP is developped by F. Georget (fabieng aT princeton DoT edu). It is part
of my Ph.D. work. The purpose of the PhD is to develop a reactive transport model
to model the coupling between hydration, drying and carbonation in cement paste.

This Ph.D. is funded by [Princeton university][6], [Lafarge][4] and the [CSTB][5].

References :

- F. Georget, J. H. Prévost, and R. J. Vanderbei. A speciation solver for cement paste modeling and the semismooth Newton method . Cement and Concrete Research, 68(0):139--147, 2015.
- F. Georget, J. H. Prévost and B. Huet Validation of a reactive transport solver based on a semismooth speciation solver (submitted to Computational Geosciences)

A list of the references used in the code can be found in the [documentation][7].

[2]: http://www.empa.ch/plugin/template/empa/*/62204/---/l=1

[3]: http://www.princeton.edu/~fabieng/specmicpdoc/index.html

[4]: http://www.lafarge.com/en

[5]: http://www.cstb.fr/

[6]: http://princeton.edu

[7]: http://www.princeton.edu/~fabieng/specmicpdoc/citelist.html
