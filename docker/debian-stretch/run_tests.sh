#!/bin/bash

export PKG_CONFIG_PATH=/opt/yaml-cpp/lib/pkgconfig

git clone https://bitbucket.org/specmicp/specmicp
cd specmicp
mkdir build && cd build
cmake ../ 
make && ctest
