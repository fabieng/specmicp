# Governing equations and variables {#reactmicp_governing_file}

This page describe the general formulation of ReactMiCP. 
The different systems may override the exact meaning of the terms, however the 
logic will be respected.


[TOC]

# Governing equations {#reactmicp_governing_equations}

The governing equations for component i in phase \f$\alpha\f$ is 
\f[
\frac{\partial \mathcal{C}_i^{\alpha}}{\partial t} = - \nabla 
\mathcal{F}_i^{\alpha} + \sum_{\beta} \mathcal{R}_i^{\beta \rightarrow \alpha}
\f]
where 
- \f$\mathcal{C}_i^{\alpha}\f$ is the concentration of component i in 
phase \f$\alpha\f$.
- \f$\mathcal{F}_i^{\alpha}\f$ is the transport flux operator of component i in 
phase \f$\alpha\f$
- \f$\mathcal{R}_i^{\beta \rightarrow \alpha}\f$ is the chemistry exchange term 
of component i from phase \f$\beta\f$ to phase \f$\alpha\f$.

According this equation, a change of concentration of a component in a phase 
can be due to the transport of this component, or an exchange of this component 
with another phase via a chemical reactions. It should be noted that 
\f$\mathcal{R}_i^{\alpha \rightarrow \alpha}\f$ can be used to model 
kinetically-controlled chemical reactions.


There is \f$N_c \times N_{\alpha}\f$ governing equations, although it should be 
noted that some are trivial or degenerate. For example, it is often assumed 
that there is no transport in the solid phase.

# Variables {#reactmicp_equations}

Three set of variables are defined, corresponding to three staggers (See the 
exact definition of the staggers in the next section).

- \f$\{x\}\f$ the primary variables, solved in the transport stagger
- \f$\{y\}\f$ the secondary variables, solved in the chemistry stagger
- \f$\{w\}\f$ the upscaling variables, solved in the upscaling stagger

## Primary variables

The primary variables are the main variables of the governing equations. For 
example, the saturation, the volume fraction of a mineral, the total liquid 
concentration of an aqueous component, or the partial pressure of a gas can be 
main variables. There is always \f$N_c \times N_{\alpha}\f$ variables even if 
some equations are not solved directly in the transport stagger. It is 
necessary to solve the mass conservation correctly in the sequential iterative 
stagger.

## Secondary variables

The secondary variables are dependant variables of the primary variables. 
Although they do not correspond to a governing equations, the chemistry stagger 
solve a constitutive equations for them.
For example, the porosity is usually defined as a secondary variables, and its 
corresponding equation is the volume closing relationship
\f[
1 = \phi + \sum_m \phi_m
\f]

Another example is the total concentration of the water component in the liquid 
phase in an unsaturated system 
\f[
\tilde{C}_w = \rho_w \left( \frac{1}{M_w} + \sum_j \nu_{jw} bj \right)
\f]
This secondary variables is necessary because the governing equation for water 
is usually solved for the saturation :
\f[
 \mathcal{C}_w^{\alpha} = \tilde{C}_w \phi S
\f]

Finally, main variables of the kinetically-controlled chemical reactions also 
fall in this category.

Since these variables appears in the transient term of the governing equations, 
both the variables, \f$\{y\}\f$ and their rate of change \f$\{\dot{y}\}\f$ must 
be computed in the chemistry stagger.

## Upscaling variables

Briefly, the upscaling variables are all the other parameters needed for the 
computation. They are computed in the upscaling stagger.

In particular, the upscaling variables contain the transport parameters such as 
the diffusion coefficient or the permeabilities.
Some of these variables can actually 
be a function of the main variables. For example, in the unsaturated system, 
the capillary pressure is a function of the saturation. Which variable is 
actually a function is defined by the system.

## Summary

The set of governing equations and variables define a system. 
Three different set of variables are defined depending on their meaning and the 
staggers in which they are solved.

| Variables   | Transport | Chemistry | Upscaling | User-defined | Functions |
| ----------  |:---------:|:---------:|:---------:|:------------:|:---------:|
| \f$\{x\}\f$ |      x    |     x     |           |     No       |           |  
| \f$\{y\}\f$ |           |     x     |           |   Possible   |           |
| \f$\{w\}\f$ |           |           |     x     |    Yes     | system defined|
