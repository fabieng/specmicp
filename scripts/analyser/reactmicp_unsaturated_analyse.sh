#! /usr/bin/env bash

# -----------------------------------------------------------------------------
# Copyright (c) 2015 Fabien Georget <fabieng@princeton.edu>,
# Princeton University
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
# may be used to endorse or promote products derived from this software without
# specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# -----------------------------------------------------------------------------

# This file is a wrapper script to analyse a hdf5 file produced by the
# unsaturated system of ReactMiCP
# It calls a python interpreter in interactive mode and loads a module


# The module where the useful functions are defined
python_script=analyse_unsaturated_hdf5.py

# Return codes
RETCODE_NO_FILE=1
RETCODE_NO_PYTHON=2
RETCODE_FILE_DOESNT_EXIST=3
RETCODE_BAD_OPTION=4
RETCODE_BAD_PYTHON_MODULE=5

echo "
===================================

           ReactMiCP
          ===========
            
 Unsaturated HDF5 output analyser
 --------------------------------

===================================

Copyright (c) 2015 F. Georget <fabieng@princeton.edu>

"

function check_file_arg {
    # check that the file argument exist
    if [[ ! $1 || -z $1 ]]
    then 
        echo "Error : one argument required. Aborting" >&2;
        exit ${RETCODE_NO_FILE};
    fi
}

function print_help {
    echo " 
Analyse hdf5 file produced by the unsaturated system of ReactMiCP.
The analyser is a script which open a python interpreter and load useful functions.
 
Usage : $0 [options] <file> 

<file> : hdf5 file 
    
Options :
    -m, --module <module> alternative python module to use
    -p, --python <interpreter> python interpreter to use
    -h, --h : print this help" 
    exit 0
}

check_file_arg $1

# Options
# -------
while [[ $# > 1 ]]
do
    key="$1"
    case ${key} in
        -h|--help)
            print_help
            ;;
        -m|--module)
            python_script="$2"
            shift
            ;;
        -p|--python)
            python_shell="$2"
            shift
            ;;
        *)
            echo "Unknow option '$1'. Aborting" >&2
            exit ${RETCODE_BAD_OPTION}
            ;;
    esac
shift
done

check_file_arg $1

if [[ $1 = "-h" || $1 = "--help" ]]
then
    print_help
fi

# check that the HDF5 file exist
# no point going further if it doesn't
if [[ ! -f $1 ]]
then
    echo "Error : no such file : '$1'. Aborting" >&2;
    exit ${RETCODE_FILE_DOESNT_EXIST}
fi

# copy file parameter
filepath=$1
shift

# Find the python script
if [[ ! -f ${python_script} ]]
then
    # Find the true directory of this bash script
    # https://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
    SOURCE="${BASH_SOURCE[0]}"
    while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
        DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
        SOURCE="$(readlink "$SOURCE")"
        [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
    done
    DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

    test_src=${DIR}/${python_script}
    if [[ ! -f ${test_src} ]]
    then
        echo "Unable to find python module. Aborting" >&2;
        exit ${RETCODE_BAD_PYTHON_MODULE};
    else
        python_script=${test_src}
    fi
fi


# find the python shell
if [[ -z  ${python_shell} ]]
then 
    python_shell=$(command -v ipython)
    if [[ $? ]]
    then
        echo " * python shell detected : ipython (${python_shell})"
    else
        python_shell=$(command -v python)
        if [[ $1 ]]
        then
            echo " * python shell detected : python (${python_shell})"
        else
            echo "Error : A python interpreter is required. Aborting\n" >&2; 
            exit ${RETCODE_NO_PYTHON};
        fi
    fi
fi

# Don't print the python banner if possible
# it makes thing just more confusing
basename_python=$(basename ${python_shell})
if [[ ${basename_python} = ipython* ]]
then 
    no_banner="--no-banner"
elif [[ ${basename_python} = python* ]]
then
    python_version=$(${python_shell} -V 2>&1 | cut -d' ' -f2 | cut -d'.' -f1 -)
    if [[ $python_version -ge 3 ]]
    then
        no_banner="-q"
    fi
fi

# run the analysis
exec ${python_shell} -i ${no_banner} ${python_script} ${filepath}
