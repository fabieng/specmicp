#!/bin/bash

# Find the python script
# Find the true directory of this bash script
# https://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
	DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
	SOURCE="$(readlink "$SOURCE")"
	[[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

test_src=${DIR}/short_iter.py
if [[ ! -f ${test_src} ]]
then
	echo "Unable to find python module. Aborting" >&2;
	exit ${RETCODE_BAD_PYTHON_MODULE};
else
	python_script=${test_src}
fi

# Analyse the iter file from ReactMiCP to extract useful information without 

# copy beginning file
head -n 8 $1 > tmp_head
# copy end file
tail -n 14 $1 > tmp_tail
# run python script
python ${python_script} $1 $2

# paste all together
cat tmp_head $2 tmp_tail > tmp_short
mv tmp_short $2
rm tmp_head
rm tmp_tail
