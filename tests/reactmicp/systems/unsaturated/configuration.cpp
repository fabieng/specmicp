#include <catch.hpp>

#include "reactmicp/io/configuration_unsaturated.hpp"
#include "reactmicp/systems/unsaturated/boundary_conditions.hpp"
#include "specmicp/adimensional/adimensional_system_solver_structs.hpp"
#include "reactmicp/systems/unsaturated/equilibrium_stagger.hpp"
#include "specmicp_common/io/safe_config.hpp"
#include "specmicp_database/database.hpp"

const char * conf_bcs = R"plop(
boundary_conditions:
    gas_nodes: 0
    fixed_nodes: 9
    constraints:
        default:
            charge_keeper: "H[+]"
        others:
            - nodes: 9
              name: fixed_node
              fork_from: default
              fixed_fugacity:
                 - label_component: "CO2"
                   label_gas: "CO2(g)"
                   amount: 4e-4
)plop";

const char * conf_options =
R"plop(
equilibrium_options:
    default:
        residual_tolerance: 1e-10
        step_tolerance: 1e-10
        cutoff_total_concentration: 1e-14
    others:
        - name: totally_reacted
          fork_from: default
          residual_tolerance: 1e-8
          step_tolerance: 1e-8
        - name: difficult
          node: 9
          residual_tolerance: 1e-12
          step_tolerance: 1e-12
)plop";

using namespace specmicp;
using namespace specmicp::reactmicp::systems::unsaturated;

static specmicp::database::RawDatabasePtr get_database()
{
    static database::RawDatabasePtr raw_data {nullptr};
    if (raw_data == nullptr)
    {
        specmicp::database::Database thedatabase(TEST_CEMDATA_PATH);
        thedatabase.keep_only_components(
            {"H2O", "H[+]", "Ca[2+]", "Si(OH)4", "HCO3[-]"}
            );
        std::map<std::string, std::string> swap = {{"HCO3[-]", "CO2"},};
        thedatabase.swap_components(swap);
        raw_data = thedatabase.get_database();
        raw_data->freeze_db();
    }
    return raw_data;
}

TEST_CASE("Boundary conditions configuration", "[io],[configuration],[bcs]")
{
    SECTION("Configuration") {
         auto bcs_yaml = io::YAMLConfigFile::load_from_string(conf_bcs);
         auto raw_data = get_database();

         auto bcs = io::configure_unsaturated_boundary_conditions(
                        10, raw_data.get(),
                        bcs_yaml.get_section("boundary_conditions")
                    );

         CHECK(bcs->is_gas_node(0) == true);
         CHECK(bcs->get_bcs_liquid_dof(9, 0) == IdBCs::FixedDof);
         CHECK(bcs->get_bcs_gas_dof(   9, 0) == IdBCs::FixedDof);
         CHECK(bcs->get_bcs_liquid_dof(9, 2) == IdBCs::FixedDof);

         const auto& default_const = bcs->get_constraint("default");
         CHECK(default_const.charge_keeper == raw_data->get_id_component("H[+]"));


         REQUIRE(bcs->has_constraint("fixed_node"));
         const auto& other_const = bcs->get_constraint("fixed_node");
         CHECK(other_const.charge_keeper == raw_data->get_id_component("H[+]"));

         const auto& from_node = bcs->get_constraint(9);
         CHECK(from_node.fixed_fugacity_cs.size() == 1);
         CHECK(from_node.fixed_fugacity_cs[0].id_gas ==
                 other_const.fixed_fugacity_cs[0].id_gas );
    }
}

TEST_CASE("Equilibrium options", "[io],[configuration],[options]")
{
    SECTION("Configuration") {
        auto opts_yaml = io::YAMLConfigFile::load_from_string(conf_options);
        units::UnitsSet units_set;
        units_set.length = units::LengthUnit::centimeter;

        auto opts = io::configure_unsaturated_equilibrium_options(
                        10, units_set,
                        opts_yaml.get_section("equilibrium_options")
                    );

        const AdimensionalSystemSolverOptions& default_opts = opts->get("default");
        CHECK(default_opts.solver_options.fvectol == 1e-10);
        CHECK(default_opts.solver_options.steptol == 1e-10);
        CHECK(default_opts.system_options.cutoff_total_concentration == 1e-14);

        const AdimensionalSystemSolverOptions& reacted_opts = opts->get("totally_reacted");
        CHECK(reacted_opts.solver_options.fvectol == 1e-8);
        CHECK(reacted_opts.solver_options.steptol == 1e-8);
        CHECK(reacted_opts.system_options.cutoff_total_concentration == 1e-14);

        const AdimensionalSystemSolverOptions& diff_opts = opts->get("difficult");
        CHECK(diff_opts.solver_options.fvectol == 1e-12);
        CHECK(diff_opts.solver_options.steptol == 1e-12);
        CHECK(diff_opts.system_options.cutoff_total_concentration ==
              AdimensionalSystemOptions().cutoff_total_concentration);


    }
}
