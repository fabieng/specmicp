#include <catch.hpp>

#include "reactmicp/systems/unsaturated/boundary_conditions.hpp"
#include "specmicp/adimensional/adimensional_system_structs.hpp"

using namespace specmicp;
using namespace specmicp::reactmicp::systems::unsaturated;

TEST_CASE("Boundary conditions", "[bcs],[constraints]")
{
    SECTION("Boundary conditions")
    {
        auto bcs = BoundaryConditions::make(5, 5);

        bcs->add_gas_node(0);
        bcs->add_fixed_node(4);
        bcs->set_fixed_flux_liquid_dof(1, 0, 3.0);
        bcs->set_fixed_flux_gas_dof(   1, 2, 4.0);
        bcs->set_fixed_flux_implicit_gas(1, 3, [](scalar_t var){return 1.0;});
        bcs->set_fixed_flux_implicit_gas(1, 4, [](scalar_t var){return 2.0;});

        CHECK(bcs->is_gas_node(0)    == true);


        CHECK(bcs->get_bcs_liquid_dof(1, 0) == IdBCs::FixedFlux);
        CHECK(bcs->get_bcs_gas_dof(   1, 0) == IdBCs::NormalNode);
        CHECK(bcs->get_bcs_liquid_dof(2, 0) == IdBCs::NormalNode);
        CHECK(bcs->get_bcs_gas_dof(   2, 0) == IdBCs::NormalNode);
        CHECK(bcs->get_bcs_liquid_dof(3, 0) == IdBCs::NormalNode);
        CHECK(bcs->get_bcs_gas_dof(   3, 0) == IdBCs::NormalNode);
        CHECK(bcs->get_bcs_liquid_dof(4, 0) == IdBCs::FixedDof);
        CHECK(bcs->get_bcs_gas_dof(   4, 0) == IdBCs::FixedDof);
        CHECK(bcs->get_bcs_liquid_dof(1, 1) == IdBCs::NormalNode);
        CHECK(bcs->get_bcs_liquid_dof(2, 1) == IdBCs::NormalNode);
        CHECK(bcs->get_bcs_gas_dof(4, 0) == IdBCs::FixedDof);
        CHECK(bcs->get_bcs_gas_dof(4, 2) == IdBCs::FixedDof);
        CHECK(bcs->get_bcs_gas_dof(4, 3) == IdBCs::FixedDof);
        CHECK(bcs->get_bcs_gas_dof(1, 2) == IdBCs::FixedFlux);


        CHECK(bcs->get_flux_liquid_dof(1, 0) == 3.0);
        CHECK(bcs->get_flux_gas_dof(   1, 0) == 0.0);
        CHECK(bcs->get_flux_gas_dof(   1, 2) == 4.0);


        CHECK(bcs->get_bcs_gas_dof(2, 3) == IdBCs::NormalNode);
        CHECK(bcs->get_bcs_gas_dof(1, 3) == IdBCs::ImplicitFixedFlux);
        CHECK(bcs->get_bcs_gas_dof(1, 4) == IdBCs::ImplicitFixedFlux);

        CHECK(bcs->get_implicit_flux_gas_dof(1, 3, 1.0) == 1.0);
        CHECK(bcs->get_implicit_flux_gas_dof(1, 4, 1.0) == 2.0);

        AdimensionalSystemConstraints& fixed_compo = bcs->fork_constraint(4, "fixed_compo");
        fixed_compo.set_inert_volume_fraction(0.2);

        AdimensionalSystemConstraints& not_fixed = bcs->get_constraint(0);
        CHECK(not_fixed.inert_volume_fraction == 0.0);

        CHECK(bcs->get_constraint("fixed_compo").inert_volume_fraction == 0.2);
    }
}
