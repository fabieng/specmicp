#include "catch.hpp"

#include "specmicp_database/database.hpp"

#include "dfpm/meshes/mesh1d.hpp"

#include "reactmicp/systems/unsaturated/equilibrium_stagger.hpp"
#include "reactmicp/systems/unsaturated/variables_interface.hpp"
#include "reactmicp/solver/staggers_base/stagger_structs.hpp"
#include "reactmicp/systems/unsaturated/variables.hpp"
#include "reactmicp/systems/unsaturated/boundary_conditions.hpp"

#include "specmicp/adimensional/adimensional_system_solver.hpp"
#include "specmicp/adimensional/adimensional_system_solver_structs.hpp"
#include "specmicp/adimensional/adimensional_system_solution_extractor.hpp"

#include "specmicp/problem_solver/formulation.hpp"
#include "specmicp/problem_solver/dissolver.hpp"

#include "specmicp_common/log.hpp"
#include "specmicp_common/cached_vector.hpp"

#include <memory>
#include <iostream>

using namespace specmicp;
using namespace specmicp::reactmicp;
using namespace specmicp::reactmicp::systems::unsaturated;

static specmicp::database::RawDatabasePtr get_database()
{
    static database::RawDatabasePtr raw_data {nullptr};
    if (raw_data == nullptr)
    {
        specmicp::database::Database thedatabase(TEST_CEMDATA_PATH);
        thedatabase.keep_only_components(
            {"H2O", "H[+]", "Ca[2+]", "Si(OH)4", "HCO3[-]"}
            );
        std::map<std::string, std::string> swap = {{"HCO3[-]", "CO2"},};
        thedatabase.swap_components(swap);
        raw_data = thedatabase.get_database();
        raw_data->freeze_db();
    }
    return raw_data;
}

static AdimensionalSystemSolution
get_init_solution(database::RawDatabasePtr the_database)
{

    Formulation formulation;
    formulation.set_mass_solution(500);
    formulation.add_mineral("C2S", 1000.0);
    formulation.add_mineral("C3S", 4000.0);
    formulation.add_aqueous_species("CO2", 20);
    
    Dissolver dissolver(the_database);
    Vector tot_conc = dissolver.dissolve(formulation, false);

    AdimensionalSystemConstraints constraints;
    constraints.total_concentrations = tot_conc;
    constraints.set_inert_volume_fraction(0.1);

    AdimensionalSystemSolver adim_solver(the_database, constraints);
    adim_solver.get_options().solver_options.set_tolerance(1e-14);

    Vector x;
    adim_solver.initialize_variables(x, 0.8, -3);
    micpsolver::MiCPPerformance perf = adim_solver.solve(x);
    specmicp_assert(perf.return_code > micpsolver::MiCPSolverReturnCode::Success);

    return adim_solver.get_raw_solution(x);
}

TEST_CASE("EquilibriumConstraints", "[Equilibrium],[Constraints]") {
    init_logger(&std::cout, logger::Warning);

    SECTION("Initialisation") {
        
        auto bcs = BoundaryConditions::make(10, 5);
        bcs->add_fixed_node(0);
        bcs->add_fixed_node(9);

        AdimensionalSystemConstraints& def_constraint = bcs->get_constraint("default");
        def_constraint.set_charge_keeper(2);

        AdimensionalSystemConstraints& fixed_constraint =
                bcs->fork_constraint(0, "fixed_compo");
        fixed_constraint.set_charge_keeper(3);


        bcs->set_constraint(9, "fixed_compo");

        CHECK(bcs->get_constraint(0).charge_keeper == 3);
        CHECK(bcs->get_constraint(1).charge_keeper == 2);
        CHECK(bcs->get_constraint(2).charge_keeper == 2);
        CHECK(bcs->get_constraint(3).charge_keeper == 2);
        CHECK(bcs->get_constraint(4).charge_keeper == 2);
        CHECK(bcs->get_constraint(5).charge_keeper == 2);
        CHECK(bcs->get_constraint(6).charge_keeper == 2);
        CHECK(bcs->get_constraint(7).charge_keeper == 2);
        CHECK(bcs->get_constraint(8).charge_keeper == 2);
        CHECK(bcs->get_constraint(9).charge_keeper == 3);
    }
}

TEST_CASE("Equilibrium stagger", "[Equilibrium],[Stagger]") {
    init_logger(&std::cout, logger::Warning);

    index_t nb_nodes {10};
    scalar_t dx {1.0};
    scalar_t cross_section {5.0};

    mesh::Mesh1DPtr the_mesh = mesh::uniform_mesh1d({dx, nb_nodes, cross_section});
    database::RawDatabasePtr raw_data = get_database();


    auto bcs = BoundaryConditions::make(the_mesh->nb_nodes(), raw_data->nb_component());


    auto options = EquilibriumOptionsVector::make(the_mesh->nb_nodes());
    options->get("default").solver_options.set_tolerance(1e-14);


    index_t id_co2 = raw_data->get_id_component_from_element("C");
    VariablesInterface interface(the_mesh, raw_data, {id_co2});

    units::UnitsSet units_set;
    
    AdimensionalSystemSolution solution = get_init_solution(raw_data);
    AdimensionalSystemSolutionExtractor extr(solution, raw_data, units_set);
    
    for (auto node: the_mesh->range_nodes())
    {
        interface.initialize_variables(node, extr);
    }

      scalar_t rho_l = extr.density_water();
      scalar_t init_saturation = extr.saturation_water();
      scalar_t init_water_aqconc = rho_l*extr.total_aqueous_concentration(0);
      scalar_t init_water_solidconc = extr.total_solid_concentration(0);

      UnsaturatedVariablesPtr vars = interface.get_variables();
      scalar_t pv_co2 = extr.fugacity_gas(vars->get_id_gas(id_co2)
                                         )*vars->get_total_pressure();
      scalar_t init_co2_aqconc = rho_l*extr.total_aqueous_concentration(id_co2);
      scalar_t init_co2_solidconc = extr.total_solid_concentration(id_co2);


    SECTION("Initialisation") {
        EquilibriumStagger stagger(vars, bcs, options);

        // Check that nothing has changed after we solved an already solved problem
        stagger.initialize(vars.get());
        stagger.initialize_timestep(1.0, vars.get());
        solver::StaggerReturnCode retcode = stagger.restart_timestep(vars.get());

        CHECK(retcode > solver::StaggerReturnCode::NotConvergedYet);

        for (index_t node: the_mesh->range_nodes()) {
            CHECK(interface.get_liquid_saturation()(node)
                  == Approx(init_saturation).epsilon(1e-10));
            CHECK(interface.get_water_aqueous_concentration()(node)/init_water_aqconc
                  == Approx(1.0).epsilon(1e-10));
            CHECK(interface.get_solid_concentration(0)(node) / init_water_solidconc
                  == Approx(1.0).epsilon(1e-10));
            CHECK(interface.get_aqueous_concentration(id_co2)(node)/init_co2_aqconc
                  == Approx(1.0).epsilon(1e-10));
            CHECK(interface.get_solid_concentration(id_co2)(node) / init_co2_solidconc
                  == Approx(1.0).epsilon(1e-10));
            CHECK(interface.get_partial_pressure(id_co2)(node) == Approx(pv_co2).epsilon(1e-10));

            CHECK(vars->get_inert_volume_fraction(node) == 0.1);
        }
    }
}
