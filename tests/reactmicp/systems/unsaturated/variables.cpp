#include "catch.hpp"

#include "reactmicp/systems/unsaturated/variables.hpp"
#include "reactmicp/systems/unsaturated/variables_box.hpp"
#include "dfpm/meshes/mesh1d.hpp"
#include "specmicp_database/database.hpp"

#include "specmicp_common/log.hpp"

#include <iostream>

using namespace specmicp;
using namespace specmicp::reactmicp::systems::unsaturated;


static mesh::Mesh1DPtr get_mesh(index_t nb_nodes)
{
    mesh::Uniform1DMeshGeometry amesh;
    amesh.dx = 1.0;
    amesh.section = 1.0;
    amesh.nb_nodes = nb_nodes;
    return mesh::uniform_mesh1d(amesh);
}


static specmicp::database::RawDatabasePtr get_database()
{
    static database::RawDatabasePtr raw_data {nullptr};
    if (raw_data == nullptr)
    {
        specmicp::database::Database thedatabase(TEST_CEMDATA_PATH);
        thedatabase.keep_only_components({"H2O", "H[+]", "Ca[2+]", "Si(OH)4"});
        raw_data = thedatabase.get_database();
        raw_data->freeze_db();
    }
    return raw_data;
}

TEST_CASE("Variables Types", "[variables],[types]") {
    init_logger(&std::cout, logger::Warning);

    SECTION("ListMainVariables") {
        ListMainVariable alist(5, 4);

        REQUIRE(alist.water().size() == 4);
        REQUIRE(alist.aqueous_component(4).size() == 4);
        REQUIRE(alist(1) == nullptr);


        ListMainVariable alist2(4, 6, {true, false, true, false});

        REQUIRE(alist2.water().size() == 6);
        REQUIRE(alist2.aqueous_component(2).size() == 6);
        REQUIRE(alist2(1) == nullptr);
        REQUIRE(alist2(3) == nullptr);
    }
}

TEST_CASE("Variables", "[variables]") {

    SECTION("Initialisation") {
        mesh::Mesh1DPtr the_mesh = get_mesh(10);
        database::RawDatabasePtr raw_db = get_database();
        REQUIRE(raw_db->is_valid());
        UnsaturatedVariables vars(the_mesh, raw_db, {true, false, false, false, false});

        REQUIRE(vars.get_database()->is_valid());
        REQUIRE(vars.get_database()->get_hash() == raw_db->get_hash());

        REQUIRE(vars.nb_nodes() == 10);
        REQUIRE(vars.nb_governing_equations() == 4);
    }

    SECTION("Saturation variable") {
        index_t nb_nodes = 10;

        mesh::Mesh1DPtr the_mesh = get_mesh(nb_nodes);
        database::RawDatabasePtr raw_db = get_database();
        UnsaturatedVariables vars(the_mesh, raw_db, {true, false, false, false, false});

        SaturationVariableBox satvar = vars.get_saturation_variables();

        REQUIRE(satvar.liquid_saturation.size() == nb_nodes);

        satvar.liquid_saturation(3) = 2.0;
        CHECK(satvar.liquid_saturation(3) == 2.0);

        satvar.liquid_saturation.velocity(2) = 3.0;
        CHECK(satvar.liquid_saturation.velocity(2) == 3.0);

    }

    SECTION("Vapor pressure variable") {
        index_t nb_nodes = 10;

        mesh::Mesh1DPtr the_mesh = get_mesh(nb_nodes);
        database::RawDatabasePtr raw_db = get_database();
        UnsaturatedVariables vars(the_mesh, raw_db, {true, false, false, false, false});

        PressureVariableBox vap_pressure = vars.get_vapor_pressure_variables();
        REQUIRE(vap_pressure.partial_pressure.size() == nb_nodes);
    }

    SECTION("liquid aqueous component variable") {
        index_t nb_nodes = 10;

        mesh::Mesh1DPtr the_mesh = get_mesh(nb_nodes);
        database::RawDatabasePtr raw_db = get_database();
        UnsaturatedVariables vars(the_mesh, raw_db, {true, false, false, false, false});

        LiquidAqueousComponentVariableBox liq_var = vars.get_liquid_aqueous_component_variables(2);
        REQUIRE(liq_var.aqueous_concentration.size() == nb_nodes);
    }

    SECTION("gaseous aqueous component variable") {
        index_t nb_nodes = 10;

        mesh::Mesh1DPtr the_mesh = get_mesh(nb_nodes);
        database::RawDatabasePtr raw_db = get_database();
        UnsaturatedVariables vars(the_mesh, raw_db, {true, false, false, false, false});

        PressureVariableBox gas_var = vars.get_pressure_variables(2);
        REQUIRE(gas_var.partial_pressure.size() == nb_nodes); // should still work
    }
}
