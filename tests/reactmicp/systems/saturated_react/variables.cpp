#include "catch.hpp"

#include "speciation_system.hpp"
#include "specmicp/adimensional/adimensional_system_solution.hpp"
#include "specmicp/adimensional/adimensional_system_solution_extractor.hpp"

#include "dfpm/mesh.hpp"

#include "reactmicp/systems/saturated_react/variables.hpp"
#include "reactmicp/systems/saturated_react/init_variables.hpp"

TEST_CASE("Saturated Variables test", "[SaturatedReact, variables, initialisation]") {


    SECTION("Initialisation") {
        specmicp::units::UnitsSet the_units;
        the_units.mass = specmicp::units::MassUnit::kilogram;
        the_units.length = specmicp::units::LengthUnit::centimeter;

        specmicp::mesh::Uniform1DMeshGeometry geom;
        geom.nb_nodes = 5;
        geom.dx = 0.1;
        geom.section = 5.0;
        specmicp::mesh::Mesh1DPtr the_mesh = specmicp::mesh::uniform_mesh1d(geom);


        specmicp::RawDatabasePtr the_database = leaching_database();

        std::vector<specmicp::AdimensionalSystemSolution> list_initial_composition;

        list_initial_composition.push_back(initial_leaching_pb(the_database, 0.005, the_units));
        list_initial_composition.push_back(initial_blank_leaching_pb(the_database, 0.005, the_units));

        std::vector<specmicp::index_t> list_fixed_nodes = {0, };

        std::vector<int> index_initial_state = {1, 0, 0, 0, 0};

        specmicp::reactmicp::systems::satdiff::SaturatedVariablesPtr variables =
                specmicp::reactmicp::systems::satdiff::init_variables(the_mesh, the_database, the_units,
                                                                      list_fixed_nodes,
                                                                      list_initial_composition,
                                                                      index_initial_state);

        REQUIRE(variables->dof_porosity(0) == 0);
        REQUIRE(variables->equilibrium_solution(1).secondary_molalities(3)
                 == list_initial_composition[0].secondary_molalities(3)  );
        REQUIRE(variables->equilibrium_solution(0).secondary_molalities(3)
                 == list_initial_composition[1].secondary_molalities(3)  );
        REQUIRE(variables->aqueous_concentration(4, 2, variables->velocity()) == 0);

        specmicp::AdimensionalSystemSolutionExtractor extractor(list_initial_composition[0], the_database, the_units);

        REQUIRE(variables->aqueous_concentration(2, 1, variables->displacement())
                == extractor.density_water()*extractor.total_aqueous_concentration(1) );
    }


}
