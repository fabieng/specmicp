
#include "specmicp_common/types.hpp"
#include <memory>

// forward declarations
namespace specmicp {

namespace database {
class DataContainer;
}
using RawDatabasePtr= std::shared_ptr<database::DataContainer>;

class AdimensionalSystemSolution;
namespace units {
struct UnitsSet;
} // end namespace units
} // end namespace specmicp

specmicp::RawDatabasePtr leaching_database();

specmicp::AdimensionalSystemSolution initial_leaching_pb(
        specmicp::RawDatabasePtr the_database,
        specmicp::scalar_t mult,
        specmicp::units::UnitsSet units);

specmicp::AdimensionalSystemSolution initial_blank_leaching_pb(
        specmicp::RawDatabasePtr the_database,
        specmicp::scalar_t mult,
        specmicp::units::UnitsSet
        );
