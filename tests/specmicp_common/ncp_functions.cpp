#include "catch.hpp"

#include "specmicp_common/micpsolver/ncp_function.hpp"

TEST_CASE("Box constrained VI", "[VI],[B-function]")
{
    SECTION("Function")
    {
        auto vi_func = specmicp::micpsolver::box_constrained_vi_b_function<double>;

        CHECK(vi_func(0.0,  2.0, 0.0, 5.0) == 0.0);
        CHECK(vi_func(5.0, -3.0, 0.0, 5.0) == 0.0);
        CHECK(vi_func(2.0,  0.0, 0.0, 5.0) == 0.0);
        CHECK(vi_func(2.0,  3.0, 0.0, 5.0) != 0.0);
        CHECK(vi_func(-2.0,  3.0, 0.0, 5.0) != 0.0);
        CHECK(vi_func(8.0,  -2.0, 0.0, 5.0) != 0.0);
        CHECK(vi_func(0.0,  0.0, 0.0, 5.0) == 0.0);
        CHECK(vi_func(5.0,  0.0, 0.0, 5.0) == 0.0);

    }

    SECTION("Jacobian")
    {

    }
}
