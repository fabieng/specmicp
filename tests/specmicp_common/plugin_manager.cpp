#include <catch.hpp>

#include "specmicp_common/plugins/plugin_manager.hpp"
#include "specmicp_common/filesystem.hpp"
#include "specmicp_common/log.hpp"

#include "test_basic_plugin_mockobject.hpp"

#include <iostream>

using namespace specmicp;
using namespace specmicp::plugins;

TEST_CASE("Plugin manager", "[plugin]")
{
    init_logger(&std::cerr, logger::Warning);
    SECTION("get_manager") {
       auto& plugin_manager = get_plugin_manager();

       std::cerr << "this error  is OK :";
       CHECK(plugin_manager.check_version(PluginAPIVersion(0, 0, 0)) == false);
    }

    SECTION("basic_plugin") {
        auto& plugin_manager = get_plugin_manager();
        plugin_manager.add_plugin_directory(specmicp::utils::get_current_directory());
        auto retcode = plugin_manager.load_plugin("test_basic_plugin.so");

        REQUIRE(retcode == true);

        REQUIRE(plugin_manager.get_module("test_module") != nullptr);

        auto object = plugin_manager.get_object<TestBaseObject>(
                    "test_module", "test_add");

        REQUIRE(object != nullptr);

        CHECK(object->add(2, 3) == 5);

        auto object2 = plugin_manager.get_object<TestBaseObject>(
                    "test_module", "test_substract");

        REQUIRE(object2 != nullptr);

        CHECK(object2->add(2, 3) == -1);
    }
}
