#include "catch.hpp"

#include "specmicp_common/odeint/butcher_tableau.hpp"

TEST_CASE("Butcher tableau")
{
    SECTION("Cash-Karp")
    {
        CHECK(specmicp::odeint::butcher_cash_karp45.a(2) == 1.0/5.0);
        CHECK(specmicp::odeint::butcher_cash_karp45.b(6,3) == 575.0/13824.0);
        CHECK(specmicp::odeint::butcher_cash_karp45.c(4) == 125.0/594.0);
        CHECK(specmicp::odeint::butcher_cash_karp45.cs(4) == 13525.0/55296.0);
    }
}
