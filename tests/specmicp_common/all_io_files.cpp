#include "catch.hpp"

#include "specmicp_common/io/all_io_files.hpp"
#include "specmicp_common/filesystem.hpp"
#include "specmicp_common/log.hpp"

#include <iostream>
#include <ctime>

using namespace specmicp::io;

TEST_CASE("AllIOFiles") {

    specmicp::init_logger(&std::cerr, specmicp::logger::LogLevel::Debug);

    std::time_t rawtime_start;
    std::time(&rawtime_start);

    const std::string filename = "test_all_io.yml";


    SECTION("Write") {
        // write

        if (specmicp::utils::path_exists(filename)) {
            specmicp::utils::remove_file(filename);
        }

        AllIOFiles all_io(filename, AllIOFilesMode::ErrorIfExist);

        const char* argv[3] {"hop", "-pop", "3"};
        std::cout << "wooup : ";
        for (auto id=0; id<3; ++id) {
            std::cout << argv[id] << " ";
        }
        std::cout << std::endl;

        all_io.save_command_line(3, argv);

        all_io.add_database(input_file("raw database", "cemdata.yml"));
        all_io.add_database(output_file("treated database", "new_cemdata.yml"));

        all_io.add_log_file(output_file("log", "log_file"));
        all_io.add_log_file(output_file("conf log","conf_log_file"));

        all_io.add_configuration_file(input_file("input", "input.yml"));

        all_io.add_solution(output_file("output_spc", "solution.yml"));

        all_io.add_solution(output_file("output_rcu","solution.hdf5"));

        all_io.sync();

        CHECK(specmicp::utils::path_exists(filename));
    }

    SECTION("ErrorIfExists") {
        CHECK(specmicp::utils::path_exists(filename));
        CHECK_THROWS_AS(AllIOFiles(filename, AllIOFilesMode::ErrorIfExist),
                        std::runtime_error);
    }

    SECTION("Read") {

        AllIOFiles all_io(filename, AllIOFilesMode::Read);

        // read

        // it happens before the start of this test
        CHECK(all_io.get_creation_time() <= rawtime_start);
        CHECK(all_io.get_last_modification_time() <= rawtime_start);

        CHECK(all_io.get_command_line() == "hop -pop 3");

        IOFileInfo file_info;
        REQUIRE(all_io.get_database("raw database", file_info));
        CHECK(file_info.filepath == "cemdata.yml");
        REQUIRE(all_io.get_database("treated database", file_info));
        CHECK(file_info.filepath == "new_cemdata.yml");

        REQUIRE(all_io.get_configuration_file("input", file_info));
        CHECK(file_info.name == "input");
        CHECK(file_info.filepath == "input.yml");

        REQUIRE(all_io.get_log_file("log", file_info));
        CHECK(file_info.filepath == "log_file");

        REQUIRE(all_io.get_solution("output_spc", file_info));
        CHECK(file_info.filepath == "solution.yml");
        REQUIRE(all_io.get_solution("output_rcu", file_info));
        CHECK(file_info.filepath == "solution.hdf5");

    }
}
