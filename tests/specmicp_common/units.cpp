/*-------------------------------------------------------------------------------

Copyright (c) 2015 F. Georget <fabieng@princeton.edu>, Princeton University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-----------------------------------------------------------------------------*/

#include "catch.hpp"

#include "specmicp_common/physics/units.hpp"
#include "specmicp_common/physics/io/configuration.hpp"
#include "specmicp_common/io/safe_config.hpp"
#include <float.h>
#include <iostream>

using namespace specmicp::units;

bool inline check_unit(const std::string& unit, AmountUnitType type, double factor)
{
    AmountUnit tmp;
    parse_amount_unit(unit, tmp);
    return (tmp.type == type and
            std::abs(tmp.factor_si-factor)/std::abs(tmp.factor_si) <= DBL_EPSILON);
}

TEST_CASE("Units", "[Units]")
{
    SECTION("Length unit")
    {
        CHECK(scaling_factor(LengthUnit::meter) == 1.0);
        CHECK(scaling_factor(LengthUnit::decimeter) == 0.1);
        CHECK(scaling_factor(LengthUnit::centimeter) == 0.01);


        CHECK(from_scaling_factor<LengthUnit>(5.0) == LengthUnit::meter);
        specmicp::scalar_t factor = 0.5;
        CHECK(from_scaling_factor<LengthUnit>(factor) == LengthUnit::decimeter);
        CHECK(factor == Approx(5.0));

        specmicp::scalar_t factor_2 = 1e-3;
        CHECK(from_scaling_factor<LengthUnit>(factor_2) == LengthUnit::centimeter);
        CHECK(factor_2 == Approx(0.1));
    }

    SECTION("Mass unit")
    {
        CHECK(scaling_factor(MassUnit::kilogram) == 1.0);
        CHECK(scaling_factor(MassUnit::gram) == 1e-3);

        CHECK(from_scaling_factor<MassUnit>(0.1) == MassUnit::kilogram);
        specmicp::scalar_t factor = 2e-3;
        CHECK(from_scaling_factor<MassUnit>(factor) == MassUnit::gram);
        CHECK(factor == Approx(2.0));
    }

    SECTION("Quantity unit")
    {
        CHECK(scaling_factor(QuantityUnit::moles) == 1.0);
        CHECK(scaling_factor(QuantityUnit::millimoles) == 1e-3);


        CHECK(from_scaling_factor<QuantityUnit>(0.1) == QuantityUnit::moles);
        specmicp::scalar_t factor = 2e-3;
        CHECK(from_scaling_factor<QuantityUnit>(factor) == QuantityUnit::millimoles);
        CHECK(factor == Approx(2.0));
    }

    SECTION("Configuration")
    {
        const char* hop =
        R"(
        units:
            length:   centimeter
            mass:     gram
            quantity: millimoles
        )";
        auto conf = specmicp::io::YAMLConfigFile::load_from_string(hop, "test string");
        auto the_units = specmicp::io::configure_units(conf.get_section("units"));

        CHECK(the_units.length   == LengthUnit::centimeter);
        CHECK(the_units.mass     == MassUnit::gram);
        CHECK(the_units.quantity == QuantityUnit::millimoles);
    }
}

TEST_CASE("Find units", "[Units]") {

    SECTION("is_volume_unit") {
        CHECK(is_volume_unit("m^3") == 1.0);
        CHECK(is_volume_unit("dm^3") == 1e-3);
        CHECK(is_volume_unit("L") == 1e-3);
        CHECK(is_volume_unit("cm^3") == 1e-6);
        CHECK(is_volume_unit("mm^3") == 1e-9);
        CHECK(is_volume_unit("m") < 0);

    }

    SECTION("is_length_unit") {
        CHECK(is_length_unit("m") == 1.0);
        CHECK(is_length_unit("dm") == 0.1);
        CHECK(is_length_unit("cm") == 0.01);
        CHECK(is_length_unit("mm") == 1e-3);
        CHECK(is_length_unit("m^3") < 0);
    }

    SECTION("is_mass_unit") {
        CHECK(is_mass_unit("kg") == 1.0);
        CHECK(is_mass_unit("g") == 1e-3);
        CHECK(is_mass_unit("m") < 0);
    }

    SECTION("is_mole_unit") {
        CHECK(is_mole_unit("mol") == 1.0);
        CHECK(is_mole_unit("mmol") == 1e-3);
        CHECK(is_mole_unit("kg") < 0);
    }


    SECTION("Find units") {

        CHECK(check_unit("kg", AmountUnitType::Mass, 1.0));
        CHECK(check_unit("g", AmountUnitType::Mass, 1e-3));

        CHECK(check_unit(" m^3 ", AmountUnitType::Volume, 1.0));
        CHECK(check_unit("dm^3", AmountUnitType::Volume, 1e-3));
        CHECK(check_unit("cm^3 ", AmountUnitType::Volume, 1e-6));
        CHECK(check_unit("mm^3", AmountUnitType::Volume, 1e-9));

        CHECK(check_unit("L/L", AmountUnitType::VolumeFraction, 1.0));
        CHECK(check_unit("L/m^3", AmountUnitType::VolumeFraction, 1e-3));
        CHECK(check_unit("L/cm^3", AmountUnitType::VolumeFraction, 1e+3));


        CHECK(check_unit("mol ", AmountUnitType::NumberOfMoles, 1.0));
        CHECK(check_unit("mmol", AmountUnitType::NumberOfMoles, 1e-3));


        CHECK(check_unit("mol/kg", AmountUnitType::Molality, 1.0));
        CHECK(check_unit("mol/g", AmountUnitType::Molality, 1e3));

        CHECK(check_unit("mol /cm^3", AmountUnitType::MoleConcentration, 1e6));

        CHECK(check_unit("kg/ m^3", AmountUnitType::MassConcentration, 1.0));

        CHECK(check_unit("oups", AmountUnitType::Unknown, -1.0));
        CHECK(check_unit("oups/ bummer", AmountUnitType::Unknown, -1.0));

    }

}
