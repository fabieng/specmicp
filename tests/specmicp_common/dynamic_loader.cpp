#include <catch.hpp>

#include "specmicp_common/plugins/dynamic_library.hpp"
#include "specmicp_common/log.hpp"
#include "specmicp_common/filesystem.hpp"

#include <iostream>

using namespace specmicp::plugins;

TEST_CASE("Dynamic Loader", "[plugin]")
{
    specmicp::init_logger(nullptr, specmicp::logger::Warning);

    SECTION("Loading")
    {
        std::string error;
        DynamicLibraryPtr lib = DynamicLibrary::load(specmicp::utils::get_current_directory()+"/test_to_load.so", error);

        CHECK(lib != nullptr);
        if (lib == nullptr)
        {
            std::cerr << error << std::endl;
            REQUIRE(false);
        }

        int (*add_f)(int, int) = (int (*)(int, int)) lib->get_symbol("add", error);
        CHECK(add_f != nullptr);
        if (add_f == nullptr)
        {
            std::cerr << error << std::endl;
            REQUIRE(false);
        }

        CHECK(add_f(1, 2) == 3);
    }

    SECTION("Loading with directories")
    {
        std::string error;
        DynamicLibraryPtr lib = DynamicLibrary::load(
                    "test_to_load.so",
                    { specmicp::utils::get_current_directory(),
                      "/usr/lib"
                    },
                    error);

        CHECK(lib != nullptr);
        if (lib == nullptr)
        {
            std::cerr << error << std::endl;
            REQUIRE(false);
        }

        int (*add_f)(int, int) = (int (*)(int, int)) lib->get_symbol("add", error);
        CHECK(add_f != nullptr);
        if (add_f == nullptr)
        {
            std::cerr << error << std::endl;
            REQUIRE(false);
        }

        CHECK(add_f(1, 2) == 3);
    }

    SECTION("Unsuccessful loading")
    {
        std::string error;
        DynamicLibraryPtr lib = DynamicLibrary::load("platyput_with_7_legs", error);

        CHECK(lib == nullptr);
    }

    SECTION("Unsuccessful symbol loading")
    {
        std::string error;
        DynamicLibraryPtr lib = DynamicLibrary::load(specmicp::utils::get_current_directory()+"/test_to_load.so", error);

        CHECK(lib != nullptr);
        if (lib == nullptr)
        {
            std::cerr << error << std::endl;
            REQUIRE(false);
        }
        void * hop = lib->get_symbol("latypus_with_7_legs", error);
        CHECK(hop == nullptr);
    }
}
