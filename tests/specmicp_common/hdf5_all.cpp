#include "catch.hpp"

#include "specmicp_common/io/hdf5_timesteps.hpp"

#include "specmicp_common/io/hdf5/file.hpp"
#include "specmicp_common/io/hdf5/group.hpp"
#include "specmicp_common/io/hdf5/dataset.hpp"
#include "specmicp_common/io/hdf5/dataspace.hpp"

#include <iostream>

using namespace specmicp::io;

TEST_CASE("HDF5 wrapper", "[io],[hdf5]")
{
    SECTION("File")
    {
        {
            auto file = hdf5::File::open("test_file.h5", hdf5::OpenMode::CreateTruncate);
            REQUIRE(file.get_id() >= 0);
            REQUIRE(file.get_path() == "/");
        }
        {
            std::cout << "Next error HDF5 is ok :\n -------------------------" << std::endl;
            REQUIRE_THROWS(hdf5::File::open("test_file.h5", hdf5::OpenMode::CreateFailIfExist));
            std::cout << "---------------------------\n"<< std::endl;
        }
        {
            auto file = hdf5::File::open("test_file.h5", hdf5::OpenMode::OpenReadOnly);
            REQUIRE(file.get_id() >= 0);
            REQUIRE(file.get_path() == "/");
        }

    }

    SECTION("Group")
    {

        auto file = hdf5::File::open("test_file.h5", hdf5::OpenMode::OpenReadWrite);
        file.create_group("test");
        hdf5::Group test_group = file.open_group("test");
        REQUIRE(test_group.get_id() >= 0);
        REQUIRE(test_group.get_path() == "/test");
        CHECK(file.has_link("test"));
    }

    SECTION("Dataset")
    {
        auto file = hdf5::File::open("test_file.h5", hdf5::OpenMode::OpenReadWrite);
        hdf5::Group test_group = file.open_group("test");

        Eigen::VectorXd x(5);
        x << 0, 1, 2, 3, 4;

        test_group.create_vector_dataset("vector", x);

        REQUIRE(test_group.has_link("vector"));

        Eigen::VectorXd y = test_group.read_vector_dataset("vector");

        REQUIRE(y.rows() == 5);
        for (int i=0; i<5; ++i) {
            CHECK(y(i) == double(i));
        }


        // dataspace

        hdf5::Dataset dset = test_group.open_dataset("vector");
        hdf5::Dataspace dspace =  dset.get_dataspace();
        REQUIRE(dspace.get_rank() == 1);
        hsize_t dims[1];
        dspace.get_dimensions(dims);
        CHECK(dims[0] == 5);
    }

    SECTION("String dataset")
    {
        auto file = hdf5::File::open("test_file.h5", hdf5::OpenMode::OpenReadWrite);
        hdf5::Group test_group = file.open_group("test");


        std::vector<std::string> to_write {"hello", "world", "!"};

        test_group.create_string_dataset("test_string", to_write);

        auto to_read = test_group.read_string_dataset("test_string");

        CHECK(to_read.size() == to_write.size());
        for(std::size_t ind=0; ind< to_read.size(); ++ind)
        {
            CHECK(to_read[ind] == to_write[ind]);
        }
    }

    SECTION("Attribute")
    {

        auto file = hdf5::File::open("test_file.h5", hdf5::OpenMode::OpenReadWrite);
        hdf5::Group test_group = file.create_group("test_dataspace");
        Eigen::VectorXd x(5);
        x << 5, 6, 7, 8, 9;
        hdf5::Dataset test_dset = test_group.create_vector_dataset("dataset", x);

        std::array<double, 3> hop = {0, 1, 2};
        test_dset.create_scalar_attribute("test_attribute", hop);

        auto res = test_dset.read_scalar_attribute<3>("test_attribute");

        for (int i=0; i<3; ++i) {
            CHECK(res[i] == double(i));
        }
    }
}

TEST_CASE("HDF5 timesteps", "[io],[hdf5],[timesteps]")
{
    // write some timesteps in a file
    auto file = hdf5::File::open("test_file.h5", hdf5::OpenMode::CreateTruncate);
    file.create_group("11.1");
    file.create_group("22.2");
    file.create_group("33.3");
    file.create_group("44.4");

    SECTION("test") {
        CHECK(file.get_number_links() == 4);

        auto grp = file.open_group("/");

        HDF5Timesteps timesteps(grp);
        CHECK(timesteps.size() == 4);

        CHECK(timesteps.get_number("22.2") == 22.2);

        CHECK(timesteps.get_string(11.1) == "11.1");
        CHECK(timesteps.get_string(22.2) == "22.2");
        CHECK(timesteps.get_string(33.3) == "33.3");
        CHECK(timesteps.get_string(44.4) == "44.4");

        CHECK(timesteps.get_string(33.2) == "33.3");


        CHECK_THROWS(timesteps.get_string(55.5) == "55.5");

    }
}
