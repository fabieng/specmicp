class TestBaseObject
{
public:
    TestBaseObject() {}

    virtual ~TestBaseObject() = default;

    virtual double add(double a, double b) = 0;
};
