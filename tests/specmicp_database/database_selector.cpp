#include "catch.hpp"

#include "specmicp_database/yaml_reader.hpp"
#include "specmicp_database/selector.hpp"
#include "specmicp_database/mineral_selector.hpp"

#include "str_database.hpp"

#include "specmicp_common/timer.hpp"

#include <iostream>
#include <sstream>

using namespace specmicp;
using namespace specmicp::database;

TEST_CASE("DatabaseSelector", "[Database, Component]")
{
    std::istringstream input(good_test_database);
    DataReaderYaml reader(input);

    auto data = reader.get_database();

    SECTION("Remove a component")
    {
        Timer timer;
        DatabaseSelector selector(data);
        selector.remove_component({data->components.get_id("C3")});
        timer.stop();
        std::cout << "Removing a component : " << timer.elapsed_time()*1000 << " ms (Ref 0.010ms)" << std::endl;

        CHECK(data->components.is_valid());
        CHECK(data->aqueous.is_valid());
        CHECK(data->minerals.is_valid());
        CHECK(data->minerals_kinetic.is_valid());
        CHECK(data->gas.is_valid());
        CHECK(data->is_valid());

        // basis
        CHECK(data->nb_component() == 4);
        CHECK(data->components.get_id("C3") == no_species);

        // aqueous
        CHECK(data->nb_aqueous() == 2);
        CHECK(data->aqueous.get_id("A3") == no_species);

        // minerals
        CHECK(data->nb_mineral() == 1);
        CHECK(data->minerals.get_id("M2") == no_species);

        // minerals_kinetic
        CHECK(data->nb_mineral_kinetic() == 0);
        CHECK(data->minerals_kinetic.get_id("MK1") == no_species);

        // Gas
        CHECK(data->nb_gas() == 0);
        CHECK(data->gas.get_id("G1") == no_species);

        // Sorbed
        CHECK(data->nb_sorbed() == 0);
        CHECK(data->sorbed.get_id("S1") == no_species);

        // compound
        CHECK(data->nb_compounds() == 0);
        CHECK(data->compounds.get_id("Comp1") == no_species);

        // Element
        CHECK(data->get_id_component_from_element("C3") == no_species);
        CHECK(data->get_label_component_from_element("C1") == "C1[-]");
        CHECK(data->get_label_component_from_element("C2") == "C2[+]");
    }
}

TEST_CASE("MineralSelector", "[Select,minerals]")
{
    std::istringstream input(good_test_database);
    auto data = DataReaderYaml(input).get_database();

    SECTION("Mineral selector") {
        size_t orig_hash = data->get_hash();

        MineralSelector(data).keep_only({"M1"});

        CHECK(data->minerals.is_valid());
        CHECK(data->minerals_kinetic.is_valid());
        CHECK(data->is_valid());
        CHECK(data->get_hash() != orig_hash);


        // Mineral
        // -------

        CHECK(data->nb_mineral() == 1);
        CHECK(data->minerals.get_id("M2") == no_species);

        CHECK(data->nu_mineral(0, 0) == 0.0);
        CHECK(data->nu_mineral(0, 1) == 0.0);
        CHECK(data->nu_mineral(0, 2) == 1.0);
        CHECK(data->nu_mineral(0, 3) == 1.0);
        CHECK(data->nu_mineral(0, 4) == 0.0);
        CHECK(data->logk_mineral(0) == -7);
        CHECK(data->molar_volume_mineral(0) == 1e-6*10.0);

        // Mineral kinetic
        // ---------------

        CHECK(data->nb_mineral_kinetic() == 2);
        CHECK(data->minerals_kinetic.get_id("M2") == 1);

        CHECK(data->nu_mineral_kinetic(0, 0) == 0.0);
        CHECK(data->nu_mineral_kinetic(0, 1) == 0.0);
        CHECK(data->nu_mineral_kinetic(0, 2) == 1.0);
        CHECK(data->nu_mineral_kinetic(0, 3) == 1.0);
        CHECK(data->nu_mineral_kinetic(0, 4) == 2.0);
        CHECK(data->logk_mineral_kinetic(0) == -9);
        CHECK(data->molar_volume_mineral_kinetic(0) == 1e-6*30.0);

        CHECK(data->nu_mineral_kinetic(1, 0) == 0.0);
        CHECK(data->nu_mineral_kinetic(1, 1) == 0.0);
        CHECK(data->nu_mineral_kinetic(1, 2) == 2.0);
        CHECK(data->nu_mineral_kinetic(1, 3) == 2.0);
        CHECK(data->nu_mineral_kinetic(1, 4) == 1.0);
        CHECK(data->logk_mineral_kinetic(1) == -6+data->logk_aqueous(1)+data->logk_aqueous(2));
        CHECK(data->unsafe_molar_volume_mineral_kinetic(1) < 0);
    }
}
