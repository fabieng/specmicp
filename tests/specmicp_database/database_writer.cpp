#include "catch.hpp"

#include "specmicp_database/database.hpp"

#include "specmicp_common/timer.hpp"

#include <string>
#include <istream>
#include <sstream>
#include <iostream>

#include "str_database.hpp"

using namespace specmicp;
using namespace specmicp::database;


TEST_CASE("Database writer", "[writer]") {

    SECTION("Good database") {

        Database db_manager;
        std::istringstream input(good_test_database);
        db_manager.parse_database(input);
        Timer timer;
        db_manager.save("out_test_database.yaml");
        timer.stop();

        std::cout << "Writing db  : " << timer.elapsed_time() << " s" << std::endl;

        Database db_manager2("out_test_database.yaml", false);

        RawDatabasePtr db1 = db_manager.get_database();
        RawDatabasePtr db2 = db_manager.get_database();

        REQUIRE(db1->is_valid());
        REQUIRE(db2->is_valid());

        // check taht all the species are here

        CHECK(db1->components.get_hash() == db2->components.get_hash());
        CHECK(db1->aqueous.get_hash() == db2->aqueous.get_hash());
        CHECK(db1->minerals.get_hash() == db2->minerals.get_hash());
        CHECK(db1->minerals_kinetic.get_hash() == db2->minerals_kinetic.get_hash());
        CHECK(db1->gas.get_hash() == db2->gas.get_hash());
        CHECK(db1->sorbed.get_hash() == db2->sorbed.get_hash());

        CHECK(db1->nu_aqueous(db1->get_id_aqueous("A1"), db1->get_id_component("C1[-]")) ==
              db2->nu_aqueous(db2->get_id_aqueous("A1"), db2->get_id_component("C1[-]")));

        CHECK(db1->nu_mineral(db1->get_id_mineral("M2"), db1->get_id_component("C2[+]")) ==
              db2->nu_mineral(db2->get_id_mineral("M2"), db2->get_id_component("C2[+]")));


        CHECK(db1->nu_mineral_kinetic(db1->get_id_mineral_kinetic("MK1"), db1->get_id_component("C3")) ==
              db2->nu_mineral_kinetic(db2->get_id_mineral_kinetic("MK1"), db2->get_id_component("C3")));

        CHECK(db1->logk_gas(0) == db2->logk_gas(0));

        CHECK(db1->nb_sorption_sites(0) == db2->nb_sorption_sites(0));
    }
}
