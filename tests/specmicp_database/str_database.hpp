#include <string>

static std::string good_test_database = R"plop(
Metadata:
    name: "test_database"
    version: "1.0.0"
    check_composition: false

Elements:
    - element: "H2O"
      component: "H2O"
    - element: "E[-]"
      component: "E[-]"
    - element: "C1"
      component: "C1[-]"
    - element: "C2"
      component: "C2[+]"
    - element: "C3"
      component: "C3"

Basis:
    - label: "H2O"
      molar_mass: 1.0
    - label: "E[-]"
      molar_mass: 0.0
    - label: "C1[-]"
      molar_mass: 2.0
      activity: {a: 1.0, b: 0.4}
    - label: "C2[+]"
      molar_mass: 3.0
      activity: {a: 1.0, b: 0.4}
    - label: "C3"
      molar_mass: 4.0
      activity: {a: 0.0, b: 0.1}

Aqueous:
    - label: "A1"
      composition: "C1[-], C2[+]"
      log_k: -2
      activity: {a: 0.0, b: 0.1}
    - label: "A2[+]"
      composition: "A1, C2[+]"
      log_k: -2.0
      activity: {a: 1.0, b: 0.4}
    - label: "A3[-]"
      composition: "C3, C1[-]"
      log_k: -2
      activity: {a: 1.0, b: 0.4}

Minerals:
    - label: "M1"
      composition: "A1"
      log_k: -5
      molar_volume: 10.0
    - label: "M2"
      composition: "A2[+], A3[-]"
      log_k: -6
    - label: "MK1"
      composition: "2C3, A1"
      log_k: -7
      molar_volume: 30.0
      flag_kinetic: 1

Gas:
    - label: "G1"
      composition: "C3"
      log_k: 3

Sorbed:
    - label: "S1"
      composition: "C3"
      nb_sites_occupied: 1
      log_k: -6.0

Compounds:
    - label: "Comp1"
      composition: "2 H2O, C3"

)plop";
