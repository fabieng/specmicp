#include "catch.hpp"
#include "specmicp_database/database.hpp"

#include "specmicp_common/timer.hpp"
#include <iostream>

using namespace specmicp;
using namespace specmicp::database;

TEST_CASE("Cemdata database", "[Cemdata],[Reader],[Selector]")
{
    Timer reader_timer;
    Database the_database(TEST_CEMDATA_PATH);
    reader_timer.stop();

    std::cout << "Reading cemdata database : " << reader_timer.elapsed_time()*1000 << "ms (Ref 2ms)" << std::endl;

    RawDatabasePtr raw_data = the_database.get_database();

    SECTION("Reading")
    {
        REQUIRE(the_database.is_valid());
    }

    SECTION("Oxydo-reduction")
    {
        Timer oxy_timer;
        the_database.remove_half_cell_reactions({"SO4[2-]"});
        oxy_timer.stop();

        std::cout << "Removing half cell reactions for SO4[2-] : " << oxy_timer.elapsed_time()*1000<< "ms (Ref 0.06ms)" << std::endl;

        REQUIRE(the_database.is_valid());
        REQUIRE(the_database.aqueous_label_to_id("HS[-]") == no_species);
        REQUIRE(the_database.gas_label_to_id("H2S(g)") == no_species);
    }
}

TEST_CASE("Other db", "[reader]")
{
    SECTION("Momas")
    {
        Database the_database(TEST_MOMAS_PATH);
    }

}
