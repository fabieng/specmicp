#include "catch.hpp"

#include "specmicp_database/yaml_reader.hpp"
#include "specmicp_database/switch_basis.hpp"

#include "str_database.hpp"

#include "specmicp_common/timer.hpp"

#include <iostream>
#include <sstream>

using namespace specmicp;
using namespace specmicp::database;

TEST_CASE("Switch basis")
{
    std::istringstream input(good_test_database);
    DataReaderYaml reader(input);

    auto data = reader.get_database();

    SECTION("switch basis") {
        size_t orig_hash = data->get_hash();

        Timer timer;
        BasisSwitcher switcher(data);
        std::map<std::string, std::string> swapping = {{"C3", "A3[-]"}};
        switcher.swap_components(swapping);
        timer.stop();

        std::cout << "Switching basis : " << timer.elapsed_time()*1000 << " ms (Ref 0.020ms)" << std::endl;

        CHECK(data->components.is_valid());
        CHECK(data->aqueous.is_valid());
        CHECK(data->minerals.is_valid());
        CHECK(data->minerals_kinetic.is_valid());
        CHECK(data->is_valid());

        CHECK(data->get_hash() != orig_hash);

        // Basis
        CHECK(data->nb_component() == 5);
        CHECK(data->components.get_id("C3") == no_species);
        CHECK(data->components.get_id("A3[-]") == 4);

        // Aqueous
        CHECK(data->nb_aqueous() == 3);
        CHECK(data->aqueous.get_id("A3[-]") == no_species);
        CHECK(data->aqueous.get_id("C3") == 2);

        CHECK(data->nu_aqueous(2, 0) == 0.0);
        CHECK(data->nu_aqueous(2, 1) == 0.0);
        CHECK(data->nu_aqueous(2, 2) == -1.0);
        CHECK(data->nu_aqueous(2, 3) == 0.0);
        CHECK(data->nu_aqueous(2, 4) == 1.0);
        CHECK(data->logk_aqueous(2) == 2.0);

        // Mineral
        CHECK(data->nb_mineral() == 2);

        CHECK(data->nu_mineral(1, 0) == 0.0);
        CHECK(data->nu_mineral(1, 1) == 0.0);
        CHECK(data->nu_mineral(1, 2) == 1.0);
        CHECK(data->nu_mineral(1, 3) == 2.0);
        CHECK(data->nu_mineral(1, 4) == 1.0);
        CHECK(data->logk_mineral(1) == -10.0);
        CHECK(data->unsafe_molar_volume_mineral(1) < 0);

        CHECK(data->molar_mass_mineral(0, units::CMG_units) == 5.0);

        // Kinetic mineral
        CHECK(data->nb_mineral_kinetic() == 1);


        CHECK(data->nu_mineral_kinetic(0, 0) == 0.0);
        CHECK(data->nu_mineral_kinetic(0, 1) == 0.0);
        CHECK(data->nu_mineral_kinetic(0, 2) == -1.0);
        CHECK(data->nu_mineral_kinetic(0, 3) == 1.0);
        CHECK(data->nu_mineral_kinetic(0, 4) == 2.0);
        CHECK(data->logk_mineral_kinetic(0) == -5.0);
        CHECK(data->molar_volume_mineral_kinetic(0) == 1e-6*30.0);
        CHECK(data->molar_mass_mineral_kinetic(0, units::CMG_units) == 13.0);

        // gas
        CHECK(data->nb_gas() == 1);

        CHECK(data->nu_gas(0, 0) == 0.0);
        CHECK(data->nu_gas(0, 1) == 0.0);
        CHECK(data->nu_gas(0, 2) == -1.0);
        CHECK(data->nu_gas(0, 3) == 0.0);
        CHECK(data->nu_gas(0, 4) == 1.0);
        CHECK(data->logk_gas(0) == 5.0);

        // Sorbed
        CHECK(data->nb_sorbed() == 1);
        CHECK(data->sorbed.get_label(0) == "S1");

        CHECK(data->nu_sorbed(0, 0) == 0.0);
        CHECK(data->nu_sorbed(0, 1) == 0.0);
        CHECK(data->nu_sorbed(0, 2) == -1.0);
        CHECK(data->nu_sorbed(0, 3) == 0.0);
        CHECK(data->nu_sorbed(0, 4) == 1.0);
        CHECK(data->logk_sorbed(0) == -4);

        // compounds
        CHECK(data->nb_compounds() == 1);
        CHECK(data->compounds.get_label(0) == "Comp1");

        CHECK(data->nu_compound(0, 0) == 2.0);
        CHECK(data->nu_compound(0, 1) == 0.0);
        CHECK(data->nu_compound(0, 2) == -1.0);
        CHECK(data->nu_compound(0, 3) == 0.0);
        CHECK(data->nu_compound(0, 4) == 1.0);


        // Element
        CHECK(data->get_id_component_from_element("plop") == -1);
        CHECK(data->get_id_component_from_element("C1") != -1);
        CHECK(data->get_label_component_from_element("C1") == "C1[-]");
        CHECK(data->get_label_component_from_element("C3") == "A3[-]");

    }
}
