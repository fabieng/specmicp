#include "catch.hpp"

#include "specmicp_database/yaml_reader.hpp"
#include "specmicp_database/unknown_class.hpp"

#include "str_database.hpp"

using namespace specmicp;
using namespace specmicp::database;

TEST_CASE("unknown aqueous")
{
    std::istringstream input(good_test_database);
    DataReaderYaml reader(input);

    auto data = reader.get_database();


    SECTION("Find aqueous class")
    {
        SpeciesTypeFinder fd(data);
        CHECK(fd.find_aqueous_species_class("C1[-]") == AqueousSpeciesClass::Component);
        CHECK(fd.find_aqueous_species_class("A1")    == AqueousSpeciesClass::Aqueous);
        CHECK(fd.find_aqueous_species_class("Comp1") == AqueousSpeciesClass::Compound);
        CHECK(fd.find_aqueous_species_class("plop")  == AqueousSpeciesClass::Invalid);
    }

    SECTION("Find aqueous")
    {
        SpeciesTypeFinder fd(data);
        GenericAqueousSpecies aq_class = fd.find_aqueous_species("C1[-]");
        CHECK(aq_class.type == AqueousSpeciesClass::Component);
        CHECK(aq_class.id == 2);

        aq_class = fd.find_aqueous_species("A1");
        CHECK(aq_class.type == AqueousSpeciesClass::Aqueous);
        CHECK(aq_class.id == 0);

        aq_class = fd.find_aqueous_species("Comp1");
        CHECK(aq_class.type == AqueousSpeciesClass::Compound);
        CHECK(aq_class.id == 0);

        aq_class = fd.find_aqueous_species("plop");
        CHECK(aq_class.type == AqueousSpeciesClass::Invalid);
        CHECK(aq_class.id == no_species);

    }

    SECTION("Molar mass aqueous")
    {
        SpeciesTypeFinder fd(data);
        CHECK(fd.molar_mass_aqueous("C3")    == Approx(4.0e-3));
        CHECK(fd.molar_mass_aqueous("A3[-]") == Approx(6.0e-3));
        CHECK(fd.molar_mass_aqueous("Comp1") == Approx(6.0e-3));
        CHECK_THROWS_AS(fd.molar_mass_aqueous("plop"), InvalidGenericAqueousSpecies);
    }

    SECTION("Find solid phase claass")
    {
        SpeciesTypeFinder fd(data);
        CHECK(fd.find_solid_phase_class("M1")   == SolidPhaseClass::EquilibriumMineral);
        CHECK(fd.find_solid_phase_class("MK1")  == SolidPhaseClass::MineralKinetics);
        CHECK(fd.find_solid_phase_class("plip") == SolidPhaseClass::Invalid);
    }

    SECTION("Find solid phase")
    {

        SpeciesTypeFinder fd(data);

        GenericSolidPhase sol_class = fd.find_solid_phase("M2");
        CHECK(sol_class.type == SolidPhaseClass::EquilibriumMineral);
        CHECK(sol_class.id == 1);

        sol_class = fd.find_solid_phase("MK1");
        CHECK(sol_class.type == SolidPhaseClass::MineralKinetics);
        CHECK(sol_class.id == 0);

        sol_class = fd.find_solid_phase("plip");
        CHECK(sol_class.type == SolidPhaseClass::Invalid);
        CHECK(sol_class.id == no_species);
    }

    SECTION("Molar mass solid")
    {
        SpeciesTypeFinder fd(data);
        CHECK(fd.molar_mass_solid_phase("M1")  == Approx(5e-3));
        CHECK(fd.molar_mass_solid_phase("MK1") == Approx(13e-3));
        CHECK_THROWS_AS(fd.molar_mass_solid_phase("plip"), InvalidSolidPhase);
    }

    SECTION("Molar volume solid")
    {
        SpeciesTypeFinder fd(data);
        CHECK(fd.molar_volume_solid_phase("M1") == Approx(10e-6));
        CHECK(fd.molar_volume_solid_phase("MK1") == Approx(30e-6));
        CHECK_THROWS_AS(fd.molar_volume_solid_phase("plip"), InvalidSolidPhase);
    }


}
