#include "catch.hpp"

#include "dfpm/meshes/mesh1d.hpp"
#include "dfpm/io/meshes.hpp"

#include "dfpm/io/hdf5_mesh.hpp"
#include "specmicp_common/io/hdf5/file.hpp"

using namespace specmicp;
using namespace specmicp::mesh;
using namespace specmicp::io;

TEST_CASE("HDF5 output", "[Mesh],[1D],[io],[hdf5]") {

    SECTION("test ramp mesh 1d") {
        Ramp1DMeshGeometry geometry;
        geometry.dx_min = 2;
        geometry.dx_max = 100;
        geometry.length_ramp = 1000;
        geometry.length_plateau = 1000;

        geometry.section = 1;


        Mesh1DPtr the_mesh = ramp_mesh1d(geometry);

        auto file = hdf5::File::open("test_mesh.hf5", hdf5::OpenMode::CreateTruncate);

        save_mesh(file, "mesh", the_mesh);

        Mesh1DPtr new_mesh = read_mesh(file, "mesh");

        CHECK(new_mesh->nb_nodes() == the_mesh->nb_nodes());
        for (auto i: the_mesh->range_nodes())
        {
            CHECK(new_mesh->get_position(i) == the_mesh->get_position(i));
            CHECK(new_mesh->get_volume_cell(i) == the_mesh->get_volume_cell(i));
        }
        for (auto e: the_mesh->range_elements())
        {
            CHECK(new_mesh->get_face_area(e) == the_mesh->get_face_area(e));
            CHECK(new_mesh->get_volume_cell_element(e, 0) ==
                  the_mesh->get_volume_cell_element(e, 0));
        }
    }
}
