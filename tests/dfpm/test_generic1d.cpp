#include "catch.hpp"

#include "dfpm/meshes/mesh1d.hpp"

#include "dfpm/io/meshes.hpp"

using namespace specmicp;
using namespace specmicp::mesh;

TEST_CASE("Generic mesh 1d", "[ReactMiCP],[Mesh],[NonUniform],[1D]") {

    SECTION("test ramp mesh 1d") {
        Ramp1DMeshGeometry geometry;
        geometry.dx_min = 2;
        geometry.dx_max = 100;
        geometry.length_ramp = 1000;
        geometry.length_plateau = 1000;

        geometry.section = 1;


        Mesh1DPtr the_mesh = ramp_mesh1d(geometry);

        CHECK(the_mesh->get_position(0) == 0);
        CHECK(the_mesh->get_position(1) == 2);
        CHECK(the_mesh->get_position(2) >= 2);
        CHECK(the_mesh->get_position(the_mesh->nb_nodes()-1) > 2000);

        io::print_mesh("test_ramp_mesh.dat", the_mesh, units::LengthUnit::meter);

    }
}
