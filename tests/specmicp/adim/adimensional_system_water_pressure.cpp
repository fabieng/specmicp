#include "catch.hpp"


#include "specmicp_common/log.hpp"
#include "specmicp/adimensional/adimensional_system_solver.hpp"
#include "specmicp/adimensional/adimensional_system_solution.hpp"
#include "specmicp/adimensional/adimensional_system_solution_extractor.hpp"
#include "specmicp/problem_solver/formulation.hpp"
#include "specmicp/problem_solver/dissolver.hpp"

#include "specmicp_common/timer.hpp"

#include "specmicp_database/database.hpp"

#include <iostream>

using namespace specmicp;

scalar_t water_pressure(scalar_t sat)
{
    const scalar_t tmp = std::pow(sat, -2.1684) -1;
    const scalar_t exp = 1.0-1.0/2.1684;
    scalar_t ln_hr = - 3.67 * std::pow(tmp, exp);
    const scalar_t p = 31.7e2*std::exp(ln_hr);
    return p; // in Pa
}

TEST_CASE("thermocarbo - with water pressure", "[Adimensional],[Thermocarbo],[Water pressure]")
{
    std::cerr.flush();
    std::cout.flush();
    specmicp::logger::ErrFile::stream() = &std::cerr;
    specmicp::stdlog::ReportLevel() = specmicp::logger::Warning;

    SECTION("Thermocarbo")
    {


        std::cout << "\n-------------------\n Thermocarbo - with water pressure\n -------------------" << std::endl;


        specmicp::database::Database thedatabase(TEST_CEMDATA_PATH);

        specmicp::RawDatabasePtr raw_data = thedatabase.get_database();

        std::map<std::string, std::string> swapping ({
                                                  {"H[+]","HO[-]"},
                                                  {"Si(OH)4", "SiO(OH)3[-]"}
                                                    });
        thedatabase.swap_components(swapping);
        thedatabase.remove_gas_phases();

        specmicp::Formulation formulation;
        specmicp::scalar_t mult = 3.0;
        formulation.mass_solution = mult*0.156;
        formulation.amount_minerals = {{"C3S", mult*0.7}, {"C2S", mult*0.3}};
        formulation.extra_components_to_keep = {"HCO3[-]", };

        specmicp::Vector total_concentrations = specmicp::Dissolver(raw_data).dissolve(formulation);
        specmicp::AdimensionalSystemConstraints constraints(total_concentrations);
        constraints.charge_keeper = raw_data->get_id_component("HO[-]");

        constraints.set_water_partial_pressure_model((water_partial_pressure_f) water_pressure);

        specmicp::index_t id_h2o = raw_data->water_index();
        specmicp::index_t id_ho  = raw_data->get_id_component("HO[-]");
        specmicp::index_t id_co2 = raw_data->get_id_component("HCO3[-]");

        specmicp::AdimensionalSystemSolverOptions options;
        options.solver_options.maxstep = 20.0;
        options.solver_options.maxiter_maxstep = 100;
        options.solver_options.disable_descent_direction();
        options.solver_options.set_tolerance(1e-10, 1e-12);
        //options.solver_options.disable_non_monotone_linesearch();
        options.solver_options.disable_condition_check();
        options.solver_options.disable_crashing();
        options.solver_options.enable_scaling();


        options.units_set.length = specmicp::units::LengthUnit::decimeter;

        specmicp::Timer tot_timer;
        tot_timer.start();

        specmicp::Vector x;
        specmicp::AdimensionalSystemSolver solver(raw_data, constraints, options);
        solver.initialize_variables(x, 0.8, -4.0);
        specmicp::scalar_t dh2co3 = 0.1;

        specmicp::index_t nb_iter {0};

        constexpr specmicp::index_t nb_step = 20;

        for (int k=0; k<nb_step; ++k)
        {
            const scalar_t tot_conc_w0 = constraints.total_concentrations(id_h2o);

            solver = specmicp::AdimensionalSystemSolver(raw_data, constraints, options);

            specmicp::micpsolver::MiCPPerformance perf =  solver.solve(x);

            REQUIRE(static_cast<int>(perf.return_code) >=
                    static_cast<int>(specmicp::micpsolver::MiCPSolverReturnCode::NotConvergedYet));
            nb_iter += perf.nb_iterations;


            specmicp::AdimensionalSystemSolution adim_solution = solver.get_raw_solution(x);
            specmicp::AdimensionalSystemSolutionExtractor extr(adim_solution, raw_data, options.units_set);
            //std::cout << "Saturation water : " << extr.volume_fraction_water()  << " / " << x(0) << " = " << extr.volume_fraction_water() / x(0)
            //          << " - "
            //          << extr.porosity() << " - "
            //          << extr.saturation_water() << std::endl;

            const scalar_t tot_conc_w = extr.total_solid_concentration(0)
                    + extr.mass_concentration_water()*extr.total_aqueous_concentration(0);
            const scalar_t tot_conc_g = extr.volume_fraction_gas_phase()*1e-3*(
                        water_pressure(extr.saturation_water())/
                        (constants::gas_constant*(273.16+25.0))
                        );
            const scalar_t tot_conc_w_pg = tot_conc_w + tot_conc_g;

            //std::cout << "Vol frac gas : " << extr.volume_fraction_gas_phase() << std::endl;
            //std::cout << "hop : " << extr.total_solid_concentration(0) << " + "
            //          << extr.mass_concentration_water()*extr.total_aqueous_concentration(0) << " + "
            //          << tot_conc_g << std::endl;
            //std::cout << "tot_conc : " << tot_conc_w0 << " - " << tot_conc_w << " = " << tot_conc_w0 - tot_conc_w << std::endl;
            //std::cout << "tot_conc : " << tot_conc_w0 << " - " << tot_conc_w_pg << " = " << tot_conc_w0 - tot_conc_w_pg << std::endl;
            CHECK(tot_conc_w0 == Approx(tot_conc_w_pg).epsilon(1e-10));
            //std::cout << "diff tot " << tot_conc_w - tot_conc_w_pg << std::endl;

            //std::cout <<  "p_v(" << extr.saturation_water() <<  ") = " <<  water_pressure(extr.saturation_water()) << " Pa"<< std::endl;


            constraints.total_concentrations(id_h2o) += mult*dh2co3;
            constraints.total_concentrations(id_ho)  -= mult*dh2co3;
            constraints.total_concentrations(id_co2) += mult*dh2co3;
        }

        tot_timer.stop();

        std::cout << "Total time : " << tot_timer.elapsed_time()  << "s (Ref 0.007s)" << std::endl;
        std::cout << "Nb iter : " << nb_iter  << " (Ref 360)" << std::endl;


    }
}
