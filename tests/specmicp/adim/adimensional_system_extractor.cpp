#include "catch.hpp"

#include "specmicp_common/log.hpp"

#include "specmicp/adimensional/adimensional_system.hpp"
#include "specmicp_common/micpsolver/micpsolver.hpp"

#include "specmicp/adimensional/adimensional_system_solver.hpp"

#include "specmicp/problem_solver/formulation.hpp"
#include "specmicp/problem_solver/dissolver.hpp"
#include "specmicp/adimensional/adimensional_system_solution_extractor.hpp"

#include "specmicp/io/adimensional_system_solution_saver.hpp"
#include "specmicp/io/adimensional_system_solution_reader.hpp"

#include "specmicp_database/database.hpp"

#include <iostream>

static specmicp::RawDatabasePtr get_test_simple_database()
{
    specmicp::database::Database thedatabase(TEST_CEMDATA_PATH);
    std::map<std::string, std::string> swapping ({
                                              {"H[+]","HO[-]"},
                                                });
    thedatabase.swap_components(swapping);

    std::vector<std::string> to_keep = {"HO[-]", "Ca[2+]"};
    thedatabase.keep_only_components(to_keep);
    thedatabase.remove_half_cell_reactions(std::vector<std::string>({"H2O", "HO[-]",})) ;
    return thedatabase.get_database();

}


using namespace specmicp;

TEST_CASE("AdimSystemExtractor", "[extractor],[units]") {
    specmicp::logger::ErrFile::stream() = &std::cerr;
    specmicp::stdlog::ReportLevel() = specmicp::logger::Error;

    specmicp::RawDatabasePtr thedatabase = get_test_simple_database();

    auto id_h2o = database::DataContainer::water_index();
    auto id_oh = thedatabase->get_id_component("HO[-]");
    auto id_ca = thedatabase->get_id_component("Ca[2+]");
    auto id_ch = thedatabase->get_id_mineral("Portlandite");

SECTION("Automatic solver") {


    Vector total_concentration = Vector::Zero(thedatabase->nb_component());
    total_concentration(id_h2o) = 0.03;
    total_concentration(id_oh) = 0.02;
    total_concentration(id_ca) = 0.01;
    specmicp::Vector x;
    specmicp::AdimensionalSystemConstraints constraints(total_concentration);

    specmicp::AdimensionalSystemSolver solver(thedatabase, constraints);
    solver.initialize_variables(x, 0.8, -2.0);
    //x(solver.dof_surface()) = -HUGE_VAL;

    solver.get_options().units_set.length = specmicp::units::LengthUnit::centimeter;
    solver.get_options().solver_options.maxstep = 10.0;
    solver.get_options().solver_options.maxiter_maxstep = 100;
    solver.get_options().solver_options.use_crashing = false;
    solver.get_options().solver_options.use_scaling = true;
    solver.get_options().solver_options.disable_descent_direction();
    solver.get_options().solver_options.factor_gradient_search_direction = 100;

    solver.solve(x);

    AdimensionalSystemSolution solution = solver.get_raw_solution(x);
    AdimensionalSystemSolutionExtractor extr(solution, thedatabase, solver.get_options().units_set);

    CHECK(extr.volume_fraction_water() == Approx(0.542049).epsilon(1e-4));
    CHECK(extr.log_molality_component(id_oh) == Approx(-1.46214).epsilon(1e-4));
    CHECK(extr.log_molality_component(id_ca) == Approx(-1.82093).epsilon(1e-4));
    //CHECK(extr.free_surface_concentration() == -HUGE_VAL);
    CHECK(extr.volume_fraction_mineral(id_ch) == Approx(0.32966).epsilon(1e-4));


    io::save_solution_yaml("test_solution.yaml", thedatabase, solution);

    auto solution2 = io::parse_solution_yaml("test_solution.yaml", thedatabase);
    REQUIRE(solution.main_variables(0) == Approx(solution2.main_variables(0)));
    REQUIRE(solution.main_variables(id_oh) == Approx(solution2.main_variables(id_oh)));
    REQUIRE(solution.main_variables(id_ca) == Approx(solution2.main_variables(id_ca)));
    REQUIRE(extr.volume_fraction_mineral(id_ch) == Approx(solution2.main_variables(extr.dof_mineral(id_ch))));
    REQUIRE(solution.log_gamma.norm() == Approx(solution2.log_gamma.norm()));
}
}
