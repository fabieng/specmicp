#include "catch.hpp"

#include "specmicp_database/database.hpp"


#include "specmicp_common/io/safe_config.hpp"
#include "specmicp_common/io/config_yaml_sections.h"

#include "specmicp_database/io/configuration.hpp"
#include "specmicp_common/physics/io/configuration.hpp"
#include "specmicp/io/configuration.hpp"

#include "specmicp/adimensional/adimensional_system_structs.hpp"
#include "specmicp/adimensional/adimensional_system_solver_structs.hpp"

#include "specmicp/problem_solver/reactant_box.hpp"

#include "specmicp/adimensional/adimensional_system_solver.hpp"
#include "specmicp/problem_solver/smart_solver.hpp"
#include "specmicp/adimensional/adimensional_system_solution_extractor.hpp"

TEST_CASE("Configuration", "[SpecMiCP],[io],[configuration]")
{
    auto conf = specmicp::io::YAMLConfigFile::load("specmicp_conf_test.yaml");
    specmicp::RawDatabasePtr raw_db = specmicp::io::configure_database(
                                          conf.get_section(SPC_CF_S_DATABASE), {});

    auto units_set = specmicp::io::configure_units(conf.get_section(SPC_CF_S_UNITS));

    SECTION("Options")
    {
        specmicp::AdimensionalSystemSolverOptions opts;
        specmicp::io::configure_specmicp_options(opts, units_set,
                                                 conf.get_section(SPC_CF_S_SPECMICP));

        CHECK(opts.solver_options.fvectol == 1e-8);
        CHECK(opts.solver_options.steptol == 1e-14);
        CHECK(opts.solver_options.maxstep == 20.0);
        CHECK(opts.system_options.non_ideality_tolerance == 1e-14);
    }

    SECTION("ReactantBox")
    {
        specmicp::ReactantBox reactant_box =
                specmicp::io::configure_specmicp_reactant_box(
                                raw_db,
                                units_set,
                                conf.get_section("speciation").get_section(0)
                                );
        auto constraints = reactant_box.get_constraints(true);
        CHECK(raw_db->get_id_component_from_element("K") != specmicp::no_species);
        CHECK(raw_db->get_id_component_from_element("Na") != specmicp::no_species);
        CHECK(raw_db->get_id_component_from_element("Cl") != specmicp::no_species);
        CHECK(raw_db->get_id_component_from_element("Ca") != specmicp::no_species);
        CHECK(raw_db->get_id_component_from_element("Al") != specmicp::no_species);
        CHECK(raw_db->get_id_component_from_element("Si") != specmicp::no_species);
        CHECK(raw_db->get_id_component_from_element("C") != specmicp::no_species);
        CHECK(raw_db->get_id_component_from_element("S") == specmicp::no_species);


        CHECK(constraints.charge_keeper == raw_db->get_id_component("HO[-]"));
        CHECK(constraints.inert_volume_fraction == 0.0);
        CHECK(constraints.water_equation == specmicp::WaterEquationType::SaturatedSystem);
        CHECK(constraints.fixed_molality_cs[0].id_component == raw_db->get_id_component("Cl[-]"));
        CHECK(constraints.fixed_molality_cs[0].log_value == Approx(std::log10(0.2)));
    }

    SECTION("Solving from configuration")
    {
        specmicp::AdimensionalSystemSolverOptions opts;
        specmicp::io::configure_specmicp_options(opts, units_set,
                                                 conf.get_section(SPC_CF_S_SPECMICP));
        specmicp::ReactantBox reactant_box =
                specmicp::io::configure_specmicp_reactant_box(
                                raw_db,
                                units_set,
                                conf.get_section("speciation").get_section(0)
                                );


        auto constraints = reactant_box.get_constraints(true);
        specmicp::AdimensionalSystemSolver solver(raw_db, constraints, opts);

        specmicp::Vector x;
        solver.initialize_variables(x, 0.5, -6);

        auto perf = solver.solve(x);
        CHECK(perf.return_code >= specmicp::micpsolver::MiCPSolverReturnCode::Success);
    }


}


auto conf_fixed_sat_str = R"plop(
database:
    path: ../../data/cemdata.yaml
    swap_components:
        - out: H[+]
          in: HO[-]
    #    - out: HCO3[-]
    #      in: CO2
        - out: Al[3+]
          in: Al(OH)4[-]
    remove_gas: true
    remove_half_cells: true

units:
    length: decimeter

speciation:
    - name: "Ca / CO2 system"
      formulation:
          solution:
              amount: 0.8
              unit: kg
          aqueous:
              - label: H2CO3
                amount: 5.0
                unit: mol/kg
              - label: KOH
                amount: 0.1
                unit: mol/kg
              - label: NaOH
                amount: 0.1
                unit: g
          solid_phases:
              - label: C3S
                amount: 3.0
                unit: mol/dm^3
              - label: C2S
                amount: 2.0
                unit: mol/dm^3
              - label: C3A
                amount: 0.3
                unit: mol/dm^3
      constraints:
          charge_keeper: "HO[-]"
          fixed_molality:
              - label: "Cl[-]"
                amount: 0.2
          fixed_saturation: 0.65

)plop";

TEST_CASE("Configuration - Fixed saturation", "[SpecMiCP],[io],[configuration]")
{
    auto conf = specmicp::io::YAMLConfigFile::load_from_string(conf_fixed_sat_str);
    specmicp::RawDatabasePtr raw_db = specmicp::io::configure_database(
                                          conf.get_section(SPC_CF_S_DATABASE), {});

    auto units_set = specmicp::io::configure_units(conf.get_section(SPC_CF_S_UNITS));


    SECTION("ReactantBox")
    {
        specmicp::ReactantBox reactant_box =
                specmicp::io::configure_specmicp_reactant_box(
                                raw_db,
                                units_set,
                                conf.get_section("speciation").get_section(0)
                                );
        auto constraints = reactant_box.get_constraints(true);

        CHECK(constraints.water_equation == specmicp::WaterEquationType::FixedSaturation);
        CHECK(constraints.water_parameter == 0.65);
    }

    SECTION("Solving from configuration")
    {
        specmicp::AdimensionalSystemSolverOptions opts;
        specmicp::ReactantBox reactant_box =
                specmicp::io::configure_specmicp_reactant_box(
                                raw_db,
                                units_set,
                                conf.get_section("speciation").get_section(0)
                                );


        auto constraints = reactant_box.get_constraints(true);
        specmicp::AdimensionalSystemSolver solver(raw_db, constraints, opts);

        specmicp::Vector x;
        solver.initialize_variables(x, 0.5, -6);

        auto perf = solver.solve(x);
        REQUIRE(perf.return_code >= specmicp::micpsolver::MiCPSolverReturnCode::Success);
        auto sol = solver.get_raw_solution(x);
        auto extr = specmicp::AdimensionalSystemSolutionExtractor(sol, raw_db, units_set);
        REQUIRE(extr.saturation_water() == Approx(0.65).epsilon(1e-6));
    }
}
