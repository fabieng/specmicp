#include "catch.hpp"

#include "specmicp_common/log.hpp"
#include "specmicp_common/micpsolver/micpsolver.hpp"

#include "specmicp_database/database.hpp"

#include "specmicp/adimensional/adimensional_system.hpp"
#include "specmicp/adimensional/adimensional_system_solver.hpp"
#include "specmicp/problem_solver/formulation.hpp"
#include "specmicp/problem_solver/dissolver.hpp"
#include "specmicp/adimensional/adimensional_system_solution_extractor.hpp"

#include <iostream>

specmicp::RawDatabasePtr get_test_database_set_kinetic()
{
    specmicp::database::Database thedatabase(TEST_CEMDATA_PATH);
    std::map<std::string, std::string> swapping ({
                                              {"H[+]","HO[-]"},
                                                });
    thedatabase.swap_components(swapping);

    std::vector<std::string> to_keep = {"HO[-]", "Ca[2+]", "HCO3[-]"};
    thedatabase.keep_only_components(to_keep);
    thedatabase.remove_half_cell_reactions(std::vector<std::string>({"H2O", "HO[-]",})) ;
    return thedatabase.get_database();

}


using namespace specmicp;

TEST_CASE("Set a mineral to be governed by kinetics", "[adimensional],[solver],[modificator],[kinetics]") {

    specmicp::logger::ErrFile::stream() = &std::cerr;
    specmicp::stdlog::ReportLevel() = specmicp::logger::Error;

    SECTION("Set kinetics") {


        specmicp::RawDatabasePtr thedatabase = get_test_database_set_kinetic();

        auto id_h2o = database::DataContainer::water_index();
        auto id_oh = thedatabase->get_id_component("HO[-]");
        auto id_ca = thedatabase->get_id_component("Ca[2+]");
        auto id_hco3 = thedatabase->get_id_component("HCO3[-]");
        auto id_ch = thedatabase->get_id_mineral("Portlandite");
        auto id_calcite_eq = thedatabase->get_id_mineral("Calcite");

        Vector total_concentration = Vector::Zero(thedatabase->nb_component());
        total_concentration(id_h2o) = 0.03;
        total_concentration(id_oh) = 0.015;
        total_concentration(id_ca) = 0.01;
        total_concentration(id_hco3) = 0.005;
        specmicp::Vector x;
        specmicp::AdimensionalSystemConstraints constraints(total_concentration);

        specmicp::AdimensionalSystemSolver solver(thedatabase, constraints);
        solver.initialize_variables(x, 0.8, -2.0);
        //x(solver.dof_surface()) = -HUGE_VAL;

        solver.get_options().units_set.length = specmicp::units::LengthUnit::centimeter;
        solver.get_options().solver_options.maxstep = 10.0;
        solver.get_options().solver_options.maxiter_maxstep = 100;
        solver.get_options().solver_options.use_crashing = false;
        solver.get_options().solver_options.use_scaling = true;
        solver.get_options().solver_options.disable_descent_direction();
        solver.get_options().solver_options.factor_gradient_search_direction = 100;

        solver.solve(x);

        AdimensionalSystemSolution solution = solver.get_raw_solution(x);
        AdimensionalSystemSolutionExtractor extr(solution, thedatabase, solver.get_options().units_set);

        scalar_t volume_fraction_calcite = extr.volume_fraction_mineral(id_calcite_eq);

        AdimensionalSystemSolutionModificator modor(solution, thedatabase, solver.get_options().units_set);
        std::vector<index_t> to_kinetic {id_calcite_eq};

        Vector vol_fracs = modor.set_minerals_kinetics(to_kinetic);

        CHECK(vol_fracs(0) == volume_fraction_calcite);
        CHECK(thedatabase->get_id_mineral_kinetic("Calcite") != no_species);
        CHECK(thedatabase->get_id_mineral("Calcite") == no_species);
        CHECK(thedatabase->get_id_mineral_kinetic("Calcite") == to_kinetic[0]);
    }

}
