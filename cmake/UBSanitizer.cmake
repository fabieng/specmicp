# SanitizerBuild
# ===============
#
# Define a new build type using the "-fsanitize=" options
# It can be activated the normal way : -DCMAKE_BUILD_TYPE=Sanitizer
#
# author : Fabien Georget
# This is part of the SpecMiCP build system


#This are the flags that may be included
set(UNDEFINED_SANITIZER_FLAG "-fsanitize=undefined")

# test if the flags exists
include(CheckCXXCompilerFlag)
set(CMAKE_REQUIRED_FLAGS "-Werror ${UNDEFINED_SANITIZER_FLAG}")
check_cxx_compiler_flag("${UNDEFINED_SANITIZER_FLAG}" HAVE_UNDEFINED_SANITIZER)
unset(CMAKE_REQUIRED_FLAGS)


if (NOT HAVE_UNDEFINED_SANITIZER)
    message(WARNING "The address sanitizer does not exist")
else()

    set(FLAGS_UBSANITIZER "${FLAGS_UBSANITIZER} -O2 -g -Wall -pedantic ${SANTIZER_EXTRA_WARNINGS}  ${UNDEFINED_SANITIZER_FLAG}")

# -Wfloat-conversion -Winline -Weffc++ -Wold-style-cast -Woverloaded-virtual
    set(SANITIZER_EXTRA_WARNINGS "-Wextra"
        CACHE STRING "Extra warnings for the sanitizer build")

    set( CMAKE_CXX_FLAGS_UBSANITIZER "${FLAGS_UBSANITIZER}" CACHE STRING
        "Flags used by the C++ compiler during sanitizer builds."
        FORCE )
    set( CMAKE_C_FLAGS_UBSANITIZER "${FLAGS_UBSANITIZER}" CACHE STRING
        "Flags used by the C compiler during sanitizer builds."
        FORCE )
    set( CMAKE_EXE_LINKER_FLAGS_UBSANITIZER
        "${FLAGS_UBSANITIZER}" CACHE STRING
        "Flags used for linking binaries during UBSANITIZER builds."
        FORCE )
    set( CMAKE_SHARED_LINKER_FLAGS_UBSANITIZER
        "${FLAGS_UBSANITIZER}" CACHE STRING
        "Flags used by the shared libraries linker during UBSANITIZER builds."
        FORCE )
    set( CMAKE_STATIC_LINKER_FLAGS_UBSANITIZER
        "" CACHE STRING
        "Flags used by the static libraries linker during UBSANITIZER builds."
        FORCE )

    mark_as_advanced(
        CMAKE_CXX_FLAGS_UBSANITIZER
        CMAKE_C_FLAGS_UBSANITIZER
        CMAKE_EXE_LINKER_FLAGS_UBSANITIZER
        CMAKE_SHARED_LINKER_FLAGS_UBSANITIZER
        CMAKE_STATIC_LINKER_FLAGS_UBSANITIZER
    )
# Update the documentation string of CMAKE_BUILD_TYPE for GUIs
    set( CMAKE_BUILD_TYPE "${CMAKE_BUILD_TYPE}" CACHE STRING
        "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel ASanitizer UBSanitizer. "
        FORCE)
endif()
