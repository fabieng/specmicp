
# For an explanation of the options see the INSTALL file
option( SPECMICP_USE_OPENMP          "Use OpenMP for parallelisation"   ON  )
option( SPECMICP_NO_DEBUG            "Disable SpecMiCP assert"          OFF )
option( SPECMICP_BUILD_STATIC        "Build static libraries"           OFF )
option( SPECMICP_BUILD_EXAMPLE       "Build the examples"               ON  )
option( SPECMICP_BENCHMARK           "Build benchmark"                  OFF )
option( SPECMICP_TEST                "Enable testing"                   ON  )
option( SPECMICP_BINARIES_USE_STATIC "Executables use static libraries" OFF )

#  Linear algebra
option( SPECMICP_USE_BLAS            "Eigen uses BLAS/LAPACK"           OFF )

# PGO sequence
option( SPECMICP_PROFILE_GENERATE "Generate profile for PGO optimization" OFF )
option( SPECMICP_PROFILE_USE      "Use profile for PGO optimization"      OFF )

# LTO optimization
option( SPECMICP_LD_GOLD "Use GNU gold linker"        ON  )
option( SPECMICP_LTO     "Use link time optimization" OFF )
option( SPECMICP_FAT_LTO "Use link time optimization with fat objects" ON )

# the following is only a debug options for developpers

# This options turns on the finite difference jacobian in specmicp system
option( SPECMICP_DEBUG_EQUATION_FD_JACOBIAN "Use a finite difference jacobian" OFF )

