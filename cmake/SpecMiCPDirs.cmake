#                        Directories
#########################################################################

# use gnu coding standards
include(GNUInstallDirs)

# the libraries install dir
set( LIBRARY_INSTALL_DIR
    ${CMAKE_INSTALL_FULL_LIBDIR}
    CACHE PATH "Installation directory for libraries"
)
# the static libraries install dir
set( STATIC_LIBRARY_INSTALL_DIR
    ${CMAKE_INSTALL_FULL_LIBDIR}
    CACHE PATH "Installation directory for static libraries"
)

# Binaries
# --------
set( BIN_INSTALL_DIR
     ${CMAKE_INSTALL_FULL_BINDIR}
     CACHE PATH "Installation directory for the programs"
)

# include
#--------
set( INCLUDE_INSTALL_DIR
     ${CMAKE_INSTALL_FULL_INCLUDEDIR}
     CACHE PATH "Installation directory for the headers"
)

# share
#------
set( SHARE_INSTALL_DIR
     "${CMAKE_INSTALL_FULL_DATADIR}/specmicp/"
     CACHE PATH "Installation directory for the miscalleneous files..."
)

mark_as_advanced(
    LIBRARY_INSTALL_DIR
    STATIC_LIBRARY_INSTALL_DIR
    BIN_INSTALL_DIR
    INCLUDE_INSTALL_DIR
    SHARE_INSTALL_DIR
    )
