# check for GMRES solver
#
# Eigen3 unsuported
# GMRES.h is really the file we are using/looking for
# If it doesn't exist then the solver will not be included in the list of the parse solvers
if(NOT EIGEN3_UNSUPPORTED_FOUND)
    # first check that eigen was found
    if(NOT EIGEN3_FOUND)
        set(EIGEN3_UNSUPPORTED_FOUND OFF CACHE BOOL "Eigen/unsupported exists")
    else()
        # then check for the files we are looking for
        set( EIGEN3_GMRES_PATH "${EIGEN3_INCLUDE_DIR}/unsupported/Eigen/src/IterativeSolvers/GMRES.h")
        set( EIGEN3_SPARSE_EXTRA "${EIGEN3_INCLUDE_DIR}/unsupported/Eigen/SparseExtra")
        if( EXISTS ${EIGEN3_GMRES_PATH} AND EXISTS ${EIGEN3_SPARSE_EXTRA})

            # if found register variables
            set(EIGEN3_UNSUPPORTED_FOUND ON CACHE BOOL "Eigen/Unsupported exists" FORCE)
            set(EIGEN3_UNSUPPORTED_INCLUDE_DIR "${EIGEN3_INCLUDE_DIR}/unsupported/"
                CACHE PATH "Include dir for eigen unsupported modules" FORCE)
            mark_as_advanced(
                EIGEN3_UNSUPPORTED_FOUND
                EIGEN3_UNSUPPORTED_INCLUDE_DIR
                )
            message(STATUS "Eigen3/unsupported found.")
        else()
            set(EIGEN3_UNSUPPORTED_FOUND OFF CACHE BOOL "Eigen/Unsupported exists")
            message(STATUS "Eigen3/unsupported not found.")
        endif()
    endif(NOT EIGEN3_FOUND)
endif(NOT EIGEN3_UNSUPPORTED_FOUND)
