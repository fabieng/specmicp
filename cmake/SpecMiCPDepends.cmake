
# Eigen
# -----

find_package(Eigen3 REQUIRED)  # This module comes from the Eigen3 Package
message(STATUS "Eigen3 version ${EIGEN3_VERSION}")

# optional
find_package(Eigen3Unsupported)

add_library(Eigen::eigen INTERFACE IMPORTED)
set_property(TARGET Eigen::eigen PROPERTY
    INTERFACE_INCLUDE_DIRECTORIES ${EIGEN3_INCLUDE_DIR})


if(SPECMICP_USE_BLAS)
    if(NOT DEFINED EIGEN3_EXTRA_LINKFLAGS)
        message(SEND_ERROR "Set EIGEN3_EXTRA_LINKFLAGS to provide the lapacke libraries")
    endif()
    set_property(TARGET Eigen::eigen PROPERTY
        INTERFACE_LINK_LIBRARIES ${EIGEN3_EXTRA_LINKFLAGS})

endif(SPECMICP_USE_BLAS)

# HDF5
# ----

# HDF5 is required and used by all modules for output
#
# Only the C serial API is used
find_package(HDF5 REQUIRED COMPONENTS C)

add_library(HDF5::hdf5 UNKNOWN IMPORTED)
set_property(TARGET HDF5::hdf5 PROPERTY
    INTERFACE_INCLUDE_DIRECTORIES ${HDF5_INCLUDE_DIRS}
)

if(DEFINED HDF5_C_LIBRARY_hdf5)
    set(hdf5_lib ${HDF5_C_LIBRARY_hdf5})
else()
    set(hdf5_lib ${HDF5_C_LIBRARIES})
endif()


set_property(TARGET HDF5::hdf5 PROPERTY
    IMPORTED_LOCATION ${hdf5_lib}
)

# Yaml-cpp
# --------

# YAML-cpp library is required and used by all modules for configuration
# and data storage

# YAML-CPP does not provide a CMake find module.
# Instead, we use pkgconfig
include(FindPkgConfig)
if(CMAKE_VERSION VERSION_LESS 3.7.0)
    # Imported target is also broken for 3.6.3 (version in Centos7)
    # was fixed for 3.7.0, this is manual set-up to correct for that
    pkg_check_modules(YAML REQUIRED yaml-cpp>=0.5)
    add_library(PkgConfig::YAML UNKNOWN IMPORTED)
    set_property(TARGET PkgConfig::YAML PROPERTY
        INTERFACE_INCLUDE_DIRECTORIES ${YAML_INCLUDE_DIRS}
    )
    set_property(TARGET PkgConfig::YAML PROPERTY
        IMPORTED_LOCATION "${YAML_LIBRARY_DIRS}/lib${YAML_LIBRARIES}.so"
    )
else()
    pkg_check_modules(YAML REQUIRED IMPORTED_TARGET yaml-cpp>=0.5)
endif()


