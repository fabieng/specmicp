# Gold linker
# ===========

# Use ld.gold if it is available and isn't disabled explicitly

# Ref : https://bugs.webkit.org/show_bug.cgi?id=137953
if ( SPECMICP_LD_GOLD )
    execute_process(
        COMMAND ${CMAKE_C_COMPILER} -fuse-ld=gold -Wl,--version
        ERROR_QUIET
        OUTPUT_VARIABLE LD_VERSION
    )
    if ( "${LD_VERSION}" MATCHES "GNU gold" ) # true if ld-gold available
        if ("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
           # if clang it needs -flto
            set( SPECMICP_LTO ON )
        endif()
            set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fuse-ld=gold" )
            set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fuse-ld=gold" )
        set( USE_LD_GOLD TRUE CACHE INTERNAL "True if LD gold is used" )

    else ()
        set( USE_LD_GOLD FALSE CACHE INTERNAL "True if LD gold is used" )
        message( WARNING "GNU gold linker isn't available, using the default system linker." )
    endif ()
endif ()
