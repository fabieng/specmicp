# -fvisibility=hidden
# ---------------------
# Change the default visibility of symbols for shared libraries
#
# The symbols visibility can be changed int the code
# using the SPECMICP_DLL_PUBLIC and SPECMICP_DLL_LOCAL macros
# from specmicp_common/macros.hpp


# check if the compiler supports it
check_cxx_compiler_flag( "-fvisibility=hidden" HAVE_VISIBILITY_HIDDEN )
check_cxx_compiler_flag( "-fvisibility-inlines-hidden" HAVE_VISIBILITY_INLINES_HIDDEN )

# The following function is used to enable the visibility flags if they exist
#
# usage : set_visibility_flag( src1 [src2 [src3 ...]] )
function( set_visibility_flag )
    if ( HAVE_VISIBILITY_HIDDEN )
    set_property( SOURCE ${ARGN} APPEND_STRING PROPERTY
         COMPILE_FLAGS " -fvisibility=hidden" )
    endif()
    if ( HAVE_VISIBILITY_INLINES_HIDDEN )
        set_property( SOURCE ${ARGN} APPEND_STRING PROPERTY
             COMPILE_FLAGS " -fvisibility-inlines-hidden" )
    endif()
endfunction( set_visibility_flag )
