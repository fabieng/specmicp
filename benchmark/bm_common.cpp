// this file is a micro-benchmark of the specmicp common libs

// It uses the google/benchmark framework


#include <benchmark/benchmark.h>

#include "specmicp_common/range_iterator.hpp"
#include "specmicp_common/timer.hpp"
#include "specmicp_common/moving_average.hpp"

using namespace specmicp;

// Range iterator
// ==============

static void BMSimpleLoop(benchmark::State& state) {
  while (state.KeepRunning()) {
      for (int i=0; i<100; ++i) {
          benchmark::DoNotOptimize(i);
      }
  }
}
BENCHMARK(BMSimpleLoop);


static void BMRangeIterator(benchmark::State& state) {
  while (state.KeepRunning()) {
      for (auto it: RangeIterator<int>(100)) {
          benchmark::DoNotOptimize(it);
      }
  }
}
BENCHMARK(BMRangeIterator);


// Timer
// =====

static void BMTimer(benchmark::State& state) {
  while (state.KeepRunning()) {
      Timer timer;
      timer.stop();
  }
}
BENCHMARK(BMTimer);

static void BMTimer_restart(benchmark::State& state) {
  while (state.KeepRunning()) {
      Timer timer;
      timer.start();
      timer.stop();
  }
}
BENCHMARK(BMTimer_restart);

static void BMTimer_elapsedtime(benchmark::State& state) {
  while (state.KeepRunning()) {
      Timer timer;
      timer.stop();
      scalar_t x;
      benchmark::DoNotOptimize(x = timer.elapsed_time());

  }
}
BENCHMARK(BMTimer_elapsedtime);

// Moving Average
// ==============

static void BMMovingAverage_init(benchmark::State& state) {
    while (state.KeepRunning()) {
        utils::ExponentialMovingAverage average(0.2, 1.0);
        benchmark::DoNotOptimize(average);
    }
}
BENCHMARK(BMMovingAverage_init);

static void BMMovingAverage_addpoint(benchmark::State& state) {
    while (state.KeepRunning()) {
        utils::ExponentialMovingAverage average(0.2, 1.0);
        scalar_t x;
        benchmark::DoNotOptimize(x = average.add_point(2.0));
    }
}
BENCHMARK(BMMovingAverage_addpoint);


BENCHMARK_MAIN()
