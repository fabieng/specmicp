#include <benchmark/benchmark.h>

#include "specmicp/adimensional/adimensional_system.hpp"
#include "specmicp_database/database.hpp"
#include "specmicp_common/physics/laws.hpp"
#include "specmicp_common/micpsolver/micpsolver.hpp"
#include "specmicp/adimensional/adimensional_system_solver.hpp"
#include "specmicp/adimensional/adimensional_system_solution.hpp"

#include <iostream>

using namespace specmicp;

static specmicp::RawDatabasePtr get_simple_database()
{
    specmicp::database::Database thedatabase(TEST_CEMDATA_PATH);
    std::map<std::string, std::string> swapping ({
                                              {"H[+]","HO[-]"},
                                               });
    thedatabase.swap_components(swapping);
    std::vector<std::string> to_keep = {"HO[-]", "Ca[2+]"};
    thedatabase.keep_only_components(to_keep);

    thedatabase.remove_half_cell_reactions(std::vector<std::string>({"H2O", "HO[-]"}));
    return thedatabase.get_database();

}

static AdimensionalSystemConstraints get_simple_constraints(RawDatabasePtr raw_data)
{
    Vector total_concentration = Vector::Zero(raw_data->nb_component());
    total_concentration(raw_data->get_id_component("H2O")) = 55.5;
    total_concentration(raw_data->get_id_component("HO[-]")) = 2e-3;
    total_concentration(raw_data->get_id_component("Ca[2+]")) = 1e-3;
    return AdimensionalSystemConstraints(total_concentration);
}

static void bm_simple_init(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    while(state.KeepRunning())
    {
        AdimensionalSystem system(raw_data, constraints);
    }
}

BENCHMARK(bm_simple_init);

static void bm_simple_secondary_variables(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    AdimensionalSystem system(raw_data, constraints);
    Vector x(4);
    x << 0.1, -3, -3, 0.0;
    while(state.KeepRunning())
    {
        system.set_secondary_variables(x);
    }
}

static void bm_ideq_w(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    AdimensionalSystem system(raw_data, constraints);
    while(state.KeepRunning())
    {
        index_t id = system.ideq_w();
        benchmark::DoNotOptimize(id);
    }
}
//BENCHMARK(bm_ideq_w);


static void bm_ideq_comp(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    AdimensionalSystem system(raw_data, constraints);
    while(state.KeepRunning())
    {
        index_t id = system.ideq_paq(2);
        benchmark::DoNotOptimize(id);
    }
}
//BENCHMARK(bm_ideq_comp);


BENCHMARK(bm_simple_secondary_variables);

static void bm_simple_secondary_concentration(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    AdimensionalSystem system(raw_data, constraints);
    Vector x(4);
    x << 0.1, -3, -3, 0.0;
    while(state.KeepRunning())
    {
        system.set_secondary_concentration(x);
    }
}

BENCHMARK(bm_simple_secondary_concentration);

static void bm_simple_log_gamma(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    AdimensionalSystem system(raw_data, constraints);
    Vector x(4);
    x << 0.1, -3, -3, 0.0;
    system.set_secondary_concentration(x);
    while(state.KeepRunning())
    {
        system.compute_log_gamma(x);
    }
}

BENCHMARK(bm_simple_log_gamma);

static void bm_debye_huckel(benchmark::State& state)
{
    scalar_t sqrtI = 0.1;
    scalar_t zi = 2;
    scalar_t ao = 3.0;
    while(state.KeepRunning())
    {
        scalar_t a = laws::debye_huckel(sqrtI, zi, ao);
        benchmark::DoNotOptimize(a);
    }
}

BENCHMARK(bm_debye_huckel);

static void bm_extended_debye_huckel(benchmark::State& state)
{
    scalar_t ionic_strength = 0.01;
    scalar_t sqrtI = 0.1;
    scalar_t zi = 2;
    scalar_t ao = 3.0;
    scalar_t bdot = 0.1;
    while(state.KeepRunning())
    {
        scalar_t a = laws::extended_debye_huckel(ionic_strength, sqrtI, zi, ao, bdot);
        benchmark::DoNotOptimize(a);
    }
}

BENCHMARK(bm_extended_debye_huckel);

static void bm_simple_residual_water(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    AdimensionalSystem system(raw_data, constraints);
    Vector x(4);
    x << 0.1, -3, -3, 0.0;
    system.set_secondary_variables(x);
    while(state.KeepRunning())
    {
        system.residual_water_conservation(x);
    }
}

BENCHMARK(bm_simple_residual_water);


static void bm_simple_residual_ca(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    AdimensionalSystem system(raw_data, constraints);
    Vector x(4);
    x << 0.1, -3, -3, 0.0;
    system.set_secondary_variables(x);
    while(state.KeepRunning())
    {
        system.residual_component(x, 2);
    }
}

BENCHMARK(bm_simple_residual_ca);


static void bm_residuals(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    AdimensionalSystem system(raw_data, constraints);
    Vector x(4);
    x << 0.1, -3, -3, 0.0;
    system.set_secondary_variables(x);
    while(state.KeepRunning())
    {
        Vector residuals(system.total_variables());
        system.get_residuals(x, residuals);
    }

}
BENCHMARK(bm_residuals);


static void bm_analytical_jacobian(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    AdimensionalSystem system(raw_data, constraints);
    Vector x(4);
    x << 0.1, -3, -3, 0.0;
    system.set_secondary_variables(x);
    while(state.KeepRunning())
    {
        index_t neq = system.total_variables();
        Matrix jacobian(neq, neq);
        system.analytical_jacobian(x, jacobian);
    }

}
BENCHMARK(bm_analytical_jacobian);


static void bm_finite_difference_jacobian(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    AdimensionalSystem system(raw_data, constraints);
    Vector x(4);
    x << 0.1, -3, -3, 0.0;
    system.set_secondary_variables(x);
    while(state.KeepRunning())
    {
        index_t neq = system.total_variables();
        Matrix jacobian(neq, neq);
        system.finite_difference_jacobian(x, jacobian);
    }

}
BENCHMARK(bm_finite_difference_jacobian);

static void bm_adim_solver_init(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    auto system = std::make_shared<AdimensionalSystem>(raw_data, constraints);
    while(state.KeepRunning())
    {
        micpsolver::MiCPSolver<AdimensionalSystem> solver(system);
    }
}
BENCHMARK(bm_adim_solver_init);


static void bm_adim_solver_residuals(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    auto system = std::make_shared<AdimensionalSystem>(raw_data, constraints);
    micpsolver::MiCPSolver<AdimensionalSystem> solver(system);
    Vector x(4);
    x << 0.1, -3, -3, 0.0;
    while(state.KeepRunning())
    {
        Vector residuals(solver.get_neq());
        solver.base::compute_residuals(x, residuals);
    }
}
BENCHMARK(bm_adim_solver_residuals);

static void bm_adim_reformulate_residuals(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    auto system = std::make_shared<AdimensionalSystem>(raw_data, constraints);
    micpsolver::MiCPSolver<AdimensionalSystem> solver(system);
    Vector x(4);
    x << 0.1, -3, -3, 0.0;
    while(state.KeepRunning())
    {
        Vector residuals(solver.get_neq());
        solver.base::compute_residuals(x, residuals);
        Vector residuals_reformulated(solver.get_neq());
        solver.reformulate_residuals(x, residuals, residuals_reformulated);
    }
}
BENCHMARK(bm_adim_reformulate_residuals);


static void bm_adim_reformulate_residuals_inplace(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    auto system = std::make_shared<AdimensionalSystem>(raw_data, constraints);
    micpsolver::MiCPSolver<AdimensionalSystem> solver(system);
    Vector x(4);
    x << 0.1, -3, -3, 0.0;
    while(state.KeepRunning())
    {
        Vector residuals(solver.get_neq());
        solver.base::compute_residuals(x, residuals);
        solver.reformulate_residuals_inplace(x, residuals);
    }
}
BENCHMARK(bm_adim_reformulate_residuals_inplace);

static void bm_adim_solver_jacobian(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    auto system = std::make_shared<AdimensionalSystem>(raw_data, constraints);
    micpsolver::MiCPSolver<AdimensionalSystem> solver(system);
    Vector x(4);
    x << 0.1, -3, -3, 0.0;
    while(state.KeepRunning())
    {
        solver.compute_jacobian(x);
    }
}
BENCHMARK(bm_adim_solver_jacobian);

static void bm_adim_solver_reformulate_jacobian(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    auto system = std::make_shared<AdimensionalSystem>(raw_data, constraints);
    micpsolver::MiCPSolver<AdimensionalSystem> solver(system);
    Vector x(4);
    x << 0.1, -3, -3, 0.0;
    while(state.KeepRunning())
    {
        solver.compute_jacobian(x);
        solver.reformulate_jacobian(x);
    }
}
BENCHMARK(bm_adim_solver_reformulate_jacobian);

static void bm_adim_solver_setup_jacobian(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    auto system = std::make_shared<AdimensionalSystem>(raw_data, constraints);
    micpsolver::MiCPSolver<AdimensionalSystem> solver(system);
    Vector x(4);
    x << 0.1, -3, -3, 0.0;
    while(state.KeepRunning())
    {
        solver.setup_jacobian(x);
    }
}
BENCHMARK(bm_adim_solver_setup_jacobian);

static void bm_adim_solver_scaling_jacobian(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    auto system = std::make_shared<AdimensionalSystem>(raw_data, constraints);
    micpsolver::MiCPSolver<AdimensionalSystem> solver(system);
    Vector x(4);
    x << 0.1, -3, -3, 0.0;
    index_t neq = solver.get_neq();
    Vector residuals(neq);
    solver.base::compute_residuals(x, residuals);
    solver.reformulate_residuals_inplace(x, residuals);
    Matrix jacobian(neq,neq);
    jacobian.setZero();
    system->get_jacobian(x, jacobian);
    solver.reformulate_jacobian_cck(x, residuals, jacobian);
    while(state.KeepRunning())
    {
        Vector rscaler(neq), cscaler(neq);
        solver.scaling_jacobian(jacobian, residuals, rscaler, cscaler);
    }
}
BENCHMARK(bm_adim_solver_scaling_jacobian);

static void bm_adim_solver_search_direction(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    auto system = std::make_shared<AdimensionalSystem>(raw_data, constraints);
    micpsolver::MiCPSolver<AdimensionalSystem> solver(system);
    Vector x(4);
    x << 0.1, -3, -3, 0.0;
    solver.setup_residuals(x);
    solver.setup_jacobian(x);
    Vector update(solver.get_neq());
    while(state.KeepRunning())
    {
        solver.search_direction_calculation_no_scaling(update);
    }
}
BENCHMARK(bm_adim_solver_search_direction);

static void bm_adim_solver_search_direction_scaling(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    auto system = std::make_shared<AdimensionalSystem>(raw_data, constraints);
    micpsolver::MiCPSolver<AdimensionalSystem> solver(system);
    Vector x(4);
    x << 0.1, -3, -3, 0.0;
    solver.setup_residuals(x);
    solver.setup_jacobian(x);
    Vector update(solver.get_neq());
    while(state.KeepRunning())
    {
        solver.search_direction_calculation(update);
    }
}
BENCHMARK(bm_adim_solver_search_direction_scaling);

static void bm_adim_system_solver_init(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);

    while(state.KeepRunning())
    {
        AdimensionalSystemSolver solver(raw_data, constraints);
    }
}
BENCHMARK(bm_adim_system_solver_init);


static void bm_adim_system_solver_initialize_variables(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    AdimensionalSystemSolver solver(raw_data, constraints);
    Vector x;
    while(state.KeepRunning())
    {
        solver.initialize_variables(x, 0.5, -4);
    }
}
BENCHMARK(bm_adim_system_solver_initialize_variables);


static void bm_adim_system_solver_solve(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    AdimensionalSystemSolver solver(raw_data, constraints);

    while(state.KeepRunning())
    {
        Vector x;
        solver.initialize_variables(x, 0.5, -4);
        solver.solve(x, false);
        benchmark::DoNotOptimize(x);
    }
}
BENCHMARK(bm_adim_system_solver_solve);


static void bm_adim_system_solver_get_solution(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    AdimensionalSystemSolver solver(raw_data, constraints);
    Vector x;
    solver.solve(x, true);

    while(state.KeepRunning())
    {
        auto sol = solver.get_raw_solution(x);
        benchmark::DoNotOptimize(sol);
    }
}
BENCHMARK(bm_adim_system_solver_get_solution);

static void bm_adim_system_solver_unsafe_get_solution(benchmark::State& state)
{
    RawDatabasePtr raw_data = get_simple_database();
    AdimensionalSystemConstraints constraints = get_simple_constraints(raw_data);
    AdimensionalSystemSolver solver(raw_data, constraints);
    Vector x;
    solver.solve(x, true);

    while(state.KeepRunning())
    {
        auto sol = solver.unsafe_get_raw_solution(x);
        benchmark::DoNotOptimize(sol);
    }
}
BENCHMARK(bm_adim_system_solver_unsafe_get_solution);


BENCHMARK_MAIN()
