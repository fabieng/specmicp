# ==================== #
#                      #
#  Micro-Benchmarking  #
#                      #
# ==================== #

# use google  benchmark framework : https://github.com/google/benchmark

find_package(Benchmark REQUIRED)

include_directories(${BENCHMARK_INCLUDE_DIR})

# Logging
# -------

add_executable(bm_logging bm_logging.cpp)
target_link_libraries(bm_logging specmicp_common_static)
target_link_libraries(bm_logging ${BENCHMARK_LIBRARIES})

# Common
# ------

add_executable(bm_common bm_common.cpp)
target_link_libraries(bm_common specmicp_common_static)
target_link_libraries(bm_common ${BENCHMARK_LIBRARIES})

# Maths
# ------

add_executable(bm_maths bm_maths.cpp)
target_link_libraries(bm_maths specmicp_common_static)
target_link_libraries(bm_maths ${BENCHMARK_LIBRARIES})

# Mesh
# ----

add_executable(bm_mesh bm_mesh.cpp)
target_link_libraries(bm_mesh specmicp_common_static dfpm_static)
target_link_libraries(bm_mesh ${BENCHMARK_LIBRARIES})

# database
# --------

set(TEST_CEMDATA_PATH \"../data/cemdata.yaml\")
set_source_files_properties(bm_database.cpp PROPERTIES COMPILE_DEFINITIONS
"TEST_CEMDATA_PATH=${TEST_CEMDATA_PATH}"
)

add_executable(bm_database bm_database.cpp)
target_link_libraries(bm_database specmicp_database_static specmicp_common_static)
target_link_libraries(bm_database ${BENCHMARK_LIBRARIES})

# adimensional system
# --------------------

set_source_files_properties(bm_adim_system.cpp PROPERTIES COMPILE_DEFINITIONS
"TEST_CEMDATA_PATH=${TEST_CEMDATA_PATH}"
)

add_executable(bm_adim_system bm_adim_system.cpp)
target_link_libraries(bm_adim_system
    specmicp_static specmicp_database_static specmicp_common_static
)
target_link_libraries(bm_adim_system ${BENCHMARK_LIBRARIES}
)
