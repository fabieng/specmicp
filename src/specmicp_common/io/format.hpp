/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_UTILS_IO_FORMAT_HPP
#define SPECMICP_UTILS_IO_FORMAT_HPP

//! \file io/format.hpp
//! \brief Format a YAML file

#include "specmicp_common/types.hpp"
#include <iosfwd>

namespace specmicp {
namespace io {

struct FormatDataContainer;

//! \brief Information on how to format output
//!
//! This is a singleton class,
//!  it should be obtained through the get_format static method
class FormatInformation
{
public:
    //! \brief Return the format information
    static FormatInformation& get_format();

    //! \brief Return the precision for a scalar in a YAML output
    int get_yaml_scalar_precision() const;
    //! \brief Set the precision for a scalar in a YAML output
    void set_yaml_scalar_precision(int n);

    //! \brief Return the number of space to use in the indentation of a YAML output
    int get_yaml_indent() const;
    //! \brief Set the number of space to use as indentation in a YAML output
    void set_yaml_indent(int n);

    //! \brief Return the precision for a stoichiometric coefficient precision
    int get_stoichiometric_coefficient_precision() const;
    //! \brief Set the precision for a stoichiometric coefficient precision
    void set_stoichiometric_coefficient_precision(int n);

    //! \brief Format a stoichiometric coefficient
    void format_stoichiometric_coefficient(std::ostream& out, scalar_t coeff) const;

    //! \brief Format a scalar to the given precision
    std::ostream* format_scalar(std::ostream* out, scalar_t coeff, int precision) const;

    //! \brief Return the CSV separator
    std::string get_csv_separator();
    //! \brief Set the CSV separator
    void set_csv_separator(const std::string& sep);

    //! \brief Add the csv separator to the stream
    template <typename CharT, typename Traits>
    std::basic_ostream<CharT,Traits>& csv_separator(std::basic_ostream<CharT,Traits>& out)
    {
        out << get_csv_separator();
        return out;
    }

    //! \brief Return the CSV comment trigger
    std::string get_csv_comment();
    //! \brief Set the CSV comment trigger
    void set_csv_comment(const std::string& comment_trigger);

private:
    FormatInformation();
    ~FormatInformation();
    FormatInformation(const FormatInformation&) = delete;
    const FormatInformation &operator=(const FormatInformation&) = delete;

    FormatDataContainer* m_data;
};

//! \brief Return the CSV formatting options
inline FormatInformation& get_format()
{
    return FormatInformation::get_format();
}

} //end namespace io
} //end namespace specmicp

#endif // SPECMICP_UTILS_IO_FORMAT_HPP
