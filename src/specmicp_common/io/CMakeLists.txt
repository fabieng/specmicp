set( specmicp_common_io_srcs
    all_io_files.cpp
    configuration.cpp
    csv_formatter.cpp
    format.cpp
    safe_config.cpp
    yaml.cpp
)

set( specmicp_common_io_headers
    all_io_files.hpp
    configuration.hpp
    config_yaml_sections.h
    csv_formatter.hpp
    format.hpp
    safe_config.hpp
    yaml.hpp

)

set(specmicp_common_io_hdf5_srcs
    hdf5_timesteps.cpp
)
set( specmicp_common_io_hdf5_headers
     hdf5_timesteps.hpp
)

if (HDF5_FOUND)
    list(APPEND specmicp_common_io_srcs
        ${specmicp_common_io_hdf5_srcs}
    )
    list(APPEND specmicp_common_io_headers
        ${specmicp_common_io_hdf5_headers}
    )

    foreach(source_file specmicp_common_io_hdf5_srcs)
        set_source_files_properties(${source_file}
             PROPERTIES COMPILE_DEFINITIONS HDF5_DEFINITIONS)
    endforeach(source_file)

endif()

add_to_main_srcs_list(specmicp_common_io_srcs)
add_to_main_headers_list(specmicp_common_io_headers)

INSTALL(FILES ${specmicp_common_io_headers}
    DESTINATION ${INCLUDE_INSTALL_DIR}/specmicp_common/io
 )

add_subdirectory(hdf5)
