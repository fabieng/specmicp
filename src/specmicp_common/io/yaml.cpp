/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "yaml.hpp"
#include <yaml-cpp/yaml.h>
#include <fstream>
#include <iostream>

#include "format.hpp"

namespace specmicp {
namespace io {


void configure_yaml_emitter(YAML::Emitter& emitter)
{
    FormatInformation& format = FormatInformation::get_format();

    emitter.SetDoublePrecision(format.get_yaml_scalar_precision());
    emitter.SetFloatPrecision(format.get_yaml_scalar_precision());
    emitter.SetIndent(format.get_yaml_indent());
}

YAML::Node parse_yaml(std::istream& input)
{
    return YAML::Load(input);
}

YAML::Node parse_yaml_file(const std::string &filepath)
{
   return YAML::LoadFile(filepath);
}

YAML::Node parse_yaml_string(const std::string& config_string)
{
    return YAML::Load(config_string);
}

//! \brief Save a YAML emitter into a file
void save_yaml(const std::string& filename, YAML::Emitter& yaml_tree)
{
    if (not yaml_tree.good())
    {
        throw std::runtime_error("Invalid YAML stream : " + yaml_tree.GetLastError());
    }
    std::ofstream ofile(filename);
    if (not ofile.is_open())
    {
        throw std::runtime_error("Could not open file : '"+filename+"'.");
    }
    ofile << yaml_tree.c_str();
    ofile.close();
}

void check_mandatory_yaml_node(const YAML::Node& node, const std::string& child, const std::string& section)
{
    if (not node[child])
        throw std::invalid_argument("Node '"+child+"' is required in section '"+section+"'.");
}

#ifndef SPC_DOXYGEN_SHOULD_SKIP_THIS
template <>
scalar_t get_yaml_attribute(const YAML::Node& node, const std::string& attribute, const std::string& section)
{
    scalar_t value;
    try
    {
        value = node[attribute].as<scalar_t>();
    }
    catch (YAML::BadConversion)
    {
        if (node[attribute].as<std::string>() == "-inf") {
            value = - INFINITY;
        }
        else {
            throw std::invalid_argument(
                    "Error while converting attribute '"+attribute+"' in section '"+section+"' to scalar");
        }
    }
    return value;
}

template <>
index_t get_yaml_attribute(const YAML::Node& node, const std::string& attribute, const std::string& section)
{
    index_t value;
    try
    {
        value = node[attribute].as<index_t>();
    }
    catch (YAML::BadConversion)
    {
        throw std::invalid_argument(
            "Error while converting attribute '"+attribute+"' in section '"+section+"' to integer");
    }
    return value;
}

template <>
std::string get_yaml_attribute(const YAML::Node& node, const std::string& attribute, const std::string& section)
{
    std::string value;
    try
    {
        value = node[attribute].as<std::string>();
    }
    catch (YAML::BadConversion)
    {
        throw std::invalid_argument("Error while converting attribute '"+attribute+"' in section '"+section+"' to string");
    }
    return value;
}

template <>
bool get_yaml_attribute(const YAML::Node& node, const std::string& attribute, const std::string& section)
{
    if (not node[attribute])
        throw std::invalid_argument("Attribute '"+attribute+"' is required in section '"+section+"'.");
    bool value;
    try
    {
        value = node[attribute].as<bool>();
    }
    catch (YAML::BadConversion)
    {
        try
        {
            value = get_yaml_attribute<int>(node, attribute, section);
        }
        catch (YAML::BadConversion)
        {
            throw std::invalid_argument("Error while converting attribute '"+attribute+"' in section '"+section+"' to boolean");
        }
    }
    return value;
}

#endif // SPC_DOXYGEN_SHOULD_SKIP_THIS

//! \brief Return a child
RORichYAMLNode RORichYAMLNode::operator[] (const std::string& child_name) {
    check_mandatory(child_name);
    return RORichYAMLNode(m_node[child_name], child_name);
}

//! \brief Return a child
const RORichYAMLNode RORichYAMLNode::operator[] (const std::string& child_name) const
{
    check_mandatory(child_name);
    return RORichYAMLNode(m_node[child_name], child_name);
}

} //end namespace io
} //end namespace spe
