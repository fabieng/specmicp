/* =============================================================================

 Copyright (c) 2014 - 2017
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_COMMON_IO_ALLIOFILES_HPP
#define SPECMICP_COMMON_IO_ALLIOFILES_HPP

#include "specmicp_common/pimpl_ptr.hpp"
#include <string>
#include <ctime>
#include <vector>

//! \file all_io_files.hpp
//! \brief Keep track of files opened and created
//!
//! Use

namespace specmicp {
namespace io {


//! \brief Mode to open the IOFiles
//!
//! It's a safety measure to avoid overwriting previous simulation
enum class AllIOFilesMode
{
    //Append,
    //ErasePreviousOutputs,
    Read,
    ErrorIfExist,
    Write
};

//! \brief Type of file: input or outputs
enum class IOFileType
{
    Input,       //!< Read only
    Output,      //!< Write
    Input_Output //!< Read and write
};

//! \brief Struct containing basic information about the files.
//!
//! \sa input_file
//! \sa output_file
struct IOFileInfo {

    IOFileInfo():
        name(), filepath()
    {}

    //! \brief Constructor, no comment
    IOFileInfo(
            std::string&& tname,
            std::string&& tfilepath,
            IOFileType ttype):
        name(tname), filepath(tfilepath), type(ttype)
    {}
    //! \brief Constructor, with comment
    IOFileInfo(
            std::string&& tname,
            std::string&& tfilepath,
            std::string&& tcomment,
            IOFileType ttype):
        name(tname), filepath(tfilepath), comment(tcomment), type(ttype)
    {}
    //! \brief Constructor, no comment
    IOFileInfo(
            const std::string& tname,
            const std::string& tfilepath,
            IOFileType ttype):
        name(tname), filepath(tfilepath), type(ttype)
    {}
    //! \brief Constructor, with comment
    IOFileInfo(
            const std::string& tname,
            const std::string& tfilepath,
            const std::string& tcomment,
            IOFileType ttype):
        name(tname), filepath(tfilepath), comment(tcomment), type(ttype)
    {}

    std::string name;       //!< Name of the file
    std::string filepath;   //!< Path to the file
    std::string comment {}; //!< Additional description
    IOFileType type;        //!< Input or output file
};

//! \brief Create description for input file
IOFileInfo inline input_file(
        std::string&& name,
        std::string&& filepath,
        std::string comment="") {
    return IOFileInfo(name, filepath, comment, IOFileType::Input);
}

//! \brief Create description for input file
IOFileInfo inline input_file(
        const std::string& name,
        const std::string& filepath,
        const std::string comment="") {
    return IOFileInfo(name, filepath, comment, IOFileType::Input);
}

//! \brief Create description for output file
IOFileInfo inline output_file(
        std::string&& name,
        std::string&& filepath,
        std::string comment="") {
    return IOFileInfo(name, filepath, comment, IOFileType::Output);
}

//! \brief Create description for output file
IOFileInfo inline output_file(
        const std::string& name,
        const std::string& filepath,
        const std::string comment="") {
    return IOFileInfo(name, filepath, comment, IOFileType::Output);
}


//! \brief Save Input/output files description
//!
//! Read/Write to a yaml file
//!
//! \sa IOFileInfo
class AllIOFiles
{
public:
    //! \brief Constructor
    AllIOFiles(std::string filepath, AllIOFilesMode mode);
    ~AllIOFiles();

    //! \brief Remove the output files
    void clean_output_files();

    //! \brief Return the creation time
    std::time_t get_creation_time();
    //! \brief Return the last modification time
    std::time_t get_last_modification_time();

    //! \brief Save the command line
    void save_command_line(
        int argc,
        const char* const argv[]
        );
    //! \brief Return the command line
    std::string get_command_line();
    //! \brief Add a configuration file description
    void add_configuration_file(IOFileInfo&& file_info);
    //! \brief Return a configuration file description
    bool get_configuration_file(
        const std::string& name,
        IOFileInfo& out
        );
    //! \brief Return the names of the configuration files
    std::vector<std::string> get_configuration_file_names();
    //! \brief Add a database file description
    void add_database(IOFileInfo&& file_info);
    //! \brief Return a database file description
    bool get_database(
        const std::string& name,
        IOFileInfo& out
        );
    //! \brief Return the names of the databases
    std::vector<std::string> get_database_names();
    //! \brief Add a solution file descriptino
    void add_solution(IOFileInfo&& file_info);
    //! \brief Return a solution file description
    bool get_solution(
        const std::string& name,
        IOFileInfo& out
        );
    //! \brief Return the names of the solution files
    std::vector<std::string> get_solution_names();
    //! \brief Add a log file description
    void add_log_file(IOFileInfo&& file_info);
    //! \brief Return a log file description
    bool get_log_file(
        const std::string& name,
        IOFileInfo& out
        );
    //! \brief Return the names of the log files
    std::vector<std::string> get_log_file_names();
    //! \brief write to disk
    //!
    //! This function is not called automatically.
    //! If the yaml file need to be created / updated, this function must be called.
    void sync();

    //! \brief Return the working directory
    std::string get_working_dir();

private:
    struct AllIOFilesImpl;
    utils::pimpl_ptr<AllIOFilesImpl> m_impl;
};

} // end namespace io
} // end namespace specmicp

#endif // SPECMICP_COMMON_IO_ALLIOFILES_HPP
