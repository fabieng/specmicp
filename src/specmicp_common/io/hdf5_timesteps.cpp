/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "hdf5_timesteps.hpp"

#include "specmicp_common/io/hdf5/path.hpp"

namespace specmicp {
namespace io {

void HDF5Timesteps::initialize(hdf5::GroupPath& the_file)
{
    reserve(the_file.get_number_links());
    the_file.iterate_over_elements(&HDF5Timesteps::add_element_if_timestep, this);
    sort();
}



herr_t HDF5Timesteps::add_element_if_timestep(
        hid_t _,
        const char* name,
        const H5L_info_t* info,
        void* void_this
        )
{
    HDF5Timesteps* true_this = static_cast<HDF5Timesteps*>(void_this);

    ScalarT value;
    try {
        value = string_to_number(name);
    }
    catch (const std::invalid_argument& _){
        return 0;
    }

    true_this->push_back(value, std::string(name));
    return 0;

}

std::string HDF5Timesteps::get_string(const ScalarT& value) const {
    if (value < 0) {
        return back().second;
    }
    const_iterator it = lower_bound(value);
    if (it == m_values.end())
    {
        --it; // if we are at the end, check if the previous value is close enough
        if(std::abs(it->first - value) > value*1e-8)
        {
            throw  std::range_error("Error : non existing value : "
                                + std::to_string(value) + ".");
        }
    }
    return it->second;
}


//! \brief Return the first timestep
HDF5Timesteps::ValT HDF5Timesteps::front() const {
    return m_values.back();
}
//! \brief Return the last timestep
HDF5Timesteps::ValT HDF5Timesteps::back() const {
    return m_values.back();
}

void HDF5Timesteps::sort() {
    return std::sort(m_values.begin(), m_values.end(), &compare_keys);
}

HDF5Timesteps::const_iterator HDF5Timesteps::lower_bound(const ScalarT& value) const {
    return std::lower_bound(m_values.cbegin(), m_values.cend(),
                            ValT(value, ""),
                            &compare_keys
                            );
}


} //end namespace io
} //end namespace specmicp
