/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_IO_CSV_FORMATTER
#define SPECMICP_IO_CSV_FORMATTER

//! \file io/csv_formatter.hpp
//! \brief Format a CSV file

#include "specmicp_common/types.hpp"

#include <string>
#include <memory>
#include <fstream>
#include <stdexcept>

#include "format.hpp"

namespace specmicp {
namespace io {

//! \brief A output file
//!
//! This is a wrapper around a std::ofstream
//!
//! The wrapper exists to make the formatting easier
class SPECMICP_DLL_PUBLIC OutFile
{
public:
    OutFile() {}

    //! \brief Open a file to write
    OutFile(const std::string& filepath);

    //! \brief Close the file
    ~OutFile() {
        close();
    }

    // This is needed for GCC
    //! \brief Move constructor
    OutFile(OutFile&& other) {
        m_stream.swap(other.m_stream);
    }

    //! \brief open a file
    void open(const std::string& filepath);

    //! \brief Stream operator
    std::ofstream& operator<< (const std::string& value) {
        _get() << value;
        return _get();
    }

    //! \brief Stream operator
    std::ofstream& operator<< (scalar_t value) {
        _get() << value;
        return _get();
    }

    //! \brief Stream operator
    std::ofstream& operator<< (index_t value) {
        _get() << value;
        return _get();
    }

    //! \brief Close a line and flush the stream
    void endl() {
        _get() << std::endl;
    }

    //! \brief Flush the stream
    void flush() {
        m_stream->flush();
    }

    //! \brief close the stream
    void close();


    //! \brief Return true if a file is open
    bool is_open()
    {
        if (m_stream == nullptr) return false;
        return m_stream->is_open();
    }

protected:
    //! \brief Return a pointer to the output stream
    std::ofstream& _get() {return (*m_stream);}
    std::unique_ptr<std::ofstream> m_stream {nullptr}; //!< The output stream
};

//! \brief A CSV file
class SPECMICP_DLL_PUBLIC CSVFile: public OutFile
{
public:
    //! \brief Default constructor
    CSVFile() {}
    //! \brief Open 'filepath' in write mode
    CSVFile(const std::string& filepath);

    //! \brief insert a comment line
    void insert_comment_line(const std::string& msg);
    //! \brief insert a comment line with the date
    void insert_comment_date();

    //! \brief insert a comment line with unit information
    void insert_comment_unit(const std::string& variable_name, const std::string& unit);

    //! \brief Write an end of line
    void eol() {
        _get() << "\n";
    }

    //! \brief Write the space sequence
    void space() {
       _get() << " ";
    }

    //! \brief Write the separator sequence
    void separator() {
        _get() << get_format().get_csv_separator();
    }

    //! \brief Write the comment sequence
    void comment() {
        _get() << get_format().get_csv_comment();
    }
};

} //end namespace io
} //end namespace specmicp

#endif // SPECMICP_IO_CSV_FORMATTER
