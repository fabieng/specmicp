/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "dataspace.hpp"

#include "H5Spublic.h"
#include <stdexcept>

namespace specmicp {
namespace io {
namespace hdf5 {

Dataspace::Dataspace(hid_t id):
    IdClass(id)
{}

Dataspace Dataspace::acquire(hid_t id)
{
    if (id < 0)
    {
        throw std::invalid_argument("Invalid dataspace");
    }
    return Dataspace(id);
}

Dataspace Dataspace::create_simple(int rank, hsize_t dims[])
{
    hid_t id = H5Screate_simple(rank, dims, NULL);
    if (id < 0)
    {
        throw std::runtime_error("Error while creating dataspace.");
    }
    return Dataspace(id);
}

Dataspace::~Dataspace()
{
    H5Sclose(get_id());
}


int Dataspace::get_rank() const
{
    return H5Sget_simple_extent_ndims(get_id());
}

int Dataspace::get_dimensions(hsize_t dims[]) const
{
    return H5Sget_simple_extent_dims(get_id(), dims, NULL);
}



} // end namespace hdf5
} // end namespace io
} // end namespace specmicp
