/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "path.hpp"
#include "group.hpp"
#include "dataset.hpp"
#include "dataspace.hpp"
#include "type.hpp"

#include "H5Gpublic.h"
#include "H5Dpublic.h"
#include "H5Ppublic.h"
#include "H5Spublic.h"
#include "H5Tpublic.h"
#include "H5Lpublic.h"
#include "H5Apublic.h"

#include "specmicp_common/scope_guard.hpp"


namespace specmicp {
namespace io {
namespace hdf5 {


std::string Path::add_to_path(const std::string& to_add) const {
    std::string complete;
    complete.reserve(m_path.size() + 1 + to_add.size());
    complete = m_path;
    if (m_path[m_path.size()-1] != '/' and *to_add.begin() != '/') {
        complete += "/";
    }
    complete += to_add;
    return complete;
}

Group GroupPath::create_group(const std::string& name) const
{
    hid_t id = H5Gcreate2(get_id(), name.c_str(),
                          H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT
                          );
    std::string complete_path = add_to_path(name);
    if (id < 0)
    {
        throw std::runtime_error("Error while creating the group"
                                 + complete_path);
    }
    return Group(id, complete_path);
}

Group GroupPath::open_group(const std::string& name) const
{
    hid_t id = H5Gopen2(get_id(), name.c_str(), H5P_DEFAULT);
    std::string complete_path = add_to_path(name);
    if (id < 0)
    {
        throw std::runtime_error("Error while opening the group"
                                 + complete_path);
    }
    return Group(id, complete_path);
}

Dataset GroupPath::open_dataset(const std::string& name) const
{
    hid_t id = H5Dopen2(get_id(), name.c_str(), H5P_DEFAULT);
    std::string complete_path = add_to_path(name);
    if (id < 0)
    {
        throw std::runtime_error("Error while opening the dataset"
                                 + complete_path);
    }
    return Dataset(id, complete_path);
}

Dataset GroupPath::create_vector_dataset(
        const std::string& name,
        const Eigen::Ref<const Vector>& data
        )
{

    auto rows = static_cast<hsize_t>(data.rows());
    hsize_t dims[] = {rows};
    hid_t dataspace_id = H5Screate_simple(1, dims, NULL);


    hid_t dataset_id = H5Dcreate2(get_id(), name.c_str(), H5T_NATIVE_DOUBLE,
                                  dataspace_id,
                                  H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    std::string complete_path = add_to_path(name);
    if (dataset_id < 0) {
        throw std::runtime_error("Error while creating datased : "
                                 + complete_path + ".");
    }
    herr_t status = H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE,
                             H5S_ALL, H5S_ALL, H5P_DEFAULT,
                             data.data()
                             );
    if (status < 0) {
        throw std::runtime_error("Error while writing data to dataset : "
                                 + complete_path + ".");
    }
    H5Sclose(dataspace_id);
    return Dataset(dataset_id, complete_path);
}

Vector GroupPath::read_vector_dataset(const std::string& path) const
{
    Vector x;
    Dataset data = open_dataset(path);
    Dataspace space = data.get_dataspace();

    int rank = space.get_rank();
    if (rank != 1)
    {
        throw std::runtime_error("Expected rank 1 dataset : " +
                                 data.get_path() + ". Obtained rank "
                                 + std::to_string(rank) + "instead.");
    }
    hsize_t dims[1];
    space.get_dimensions(dims);
    x.resize(dims[0]);

    herr_t status = H5Dread(data.get_id(), H5T_NATIVE_DOUBLE,
                            H5S_ALL, H5S_ALL, H5P_DEFAULT,
                            x.data());
    if (status < 0)
    {
        throw std::runtime_error("Error while reading dataset "
                                 + data.get_path() + ".");
    }

    return x;
}

bool GroupPath::has_link(const std::string& name) const
{
    htri_t result = H5Lexists(get_id(), name.c_str(), H5P_DEFAULT);
    return (result > 0);
}

//! \brief iterate over the links of the group
herr_t GroupPath::iterate_over_elements(
        iterate_function_t func,
        void* extra_data
        )
{
    hsize_t idx=0;
    // note : H5_INDEX_CRT_ORDER is working only if property has been set first
    // ref : http://hdf-forum.184993.n3.nabble.com/H5Literate-and-H5-INDEX-CRT-ORDER-td3449189.html
    // not a big deal, we use name
    return H5Literate(get_id(),
               H5_INDEX_NAME, H5_ITER_NATIVE, &idx,
               func, extra_data
               );
}

hsize_t GroupPath::get_number_links()
{
    H5G_info_t ginfo;
    H5Gget_info(get_id(), &ginfo);
    return ginfo.nlinks;
}

Dataset GroupPath::create_string_dataset(
        const std::string& name,
        const std::vector<std::string>& data
        )
{
    std::size_t size = data.size();
    std::string error_msg = "";
    herr_t status;
    // /!\ we need to create a vector of char* to pass to hdf5
    // danger => explicit use of new[] and delete[]
    std::vector<char *> raw_data(size);
    for (std::size_t ind=0; ind<size; ++ind)
    {
        raw_data[ind] = new char[data[ind].size()+1];
        std::strcpy(raw_data[ind], data[ind].c_str());
    }
    // cleaner
    auto delete_buffer = utils::make_scope_guard([&raw_data,size]{
        for (std::size_t id=0; id<size; ++id) { delete[] raw_data[id]; }
    });

    // create the dataset
    hdf5::Type str_t = hdf5::Type::get_c_str_type();
    hsize_t dims[]   = {static_cast<hsize_t>(size)};
    auto dspace      = hdf5::Dataspace::create_simple(1, dims);
    hid_t dset_id    = H5Dcreate2(
                            get_id(), name.c_str(), str_t.get_id(),
                            dspace.get_id(),
                            H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT
                            );
    if (dset_id < 0)
    {
        throw std::runtime_error("Failed to create string dataset");
    }
    // write to the dataset
    status    = H5Dwrite(dset_id, str_t.get_id(),
                         dspace.get_id(), dspace.get_id(), H5P_DEFAULT,
                         raw_data.data()
                         );
    if (status < 0)
    {
        throw std::runtime_error("Failed to write string dataset");
    }

    auto dset = hdf5::Dataset::acquire(dset_id, add_to_path(name));
    return dset;
}

std::vector<std::string> GroupPath::read_string_dataset(const std::string& name)
{
    auto dset = open_dataset(name);
    auto dspace = dset.get_dataspace();

    // Get size of the dataset
    const auto rank = dspace.get_rank();
    if (rank != 1) {
        throw std::runtime_error("Expected rank 1 dataset : " +
                                 dset.get_path() + ". Obtained rank "
                                 + std::to_string(rank) + "instead.");
    }

    hsize_t dims[1];
    dspace.get_dimensions(dims);

    // Allocate memory for the buffer
    char** raw_buffer = new char*[dims[0]];
    auto delete_buffer = utils::make_scope_guard(
        [&raw_buffer]{ delete[] raw_buffer; }
    );

    // Read the data
    hdf5::Type str_t = hdf5::Type::get_c_str_type();
    auto status = H5Dread(dset.get_id(), str_t.get_id(),
                          H5S_ALL, H5S_ALL, H5P_DEFAULT,
                          raw_buffer);
    if (status < 0)
    {
        throw std::runtime_error("Failed to write string dataset : "
                                 + dset.get_path() + ".");
    }
    auto delete_sub_buffer = utils::make_scope_guard(
        [&str_t, &dspace, &raw_buffer]{
            H5Dvlen_reclaim(str_t.get_id(), dspace.get_id(),
                            H5P_DEFAULT, raw_buffer);
        }
    );

    // Copy buffer to final destination
    std::vector<std::string> to_return {dims[0]};
    for (std::size_t ind=0; ind<dims[0]; ++ind) {
        to_return[ind] = raw_buffer[ind];
    }

    return to_return;
}

Attribute Path::create_scalar_attribute(
        const std::string& name,
        const Dataspace& d_space
        )
{
    hid_t id = H5Acreate2(get_id(), name.c_str(), H5T_NATIVE_DOUBLE,
                        d_space.get_id(), H5P_DEFAULT, H5P_DEFAULT);
    return Attribute::acquire(id);
}

Attribute Path::open_attribute(const std::string& name) const
{
    hid_t id = H5Aopen(get_id(), name.c_str(), H5P_DEFAULT);
    return Attribute::acquire(id);
}

Attribute Path::create_scalar_attribute(
    const std::string& name,
    std::size_t N,
    const double values[]
    )
{
    hsize_t dims[] = {N,};
    auto d_space = Dataspace::create_simple(1, dims);
    auto attr = create_scalar_attribute(name, d_space);
    H5Awrite(attr.get_id(), H5T_NATIVE_DOUBLE, values);
    return attr;
}


void Path::read_scalar_attribute(
    const std::string& name,
    std::size_t size,
    double values[]
) const
{
    Attribute attr = open_attribute(name);
    Dataspace d_space = attr.get_dataspace();
    if (d_space.get_rank() != 1) {
        throw std::runtime_error("Expected attribute with rank 1");
    }
    hsize_t dims[1];
    d_space.get_dimensions(dims);
    if (dims[0] != size)
    {
        throw std::runtime_error("Unexpected dimensions for the attribute."
                                 "Expected " + std::to_string(size) + "x1;"
                                   " got : " + std::to_string(dims[0]) + "x1."
                );
    }
    H5Aread(attr.get_id(), H5T_NATIVE_DOUBLE, values);
}


std::vector<double> Path::read_scalar_attribute(const std::string& name)
{
    Attribute attr = open_attribute(name);
    Dataspace d_space = attr.get_dataspace();
    if (d_space.get_rank() != 1) {
        throw std::runtime_error("Expected attribute with rank 1");
    }
    hsize_t dims[1];
    d_space.get_dimensions(dims);

    std::vector<double> result(dims[0]);
    H5Aread(attr.get_id(), H5T_NATIVE_DOUBLE, result.data());
    return result;
}
} // end namespace hdf5
} // end namespace io
} // end namespace specmicp
