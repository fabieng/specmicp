/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_IO_HDF5_TYPE_HPP
#define SPECMICP_IO_HDF5_TYPE_HPP

//! \file hdf5/type.hpp
//! \brief wrapper for the HDF5 type API

#include "id_class.hpp"

namespace specmicp {
namespace io {
namespace hdf5 {

//! \brief Wrapper for an HDF5 type
class Type:
        public IdClass
{
public:
    //! \brief Move constructor
    Type(Type&& other) = default;
    ~Type();

    //! \brief Copy an existing type
    static Type copy(hid_t id);
    //! \brief Copy an existing path
    static Type copy(const Type& other);
    //! \brief Acquire ownership of a type
    static Type acquire(hid_t id);

    //! \brief Return the type of a C style variable-length string
    static Type get_c_str_type();


protected:
    //! \brief Build the wrapper around an HDF5 type
    Type(hid_t);
private:
    Type(const Type& other) = delete;
    Type& operator=(const Type& other) = delete;

};

} // end namespace hdf5
} // end namespace io
} // end namespace specmicp


#endif // SPECMICP_IO_HDF5_TYPE_HPP
