/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_IO_HDF5_ATTRIBUTE_HPP
#define SPECMICP_IO_HDF5_ATTRIBUTE_HPP

//! \file hdf5/attribute.hpp
//! \brief H5A wrapper

#include "id_class.hpp"

namespace specmicp {
namespace io {
namespace hdf5 {

class Dataspace;

//! \brief Wrapper for the HDF5 Attribute
class Attribute:
        public IdClass
{
public:
    //! \brief Move constructor
    Attribute(Attribute&& other) = default;

    //! \brief Destructor
    ~Attribute();

    //! \brief acquire ownership
    static Attribute acquire(hid_t id);

    //! \brief Return the dataspace
    Dataspace get_dataspace() const;

protected:
    //! \brief Create an attribute wrapper
    Attribute(hid_t id);

private:
    Attribute(const Attribute& other) = delete;
    Attribute& operator=(const Attribute& other) = delete;


};

} // end namespace hdf5
} // end namespace io
} // end namespace specmicp

#endif // SPECMICP_IO_HDF5_ATTRIBUTE_HPP
