/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_UTILS_IO_CONFIGYAMLSECTIONS_H
#define SPECMICP_UTILS_IO_CONFIGYAMLSECTIONS_H

//! \file io/config_yaml_sections.h
//! \brief Define sections in the YAML config files
//!
//! Name are built as follow
//! PREFIX_[X_NAME]n
//!
//! where PREFIX is SPC_CF (specmicp conf)
//! X is either A (for attribute), v (for value), S (for section) or SS (for subsection)
//! and NAME is the name used in the conf file

//! \def SPC_CF_NAME_PATH_ROOT
//! \brief Name of the first level
#define SPC_CF_NAME_PATH_ROOT "root"

// common options
// --------------

//! \def SPC_CF_A_MAX_ITER
//! \brief Maximum iterations
#define SPC_CF_A_MAX_ITER          "maximum_iterations"
//! \def SPC_CF_A_RES_TOL
//! \brief The residuals tolerance
//!
//! Absolute in SpecMiCP
//! Relative for DFPM
#define SPC_CF_A_RES_TOL           "residual_tolerance"
//! \def SPC_CF_A_ABS_TOL
//! \brief Absolute residuals tolerance
#define SPC_CF_A_ABS_TOL           "absolute_tolerance"
//! \def SPC_CF_A_STEP_TOL
//! \brief Absolute tolerance for the update
#define SPC_CF_A_STEP_TOL          "step_tolerance"
//! \def SPC_CF_A_MAX_STEP_LENGTH
//! \brief Maximum value for the update
#define SPC_CF_A_MAX_STEP_LENGTH   "maximum_step_length"
//! \def SPC_CF_A_MAX_STEP_MAX_ITER
//! \brief Maximum number at iterations when update value is maximum
#define SPC_CF_A_MAX_STEP_MAX_ITER "maximum_step_maximum_iterations"

//! \def SPC_CF_A_FILEPATH
//! \brief Path to a file
#define SPC_CF_A_FILEPATH "file"

//! \def SPC_CF_A_COMPONENT
//! \brief A component
#define SPC_CF_A_COMPONENT "component"
//! \def SPC_CF_A_NODES
//! \brief one or several nodes
//!
//! example: "1,2,5-8" for nodes 1,2,5,6,7 and 8
#define SPC_CF_A_NODES     "nodes"
//! \def SPC_CF_A_TYPE
//! \brief Used when a thing can have several values
#define SPC_CF_A_TYPE      "type"

//! \def SPC_CF_V_DEFAULT
//! \brief To indicate that a thing should be the default type
#define SPC_CF_V_DEFAULT   "default"
//! \def SPC_CF_V_PLUGIN
//! \brief To indicate that a thing should use a user provided plugin
#define SPC_CF_V_PLUGIN    "plugin"

//! \def SPC_CF_A_PLUGIN_FILE
//! \brief Attribute used to provide the path to a plugin file
#define SPC_CF_A_PLUGIN_FILE "plugin_file" // filepath to a plugin
//! \def SPC_CF_A_PLUGIN_NAME
//! \brief Attribute used to provide the name of a plugin
#define SPC_CF_A_PLUGIN_NAME "plugin_name" // name of the plugin to use

// user variables
// --------------

//! \def SPC_CF_S_USRVARS
//! \brief Section containing user defined variables
//!
//! These variables are used to compute algebraic expressions at runtime
#define SPC_CF_S_USRVARS          "vars"
//! \def SPC_CF_S_USRVARS_SS_INT
//! \brief Subsection containing user defined integral variables
//!
//! Format is name: value
//!
//! Value can be an algebraic expression of previously defined integer variables
#define SPC_CF_S_USRVARS_SS_INT   "int"
//! \def SPC_CF_S_USRVARS_SS_FLOAT
//! \brief Subsection containing user defined floating-point variables
//!
//! Format is name: value
//!
//! Value can be an algebraic expression of previously defined variables
//! (integer and floating points)
#define SPC_CF_S_USRVARS_SS_FLOAT "float"

// plugin manager
// --------------

//! \def SPC_CF_S_PLUGINS
//! \brief Section describing plugin options
#define SPC_CF_S_PLUGINS              "plugins"
//! \def SPC_CF_S_PLUGINS_A_SEARCHDIRS
//! \brief Directories where plugins can be found
#define SPC_CF_S_PLUGINS_A_SEARCHDIRS "dirs"
//! \def SPC_CF_S_PLUGINS_A_TOLOAD
//! \brief Plugins to load
#define SPC_CF_S_PLUGINS_A_TOLOAD     "to_load"

// Logs
// ----

// type of output

//! \def SPC_CF_S_LOGS_V_COUT
//! \brief Logs should be written to standard output
#define SPC_CF_S_LOGS_V_COUT        "cout"
//! \def SPC_CF_S_LOGS_V_CERR
//! \brief Logs should be written to standard error output
#define SPC_CF_S_LOGS_V_CERR        "cerr"
//! \def SPC_CF_S_LOGS_V_FILE
//! \brief Logs should be written to a file
#define SPC_CF_S_LOGS_V_FILE        "file"

// the different levels
//! \def SPC_CF_S_LOGS_V_CRITICAL
//! \brief Log message should be at least of level "critical" to be written
#define SPC_CF_S_LOGS_V_CRITICAL    "critical"
//! \def SPC_CF_S_LOGS_V_ERROR
//! \brief Log message should be at least of level "error" to be written
#define SPC_CF_S_LOGS_V_ERROR       "error"
//! \def SPC_CF_S_LOGS_V_WARNING
//! \brief Log message should be at least of level "warning" to be written
#define SPC_CF_S_LOGS_V_WARNING    "warning"
//! \def SPC_CF_S_LOGS_V_INFO
//! \brief Log message should be at least of level "info" to be written
#define SPC_CF_S_LOGS_V_INFO        "info"
//! \def SPC_CF_S_LOGS_V_DEBUG
//! \brief Log message should be at least of level "debug" to be written
#define SPC_CF_S_LOGS_V_DEBUG       "debug"

// runtime logs
//! \def SPC_CF_S_LOGS
//! \brief Runtime logs
#define SPC_CF_S_LOGS               "logs"
//! \def SPC_CF_S_LOGS_A_LEVEL
//! \brief Output level for the runtime logs
#define SPC_CF_S_LOGS_A_LEVEL       "level"
//! \def SPC_CF_S_LOGS_A_OUTPUT
//! \brief The ouput of the runtime logs
//!
//! Values are given by SPC_CF_S_LOGS_V_X where x is
//! either COUT, CERR, or FILE
#define SPC_CF_S_LOGS_A_OUTPUT      "output"
//! \def SPC_CF_S_LOGS_A_FILEPATH
//! \brief the path to a file where logs should be written
//!
//! Only needed if logs output is a file
#define SPC_CF_S_LOGS_A_FILEPATH    SPC_CF_A_FILEPATH

// configuration logs
//! \def SPC_CF_S_CONF_LOGS
//! \brief Logs written when configuration is parsed
#define SPC_CF_S_CONF_LOGS          "configuration_logs"
//! \def SPC_CF_S_CONF_LOGS_A_OUTPUT
//! \brief The ouput of the configuration logs
//!
//! Values are given by SPC_CF_S_LOGS_V_X where x is
//! either COUT, CERR, or FILE
#define SPC_CF_S_CONF_LOGS_A_OUTPUT "output"
//! \def SPC_CF_S_CONF_LOGS_A_FILE
//! \brief the path to a file where logs configuration should be written
//!
//! Only needed if logs output is a file
#define SPC_CF_S_CONF_LOGS_A_FILE   SPC_CF_A_FILEPATH


// units
// -----

//! \def SPC_CF_S_UNITS
//! \brief Configuration section for the units
#define SPC_CF_S_UNITS              "units"
//! \def SPC_CF_S_UNITS_A_LENGTH
//! \brief Configure the length unit
#define SPC_CF_S_UNITS_A_LENGTH     "length"
//! \def SPC_CF_S_UNITS_A_MASS
//! \brief Configure the mass unit
#define SPC_CF_S_UNITS_A_MASS       "mass"
//! \def SPC_CF_S_UNITS_A_QUANTITY
//! \brief Configure the amount of substance unit
#define SPC_CF_S_UNITS_A_QUANTITY   "quantity"

// mesh
// ----
//! \def SPC_CF_S_MESH
//! \brief Section to configure the mesh
#define SPC_CF_S_MESH                              "mesh"
//! \def SPC_CF_S_MESH_A_TYPE
//! \brief Type of the mesh
#define SPC_CF_S_MESH_A_TYPE                       "type"

//! \def SPC_CF_S_MESH_SS_UNIFMESH
//! \brief Subsection to configure a uniform mesh
#define SPC_CF_S_MESH_SS_UNIFMESH                  "uniform_mesh"
//! \def SPC_CF_S_MESH_SS_UNIFMESH_A_DX
//! \brief The length of an element
#define SPC_CF_S_MESH_SS_UNIFMESH_A_DX             "dx"
//! \def SPC_CF_S_MESH_SS_UNIFMESH_A_NBNODES
//! \brief The number of nodes
#define SPC_CF_S_MESH_SS_UNIFMESH_A_NBNODES        "nb_nodes"
//! \def SPC_CF_S_MESH_SS_UNIFMESH_A_SECTION
//! \brief Cross section of the sample
#define SPC_CF_S_MESH_SS_UNIFMESH_A_SECTION        "section"

//! \def SPC_CF_S_MESH_SS_RAMPMESH
//! \brief Subsection to configure a ramp mesh
//!
//! Ramp mesh start by an increasing element size and finish with a plateau
//! at a max element size
#define SPC_CF_S_MESH_SS_RAMPMESH                  "ramp_mesh"
//! \def SPC_CF_S_MESH_SS_RAMPMESH_A_MIN_DX
//! \brief The minimum size of an element
#define SPC_CF_S_MESH_SS_RAMPMESH_A_MIN_DX         "min_dx"
//! \def SPC_CF_S_MESH_SS_RAMPMESH_A_MAX_DX
//! \brief The maximum size of an element
#define SPC_CF_S_MESH_SS_RAMPMESH_A_MAX_DX         "max_dx"
//! \def SPC_CF_S_MESH_SS_RAMPMESH_A_RAMP_LENGTH
//! \brief Total length of the ramp
#define SPC_CF_S_MESH_SS_RAMPMESH_A_RAMP_LENGTH    "length_ramp"
//! \def SPC_CF_S_MESH_SS_RAMPMESH_A_PLATEAU_LENGTH
//! \brief Total lenght of the plateau
#define SPC_CF_S_MESH_SS_RAMPMESH_A_PLATEAU_LENGTH "length_plateau"
//! \def SPC_CF_S_MESH_SS_RAMPMESH_A_SECTION
//! \brief Cross section of the sample
#define SPC_CF_S_MESH_SS_RAMPMESH_A_SECTION        "section"

//! \def SPC_CF_S_MESH_SS_UNIFAXISYMESH
//! \brief An axysimetric mesh, uniform (in radius, no volume)
//!
//! Either the number of nodes or the step can be provided.
#define SPC_CF_S_MESH_SS_UNIFAXISYMESH             "uniform_axisymmetric"
//! \def SPC_CF_S_MESH_SS_UNIFAXISYMESH_A_RADIUS
//! \brief Total radius of the sample
#define SPC_CF_S_MESH_SS_UNIFAXISYMESH_A_RADIUS    "radius"
//! \def SPC_CF_S_MESH_SS_UNIFAXISYMESH_A_NBNODES
//! \brief Number of nodes
#define SPC_CF_S_MESH_SS_UNIFAXISYMESH_A_NBNODES   "nb_nodes"
//! \def SPC_CF_S_MESH_SS_UNIFAXISYMESH_A_HEIGHT
//! \brief Height of the sample
#define SPC_CF_S_MESH_SS_UNIFAXISYMESH_A_HEIGHT    "height"
//! \def SPC_CF_S_MESH_SS_UNIFAXISYMESH_A_DX
//! \brief Lenght between two nodes
#define SPC_CF_S_MESH_SS_UNIFAXISYMESH_A_DX        "dx"

// database
// --------
//! \def SPC_CF_S_DATABASE
//! \brief Section to configure the database
#define SPC_CF_S_DATABASE                      "database"

//! \def SPC_CF_S_DATABASE_A_PATH
//! \brief Path to teh database file
//!
//! Can be name of a file if search directories has been correctly set.
//! In particular, the database should search in directories provided by
//! the environment variable SPECMICP_DATABASE_PATH
#define SPC_CF_S_DATABASE_A_PATH               "path"

//! \def SPC_CF_S_DATABASE_A_CHECKCOMPO
//! \brief If true, database will run a consistency check
//! (formula, electroneutrality...)
//!
//! It is overwritten by a flag in database file if provided.
#define SPC_CF_S_DATABASE_A_CHECKCOMPO         "check_compo"

//! \def SPC_CF_S_DATABASE_A_SWAP
//! \brief Map containing species to be swapped
#define SPC_CF_S_DATABASE_A_SWAP               "swap_components"
//! \def SPC_CF_S_DATABASE_A_SWAP_K_IN
//! \brief Aqueous species to add as a component
#define SPC_CF_S_DATABASE_A_SWAP_K_IN          "in"
//! \def SPC_CF_S_DATABASE_A_SWAP_K_OUT
//! \brief Current compoment to remove from basis
#define SPC_CF_S_DATABASE_A_SWAP_K_OUT         "out"

//! \def SPC_CF_S_DATABASE_A_REMOVE_GAS
//! \brief A list of gas to remove from the database
#define SPC_CF_S_DATABASE_A_REMOVE_GAS         "remove_gas"
//! \def SPC_CF_S_DATABASE_A_REMOVE_SOLIDS
//! \brief A list of solids to remove from the database
#define SPC_CF_S_DATABASE_A_REMOVE_SOLIDS      "remove_solids"
//! \def SPC_CF_S_DATABASE_A_REMOVE_SORBED
//! \brief A list of sorbed species to remove from the database
#define SPC_CF_S_DATABASE_A_REMOVE_SORBED      "remove_sorbeds"

//! \def SPC_CF_S_DATABASE_A_EXTRA_GAS
//! \brief a YAML-format string to add more gas to the database
#define SPC_CF_S_DATABASE_A_EXTRA_GAS          "extra_gas"
//! \def SPC_CF_S_DATABASE_A_EXTRA_SOLIDS
//! \brief a YAML-format string to add more solid to the database
#define SPC_CF_S_DATABASE_A_EXTRA_SOLIDS       "extra_solids"
//! \def SPC_CF_S_DATABASE_A_EXTRA_SORBED
//! \brief a YAML-format string to add more sorbed to the database
#define SPC_CF_S_DATABASE_A_EXTRA_SORBED       "extra_sorbeds"


//! \def SPC_CF_S_DATABASE_A_REMOVE_HALF_CELLS
//! \brief If true remove all half cells reaction from the database
//!
//! half-cells reactions are reaction involving the electron component
#define SPC_CF_S_DATABASE_A_REMOVE_HALF_CELLS  "remove_half_cells"

//! \def SPC_CF_S_DATABASE_A_LIST_SOLIDS_TOKEEP
//! \brief a list of solid phases to keep in the database
//!
//! All solid phases not listed will be removed from the database
#define SPC_CF_S_DATABASE_A_LIST_SOLIDS_TOKEEP "list_solids_to_keep"

// SpecMiCP options
// ----------------


//! \def SPC_CF_S_SPECMICP
//! \brief Section to configure specmicp solver options
//!
//! May have a different name if several option sets are needed (ex. ReactMiCP),
//! but attributes are the same.
#define SPC_CF_S_SPECMICP                           "specmicp_options"
//! \def SPC_CF_S_SPECMICP_A_MAX_ITER
//! \brief The maximum number of iterations allowed
#define SPC_CF_S_SPECMICP_A_MAX_ITER                SPC_CF_A_MAX_ITER
//! \def SPC_CF_S_SPECMICP_A_RES_TOL
//! \brief The absolute residual tolerance
#define SPC_CF_S_SPECMICP_A_RES_TOL                 SPC_CF_A_RES_TOL
//! \def SPC_CF_S_SPECMICP_A_STEP_TOL
//! \brief The update tolerance
#define SPC_CF_S_SPECMICP_A_STEP_TOL                SPC_CF_A_STEP_TOL
//! \def SPC_CF_S_SPECMICP_A_MAX_STEP_LENGTH
//! \brief The maximum update allowed
//!
//! This option is used to avoid divergence
#define SPC_CF_S_SPECMICP_A_MAX_STEP_LENGTH         SPC_CF_A_MAX_STEP_LENGTH
//! \def SPC_CF_S_SPECMICP_A_MAX_STEP_MAX_ITER
//! \brief The maximum number of iterations at maximum update allowed
//!
//! This option is used to detect tolerances
#define SPC_CF_S_SPECMICP_A_MAX_STEP_MAX_ITER       SPC_CF_A_MAX_STEP_MAX_ITER
//! \def SPC_CF_S_SPECMICP_A_SCALING
//! \brief If true solver automatically scale the linear problem
#define SPC_CF_S_SPECMICP_A_SCALING                 "enable_scaling"
//! \def SPC_CF_S_SPECMICP_A_NONMONOTONE
//! \brief If true, the solver uses a nonmonotone linesearch algorithm
#define SPC_CF_S_SPECMICP_A_NONMONOTONE             "enable_nonmonotone_linesearch"
//! \def SPC_CF_S_SPECMICP_A_DESCENT_DIRECTION
//! \brief Multiplication applied to the descent direction when used as update
//!
//! Must be positive.
#define SPC_CF_S_SPECMICP_A_DESCENT_DIRECTION       "factor_descent_direction"
//! \def SPC_CF_S_SPECMICP_A_COND_CHECK
//! \brief Upper threshold for the condition number of the matrix
//!
//! if =-1, the check is not run
#define SPC_CF_S_SPECMICP_A_COND_CHECK              "threshold_condition_check"
//! \def SPC_CF_S_SPECMICP_A_TRSHOLD_CYCLING_LSEARCH
//! \brief Threshold used to check if linesearch is stuck in a loop
#define SPC_CF_S_SPECMICP_A_TRSHOLD_CYCLING_LSEARCH "threshold_cycling_linesearch"
//! \def SPC_CF_S_SPECMICP_A_NONIDEAL_TOL
//! \brief Relative tolerance for the non-ideality model solution
#define SPC_CF_S_SPECMICP_A_NONIDEAL_TOL            "non_ideality_tolerance"
//! \def SPC_CF_S_SPECMICP_A_NONIDEAL_MAX_ITER
//! \brief The maximum number of iteration to solve the non-ideality model
#define SPC_CF_S_SPECMICP_A_NONIDEAL_MAX_ITER       "non_ideality_" SPC_CF_A_MAX_ITER
//! \def SPC_CF_S_SPECMICP_A_CUTOFF_TOT_CONC
//! \brief Cutoff to include an equation into the system
#define SPC_CF_S_SPECMICP_A_CUTOFF_TOT_CONC         "cutoff_total_concentration"
//! \def SPC_CF_S_SPECMICP_A_RESTART_CONCENTRATION
//! \brief If the solver fails, the component concentrations will be reset to this value
//!
//! Value must be the log10 of the concentration (e.g. -3 instead of 10^-3)
#define SPC_CF_S_SPECMICP_A_RESTART_CONCENTRATION   "restart_concentration"
//! \def SPC_CF_S_SPECMICP_A_RESTART_WATER_VOL_FRAC
//! \brief If the solver fails, the volume fraction of water will be reset to this value
#define SPC_CF_S_SPECMICP_A_RESTART_WATER_VOL_FRAC  "restart_water_volume_fraction"
//! \def SPC_CF_S_SPECMICP_A_UNDER_RELAXATION
//! \brief Under relaxation factor for water volume fraction update
#define SPC_CF_S_SPECMICP_A_UNDER_RELAXATION        "under_relaxation"

// SpecMiCP formulation
// --------------------

//! \def SPC_CF_S_FORMULATION
//! \brief Create a new chemical system
#define SPC_CF_S_FORMULATION             "formulation"
//! \def SPC_CF_S_FORMULATION_A_SOLUTION
//! \brief Configure the water in the system
//!
//! A map expecting keys amount and unit
#define SPC_CF_S_FORMULATION_A_SOLUTION  "solution"
//! \def SPC_CF_S_FORMULATION_A_AQUEOUS
//! \brief Configure the aqueous solution in the system
//!
//! A list of maps expecting keys amount, unit and label
#define SPC_CF_S_FORMULATION_A_AQUEOUS   "aqueous"
//! \def SPC_CF_S_FORMULATION_A_MINERALS
//! \brief Configure the solid phases in the system
//!
//! A list of maps expecting keys amount, unit and label
#define SPC_CF_S_FORMULATION_A_MINERALS  "solid_phases"
//! \def SPC_CF_S_FORMULATION_A_AMOUNT
//! \brief Provide the amount of a species to be added
//!
//! Meaning depends on the unit
#define SPC_CF_S_FORMULATION_A_AMOUNT    "amount"
//! \def SPC_CF_S_FORMULATION_A_UNIT
//! \brief Provide the unit
//!
//! Available units depend on the type of the species being added
#define SPC_CF_S_FORMULATION_A_UNIT      "unit"
//! \def SPC_CF_S_FORMULATION_A_LABEL
//! \brief Specify the species to be added
//!
//! A list of maps expecting keys amount, unit and label
#define SPC_CF_S_FORMULATION_A_LABEL     "label"


//! \def SPC_CF_S_INITIALIZATION
//! \brief Initialization of AdimSmartSolver
#define SPC_CF_S_INITIALIZATION             "initialization"
//! \def SPC_CF_S_INITIALIZATION_A_SOLUTION
//! \brief The initial volume fraction
#define SPC_CF_S_INITIALIZATION_A_SOLUTION  "solution"
//! \def SPC_CF_S_INITIALIZATION_A_MINERALS
//! \brief Initialization of solid phases volume fractions
//!
//! A map {solid_phase, volume_fraction} is expected
#define SPC_CF_S_INITIALIZATION_A_MINERALS  "solid_phases"
//! \def SPC_CF_S_INITIALIZATION_A_AQUEOUS
//! \brief Initialization of aqueous species
//!
//! A map {aq_species, volume_fraction} is expected.
//! If aq_species is "all", values are applied to all components.
#define SPC_CF_S_INITIALIZATION_A_AQUEOUS   "aqueous"



// SpecMiCP constraints
// --------------------

//! \def SPC_CF_S_CONSTRAINTS
//! \brief Contraintes for the SpecMiCP solver
#define SPC_CF_S_CONSTRAINTS                         "constraints"
//! \def SPC_CF_S_CONSTRAINTS_A_CHARGEKEEPER
//! \brief A component that will be adjusted to keep the electroneutrality
#define SPC_CF_S_CONSTRAINTS_A_CHARGEKEEPER          "charge_keeper"
//! \def SPC_CF_S_CONSTRAINTS_A_FIXEDACTIVITY
//! \brief List of components with fixed activity
//!
//! List of maps {label, amount} or {label, amount_log}
#define SPC_CF_S_CONSTRAINTS_A_FIXEDACTIVITY         "fixed_activity"
//! \def SPC_CF_S_CONSTRAINTS_A_FIXEDMOLALITY
//! \brief List of components with fixed molality
//!
//! List of maps {label, amount} or {label, amount_log}
#define SPC_CF_S_CONSTRAINTS_A_FIXEDMOLALITY         "fixed_molality"
//! \def SPC_CF_S_CONSTRAINTS_A_FIXEDFUGACITY
//! \brief List of gas/components with fixed fugacity
//!
//! List of maps {label_gas, label_component, amount} or
//!  {label_gas, label_component, amount_log}
#define SPC_CF_S_CONSTRAINTS_A_FIXEDFUGACITY         "fixed_fugacity"
//! \def SPC_CF_S_CONSTRAINTS_A_FIXEDSATURATION
//! \brief Set the system to have a fix saturation
//!
//! Expect the saturation (0<S<1)
#define SPC_CF_S_CONSTRAINTS_A_FIXEDSATURATION      "fixed_saturation"
//! \def SPC_CF_S_CONSTRAINTS_A_SATURATED_SYSTEM
//! \brief If true, the system is saturated
#define SPC_CF_S_CONSTRAINTS_A_SATURATED_SYSTEM      "saturated_system"
//! \def SPC_CF_S_CONSTRAINTS_A_CONSERVATION_WATER
//! \brief If true, mass conservation for water is solved, the default
#define SPC_CF_S_CONSTRAINTS_A_CONSERVATION_WATER    "conservation_water"
//! \def SPC_CF_S_CONSTRAINTS_A_SURFACE_MODEL
//! \brief The surface model
//!
//! Only equilibrium is implemented
#define SPC_CF_S_CONSTRAINTS_A_SURFACE_MODEL         "surface_model"
//! \def SPC_CF_S_CONSTRAINTS_A_SURFACE_CONCENTRATION
//! \brief The concentration of surface sites
#define SPC_CF_S_CONSTRAINTS_A_SURFACE_CONCENTRATION "surface_site_concentration"
//! \def SPC_CF_S_CONSTRAINTS_A_INERT_VOLUME_FRACTION
//! \brief The volume fraction of inert materials
#define SPC_CF_S_CONSTRAINTS_A_INERT_VOLUME_FRACTION "inert_volume_fraction"


//! \def SPC_CF_S_CONSTRAINTS_A_LABEL
//! \brief Key in maps, label of a species
#define SPC_CF_S_CONSTRAINTS_A_LABEL           "label"
//! \def SPC_CF_S_CONSTRAINTS_A_AMOUNT
//! \brief Key in maps, The amount of a species (in unit given by variables)
#define SPC_CF_S_CONSTRAINTS_A_AMOUNT          "amount"
//! \def SPC_CF_S_CONSTRAINTS_A_AMOUNTLOG
//! \brief Key in maps, The log10 amount of a species (in unit given by variables
#define SPC_CF_S_CONSTRAINTS_A_AMOUNTLOG       "amount_log"
//! \def SPC_CF_S_CONSTRAINTS_A_LABEL_COMPONENT
//! \brief Key in maps, Label of a component
#define SPC_CF_S_CONSTRAINTS_A_LABEL_COMPONENT "label_component"
//! \def SPC_CF_S_CONSTRAINTS_A_LABEL_GAS
//! \brief Key in maps, Label of a gas
#define SPC_CF_S_CONSTRAINTS_A_LABEL_GAS       "label_gas"


// Variables
// ---------

//! \def SPC_CF_S_REACTMICPVARIABLES
//! \brief Initialize reactmicp variables
//!
//! Usually done through a plugin
#define SPC_CF_S_REACTMICPVARIABLES        "variables"
//! \def SPC_CF_S_REACTMICPVARIABLES_A_TYPE
//! \brief The method to use to initialize variables.
//!
//! If a plugin is used, it also needs the filepath and the name attributes.
#define SPC_CF_S_REACTMICPVARIABLES_A_TYPE SPC_CF_A_TYPE

// Boundary conditions
// -------------------

//! \def SPC_CF_S_BOUNDARYCONDITIONS
//! \brief The boundary conditions
//!
//! Details are dependant on context
//! e.g. for reactmicp unsaturation look in reactmicp/io/configuration_unsaturated
#define SPC_CF_S_BOUNDARYCONDITIONS "boundary_conditions"



// transport options
// -----------------

//! \def SPC_CF_S_DFPM
//! \brief Configuration for the dfpm solver
#define SPC_CF_S_DFPM                         "transport_options"
//! \def SPC_CF_S_DFPM_A_MAX_ITER
//! \brief The maximum number of iterations allowed
#define SPC_CF_S_DFPM_A_MAX_ITER              SPC_CF_A_MAX_ITER
//! \def SPC_CF_S_DFPM_A_RES_TOL
//! \brief the relative residuals tolerance
#define SPC_CF_S_DFPM_A_RES_TOL               SPC_CF_A_RES_TOL
//! \def SPC_CF_S_DFPM_A_ABS_TOL
//! \brief the absolute residuals tolerance
#define SPC_CF_S_DFPM_A_ABS_TOL               SPC_CF_A_ABS_TOL
//! \def SPC_CF_S_DFPM_A_STEP_TOL
//! \brief The tolerance for the update
#define SPC_CF_S_DFPM_A_STEP_TOL              SPC_CF_A_STEP_TOL
//! \def SPC_CF_S_DFPM_A_TRSHOLD_STATIONARY
//! \brief Detection of a stationnary point
//!
//! This condition is checked if update is less than step_tol but the residuals
//! is greater that the residuals tolerance
#define SPC_CF_S_DFPM_A_TRSHOLD_STATIONARY    "threshold_stationary"
//! \def SPC_CF_S_DFPM_A_MAX_STEP_LENGTH
//! \brief The maximum update allowed
#define SPC_CF_S_DFPM_A_MAX_STEP_LENGTH       SPC_CF_A_MAX_STEP_LENGTH
//! \def SPC_CF_S_DFPM_A_MAX_STEP_MAX_ITER
//! \brief The maximum number of iterations allowed to have the maximum update
//!
//! Used to detect divergence
#define SPC_CF_S_DFPM_A_MAX_STEP_MAX_ITER     SPC_CF_A_MAX_STEP_MAX_ITER
//! \def SPC_CF_S_DFPM_A_SPARSE_SOLVER
//! \brief The sparse solver to use
#define SPC_CF_S_DFPM_A_SPARSE_SOLVER         "sparse_solver"
//! \def SPC_CF_S_DFPM_A_SPARSE_PIVOTS_TRSHOLD
//! \brief Pivots threshold, usage depends on sparse solver used
#define SPC_CF_S_DFPM_A_SPARSE_PIVOTS_TRSHOLD "sparse_solver_pivots_threshold"
//! \def SPC_CF_S_DFPM_A_LINESEARCH
//! \brief The linesearch to use
#define SPC_CF_S_DFPM_A_LINESEARCH            "linesearch"
//! \def SPC_CF_S_DFPM_A_QUASI_NEWTON
//! \brief Expect an integer, the number of iterations without updating the jacobian
#define SPC_CF_S_DFPM_A_QUASI_NEWTON          "quasi_newton"

// ReactMiCP coupling solver
// -------------------------

//! \def SPC_CF_S_REACTMICP
//! \brief Reactmicp coupling algorithm options
#define SPC_CF_S_REACTMICP                   "reactmicp_options"
//! \def SPC_CF_S_REACTMICP_A_RES_TOL
//! \brief The relative residual tolerance
#define SPC_CF_S_REACTMICP_A_RES_TOL         SPC_CF_A_RES_TOL
//! \def SPC_CF_S_REACTMICP_A_ABS_TOL
//! \brief The absolute residual tolerance
#define SPC_CF_S_REACTMICP_A_ABS_TOL         SPC_CF_A_ABS_TOL
//! \def SPC_CF_S_REACTMICP_A_STEP_TOL
//! \brief The update tolerance
#define SPC_CF_S_REACTMICP_A_STEP_TOL        SPC_CF_A_STEP_TOL
//! \def SPC_CF_S_REACTMICP_A_GOOD_ENOUGH_TOL
//! \brief Relaxed tolerance when max. iterations are reached
#define SPC_CF_S_REACTMICP_A_GOOD_ENOUGH_TOL "good_enough_tolerance"
//! \def SPC_CF_S_REACTMICP_A_MAX_ITER
//! \brief Maximum number of iterations allowed
#define SPC_CF_S_REACTMICP_A_MAX_ITER        SPC_CF_A_MAX_ITER
//! \def SPC_CF_S_REACTMICP_A_IMPL_UPSCALING
//! \brief if true, includes upscaling stagger in SIA
#define SPC_CF_S_REACTMICP_A_IMPL_UPSCALING  "implicit_upscaling"


// ReactMiCP output
// ----------------

//! \def SPC_CF_S_REACTOUTPUT
//! \brief Configuration of the main output file for Reactmicp
#define SPC_CF_S_REACTOUTPUT                   "reactmicp_output"
//! \def SPC_CF_S_REACTOUTPUT_A_FILEPATH
//! \brief Path to the output file
#define SPC_CF_S_REACTOUTPUT_A_FILEPATH        SPC_CF_A_FILEPATH
//! \def SPC_CF_S_REACTOUTPUT_A_TYPE
//! \brief type of the output file
#define SPC_CF_S_REACTOUTPUT_A_TYPE            "type"
//! \def SPC_CF_S_REACTOUTPUT_A_TYPE_V_HDF5
//! \brief Output file is HDF5
#define SPC_CF_S_REACTOUTPUT_A_TYPE_V_HDF5     "hdf5"
//! \def SPC_CF_S_REACTOUTPUT_A_TYPE_V_CSV
//! \brief output file is CSV
#define SPC_CF_S_REACTOUTPUT_A_TYPE_V_CSV      "csv"


//! \def SPC_CF_S_REACTOUTPUT_A_POROSITY
//! \brief ReactMiCP saturated - output prosity
#define SPC_CF_S_REACTOUTPUT_A_POROSITY        "porosity"
//! \def SPC_CF_S_REACTOUTPUT_A_PH
//! \brief ReactMiCP saturated - output pH
#define SPC_CF_S_REACTOUTPUT_A_PH              "ph"
//! \def SPC_CF_S_REACTOUTPUT_A_DIFFUSIVITY
//! \brief ReactMiCP saturated - output diffusion coefficient
#define SPC_CF_S_REACTOUTPUT_A_DIFFUSIVITY     "diffusivity"
//! \def SPC_CF_S_REACTOUTPUT_A_VOLFRAC_MINERAL
//! \brief ReactMiCP saturated - ouput volume fraction on solid phases
#define SPC_CF_S_REACTOUTPUT_A_VOLFRAC_MINERAL "volume_fraction_solid"
//! \def SPC_CF_S_REACTOUTPUT_A_TOT_AQ_CONC
//! \brief ReactMiCP saturated - ouput total aqueous concentrations
#define SPC_CF_S_REACTOUTPUT_A_TOT_AQ_CONC     "total_aqueous_concentration"
//! \def SPC_CF_S_REACTOUTPUT_A_TOT_S_CONC
//! \brief ReactMiCP saturated - ouput total solid concentrations
#define SPC_CF_S_REACTOUTPUT_A_TOT_S_CONC      "total_solid_concentration"
//! \def SPC_CF_S_REACTOUTPUT_A_TOT_CONC
//! \brief ReactMiCP saturated - ouput total concentrations
#define SPC_CF_S_REACTOUTPUT_A_TOT_CONC        "total_concentration"
//! \def SPC_CF_S_REACTOUTPUT_A_SATINDEX
//! \brief ReactMiCP saturated - ouput volume fraction on solid phases
#define SPC_CF_S_REACTOUTPUT_A_SATINDEX        "saturation_index"

//! \def SPC_CF_S_REACTOUTPUT_A_DATABASE
//! \brief Path to working database save file
#define SPC_CF_S_REACTOUTPUT_A_DATABASE        "database"

// ReactMiCP simulation stuff
// --------------------------

//! \def SPC_CF_S_SIMULINFO
//! \brief Configure some information about simulation
#define SPC_CF_S_SIMULINFO                "simulation"
//! \def SPC_CF_S_SIMULINFO_A_NAME
//! \brief Name of the simulation
#define SPC_CF_S_SIMULINFO_A_NAME         "name"
//! \def SPC_CF_S_SIMULINFO_A_OUTPUTPREFIX
//! \brief Prefix to apply to solver output files
#define SPC_CF_S_SIMULINFO_A_OUTPUTPREFIX "output_prefix"
//! \def SPC_CF_S_SIMULINFO_A_PRINTITER
//! \brief If true, output debug information about every timesteps
//!
//! Can be quite verbose
#define SPC_CF_S_SIMULINFO_A_PRINTITER    "print_iter_info"
//! \def SPC_CF_S_SIMULINFO_A_OUTPUTSTEP
//! \brief Output will be produced every 'output_step' seconds of simulation
#define SPC_CF_S_SIMULINFO_A_OUTPUTSTEP   "output_step"

// ReactMiCP adaptive timestep
// ---------------------------

//! \def SPC_CF_S_TIMESTEPPER
//! \brief Configure the adaptative timestepper algorithm
#define SPC_CF_S_TIMESTEPPER                   "timestepper"
//! \def SPC_CF_S_TIMESTEP_A_LOWER_BOUND
//! \brief The minimum timestep allowed
#define SPC_CF_S_TIMESTEP_A_LOWER_BOUND        "minimum_dt"
//! \def SPC_CF_S_TIMESTEP_A_UPPER_BOUND
//! \brief The maximum timestep allowed
#define SPC_CF_S_TIMESTEP_A_UPPER_BOUND        "maximum_dt"
//! \def SPC_CF_S_TIMESTEP_A_RESTART_DT
//! \brief Timestep used after a critical failure
#define SPC_CF_S_TIMESTEP_A_RESTART_DT         "restart_dt"
//! \def SPC_CF_S_TIMESTEP_A_LOWER_TARGET
//! \brief The lower bound target for the number of iterations
#define SPC_CF_S_TIMESTEP_A_LOWER_TARGET       "lower_iterations_target"
//! \def SPC_CF_S_TIMESTEP_A_UPPER_TARGET
//! \brief The upper bound target for the number of iterations
#define SPC_CF_S_TIMESTEP_A_UPPER_TARGET       "upper_iterations_target"
//! \def SPC_CF_S_TIMESTEP_A_AVERAGE_PARAMETER
//! \brief Parameter for the exponential moving average
#define SPC_CF_S_TIMESTEP_A_AVERAGE_PARAMETER  "average_parameter"
//! \def SPC_CF_S_TIMESTEP_A_FACTOR_IF_FAILURE
//! \brief Multiplication factor for timestep after a failure
#define SPC_CF_S_TIMESTEP_A_FACTOR_IF_FAILURE  "decrease_factor_if_failure"
//! \def SPC_CF_S_TIMESTEP_A_FACTOR_IF_MINIMUM
//! \brief Multiplication factor if error is minimized
#define SPC_CF_S_TIMESTEP_A_FACTOR_IF_MINIMUM  "increase_factor_if_minumum"
//! \def SPC_CF_S_TIMESTEP_A_FACTOR_IF_DECREASE
//! \brief Multiplication factor if upper bound is reached
#define SPC_CF_S_TIMESTEP_A_FACTOR_IF_DECREASE "decrease_factor"
//! \def SPC_CF_S_TIMESTEP_A_FACTOR_IF_INCREASE
//! \brief Multiplication factor if lower bound is reached
#define SPC_CF_S_TIMESTEP_A_FACTOR_IF_INCREASE "increase_factor"


// ReactMiCP Staggers
// ------------------

//! \def SPC_CF_S_STAGGERS
//! \brief Configuration for ReactMiCP staggers
#define SPC_CF_S_STAGGERS "staggers"

//! \def SPC_CF_S_STAGGERS_SS_UPSCALINGSTAGGER
//! \brief Configuration for ReactMiCP upscaling stagger
#define SPC_CF_S_STAGGERS_SS_UPSCALINGSTAGGER "upscaling"
//! \def SPC_CF_S_STAGGERS_SS_UPSCALINGSTAGGER_A_TYPE
//! \brief the type of upscaling stagger
//!
//! Usually a plugin
#define SPC_CF_S_STAGGERS_SS_UPSCALINGSTAGGER_A_TYPE     SPC_CF_A_TYPE

//! \def SPC_CF_S_STAGGERS_SS_CHEMISTRYSTAGGER
//! \brief Configuration section for the chemistry stagger
#define SPC_CF_S_STAGGERS_SS_CHEMISTRYSTAGGER "chemistry"
//! \def SPC_CF_S_STAGGERS_SS_CHEMISTRYSTAGGER_A_TYPE
//! \brief The type of chemistry stagger
#define SPC_CF_S_STAGGERS_SS_CHEMISTRYSTAGGER_A_TYPE               SPC_CF_A_TYPE
//! \def SPC_CF_S_STAGGERS_SS_CHEMISTRYSTAGGER_A_TYPE_V_EQUILIBRIUM
//! \brief The chemistry stagger only solves equilibrium
#define SPC_CF_S_STAGGERS_SS_CHEMISTRYSTAGGER_A_TYPE_V_EQUILIBRIUM "equilibrium"
//! \def SPC_CF_S_STAGGERS_SS_CHEMISTRYSTAGGER_SS_EQUILIBRIUM_OPTS
//! \brief Options for the equilibrium chemistry stagger
#define SPC_CF_S_STAGGERS_SS_CHEMISTRYSTAGGER_SS_EQUILIBRIUM_OPTS  "equilibrium_options"

//! \def SPC_CF_S_STAGGERS_SS_TRANSPORTSTAGGER
//! \brief Configuration for ReactMiCP transport stagger
#define SPC_CF_S_STAGGERS_SS_TRANSPORTSTAGGER "transport"
//! \def SPC_CF_S_STAGGERS_SS_TRANSPORTSTAGGER_A_MERGE_SATURATION
//! \brief Merge saturation and water pressure equation (deprecated)
#define SPC_CF_S_STAGGERS_SS_TRANSPORTSTAGGER_A_MERGE_SATURATION  "merge_saturation"
//! \def SPC_CF_S_STAGGERS_SS_TRANSPORTSTAGGER_A_MERGE_AQUEOUS
//! \brief MERGE liquid and gas transport equations for aq. component (deprecated)
#define SPC_CF_S_STAGGERS_SS_TRANSPORTSTAGGER_A_MERGE_AQUEOUS     "merge_aqueous"
//! \def SPC_CF_S_STAGGERS_SS_TRANSPORTSTAGGER_A_CUTOFFRESIDUAL
//! \brief Cutoff for the first residual
//!
//! If less than the threshold, it is set to 1.
#define SPC_CF_S_STAGGERS_SS_TRANSPORTSTAGGER_A_CUTOFFRESIDUAL    "cutoff_R_0_squared"
//! \def SPC_CF_S_STAGGERS_SS_TRANSPORTSTAGGER_SS_DEFAULT_OPTS
//! \brief Default options for the dfpm solver
#define SPC_CF_S_STAGGERS_SS_TRANSPORTSTAGGER_SS_DEFAULT_OPTS     "default_options"
//! \def SPC_CF_S_STAGGERS_SS_TRANSPORTSTAGGER_SS_SATURATION_OPTS
//! \brief Options for the saturation equations solver
#define SPC_CF_S_STAGGERS_SS_TRANSPORTSTAGGER_SS_SATURATION_OPTS  "saturation_options"
//! \def SPC_CF_S_STAGGERS_SS_TRANSPORTSTAGGER_SS_AQUEOUS_OPTS
//! \brief Options for the aqueous component liquid transport equations
#define SPC_CF_S_STAGGERS_SS_TRANSPORTSTAGGER_SS_AQUEOUS_OPTS     "aqueous_options"
//! \def SPC_CF_S_STAGGERS_SS_TRANSPORTSTAGGER_SS_GAS_OPTS
//! \brief Options for the gas diffusion equations
#define SPC_CF_S_STAGGERS_SS_TRANSPORTSTAGGER_SS_GAS_OPTS         "gas_options"

// Run !
// -----

//! \def SPC_CF_S_RUN
//! \brief Configuration of simulation run
#define SPC_CF_S_RUN            "run"
//! \def SPC_CF_S_RUN_A_RUNUNTIL
//! \brief run simulation until t='rununtil' s
#define SPC_CF_S_RUN_A_RUNUNTIL "run_until"



#endif // SPECMICP_UTILS_IO_CONFIGYAMLSECTIONS_H
