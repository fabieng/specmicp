/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "csv_formatter.hpp"
#include "format.hpp"

#include "specmicp_common/dateandtime.hpp"

#include <iostream>

namespace specmicp {
namespace io {


OutFile::OutFile(const std::string& filepath):
    m_stream(new std::ofstream(filepath))
{
    if (m_stream->fail()) {
        throw std::runtime_error("Cannot open the file : '"+filepath+"'.");
    }
}

void OutFile::open(const std::string& filepath)
{
    if (m_stream == nullptr)
    {
        m_stream.reset(new std::ofstream(filepath));
    }
    else
    {
        if (m_stream->is_open()) close();
        m_stream->open(filepath);
    }
}

void OutFile::close()
{
    if (m_stream == nullptr)
    {
        return;
    }
    if (m_stream->is_open()) m_stream->flush();
    m_stream->close();
}


void CSVFile::insert_comment_line(const std::string& msg) {
    comment(); space();
    _get() << msg;
    eol();
}
//! \brief insert a comment line with the date
void CSVFile::insert_comment_date() {
    insert_comment_line(dateandtime::now());
}

void CSVFile::insert_comment_unit(
        const std::string& variable_name,
        const std::string& unit)
{
    comment();
    space();
    _get() << variable_name << " unit : " << unit;
    eol();
}

CSVFile::CSVFile(const std::string& filepath):
    OutFile(filepath)
{}

} //end namespace io
} //end namespace specmicp
