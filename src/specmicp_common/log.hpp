/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_UTILS_LOG_HPP
#define SPECMICP_UTILS_LOG_HPP

#include <sstream>

/*!
\file log.hpp
\brief logger

A common logger is shared by all SpecMiCP/ReactMiCP module.
The logger contains several level which can be activated (or not) by the user.
The user also decides of where the logger output its information.

\code{.cpp}
// output to std::cerr for Warning and Errors (nmo Debug messages)
init_logger(&std::cerr, logger::LogLevel::Warning)
// Log an Error
ERROR << "Error";
// Log a Warning
WARNING << "Warning";
// Log a debug message (no effect);
DEBUG << "Debug (disabled message)
\endcode

This module was inspired by :
  - http://www.drdobbs.com/cpp/a-lightweight-logger-for-c/240147505
  - http://www.drdobbs.com/cpp/logging-in-c/201804215?pgno=1
 */

#include "log_impl.hpp"

namespace specmicp {

//! \namespace specmicp::logger
//! \brief The logger used by SpecMiCP/ReactMiCP
namespace logger {

extern template class Log<ConfFile>;
extern template class Log<ErrFile>;

} // end namespace logger


//! \brief Standard logger type
using stdlog = logger::Log<logger::ErrFile>;
//! \brief The configuration logger
using conflog = logger::Log<logger::ConfFile>;

//! \brief Initialize the logger
//!
//! \param out the output stream
//! \param level the output level
void init_logger(std::ostream* out, specmicp::logger::LogLevel level);

//! \brief log a message and throw a runtime error
void log_and_throw_runtime_error(const std::string& error_msg);

//! \brief Configure the special logger containing information about
//! the configuration of specmicp
void init_conf_logger(std::ostream* out);

#ifndef SPC_DOXYGEN_SHOULD_SKIP_THIS

// filter for the configuration log
#define SPC_CONF_LOG_FILTER(level) \
    if (logger::ConfFile::stream() == nullptr) ;\
    else conflog().get(level)

#endif // SPC_DOXYGEN_SHOULD_SKIP_THIS

//! \def SPC_CONF_LOG
//! \brief Log a message to the configuration log (info level)
#define SPC_CONF_LOG          SPC_CONF_LOG_FILTER(logger::Info)
//! \def SPC_CONF_LOG_WARNING
//! \brief Log a message to the configuration log (warning level))
#define SPC_CONF_LOG_WARNING  SPC_CONF_LOG_FILTER(logger::Warning)
//! \def SPC_CONF_LOG_ERROR
//! \brief Log a message to the configuration log (error level)
#define SPC_CONF_LOG_ERROR    SPC_CONF_LOG_FILTER(logger::Error)
//! \def SPC_CONF_LOG_CRITICAL
//! \brief Log a message to the configuration log (critical level)
#define SPC_CONF_LOG_CRITICAL SPC_CONF_LOG_FILTER(logger::Critical)

//! \def SPC_CONF_LOG_SECTION
//! \brief Separator for the configuration log
#define SPC_CONF_LOG_SECTION  "======================================"
//! \def SPC_CONF_LOG_HLINE
//! \brief Separator for the
#define SPC_CONF_LOG_HLINE    "--------------------------------------"

#ifndef SPC_DOXYGEN_SHOULD_SKIP_THIS

// Filter logs to stdlog
#define SPC_LOG_FILTER(level) \
    if (level >  stdlog::ReportLevel() || logger::ErrFile::stream() == nullptr) ;\
    else stdlog().get(level) 
// Special filter to use when NDEBUG is used
#define SPC_LOG_FILTER_NDEBUG \
    if (true) ; \
    else stdlog().get(logger::Warning)

#endif // SPC_DOXYGEN_SHOULD_SKIP_THIS

//! \def SPAM
//! \brief Output spam-level log to stdlog
#ifdef NDEBUG
#define SPAM SPC_LOG_FILTER_NDEBUG
#else
#define SPAM SPC_LOG_FILTER(logger::Spam)
#endif

//! \def DEBUG
//! \brief Output debug-level log to stdlog
#ifdef NDEBUG
#define DEBUG SPC_LOG_FILTER_NDEBUG
#else
#define DEBUG SPC_LOG_FILTER(logger::Debug)
#endif

//! \def INFO
//! \brief Ooutput info-level log to stdlog
#ifdef INFO
#undef INFO // jsoncpp is also using this, remove compiler warnings
#endif

#ifdef NDEBUG
#define INFO SPC_LOG_FILTER_NDEBUG
#else
#undef INFO
#define INFO SPC_LOG_FILTER(logger::Info)
#endif

//! \def WARNING
//! \brief Output warning-level log to stdlog
#define WARNING SPC_LOG_FILTER(logger::Warning)

//! \def ERROR
//! \brief Output error-level log to stdlog
#define ERROR SPC_LOG_FILTER(logger::Error)

//! \def ERROR_THROW
//! \brief Output error level log to stdlog and throw runtime error
#define ERROR_THROW(msg) log_and_throw_runtime_error(msg)

//! \def CRITICAL
//! \brief Output critical-level log to stdlog
#define CRITICAL SPC_LOG_FILTER(logger::Critical)

} // end namespace specmicp

#endif // SPECMICP_UTILS_LOG_HPP
