/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_ODEINT_RUNGEKUTTASTEPSTRUCTS_HPP
#define SPECMICP_ODEINT_RUNGEKUTTASTEPSTRUCTS_HPP

//! \file odeint/runge_kutta_step_structs.hpp
//! \brief Options for the Runge Kutta timestepper

#include "specmicp_common/types.hpp"

namespace specmicp {
namespace odeint {

//! \brief Options for the embedded runge kutta sver
struct EmbeddedRungeKuttaStepOptions
{
    scalar_t safety;     //!< relaxation factor
    scalar_t p_grow;     //!< Timestep exponent growth
    scalar_t p_shrink;   //!< Timestep exponent decrease
    scalar_t errcon;     //!< Error threshold
    scalar_t max_try;    //!< Max. numer of try
    scalar_t eps;        //!< Small increment
    bool non_negativity; //!< Enforce non-negativity

    EmbeddedRungeKuttaStepOptions():
        safety(0.9),
        p_grow(-0.2),
        p_shrink(-0.25),
        errcon(1.89e-4),
        max_try(20),
        eps(1e-5),
        non_negativity(false)
    {}
};


} // end namespace odeint
} // end namespace specmicp


#endif// SPECMICP_ODEINT_RUNGEKUTTASTEPSTRUCTS_HPP
