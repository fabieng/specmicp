/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_MICPSOLVER_MICPSOLVERSTRUCTS_HPP
#define SPECMICP_MICPSOLVER_MICPSOLVERSTRUCTS_HPP

//! \file micpsolver_structs.hpp
//! \brief structures and enum used by the micpsolver

// this file exists to reduce the header dependancy

// These are the default options
// they are define as macro so they can be used to parse configuration with the correct default values


#ifndef SPC_DOXYGEN_SHOULD_SKIP_THIS

#define MICPSOLVER_DEFAULT_USE_CRASHING false
#define MICPSOLVER_DEFAULT_USE_SCALING true
#define MICPSOLVER_DEFAULT_USE_NONMONOTONE_LSEARCH true

#define MICPSOLVER_DEFAULT_MAX_ITER 100
#define MICPSOLVER_DEFAULT_MAX_ITER_MAXSTEP MICPSOLVER_DEFAULT_MAX_ITER
#define MICPSOLVER_DEFAULT_MAX_FACT_STEP 4


#define MICPSOLVER_DEFAULT_RES_TOL 1e-8
#define MICPSOLVER_DEFAULT_STEP_TOL 1e-10
#define MICPSOLVER_DEFAULT_COND_THRESHOLD 1e6
#define MICPSOLVER_DEFAULT_PENALIZATION_FACTOR 0.8
#define MICPSOLVER_DEFAULT_MAX_STEP 100.0
#define MICPSOLVER_DEFAULT_FACTOR_DESC_COND 1e-4
#define MICPSOLVER_DEFAULT_POWER_DESC_COND 2
#define MICPSOLVER_DEFAULT_COEFF_ACCEPT_NEWTON 0.95
#define MICPSOLVER_DEFAULT_FACTOR_GRADIENT 5.0
#define MICPSOLVER_DEFAULT_PROJ_VAR DBL_EPSILON
#define MICPSOLVER_DEFAULT_TRHSOLD_STATIONARY 1e-6
#define MICPSOLVER_DEFAULT_TRHSOLD_CYCLING_LSEARCH 1e-5


#endif // SPC_DOXYGEN_SHOULD_SKIP_THIS

#include <float.h>

namespace specmicp {
namespace micpsolver {

//! \brief Options for the MiCPSolver
//!
//! \sa specmicp::micpsolver::MiCPSolver
struct MiCPSolverOptions
{
    //! Use a crashing step to improve the starting guess
    bool use_crashing {MICPSOLVER_DEFAULT_USE_CRASHING};
    //! Use scaling to improve convergence
    bool use_scaling {MICPSOLVER_DEFAULT_USE_SCALING};
    //! Use non monotone linesearch
    bool non_monotone_linesearch {MICPSOLVER_DEFAULT_USE_NONMONOTONE_LSEARCH};
    //! Maximum number of iterations allowed
    int max_iter {MICPSOLVER_DEFAULT_MAX_ITER};
    //! the maximum number of step of length maxstep allowed
    int maxiter_maxstep {MICPSOLVER_DEFAULT_MAX_ITER_MAXSTEP};
    //! Max number of factorization in an iteration
    int max_factorization_step {MICPSOLVER_DEFAULT_MAX_FACT_STEP};
    //! Tolerance for minimization of residuals
    double fvectol {MICPSOLVER_DEFAULT_RES_TOL};
    //! Tolerance for minimization of error
    double steptol {MICPSOLVER_DEFAULT_STEP_TOL};
    //! Condition number limit, -1 to disable it
    double condition_limit {MICPSOLVER_DEFAULT_COND_THRESHOLD};
    //! Penalization factor for the penalized Fisher-Burmeister function
    double penalization_factor {MICPSOLVER_DEFAULT_PENALIZATION_FACTOR};
    //! the maximum step allowed
    double maxstep {MICPSOLVER_DEFAULT_MAX_STEP};
    //! factor in front of the descent condition > 0
    double factor_descent_condition {MICPSOLVER_DEFAULT_FACTOR_DESC_COND};
    //!  power used in the traditional format of the descent condition > 2
    double power_descent_condition {MICPSOLVER_DEFAULT_POWER_DESC_COND};
    //! Accept the newton step without line search if merit(x+d)<coeff*merit(x^k)
    double coeff_accept_newton_step {MICPSOLVER_DEFAULT_COEFF_ACCEPT_NEWTON};
    //! factor which multiply the gradient in the search direction system
    double factor_gradient_search_direction {MICPSOLVER_DEFAULT_FACTOR_GRADIENT};
    //! threshold under which a mineral is considered to be 0
    double projection_min_variable {MICPSOLVER_DEFAULT_PROJ_VAR};
    //! If norm(residual)>threshold_stationary_point and ErrorMinimized -> stationary point
    double threshold_stationary_point {MICPSOLVER_DEFAULT_TRHSOLD_STATIONARY};
    //! factor to detect cycling in linesearch
    double threshold_cycling_linesearch {MICPSOLVER_DEFAULT_TRHSOLD_CYCLING_LSEARCH};

    MiCPSolverOptions() {}

    //! \brief Set the maximum number of iterations
    void set_maximum_iterations(int maximum_iterations) {
        max_iter = maximum_iterations;
    }
    //! \brief Set the maximum step length
    void set_maximum_step_length(double step_length) {
        maxstep = step_length;
    }
    //! \brief Set the maximum step length
    void set_maximum_step_length(double step_length, int max_iter_at_step_length) {
        maxstep = step_length;
        maxiter_maxstep = max_iter_at_step_length;
    }


    //! \brief Disable the descent condition
    void disable_descent_direction() {
        factor_descent_condition = -1.0;
    }
    //! \brief Enable the descent condition
    void enable_descent_direction(double  factor, double power) {
        factor_descent_condition = factor;
        power_descent_condition = power;
    }
    //! \brief Disable the condition check
    void disable_condition_check() {
        condition_limit = -1;
    }
    //! \brief Enable the condition check
    void enable_condition_check(double threshold) {
        condition_limit = threshold;
    }
    //! \brief Disable the non-monotone linesearch
    void disable_non_monotone_linesearch() {
        non_monotone_linesearch = false;
    }
    //! \brief Enable the non-monotone linesearch
    void enable_non_monotone_linesearch() {
        non_monotone_linesearch = true;
    }

    //! \brief Enable the scaling
    void enable_scaling() {
        use_scaling = true;
    }
    //! \brief Disable the scaling
    void disable_scaling() {
        use_scaling = false;
    }
    //! \brief Enable the crashing
    void enable_crashing() {
        use_crashing = true;
    }
    //! \brief Disable the scaling
    void disable_crashing() {
        use_crashing = false;
    }
    //! \brief Set tolerance
    void set_tolerance(double residuals_tolerance) {
        fvectol = residuals_tolerance;
    }
    //! \brief Set tolerances
    void set_tolerance(double residuals_tolerance, double step_tolerance) {
        fvectol = residuals_tolerance;
        steptol = step_tolerance;
    }
};

//! \brief Return code of the MiCP solver
//!
//! A positive return code means that the algorithm converged (but maybe to a stationnary points)
//! A negative return code means that something wrong happened and was detected
enum class MiCPSolverReturnCode
{
    LolItsNotSupposedToHappen = -10,    //!< bummer...
    LinesearchFailure = -5,             //!< Failure in linesearch
    MaxStepTakenTooManyTimes = -4,      //!< Probably detect a divergence
    FailedToSolveLinearSystem = -3,     //!< Problem in the decomposition... shouldn't be raised, use of the gradient step
    MaxIterations = -2,                 //!< Algorithm has reached the maximum number of iterations
    StationaryPoint = -1,               //!< Algorithm is stuck in a stationnary point of the merit function
    NotConvergedYet = 0,                //!< Keep going...
    Success = 1,                        //!< Test success (well be careful of stationnary points)
    ResidualMinimized = 2,              //!< The residual is minimized (i.e. close to zero)
    ErrorMinimized = 3                  //!< The error is minimized, may indicate a stationary point
};

//! \brief This structure contains counter to check/query the performance of the algorithm
//!
//! It should be updated after each operations, not at the end
struct MiCPPerformance
{
    int nb_call_residuals;            //!< Number of calls of the residual
    int nb_call_jacobian;             //!< Number of calls of the jacobian
    int nb_factorization;             //!< Number of factorization performed (may be > number of iterations)
    int nb_gradient_step;             //!< Number of gradient steps (too many gradient steps indicate a bad starting guess)
    int nb_crashing_iterations;       //!< Number of crashing iterations
    int nb_iterations;                //!< Number of iterations
    bool max_taken;                   //!< Maximum step has been taken
    int nb_max_taken;                 //!< Number of time the maximum has been taken
    int nb_consecutive_max_taken;     //!< Number of consecutive step at max length
    double current_residual;          //!< Current residual
    double current_update;            //!< Current update
    MiCPSolverReturnCode return_code; //!< Return code

    //! \brief Default constructor
    MiCPPerformance():
        nb_call_residuals(0),
        nb_call_jacobian(0),
        nb_factorization(0),
        nb_gradient_step(0),
        nb_crashing_iterations(0),
        nb_iterations(0),
        max_taken(false),
        nb_max_taken(0),
        nb_consecutive_max_taken(0),
        current_residual(INFINITY),
        current_update(INFINITY),
        return_code(MiCPSolverReturnCode::NotConvergedYet)
    {}

    //! \brief Add two performance instances
    //!
    //! Used when restarting a computation in case of failure
    MiCPPerformance& operator+=(const MiCPPerformance& other)
    {
        nb_call_residuals += other.nb_call_residuals;
        nb_call_jacobian += other.nb_call_jacobian;
        nb_factorization += other.nb_factorization;
        nb_gradient_step += other.nb_gradient_step;
        nb_crashing_iterations += other.nb_crashing_iterations;
        nb_iterations += other.nb_iterations;
        current_residual = other.current_residual;
        current_update = other.current_update;
        return_code = other.return_code; // we take the last one
        return *this;
    }
};

//! The NCP function that are implemented
enum class NCPfunction
{
    penalizedFB, // Penalized Fischer-Burmeister function
    min          // The minimum function
};

} // end namespace micpsolver
} // end namespace specmicp

#endif //SPECMICP_MICPSOLVER_MICPSOLVERSTRUCTS_HPP
