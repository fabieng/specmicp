/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_MICPSOLVER_BOXVIPROG_HPP
#define SPECMICP_MICPSOLVER_BOXVIPROG_HPP

#include "micpprog.hpp"

//! \file micpsolver/micpprog.hpp
//! \brief The static interface of a MiCP program

namespace specmicp {
namespace micpsolver {

//! \class BoxVIProg
//! \brief A base clase for a box VI constrained program
//!
//! This class describes a static interface.
//! Any box VI program should derived from this class and implement its methods
template <class Derived>
class BoxVIProg: public MiCPProg<Derived>
{
public:
    //! \brief Return the derived class
    Derived& derived() {return static_cast<Derived*>(*this);}

    //! \brief Return true if ideq is a box-contrained VI equation
    //!
    //! \param[in] ideq id of the equation
    //! \param[out] upper_bound upper bound used in the equation, lower bound is 0
    bool is_box_vi(index_t ideq, scalar_t& upper_bound);


};

} // end namespace micpsolver
} // end namespace specmicp

#endif // SPECMICP_MICPSOLVER_MICPPROG_HPP
