/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_MICPSOLVER_MICPPROG_HPP
#define SPECMICP_MICPSOLVER_MICPPROG_HPP

#include "specmicp_common/types.hpp"

//! \file micpsolver/micpprog.hpp
//! \brief The static interface of a MiCP program

namespace specmicp {
namespace micpsolver {

//! \class MiCPProg
//! \brief A base clase for a MiCP program
//!
//! This class describes a static interface.
//! Any MiCP program should derived from this class and implement its methods
template <class Derived>
class MiCPProg
{
public:
    //! \brief Return the derived class
    Derived& derived() {return static_cast<Derived*>(*this);}


    //! \brief Return the number of variables
    //!
    //! Sould be implemented by the program
    index_t total_variables() {return derived()->total_variables();}
    //! \brief Return the number of free variables
    //!
    //! Sould be implemented by the variables
    //!
    //! The free variables are the variables not subjected to the complementarity condition.
    index_t nb_free_variables() {return derived()->total_variables();}
    //! \brief Return the number of constrained variables
    //!
    //! Not need to implemented this method by the program.
    index_t nb_complementarity_variables() {return total_variables() - nb_free_variables();}

    //! \brief Return the residuals
    //!
    //! \param[in] x the variables
    //! \param[out] residual the residuals
    void get_residuals(const Vector& x,
                       Vector& residual);

    //! \brief Return the jacobian
    //!
    //! \param[in] x the variables
    //! \param[out] jacobian the jacobian
    void get_jacobian(Vector& x,
                      Matrix& jacobian);

    //! \brief Called at the beginning of an iteration
    //!
    //! \param x the variables
    //! \param norm_residual norm of the residuals of the previous iteration
    //! \return A boolean indicating if the system can have converged
    //!
    //! Return true by default
    bool hook_start_iteration(const Vector& x, scalar_t norm_residual) {return true;}
    //! \brief Return the maximum update length that the algorithm can take
    //!
    //! This is usually used to impose nonnegativity conditions
    //!
    //! \param x the variables
    //! \param update solution of the linear system
    //! \return multiplicating factor to scale the update
    scalar_t max_lambda(const Vector& x, const Vector& update) {return 1.0;}


    //! \brief Return the value used for infinity
    static double infinity() {return HUGE_VAL;}


};

} // end namespace micpsolver
} // end namespace specmicp

#endif // SPECMICP_MICPSOLVER_MICPPROG_HPP
