/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_MICPSOLVER_MICPSOLVERMIN_HPP
#define SPECMICP_MICPSOLVER_MICPSOLVERMIN_HPP

//! \file micpsolver_min.hpp
//! \brief MiCP solver based on the min function

#include "micpsolver_base.hpp"

namespace specmicp {
namespace micpsolver {

//! \brief MiCP solver using the min function
template <class program_t>
class MiCPSolverMin: public MiCPSolverBaseProgram<program_t>
{
public:

    using base = MiCPSolverBaseProgram<program_t>; //!< Type of the base class
    using MiCPSolverBaseProgram<program_t>::get_options;
    using MiCPSolverBaseProgram<program_t>::get_program;
    using MiCPSolverBaseProgram<program_t>::get_perf;
    using MiCPSolverBaseProgram<program_t>::get_neq;
    using MiCPSolverBaseProgram<program_t>::get_neq_free;

    //! \brief Constructor
    //!
    //! @param prog smart pointer toward an instance of a Program to solve
    MiCPSolverMin(std::shared_ptr<program_t> prog):
        MiCPSolverBaseProgram<program_t>(prog),
        m_residuals(prog->total_variables()),
        m_grad_phicck(prog->total_variables()),
        m_jacobian(prog->total_variables(),prog ->total_variables())
    {

        m_max_merits.reserve(4);
    }

    //! \brief Solve the program
    //!
    //! @param[in,out] x (in) initial guess, (out) solution
    MiCPSolverReturnCode solve(Eigen::VectorXd& x);

    //! \brief Compute the search direction
    MiCPSolverReturnCode search_direction_calculation(Eigen::VectorXd& x,
                                      Eigen::VectorXd& update);

    //! \brief Perform a linesearch
    int linesearch(Eigen::VectorXd& p, Eigen::VectorXd& x);

    //! \brief Set up the residuals
    //!
    //! @param[in] x the current solution
    void setup_residuals(const Eigen::VectorXd& x) {
        base::compute_residuals(x, m_residuals);
    }
    //! \brief Set up the jacobian
    //!
    //! @param[in] x the current solution
    void setup_jacobian(Eigen::VectorXd& x) {
        base::compute_jacobian(x, m_jacobian);
    }


    //! Reduce the system
    //!
    //! @return reduced number of freedom
    int reduce_system(const Eigen::VectorXd& x,
                       Eigen::MatrixXd& reduced_jacobian,
                       Eigen::VectorXd& reduced_residual);

    //! \brief Reformulate the residuals using the CCK formulation
    void reformulate_residuals_cck_inplace(const Eigen::VectorXd& x,
                                           Eigen::VectorXd& residuals);
    //! \brief Reformulate the residuals
    void reformulate_result(const Eigen::VectorXd &x,
                       Eigen::VectorXd &update);
private:
    //! \brief Clean the results
    void sanitize(Eigen::VectorXd &x);
    Eigen::VectorXd m_residuals; //!< The residuals
    Eigen::VectorXd m_grad_phicck; //!< gradient of CCK reformulation
    Eigen::MatrixXd m_jacobian; //!< The jacobian

    bool m_gradient_step_taken; //!< True if gradient step was chosen

    std::vector<double> m_max_merits;   //!< Contains the m best value of the merit function

    double m_newton_length; //!< The newton step length
};


} // end namespace micpsolver
} // end namespace specm

// implementation
// --------------
#include "micpsolver_min.inl"

#endif // SPECMICP_MICPSOLVER_MICPSOLVERMIN_HPP
