/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_COMMON_LOG_IMPL_HPP
#define SPECMICP_COMMON_LOG_IMPL_HPP


#include <sstream>

/*!
\file log_impl.hpp
\brief a logger

 */

namespace specmicp {
namespace logger {

//! \enum LogLevel the different log level
//!
//!  If changed, the function to_string must also be changed
enum LogLevel {Critical, //!< Error that should lead to abortion
               Error,    //!< Error
               Warning,  //!< Warnings
               Info,     //!< May be worth mentionning, not worth listening...
               Debug,    //!< (Relevant) Debugging information
               Spam      //!< How is your life ? ususally contains large debugging information such as the variables vector
              };

//!  \brief Format the log level into a string
std::string to_string(LogLevel);
//!  \brief Format the log level into a string for the conf logger
std::string to_string_conf(LogLevel);

/*! \brief A log message
 *
 *  The message is written during the destruction of the Log object.
 *  The stream which contain the message is accessed with the member get(Level)
 */
template <typename outputPolicy>
class Log
{
public:
    //! Constructor
    Log() {}
    //! Destructor - Output the message
    ~Log() {
        outputPolicy::output(msg.str());
    }


    //! \brief Return the steam so we can write the message
    //!
    //! \param level Level of the message
    std::ostringstream& get(LogLevel level);

    //! \brief Return the report level
    static LogLevel& ReportLevel();

protected:
    std::ostringstream msg; //!< the actual message
private:
    // this are hidden on purpose, no need
    Log(Log&) = delete;
    Log& operator=(Log&) = delete;
};


//! \brief Standard output Policy to use for logging
class ErrFile
{
public:
    //! \brief Return a pointer to the stream we want to write in
    static std::ostream*& stream();
    //! \brief Output the message to the stream
    static void output(const std::string& msg);
};


//! \brief Standard output Policy to use for logging
class ConfFile
{
public:
    //! \brief Return a pointer to the stream we want to write in
    static std::ostream*& stream();
    //! \brief Output the message to the stream
    static void output(const std::string& msg);
};

} // end namespace logger
} // end namespace specmicp


#endif // SPECMICP_COMMON_LOG_IMPL_HPP
