/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_UTILS_TIMER_HPP
#define SPECMICP_UTILS_TIMER_HPP

/*!
\file timer.hpp
\brief Timer and related tools

Implements a simple timer using std::chrono.

\code{.cpp}
Timer timer;
// do stuff;
timer.stop();
std::cout << "Elapsed time : " << timer.elapsed_time() << "\n";
\endcode

*/

#include "types.hpp"
#include <chrono>

namespace specmicp {

//! \brief A simple timer
class SPECMICP_DLL_PUBLIC Timer
{
public:
    //! \brief Initialize the timer and start it
    Timer() {
        start();
    }

    //! \brief Start the timer
    void start() {
        m_start = std::chrono::system_clock::now();
    }
    //! \brief Stop the timer
    void stop() {
        m_end = std::chrono::system_clock::now();
    }

    //! \brief Return the time of the starting point
    std::time_t get_start() const;

    //! \brief Return the time of the ending point
    std::time_t get_stop() const;

    //! \brief Return a textual representation (ctime) of the starting point
    char* get_ctime_start() const;

    //! \brief Return a textual representation (ctime) of the ending point
    char* get_ctime_stop() const;

    //! \brief Return the elapsed time (in seconds)
    scalar_t elapsed_time() const;

private:
    using time_point = std::chrono::time_point<std::chrono::system_clock>;

    time_point m_start;
    time_point m_end;
};

} // end namespace specmicp

#endif // SPECMICP_UTILS_TIMER_HPP
