/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_PHYSICS_UNSATURATEDLAWS_HPP
#define SPECMICP_PHYSICS_UNSATURATEDLAWS_HPP

//! \file physics/unsaturated_laws.hpp
//! \brief Common laws and models in unsaturated porous medium

#include "specmicp_common/types.hpp"

namespace specmicp {
namespace laws {

//! \brief Return the capillary pressure as function of the relative humidity
scalar_t SPECMICP_DLL_PUBLIC kelvin_equation(scalar_t rh);

//! \brief Return the relative humidity as function of the capillary pressure
scalar_t SPECMICP_DLL_PUBLIC invert_kelvin_equation(scalar_t pc);

//! \brief Capillary Pressure model used by V. Baroghel-Bouny et al.
//!
//! \f$ Pc(S_l) = a (S_l^{-b} - 1)^{1-1/b} \f$
//!
//! \param saturation the liquid saturation
//! \param a fitting coefficient, unit of pressure
//! \param b fitting coefficient, dimensioneless, >1
scalar_t SPECMICP_DLL_PUBLIC capillary_pressure_BaroghelBouny(
        scalar_t saturation,
        scalar_t a,
        scalar_t b
        );

//! \brief Van Genutchen capillary Pressure model
//!
//! \f$ Pc(S_l) = P_0 (S_l^{-1/m} - 1)^{1-m} \f$
//!
//! \param saturation the liquid saturation
//! \param P_0 fitting coefficient, unit of pressure
//! \param m fitting coefficient, dimensioneless, 0<m<1
scalar_t SPECMICP_DLL_PUBLIC capillary_pressure_VanGenutchen(
        scalar_t saturation,
        scalar_t P_0,
        scalar_t m
        );

//! \brief The Mualem model for relative liquid permeability
//!
//! \f$ k_{rl}(S_l) = \sqrt{S_l}\left(1 - \left(1-S_l^{1/m}\right)^m\right)^2 \f$
//!
//! \param saturation the liquid saturation
//! \param m a fitting coefficient
//!
//! In Baroghel-Bouny et al. m=1.0/b
scalar_t SPECMICP_DLL_PUBLIC relative_liquid_permeability_Mualem(
        scalar_t saturation,
        scalar_t m
        );

//! \brief The mualem Model for relative gas permeability
//!
//! \f$ k_{rg} = \sqrt{1-S_l} \left(1 - S)l^{1/m} \right)^{2m} \f$
scalar_t SPECMICP_DLL_PUBLIC relative_gas_permeability_Mualem(
        scalar_t saturation,
        scalar_t m
        );

//! \brief Type of a model of the saturation
using saturation_model_f = std::function<scalar_t (scalar_t)>;

// don't work with GCC < 4.9.0 / no fix

//! \brief Return a model function of the saturation
//!
//! \tparam F a model where saturation is the first parameter
//! \tparam ...Args Parameter pack representing the parameters of the model
//! \return a function taking only the saturation
//!
//! Bind the argument of the model
template <typename F, typename ...Args>
saturation_model_f make_saturation_model(F func, Args...args)
{
    return [func,args...](scalar_t saturation) -> scalar_t {
        return func(saturation, args...);
    };
}

//! \brief Return a relative humidity model from a capillary pressure model
//!
//! Use the kelvin equation
inline saturation_model_f make_rh_model(saturation_model_f cap_pressure)
{
    return [cap_pressure](scalar_t saturation) -> scalar_t {
        return invert_kelvin_equation(cap_pressure(saturation));
    };
}

} //end namespace laws
} //end namespace specmicp

#endif // SPECMICP_PHYSICS_UNSATURATEDLAWS_HPP
