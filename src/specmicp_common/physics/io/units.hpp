/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_IO_UNITS_HPP
#define SPECMICP_IO_UNITS_HPP

//! \file physics/io/units.hpp
//! \brief Print information about the units

#include "specmicp_common/types.hpp"
#include <string>

namespace specmicp {

namespace units {

enum class LengthUnit;
enum class MassUnit;
enum class QuantityUnit;

} //end namespace units

//! \namespace specmicp::io
//! \brief Input/output helper functions
namespace io {

//! \brief Return the string representation of a length unit
std::string
SPECMICP_DLL_PUBLIC SPECMICP_PURE_F
to_string(units::LengthUnit length_u);
//! \brief Return the string representation of a mass unit
std::string
SPECMICP_DLL_PUBLIC SPECMICP_PURE_F
to_string(units::MassUnit mass_u);
//! \brief Return the string representation of a quantity unit
std::string
SPECMICP_DLL_PUBLIC SPECMICP_PURE_F
to_string(units::QuantityUnit mass_u);

// abbreviated version

//! \brief Transform a length unit into a string
std::string
SPECMICP_DLL_PUBLIC SPECMICP_PURE_F
length_unit_to_string(units::LengthUnit length_u);

//! \brief Return a string describing the surface unit
std::string
SPECMICP_DLL_PUBLIC SPECMICP_PURE_F
surface_unit_to_string(units::LengthUnit length_u);

//! \brief Return a string describing the volume unit
std::string
SPECMICP_DLL_PUBLIC SPECMICP_PURE_F
volume_unit_to_string(units::LengthUnit length_u);

} // end namespace io
} // end namespace specmicp

#endif // SPECMICP_IO_UNITS_HPP
