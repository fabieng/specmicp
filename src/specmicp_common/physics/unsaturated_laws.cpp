/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "unsaturated_laws.hpp"

#include "laws.hpp"
#include "units.hpp"

namespace specmicp {
namespace laws {

scalar_t kelvin_equation(scalar_t rh)
{
    return -(constants::gas_constant * units::celsius(25.0) *
             constants::water_density_25 / constants::water_molar_mass
             )*std::log(rh);
}

scalar_t invert_kelvin_equation(scalar_t pc)
{
    return std::exp( -constants::water_molar_mass /
                     ( constants::gas_constant *
                       units::celsius(25.0) *
                       constants::water_density_25
                     ) * pc
                   );
}

scalar_t capillary_pressure_BaroghelBouny(
        scalar_t saturation,
        scalar_t a,
        scalar_t b
        )
{
    auto tmp = std::pow(saturation, -b) - 1.0;
    return a*std::pow(tmp, 1.0-1.0/b);
}

scalar_t capillary_pressure_VanGenutchen(
        scalar_t saturation,
        scalar_t P_0,
        scalar_t m
        )
{
    auto tmp = std::pow(saturation, -1.0/m) - 1.0;
    return P_0*std::pow(tmp, 1.0-m);
}

scalar_t relative_liquid_permeability_Mualem(
        scalar_t saturation,
        scalar_t m
        )
{
    auto tmp = 1.0 - std::pow(saturation, 1.0/m);
    auto tmp2 = 1.0 - std::pow(tmp, m);
    return std::sqrt(saturation) * std::pow(tmp2, 2);
}

scalar_t relative_gas_permeability_Mualem(
        scalar_t saturation,
        scalar_t m
        )
{
    auto tmp = 1.0 - std::pow(saturation, 1.0/m);
    return std::sqrt(1.0-saturation)*std::pow(tmp, 2*m);
}

} //end namespace laws
} //end namespace specmicp
