/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_UTILS_MATHS_HPP
#define SPECMICP_UTILS_MATHS_HPP

/*!
\file maths.hpp
\brief Simple math functions
 */

#include "specmicp_common/types.hpp"

namespace specmicp {

//! \brief Average methods available
enum class Average {
    arithmetic,
    geometric,
    harmonic
};

//! \brief Average two numbers
//!
//! \tparam average the average method
template <Average average_method>
scalar_t SPECMICP_CONST_F average(scalar_t a, scalar_t b);

#ifndef SPC_DOXYGEN_SHOULD_SKIP_THIS
template<>
inline
scalar_t SPECMICP_CONST_F average<Average::arithmetic>(scalar_t a, scalar_t b)
{
    return (a + b)/2.0;
}

template<>
inline
scalar_t SPECMICP_CONST_F average<Average::harmonic>(scalar_t a, scalar_t b)
{
    return 2.0/(1.0/a + 1.0/b);
}

template<>
inline
scalar_t SPECMICP_CONST_F average<Average::geometric>(scalar_t a, scalar_t b)
{
    return std::sqrt(a*b);
}

#endif // SPC_DOXYGEN_SHOULD_SKIP_THIS

//! \brief Average a vector
//!
//! \tparam average the average method
//! \param vector the vector to average
template <Average average_method>
scalar_t SPECMICP_CONST_F  average(const Vector& vector);

#ifndef SPC_DOXYGEN_SHOULD_SKIP_THIS

template<>
inline
scalar_t SPECMICP_CONST_F  average<Average::arithmetic>(const Vector& vector)
{
    return vector.sum()/vector.rows();
}

template<>
inline
scalar_t SPECMICP_CONST_F average<Average::geometric>(const Vector& vector)
{
    return std::pow(vector.prod(), 1.0/vector.rows());
}

template<>
inline
scalar_t SPECMICP_CONST_F  average<Average::harmonic>(const Vector& vector)
{
    scalar_t sum = 0;
    for (index_t ind=0; ind<vector.rows(); ++ind) {
        sum += 1.0 / vector(ind);
    }
    return vector.rows()/sum;
}

#endif // SPC_DOXYGEN_SHOULD_SKIP_THIS

} //end namespace specmicp

#endif // SPECMICP_UTILS_MATHS_HPP
