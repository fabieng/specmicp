/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_UNITS_UNITS
#define SPECMICP_UNITS_UNITS

#include "specmicp_common/types.hpp"

//! \file physics/units.hpp
//! \brief units management and conversion

namespace specmicp {

//! \namespace specmicp::units
//! \brief Units management
namespace units {


//! \brief Return the unit from the scaling factor
template <typename UnitT>
auto SPECMICP_CONST_F from_scaling_factor(scalar_t& scaling) -> UnitT;

//! \brief Return the unit from the scaling factor
template <typename UnitT>
auto SPECMICP_CONST_F from_scaling_factor(scalar_t&& scaling) -> UnitT
{
    return from_scaling_factor<UnitT>(scaling);
}

//! \brief Length units
//!
//! also used for surface/volume
enum class LengthUnit {
    meter,
    decimeter,
    centimeter
};

//! \brief Return the scaling factor from the SI unit to the given unit
//!
//! Ex: if length_unit=cm, it returns 1e-2
scalar_t scaling_factor(LengthUnit length_unit) SPECMICP_CONST_F;

//! \brief Return the Lenght unit from the scaling factor
template <>
auto SPECMICP_CONST_F from_scaling_factor<LengthUnit>(scalar_t& scaling) -> LengthUnit ;

//! \brief Mass units
enum class MassUnit {
    kilogram,
    gram
};

//! \brief Return the scaling factor from the SI unit to the given unit
//!
//! Ex: if mass_unit=grams, it returns 1e-3
scalar_t scaling_factor(MassUnit mass_unit) SPECMICP_CONST_F;

//! \brief Return the mass unit from the scaling factor
template <>
auto SPECMICP_CONST_F from_scaling_factor<MassUnit>(scalar_t& scaling) -> MassUnit ;


//! Amount of substance units
enum class QuantityUnit {
    moles,
    millimoles
};

//! \brief Return the scaling factor from the SI unit to the given unit
//!
//! Ex: if quantity_unit=millimoles, it returns 1e-3
scalar_t scaling_factor(QuantityUnit qty_unit) SPECMICP_CONST_F;

//! \brief Return the amount of substnce unit from the scaling factor
template <>
auto SPECMICP_CONST_F from_scaling_factor<QuantityUnit>(scalar_t& scaling) -> QuantityUnit ;



//! \brief Simple struct which contains the unit information
struct UnitsSet
{
    MassUnit mass;         //!< The unit used for mass
    LengthUnit length;     //!< The unit used for length (also area and volume)
    QuantityUnit quantity; //!< The unit used for the quantity of matter

    //! \brief Defaults are the SI unit system
    UnitsSet():
        mass(MassUnit::kilogram),
        length(LengthUnit::meter),
        quantity(QuantityUnit::moles)
    {}

    //! \brief Initialize units system using user inputs
    UnitsSet(MassUnit mass_u, LengthUnit length_u, QuantityUnit qty_u):
        mass(mass_u),
        length(length_u),
        quantity(qty_u)
    {}
};

// general Units
extern const UnitsSet SI_units;   //!< SI units
extern const UnitsSet CMG_units;  //!< cm,mol,g units


//! \brief Base class that handles the units
//!
//!  To be inherited by other classes that need units
class UnitBaseClass
{
public:
    //! \brief Default constructor, use default units
    UnitBaseClass() {}
    //! \brief Constructor with custom units
    UnitBaseClass(UnitsSet units):
        m_units(units)
    {}
    //! \brief Return the units
    const UnitsSet& get_units() const {return m_units;}

    //! \brief Set the units
    void set_units(UnitsSet units) {m_units = units;}

    //! \brief Return the Mass unit
    MassUnit mass_unit() const {return m_units.mass;}
    //! \brief Return the Length unit
    LengthUnit length_unit() const {return m_units.length;}

    //! \brief Return the mass unit
    MassUnit& mass_unit() {return m_units.mass;}
    //! \brief Return the length unit
    LengthUnit& length_unit() {return m_units.length;}

private:
    UnitsSet m_units;
};


//! \brief the type of an 'amount' unit
enum class AmountUnitType
{
    Mass,
    Volume,
    VolumeFraction,
    NumberOfMoles,
    MoleConcentration,
    MassConcentration,
    Molality,
    Unknown
};

//! \brief Description of an amount unit
struct AmountUnit
{
    AmountUnitType type; //!< The unit type
    scalar_t factor_si; //!< unit*factor_si = SI unit
};

//! \brief Find the unit and factor of 'unit'
//!
//! \param unit the unit to analyse
//! \param amounts
void parse_amount_unit(std::string unit, AmountUnit& amounts);

//! \brief Finf the unit and factor of 'unit'
AmountUnit parse_amount_unit(const std::string& unit);

//! \brief Check if 'unit' is a mass unit
//!
//! \return if it is a mass unit return the factor to transform it to its SI equivalent, else return a negative number
scalar_t is_mass_unit(const std::string& unit);
//! \brief Check if 'unit' is a length unit
//!
//! \return if it is a length unit return the factor to transform it to its SI equivalent, else return a negative number
scalar_t is_length_unit(const std::string& unit);
//! \brief Check if 'unit' is a volume unit
//!
//! \return if it is a volume unit return the factor to transform it to its SI equivalent, else return a negative number
scalar_t is_volume_unit(const std::string& unit);
//! \brief Check if 'unit' is a mole unit
//!
//! \return if it is a mole unit return the factor to transform it to its SI equivalent, else return a negative number
scalar_t is_mole_unit(const std::string& unit);

// Temperature
// -----------

//! \brief Convert a temperature from Celsius to Kelvin
inline constexpr scalar_t celsius(scalar_t tc) {return tc + 273.15;}
//! \brief Convert a temperature from Kelvin to Celsius
inline constexpr scalar_t to_celsius(scalar_t tk) { return tk - 273.15;}

//! \brief Convert a pressure from bar to pascal
inline constexpr scalar_t bar(scalar_t pb) {return 1e5*pb;}
//! \brief Convert a pressure from pascal to bar
inline constexpr scalar_t to_bar(scalar_t ppa) {return 1e-5*ppa;}
//! \brief Convert a pressure from atm to pascal
inline constexpr scalar_t atm(scalar_t patm) {return 101325*patm;}
//! \brief Convert a pressure from pascal to atm
inline constexpr scalar_t to_atm(scalar_t ppa) {return ppa/101325;}

// Volume
// ------

//! Convert liter to cubic meter
inline constexpr scalar_t liter(scalar_t vl) {return 1e-3*vl;}
//! convert cubic meter to liter
inline constexpr scalar_t to_liter(scalar_t vcm) {return 1e3*vcm;}

// Pressure
// --------

//! \brief Convert pressure from SI
scalar_t SPECMICP_DLL_PUBLIC convert_pressure(scalar_t pressure_si, LengthUnit length_unit);

} // end namespace units
} // end namespace specmicp

#endif // SPECMICP_UNITS_UNITS
