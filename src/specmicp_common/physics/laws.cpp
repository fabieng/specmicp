/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "laws.hpp"
#include "units.hpp"

#include <cmath>

namespace specmicp {
namespace laws {

scalar_t debye_huckel(scalar_t sqrtI, scalar_t zi, scalar_t ao)
{
    if (zi == 0.0) return 0.0;

    scalar_t res = 0.0;
    const scalar_t zisquare = std::pow(zi, 2);
    res = - constants::Adebye*zisquare*sqrtI;
    const scalar_t denom = (1 + ao*constants::Bdebye*sqrtI);
    res /= denom;

    return res;
}

scalar_t extended_debye_huckel(scalar_t I, scalar_t sqrtI, scalar_t zi, scalar_t ao, scalar_t bdot)
{
    const scalar_t tmp = debye_huckel(sqrtI, zi, ao);
    return tmp + bdot*I;
}

scalar_t extended_debye_huckel(scalar_t I, scalar_t zi, scalar_t ao, scalar_t bdot)
{
    const scalar_t tmp = debye_huckel(std::sqrt(I), zi, ao);
    return tmp + bdot*I;
}

scalar_t density_water(scalar_t temperature, units::LengthUnit length_unit, units::MassUnit mass_unit)
{
    scalar_t scaling = 1.0;
    if (mass_unit == units::MassUnit::gram) scaling *= 1e-3;
    if (length_unit == units::LengthUnit::centimeter) scaling *= 1e-6;
    else if (length_unit == units::LengthUnit::decimeter) scaling = 1e-3;
    return scaling*constants::water_density_25;
}

scalar_t density_water(units::UnitsSet units_set)
{
    return density_water(0, units_set.length, units_set.mass);
}

} // end namespace laws
} // end namespace specmicp
