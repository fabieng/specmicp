/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_COMMON_VECTORCHECKER_EXPR_HPP
#define SPECMICP_COMMON_VECTORCHECKER_EXPR_HPP

//! \file eigen/vector_checker_expr.hpp
//! \brief Expressions for the vector checker module

namespace specmicp {
namespace vector_checker {

//! \brief Type of a vector
//!
//! This is the only type that can be checked with this module
template <typename ScalarT>
using VectorT = Eigen::template Matrix<ScalarT, Eigen::Dynamic, 1>;


namespace expr {

//! \brief The base class of an expression
template <typename Derived>
struct BaseBoolExpr
{
    //! \brief Return the Derived class
    const Derived& self() const {return static_cast<const Derived&>(*this);}

    //! \brief Evaluate the expression
    bool operator() () const {return self().out_impl();}

    //! \brief Evalute the expression when assigned to a boolean
    operator bool() const {return self().out_impl();}
};


//! \brief Evaluate a logical AND expression
template <typename Derived0, typename Derived1>
class AndBoolExpr:
        public BaseBoolExpr<AndBoolExpr<Derived0, Derived1>>
{

public:
    //! \brief Constructor
    //!
    //! \param child_0 LHS expression
    //! \param child_1 RHS expression
    AndBoolExpr(
            const Derived0& child_0,
            const Derived1& child_1
            ):
        m_child_0(child_0),
        m_child_1(child_1)
    {}
    //! \brief Move constructor
    //!
    //! \param child_0 LHS expression
    //! \param child_1 RHS expression
    AndBoolExpr(
            BaseBoolExpr<Derived0>&& child_0,
            BaseBoolExpr<Derived1>&& child_1
            ):
        m_child_0(std::move(child_0.self())),
        m_child_1(std::move(child_1.self()))
    {}

    //! \brief Implementation of an AND expression
    bool out_impl() const {
        return (m_child_0() && m_child_1());
    }

private:
    Derived0 m_child_0;
    Derived1 m_child_1;
};

//! \brief Evaluate a logical OR Expression
template <typename Derived0, typename Derived1>
class OrBoolExpr:
        public BaseBoolExpr<OrBoolExpr<Derived0, Derived1>>
{
public:
    //! \brief Constructor
    //!
    //! \param child_0 LHS expression
    //! \param child_1 RHS expression
    OrBoolExpr(const Derived0& child_0, const Derived1& child_1):
        m_child_0(child_0),
        m_child_1(child_1)
    {}
    //! \brief Move constructor
    //!
    //! \param child_0 LHS expression
    //! \param child_1 RHS expression
    OrBoolExpr(
            BaseBoolExpr<Derived0>&& child_0,
            BaseBoolExpr<Derived1>&& child_1
            ):
        m_child_0(std::move(child_0.self())),
        m_child_1(std::move(child_1.self()))
    {}

    //! \brief Implementation of an OR expression
    bool out_impl() const {
        return (m_child_0() || m_child_1());
    }

private:
    Derived0 m_child_0;
    Derived1 m_child_1;
};

//! \brief True if the underlying condition is true for all element
//!
//! Evaluate a condition for all element in a vector,
//! check if the condition formed by 'element BinaryOpTag rhs' is true
//! for all element
template <typename BinaryOpTag, typename ScalarT>
class AllExpr:
        public BaseBoolExpr<AllExpr<BinaryOpTag, ScalarT>>
{
public:
    //! \brief Constructor
    //!
    //! \param vector the vector to check
    //! \param rhs the value to check the vector against
    AllExpr(const VectorT<ScalarT>& vector, ScalarT rhs):
        m_vector(vector),
        m_rhs(rhs)
    {}

    //! \brief Implementation of the check
    bool out_impl() const
    {
        bool is_true = true;
        for (typename VectorT<ScalarT>::Index id=0; id< m_vector.rows(); ++id)
        {
            if (not op(m_vector(id), m_rhs)) {
                is_true = false;
                break;
            }
        }
        return is_true;
    }

private:
    BinaryOpTag op; //!< The operator
    const VectorT<ScalarT>& m_vector; //!< The vector to check
    ScalarT m_rhs; //!< The value to test against
};

//! \brief True if the underlying condition is true for at least one element
//!
//! Evaluate the condition 'element BinaryOpTag rhs' to see if it is true for
//! at least one element
template <typename BinaryOpTag, typename ScalarT>
class AnyExpr:
        public BaseBoolExpr<AnyExpr<BinaryOpTag, ScalarT>>
{
public:
    //! \brief Constructor
    //!
    //! \param vector the vector to check
    //! \param rhs the value to check the vector against
    AnyExpr(const VectorT<ScalarT>& vector, ScalarT rhs):
        m_vector(vector),
        m_rhs(rhs)
    {}
    //! \brief Implementation of the check
    bool out_impl() const
    {
        bool is_true = false;
        for (typename VectorT<ScalarT>::Index id=0; id< m_vector.rows(); ++id)
        {
            if (op(m_vector(id), m_rhs)) {
                is_true = true;
                break;
            }
        }
        return is_true;
    }

private:
    BinaryOpTag op; //!< The operator
    const VectorT<ScalarT>& m_vector; //!< The vector to test
    ScalarT m_rhs; //!< The value to test against
};


//! \brief True if all elements of a vector are bounded
//!
//! Bounds must be finite
template <typename ScalarT>
class BoundedExpr:
        public BaseBoolExpr<BoundedExpr<ScalarT>>
{
public:
    //! \brief Constructor
    //!
    //! \param vector the vector to check
    //! \param lower_bound the value of the lower bound
    //! \param upper_bound the value of the upper bound
    BoundedExpr(
            const VectorT<ScalarT>& vector,
            ScalarT lower_bound,
            ScalarT upper_bound
            ):
        m_vector(vector),
        m_lower(lower_bound),
        m_upper(upper_bound)
    {}
    //! \brief Implementation of the check
    bool out_impl() const
    {
        bool is_true = true;
        for (typename VectorT<ScalarT>::Index id=0; id< m_vector.rows(); ++id)
        {
            const ScalarT& value = m_vector(id);
            if (not
                (std::isfinite(value) &&
                 value >= m_lower     &&
                 value <= m_upper)
                ) {
                is_true = false;
                break;
            }
        }
        return is_true;
    }

private:
    const VectorT<ScalarT>& m_vector; //!< The vector to test
    ScalarT m_lower; //!< The lower bound
    ScalarT m_upper; //!< The upper bound
};


} // end namespace expr
} // end namespace vector_checker
} // end namespace specmicp

#endif // SPECMICP_COMMON_VECTORCHECKER_EXPR_HPP
