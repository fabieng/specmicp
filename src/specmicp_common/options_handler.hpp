/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_UTILS_OPTIONSHANDLER_HPP
#define SPECMICP_UTILS_OPTIONSHANDLER_HPP

/*!

\file options_handler.hpp
\brief Base class for handling options in a solver

Options are contained in a struct.

Example :
\code{.cpp}
struct MyOptions {
    // define stuff
};

class MySolver:
    public OptionsHandler<MyOptions>
{
    // do stuff
};
\endcode

 */

#include <utility>

namespace specmicp {

//! \brief Base class that handles an option struct.
//!
//! \tparam OptionsClass A structure containing options
template <class OptionsClass>
class OptionsHandler
{
public:
    //! \brief Initialize with the default options
    OptionsHandler() {}

    //! \brief Initialise the options struct using the arguments provided
    //!
    //! It will call the OptionsClass(args...) constructor
    template <typename... Args>
    OptionsHandler(Args... args):
        m_options(args...)
    {}

    //! \brief Initialize the options by copying them from the argument
    OptionsHandler(const OptionsClass& options):
        m_options(options)
    {}

    //! \brief Return a reference to the options
    OptionsClass& get_options() {return m_options;}
    //! \brief Return a const reference to the options
    const OptionsClass& get_options() const {return m_options;}

    //! \brief Set the options
    void set_options(const OptionsClass& options) {m_options = options;}

    //! \brief Swap the options with another set
    //!
    //! May be useful for temporary change
    void swap(OptionsClass& options) {std::swap(m_options, options);}

    //! \brief Reset to default options
    void reset_to_default() {m_options = OptionsClass();}

private:
    OptionsClass m_options; //!< The options
};

} // end namespace specmicp

#endif // SPECMICP_UTILS_OPTIONSHANDLER_HPP
