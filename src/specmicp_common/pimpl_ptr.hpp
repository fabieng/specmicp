/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_UTILS_PIMPLPTR_HPP
#define SPECMICP_UTILS_PIMPLPTR_HPP

//! \file pimpl_ptr.hpp
//! \brief Pointer to implementation interface with deep const-correctness
//!
//! The main point of this interface is to forward the constness
//! of the operations.

#include <utility>

namespace specmicp {
namespace utils {

//! \brief Pointer to implementation interface with deep const-correctness
//!
//! This is a 'unique_ptr' like class to contain a pointer
//! but it forwards the constness.
template <typename T>
class pimpl_ptr
{
public:
    //! \brief Create an empty implementation
    pimpl_ptr():
        m_ptr(nullptr)
    {}

    //! \brief Create a new pimpl from a pointer
    pimpl_ptr(T* pointer):
        m_ptr(pointer)
    {}

    //! \brief Copy constructor
    pimpl_ptr(const pimpl_ptr<T>& other);
    //! \brief Move constructor
    pimpl_ptr(pimpl_ptr<T>&& other);
    //! \brief Assignement operator
    pimpl_ptr<T>& operator=(const pimpl_ptr<T>& other);
    //! \brief Assignement move operator
    pimpl_ptr<T>& operator=(const pimpl_ptr<T>&& other);

    ~pimpl_ptr() {
        if (m_ptr != nullptr)  delete m_ptr;
    }

    //! \brief Mutable dereference
    T& operator* () {return *m_ptr;}

    //! \brief Const dereference
    const T& operator* () const {return m_ptr;}

    //! \brief Mutable call to member
    T * operator-> () {return m_ptr;}

    //! \brief Const call to member
    const T* operator-> () const {return m_ptr;}

    //! \brief Return the owned pointer
    T* get() {
        return m_ptr;
    }

private:
    T* m_ptr {nullptr};
};

//! \brief Make a new pimpl ptr
template <typename T, typename ...Args>
pimpl_ptr<T> make_pimpl(Args&&...args)
{
    return pimpl_ptr<T>(new T(std::forward<Args>(args)...));
}


//! \brief Assignement operator
template <typename T>
pimpl_ptr<T>& pimpl_ptr<T>::operator=(const pimpl_ptr<T>& other)
{
    *m_ptr = *other.m_ptr;
    return *this;
}


//! \brief Assignement move operator
template <typename T>
pimpl_ptr<T>& pimpl_ptr<T>::operator=(const pimpl_ptr<T>&& other)
{
    *m_ptr = std::move(*other.m_ptr);
    return *this;
}

template <typename T>
pimpl_ptr<T>::pimpl_ptr(const pimpl_ptr<T>& other)
{
    *m_ptr = *other.m_ptr;
}


template <typename T>
pimpl_ptr<T>::pimpl_ptr(pimpl_ptr<T>&& other)
{
    if (m_ptr != nullptr)
        delete m_ptr;
    m_ptr = other.m_ptr;
    other.m_ptr = nullptr;
}

} //end namespace utils
} //end namespace specmicp

#endif // SPECMICP_UTILS_PIMPLPTR_HPP
