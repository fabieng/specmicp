/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_COMMON_PLUGIN_DYNAMICLIBRARY_HPP
#define SPECMICP_COMMON_PLUGIN_DYNAMICLIBRARY_HPP

#include <string>
#include <vector>
#include <memory>

/*!
\file dynamic_library.hpp
\brief Dynamic library loader

Load the libraries and the symbols within.

\warning Only implemented for POSIX platforms

\code{.cpp}
std::string error_str;
auto lib = DynamicLibraryPtr::load(<path>, error_str);
if (lib == nullptr) {
    ERROR << error_str;
}
void* symbol = lib.get_symbol("func_name", error_str)
if (symbol == nullptr) {
    ERROR << error_str;
}
\endcode
 */
namespace specmicp {
namespace plugins {

class DynamicLibrary;
//! \brief Pointer to a dynamic library
//!
//! All this stuff should be managed in only one place
//!
//! \internal
using DynamicLibraryPtr = std::unique_ptr<DynamicLibrary>;

//! \brief A dynamic library
//!
//! Open a dynamic library. It is closed at the destruction of the object.
//!
//! \internal
class DynamicLibrary
{
public:

    //! \brief Create a dynamic library
    static DynamicLibraryPtr load(
            const std::string& path,
            std::string& error
            );

    //! \brief Create a dynamic library
    static DynamicLibraryPtr load(
            const std::string& path,
            const std::vector<std::string>& list_dir,
            std::string& error
            );

    ~DynamicLibrary();

    //! \brief Return a symbol
    void* get_symbol(const std::string& name, std::string& error);

private:
    // Cannot be created directly
    DynamicLibrary(void* handle);

    // Non copyable
    DynamicLibrary(const DynamicLibrary& other) = delete;
    DynamicLibrary& operator=(const DynamicLibrary& other) = delete;

    void* m_handle; //!< The library handle
};


} //end namespace plugins
} //end namespace specmicp

#endif // SPECMICP_COMMON_PLUGIN_DYNAMICLIBRARY_HPP
