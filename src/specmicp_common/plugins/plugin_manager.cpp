/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "plugin_manager.hpp"

#include "plugin_api_version.h"
#include "module_base.hpp"
#include "dynamic_library.hpp"
#include "plugin_base.hpp"

#include "specmicp_common/filesystem.hpp"
#include "specmicp_common/compat.hpp"
#include "specmicp_common/log.hpp"

#include <unordered_map>
#include <stdexcept>


namespace specmicp {
namespace plugins {

PluginManager& get_plugin_manager()
{
    static PluginManager manager;
    return manager;

}

struct PluginManager::PluginManagerImpl
{
    PluginManagerImpl() {};

    void set_services(PluginManager* parent);

    std::vector<DynamicLibraryPtr> m_libraries;
    std::vector<std::unique_ptr<PluginBase>> m_plugins;
    std::unordered_map<std::string, std::unique_ptr<ModuleBase>> m_modules;
    std::vector<std::string> m_dir_paths;

    PluginManagerServices m_services;
};

PluginManager::PluginManager():
    m_impl(make_unique<PluginManagerImpl>())
{
    m_impl->set_services(this);
    add_plugin_directory(utils::get_current_directory());
}

PluginManager::~PluginManager() = default;

ModuleBase* PluginManager::get_module(const std::string& name)
{
    ModuleBase* mod = m_impl->m_modules[name].get();
    return mod;
}

void* PluginManager::get_object(
        const std::string& module,
        const std::string& name
        )
{
    ModuleBase* mod = get_module(module);
    if (mod == nullptr) {
        throw std::runtime_error("No such plugin module : " + module + ".");
    }
    return mod->get_object(name);
}

bool PluginManager::load_plugin(const std::string& filepath)
{
    std::string error;
    DynamicLibraryPtr lib = DynamicLibrary::load(
                filepath, m_impl->m_dir_paths, error);
    if (lib == nullptr) {
        ERROR << "Error while loading shared library :\n" << error;
        return false;
    }

    void* initter = lib->get_symbol("SPC_plugin_init", error);

    if (initter == nullptr) {
        ERROR << "Error while loading shared library :\n" << error;
        return false;
    }

    void* plugin = ((void* (*)())(initter))();

    if (plugin == nullptr) {
        ERROR << "Error while loading shared library :\n"
              << "Failed to initialize plugin.";
        return false;
    }


    auto plugin_base_ptr =
            std::unique_ptr<PluginBase>(static_cast<PluginBase*>(plugin));

    auto retcode = check_version(plugin_base_ptr->get_api_version());
    if (not retcode) {return false;}

    retcode = plugin_base_ptr->initialize(m_impl->m_services);
    if (not retcode) {return false;}

    // only register if no problem was detected
    m_impl->m_libraries.emplace_back(nullptr);
    auto ind = m_impl->m_libraries.size()-1;
    m_impl->m_libraries[ind].swap(lib);
    m_impl->m_plugins.emplace_back(plugin_base_ptr.release());

    return true;
}


bool PluginManager::check_version(const PluginAPIVersion& api_version)
{

    if (    (api_version.v_major == 0)
        and (api_version.v_minor == 0)
        and (api_version.v_patch == 0)
       )
    {
        ERROR << "API version of the plugin was not initialized";
        return false;
    }

    return true;
}

bool PluginManager::register_object(
        const std::string& module,
        const std::string& name,
        object_factory_f func
        )
{
    auto* mod = get_module(module);
    return mod->register_object(name, func);
}


bool PluginManager::register_module(
        const std::string& module_name,
        std::unique_ptr<ModuleBase>&& module
        )
{
    m_impl->m_modules.emplace(module_name, std::unique_ptr<ModuleBase>(module.release()));
    return true;
}

void PluginManager::add_plugin_directory(
        const std::string& directory_name
        )
{
    if (not utils::is_directory(directory_name)) {
        WARNING << "'" << directory_name
                << "' is not a valid directory";
    }
    m_impl->m_dir_paths.push_back(directory_name);
}


void PluginManager::add_plugin_directories(
        const std::vector<std::string>& directory_names
        )
{
    for (const auto& dir: directory_names) {
        add_plugin_directory(dir);
    }
}

std::vector<std::string> PluginManager::get_plugin_directories()
{
    return m_impl->m_dir_paths;
}



void PluginManager::PluginManagerImpl::set_services(PluginManager* parent)
{
    m_services.api_version = PluginAPIVersion(
                SPECMICP_API_VERSION_MAJOR,
                SPECMICP_API_VERSION_MINOR,
                SPECMICP_API_VERSION_PATCH
                );

    m_services.register_module = [parent](
            const std::string& module_name,
            std::unique_ptr<ModuleBase>&& module_ptr
            ) {
        return parent->register_module(
                    module_name,
                    std::forward<std::unique_ptr<ModuleBase>>(module_ptr)
                    );
    };

    m_services.register_object = [parent](const std::string& module,
                                          const std::string& object_name,
                                          object_factory_f factory) {
        return parent->register_object(module, object_name, factory);
    };

}


} //end namespace plugins
} //end namespace specmicp
