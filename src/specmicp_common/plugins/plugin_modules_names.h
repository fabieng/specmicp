/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_PLUGINS_MODULES_NAMES_H
#define SPECMICP_PLUGINS_MODULES_NAMES_H

//! \file plugins/plugin_modules_names.h
//! \brief Define the names of the plugins modules

#ifndef SPC_DOXYGEN_SHOULD_SKIP_THIS

// SpecMiCP
// ========
#define SPC_PLUGIN_SPECMICP_BASE "specmicp/"


// ReactMiCP
// =========
#define SPC_PLUGIN_REACTMICP_BASE "reactmicp/"

// Unsaturated system
// ------------------
#define SPC_PLUGIN_REACTMICP_UNSATURATED_BASE \
    SPC_PLUGIN_REACTMICP_BASE "unsaturated/"

// Name of the plugin module for the variables of the unsaturated system
#define SPC_PLUGIN_REACTMICP_UNSATURATED_VARIABLES \
    SPC_PLUGIN_REACTMICP_UNSATURATED_BASE  "variables"

// Name of the plugin module for the upscaling stagger of the unsaturated system
#define SPC_PLUGIN_REACTMICP_UNSATURATED_UPSCALING \
    SPC_PLUGIN_REACTMICP_UNSATURATED_BASE "upscaling"

#endif // SPC_DOXYGEN_SHOULD_SKIP_THIS

#endif // SPECMICP_PLUGINS_MODULES_NAMES_H
