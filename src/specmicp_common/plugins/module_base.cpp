/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "module_base.hpp"

#include "specmicp_common/compat.hpp"

namespace specmicp {
namespace plugins {

struct ModuleBase::ModuleBaseImpl {
    std::unordered_map<std::string, object_factory_f> m_objects;
};

ModuleBase::ModuleBase():
    m_impl(make_unique<ModuleBaseImpl>())
{

}

ModuleBase::~ModuleBase() = default;


std::unique_ptr<ModuleBase> ModuleBase::create_module() {
    auto module = make_unique<ModuleBase>();
    return module;
}

bool ModuleBase::register_object(
        const std::string& name,
        object_factory_f func
        )
{
    m_impl->m_objects.insert({name, func});
    return true;
}

void* ModuleBase::get_object(const std::string& name)
{
    void* obj;
    try {
        obj = m_impl->m_objects.at(name)();
    }
    catch (const std::out_of_range& e) {
        throw std::invalid_argument("PluginManager error : "
                                    "no plugin with name '"
                                    + name + "'.");
    }
    return obj;
}

} //end namespace plugins
} //end namespace specmicp
