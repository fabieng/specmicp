/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_PLUGIN_PLUGINBASE_HPP
#define SPECMICP_PLUGIN_PLUGINBASE_HPP

/*!
\file plugin_base.hpp
\brief Base class for a plugin

Example of how to declare a new plugin :

\code{.cpp}
// necessary headers
#include "utils/plugins/plugin_interface.h"
#include "utils/plugins/plugin_base.hpp"
#include "utils/plugins/module_base.hpp"

// First declare the plugin
class ExamplePlugin: public specmicp::plugins::PluginBase
{
public:
    ExamplePlugin():
        PluginBase()
    {
        // set required version
        set_api_version(0, 1, 0);
    }

    ~ExamplePlugin() = default // The destructor can be used to free memory if needed

    bool initialize(const specmicp::plugins::PluginManagerServices& services) override
    {
        // register a new module
        auto test_module = specmicp::plugins::ModuleBase::create_module();
        auto retcode = services.register_module("test_module", std::move(test_module));
        if (not retcode) {
            return false;
        }

        // register a new object
        auto factory = []() {return new TestObject();
            };
        retcode = services.register_object("test_module", "test_object", factory);
        if (not retcode) {
            return false;
        }

        return true;
    }
};

// Then we register the plugin
SPECMICP_PLUGIN(BasicPlugin);
\endcode
 */

#include "plugin_types.hpp"

namespace specmicp {
namespace plugins {

//! \brief Base class for a plugin
class SPECMICP_DLL_PUBLIC PluginBase
{
public:
    PluginBase() {}

    //! \brief Initialize the plugin
    virtual bool initialize(const PluginManagerServices& services) = 0;

    //! \brief Return the API version used by the plugin
    PluginAPIVersion get_api_version() {
        return m_version;
    }

    virtual ~PluginBase() = default;

protected:
    //! \brief Set the API version
    void set_api_version(
            api_version_t major,
            api_version_t minor,
            api_version_t patch);
private:
    //! \brief The API version
    PluginAPIVersion m_version;
};

} //end namespace plugins
} //end namespace specmicp

#endif // SPECMICP_PLUGIN_PLUGINBASE_HPP
