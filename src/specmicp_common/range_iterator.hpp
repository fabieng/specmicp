/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_UTILS_RANGEITERATOR_HPP
#define SPECMICP_UTILS_RANGEITERATOR_HPP

// /!\ this file is included in types.hpp

/*!
\file range_iterator.hpp
\brief Iterator over a range

This is similar to the boost:range_iterator, but without having to use boost

Example :
\code{.cpp}
for (auto x: range(2, 10)) {
    # do something with x
}
\endcode

 */


namespace specmicp {

//! \namespace specmicp::interator_impl
//! \brief Implementation of the iterator
//! \internal
namespace iterator_impl {

//! \brief Iterator over a range of integers
template <typename T>
class IntegralIterator;

//! \brief Constant iterator
template <typename T>
class ConstIntegralIterator;

} //end namespace iterator_impl

//! \brief An iterator over a range
//!
//! \tparam T an integral type
template <typename T>
class RangeIterator
{
public:
    //! \brief type of an iterator
    using iterator = iterator_impl::IntegralIterator<T>;
    //! \brief type of a const iterator
    using const_iterator = const iterator_impl::ConstIntegralIterator<T>;

    //! \brief Construct a range iterator
    RangeIterator(T start, T end):
        m_start(start),
        m_end(end)
    {}

    //! \brief Construct a range iterator
    RangeIterator(T end):
        m_start(0),
        m_end(end)
    {}

    //! \brief Construct a range iterator from two iterators
    RangeIterator(const iterator& start, const iterator& end):
        m_start(*start),
        m_end(*end)
    {}

    //! \brief Construct a range iterator from an iterator
    RangeIterator(const iterator& end):
        m_start(0),
        m_end(*end)
    {}

    //! \brief Return an iterator to the beginning of the sequence
    iterator begin() const;
    //! \brief Return an iterator to the end of the sequence
    iterator end() const;

    //! \brief Return a constant iterator to the beginning of the sequence
    const_iterator cbegin() const;
    //! \brief Return a constant iterator to the end of the sequence
    const_iterator cend() const;

private:
    T m_start;
    T m_end;
};

//! \brief Return a range of numbers to be use in a for loop
template <typename T>
RangeIterator<T> range(T start, T end) {
    return RangeIterator<T>(start, end);
}
//! \brief Return a range of numbers to be use in a for loop
template <typename T>
RangeIterator<T> range(T end) {
    return RangeIterator<T>(end);
}

} // end namespace specmicp

// implementation
#include "range_iterator.inl"

#endif // SPECMICP_UTILS_RANGEITERATOR_HPP
