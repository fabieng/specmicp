/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMCICP_MACROS_HPP
#define SPECMCICP_MACROS_HPP

#include <cassert>

//! \file macros.hpp
//! \brief Macros and config

//! \var ndebug
//! \brief true of NDEBUG is defined (i.e. false in DEBUG mode)

// a lot of the eigen stuff may raise an error if in Debug mode
#ifdef NDEBUG
constexpr bool ndebug { false };
#else
constexpr bool ndebug { true };
#endif

//! \def NOEXCEPT
//! \brief Tag a function to be noexcept when in non-debug mod but can raise exception in debug mod
#define NOEXCEPT noexcept(ndebug)

//! \def specmicp_assert
//! \brief Assertion macro used in SpecMiCP
//!
//! This is the assertion macro used in SpecMiCP.
//! It can be selectively disabled by declaring SPECMICP_NO_DEBUG.
#ifdef SPECMICP_NO_DEBUG
#define specmicp_assert
#else
#define specmicp_assert(x) assert(x)
#endif // SPECMICP_NO_DEBUG


// deprecation
// -----------
//! \def SPECMICP_DEPRECATED
//! \brief Declare a symbol to be deprecated
//!
//! This symbol will be removed shortly. Do not use it.
#if defined(__GNUC__) && (__GNUC__ > 4 || ((__GNUC__ == 4) && (__GNUC_MINOR__ >= 5)))
    #define SPECMICP_DEPRECATED(message) __attribute__((deprecated(message)))
#elif defined(__clang__) && defined(__has_feature)
    #if __has_feature(attribute_deprecated_with_message)
        #define SPECMICP_DEPRECATED(message)  __attribute__ ((deprecated(message)))
    #endif
#else
    #define SPECMICP_DEPRECATED
#endif


// visibility
// ----------
//! \def SPECMICP_DLL_PUBLIC
//! \brief Declare a symbol to be exported in a shared library
//!
//! Only symbols defined with this macro will be exported in the shared library.
//! The others symbols are hidden.
//!
//! \sa SPECMICP_DLL_LOCAL

//! \def SPECMICP_DLL_LOCAL
//! \brief Declare a symbol to be hidden in a shared library
//!
//! This symbols are not available to the user. They are not part of the API.
//!
//! \sa SPECMICP_DLL_PUBLIC
#if defined(_WIN32) || defined(__CYGWIN__)
    #ifdef _EXPORTING
        #ifdef __GNUC__
            #define SPECMICP_DLL_PUBLIC __attribute__((dllexport))
        #else
            #define SPECMICP_DLL_PUBLIC __declspec(dllexport)
        #endif
    #else
        #ifdef __GNUC__
            #define SPECMICP_DLL_PUBLIC __attribute__((dllimport))
        #else
            #define SPECMICP_DLL_PUBLIC __declspec(dllimport)
        #endif
    #endif
#else
    #if __GNUC__ >= 4
        #define SPECMICP_DLL_PUBLIC __attribute__ ((visibility("default")))
        #define SPECMICP_DLL_LOCAL __attribute__ ((visibility("hidden")))
    #else
        #define SPECMICP_DLL_PUBLIC
        #define SPECMICP_DLL_LOCAL
    #endif
#endif

// likely / unlikely
// -----------------
//! \def likely
//! \brief Branch prediction information for the compiler
//!
//! Inform the compiler that this pbranch is very likely
//!
//! \sa unlikely

//! \def unlikely
//! \brief Branch prediction information for the compiler
//!
//! Inform the compiler that this branch is unlikely.
//!
//! \sa likely
#if defined(__GNUC__)
    #define likely(x)   __builtin_expect(!!(x), 1)
    #define unlikely(x) __builtin_expect(!!(x), 0)
#else
    #define likely(x)   (x)
    #define unlikely(x) (x)
#endif



// Pure function
// ---------------
//! \def SPECMICP_PURE_F
//! \brief Define a function to have no side effect (but can read global memory)

#ifdef __GNUC__
    #define SPECMICP_PURE_F __attribute__ ((pure))
#else
    #define SPECMICP_PURE_F
#endif


//! \def SPECMICP_CONST_F
//! \brief Define a function to have no side effect (but can NOT read global memory)

#ifdef __GNUC__
    #define SPECMICP_CONST_F __attribute__ ((const))
#else
    #define SPECMICP_CONST_F
#endif


#endif // SPECMCICP_MACROS_HPP
