
/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "log_impl.hpp"

#include "macros.hpp"
#include <iostream>


#ifndef SPC_DOXYGEN_SHOULD_SKIP_THIS

namespace specmicp {
namespace logger {

// Explicit instanciation
template class Log<ErrFile>;
template class Log<ConfFile>;

template <>
std::ostringstream& Log<ErrFile>::get(LogLevel level)
{
    msg << to_string(level) << " : ";
    return msg;
}

template <>
std::ostringstream& Log<ConfFile>::get(LogLevel level)
{
    msg << to_string_conf(level);
    return msg;
}

template <>
LogLevel& Log<ErrFile>::ReportLevel(){
    static LogLevel report_level = Debug;
    return report_level;
}

std::ostream*& ErrFile::stream()
{
    static std::ostream* stream = nullptr;
    return stream;
}


void ErrFile::output(const std::string &msg)
{
    std::ostream* out = stream();
    specmicp_assert(out != nullptr);
    (*out) << msg << std::endl;
    out->flush();
}

template <>
LogLevel& Log<ConfFile>::ReportLevel(){
    static LogLevel report_level = Info;
    return report_level;
}

std::ostream*& ConfFile::stream()
{
    static std::ostream* stream = nullptr;
    return stream;
}

void ConfFile::output(const std::string& msg)
{
    std::ostream* out = stream();
    specmicp_assert(out != nullptr);
    (*out) << msg << std::endl;
    out->flush();
}

std::string to_string(LogLevel level)
{
    static const char* list_level[] = {"CRITICAL", "ERROR", "Warning", "info", "debug", "spam"};
    return list_level[static_cast<int>(level)];
}

std::string to_string_conf(LogLevel level)
{
    static const char* list_level[] = {"CRITICAL : ", "ERROR : ", "Warning : ", "", "", ""};
    return list_level[static_cast<int>(level)];
}


} //end namespace logger


//! \brief Standard logger type
using stdlog = logger::Log<logger::ErrFile>;
using conflog = logger::Log<logger::ConfFile>;

void init_logger(std::ostream* out, specmicp::logger::LogLevel level)
{
    logger::ErrFile::stream() = out;
    stdlog::ReportLevel() = level;
}

void init_conf_logger(std::ostream* out)
{
    logger::ConfFile::stream() = out;
}

void log_and_throw_runtime_error(const std::string& error_msg)
{
    stdlog().get(logger::LogLevel::Error) << error_msg;
    throw std::runtime_error(error_msg);
}

} //end namespace specmicp

#endif // SPC_DOXYGEN_SHOULD_SKIP_THIS
