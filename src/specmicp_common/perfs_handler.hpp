/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_UTILS_PERFSHANDLER_HPP
#define SPECMICP_UTILS_PERFSHANDLER_HPP

/*!

\file perfs_handler.hpp
\brief BaseClass for a class managing performance struct

A performance struct containts information such as the number of iterations,
 the return code and any relevant information provided by a solver.

It is used like this :
\code{.cpp}
struct MyPerf {
 // define iterations
};

class MySolver:
    public PerformanceHandler<MyPerf>
{
  // blah blah
};
\endcode
 */

#include <utility>

namespace specmicp {

//! \brief Handle a performance struct for the algorithms
//!
//! Like options handler but for the performance
template <class PerformanceStruct>
class PerformanceHandler
{
public:
    //! \brief Return the performance (read-only)
    const PerformanceStruct& get_perfs() const {return m_perfs;}

    //! \brief Reset the performance
    void reset_perfs() {m_perfs = PerformanceStruct();}

    //! \brief Return a reference to the performance
    PerformanceStruct& get_perfs() {return m_perfs;}

protected:
    //! \brief Swap the performance with another
    void swap(PerformanceStruct& other) {std::swap(m_perfs, other);}
private:
    PerformanceStruct m_perfs;
};

} // end namespace specmicp

#endif // SPECMICP_UTILS_PERFSHANDLER_HPP
