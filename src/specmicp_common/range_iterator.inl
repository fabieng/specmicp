/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "range_iterator.hpp" // syntaxic coloration only...

//! \file specmicp_common/range_iterator.inl
//! \brief header implementation of range iterators

namespace specmicp {

namespace iterator_impl {

//! \brief  Iterator over a range of integers
//!
//! \internal
template <typename T>
class IntegralBaseIterator
{
public:
    using value_type = T;
    using reference = T&;

    //! \brief Constructor
    explicit IntegralBaseIterator(T value):
        m_value(value)
    {}
    //! \brief Less than operator
    bool operator< (const IntegralBaseIterator& other) {
        return (m_value < other.m_value);
    }
    //! \brief Less than or equal operator
    bool operator<= (const IntegralBaseIterator& other) {
        return (m_value <= other.m_value);
    }
    //! \brief Greater than operator
    bool operator> (const IntegralBaseIterator& other) {
        return (m_value > other.m_value);
    }
    //! \brief Greater than or equal operator
    bool operator>= (const IntegralBaseIterator& other) {
        return (m_value >= other.m_value);
    }
    //! \brief Equal operator
    bool operator== (const IntegralBaseIterator& other) {
        return (m_value == other.m_value);
    }
    //! \brief Not equal operator
    bool operator!= (const IntegralBaseIterator& other) {
        return (m_value != other.m_value);
    }

    //! \brief increment operator
    value_type operator++ (int) {
        T tmp = m_value;
        ++m_value;
        return tmp;
    }

protected:
    value_type m_value; //!< Current Value
};

//! \brief Iterator over an integral range
template <typename T>
class IntegralIterator: public IntegralBaseIterator<T>
{
public:
    //! \brief Creates an iterator over an integer value
    explicit IntegralIterator(T value):
        IntegralBaseIterator<T>(value)
    {}

    //! \brief Returns the value of the iterator
    T& operator* () {
        return IntegralBaseIterator<T>::m_value;
    }

    //! \brief Increment the iterator
    IntegralIterator& operator++ () {
        ++(IntegralBaseIterator<T>::m_value);
        return *this;
    }
};

//! \brief A const iterator over an integral range
template <typename T>
class ConstIntegralIterator: public IntegralBaseIterator<T>
{
public:
    //! \brief Creates an iterator over an integer value
    explicit ConstIntegralIterator(T value):
        IntegralBaseIterator<T>(value)
    {}

    //! \brief Returns the value of the iterator
    T operator* () const {
        return IntegralBaseIterator<T>::m_value;
    }

    //! \brief Increment the iterator
    ConstIntegralIterator& operator++ () {
        ++(IntegralBaseIterator<T>::m_value);
        return *this;
    }
};

} //end namespace iterator_impl

template <typename T>
typename RangeIterator<T>::iterator RangeIterator<T>::begin() const {
    return RangeIterator<T>::iterator(m_start);
}

template <typename T>
typename RangeIterator<T>::iterator RangeIterator<T>::end() const {
    return RangeIterator<T>::iterator(m_end);
}

template <typename T>
typename RangeIterator<T>::const_iterator RangeIterator<T>::cbegin() const {
    return RangeIterator<T>::iterator(m_start);
}

template <typename T>
typename RangeIterator<T>::const_iterator RangeIterator<T>::cend() const {
    return RangeIterator<T>::iterator(m_end);
}


} // end namespace specmicp
