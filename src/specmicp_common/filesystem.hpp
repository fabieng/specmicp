/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_UTILS_FILESYSTEM
#define SPECMICP_UTILS_FILESYSTEM

//! \file filesystem.hpp
//! \brief helper functions to deal with the filesystem

#include "macros.hpp"
#include <string>
#include <vector>
#include <array>

namespace specmicp {
namespace utils {

//! \brief Check that the path exists
bool SPECMICP_DLL_PUBLIC path_exists(const std::string& path);

//! \brief Check that the directory exist
bool SPECMICP_DLL_PUBLIC is_directory(const std::string& path);

//! \brief Check that the directory exist
bool SPECMICP_DLL_PUBLIC is_file(const std::string& path);

//! \brief Return the current directory
std::string SPECMICP_DLL_PUBLIC get_current_directory();

//! \brief Complete a path from a given directory and a file
//!
//! \param dir the directory
//! \param file the basename of the file
std::string SPECMICP_DLL_PUBLIC complete_path(
        const std::string& dir,
        const std::string& file
        );

//! \brief Return true if the given path is absolute
//!
//! \param path the path to test
bool SPECMICP_DLL_PUBLIC is_path_absolute(const std::string& path);

//! \brief Return an absolute path from a relative one
//!
//! \param[in] rel_path a path
//! \param[out] error a string to contain error message if needed
//! \return An aboslute path, or an empty string if an error occured
//!
//! If an error is detected then the return string will be empty,
//! and error will contain a message about the error
std::string SPECMICP_DLL_PUBLIC relative_to_absolute(
        const std::string& rel_path,
        std::string& error
        );

//! \brief Return the complete path to a file from a set of directories
//!
//! \param filename basename of the file to seek
//! \param directories list of directories where to search the file
//! \return an absolute path, or an empty string if the file wasn't found
std::string SPECMICP_DLL_PUBLIC find_path(
        std::string filename,
        const std::vector<std::string>& directories
        );

//! \brief Split basename
//!
std::array<std::string, 3> SPECMICP_DLL_PUBLIC split_filepath(
        const std::string& in);

//! \brief Remove a file
//!
//! \throw runtime_error if an error occurs.
void SPECMICP_DLL_PUBLIC remove_file(const std::string& filepath);

//! \brief Rename a file
//!
//! \param old_name current name of the file
//! \param new_name new name for the file
//! \param check_new_name_exists if true, and new name exists raises an error
//!
//! \throw runtime_error if an error occurs.
void SPECMICP_DLL_PUBLIC rename_file(
        const std::string& old_name,
        const std::string& new_name,
        bool check_new_name_exists = true
        );

//! \brief Return true if the environment variable is defined
//!
//! \param env_var the environment variable to check
bool SPECMICP_DLL_PUBLIC has_env(
        const std::string& env_var
        );

//! \brief Return an environment variable
//!
//! \param env_var the environment variable
//! \return the value stored in the environment variable, or an empty string
std::string SPECMICP_DLL_PUBLIC get_env(
        const std::string& env_var
        );

//! \brief Struct containing resource usage
struct resource_usage
{
    double system_cpu_time {-1};       //!< The system CPU time
    double user_cpu_time {-1};         //!< The user CPU time
    long   max_resident_set_size {-1}; //!< The maximum size in RAM
};

//! \brief Return resource usage information
resource_usage get_resource_usage();


} // end namespace utils
} // end namespace specmicp

#endif // SPECMICP_UTILS_FILESYSTEM
