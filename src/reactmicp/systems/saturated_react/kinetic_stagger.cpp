/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "kinetic_stagger.hpp"
#include "variables.hpp"

#include "specmicp/adimensional/adimensional_system_solver.hpp"
#include "specmicp/adimensional/adimensional_system_solution_extractor.hpp"
#include "specmicp_common/log.hpp"
#include "reactmicp/solver/staggers_base/stagger_structs.hpp"

#include <iostream>

namespace specmicp {
namespace reactmicp {
namespace systems {
namespace satdiff {

using TrueConstPtr = SaturatedVariables * const;

inline TrueConstPtr cast_to_var(VariablesBase * const var) {
    return static_cast<TrueConstPtr>(var);
}

//! \brief Initialize the stagger at the beginning of an iteration
void KineticStagger::initialize_timestep(scalar_t dt, VariablesBase * const var)
{
    TrueConstPtr true_var = cast_to_var(var);
    m_dt = dt;

    // Initialize velocity using values from previous timestep
    for (index_t node=0; node<true_var->nb_nodes(); ++node)
    {
        if (true_var->is_fixed_composition(node)) continue;
        scalar_t alpha = 1.0;
        for (index_t component: true_var->get_database()->range_aqueous_component())
        {
            alpha = std::max(alpha,
                             0.9*dt*true_var->solid_concentration(node, component, true_var->chemistry_rate())
                               /(true_var->solid_concentration(node, component, true_var->displacement()))
                             );
        }
        auto solid_velocity = true_var->velocity().segment(
                    true_var->offset_node(node)+true_var->offset_solid_concentration(),
                    true_var->nb_component());
        auto solid_chemistry_rate = true_var->chemistry_rate().segment(
                    true_var->offset_node(node)+true_var->offset_solid_concentration(),
                    true_var->nb_component());
        solid_velocity = 1/alpha * solid_chemistry_rate;
    }

    m_model->initialize_timestep(dt, true_var->kinetic_variables());
    for (index_t node=0; node<true_var->nb_nodes(); ++node)
    {
        if (true_var->is_fixed_composition(node)) continue;
        m_model->restart_timestep(node, true_var->equilibrium_solution(node), true_var->kinetic_variables());

        for (index_t component=0; component<true_var->nb_component(); ++component)
        {

            true_var->aqueous_concentration(node, component, true_var->chemistry_rate()) = -
                    m_model->get_velocity_kinetic(node, component, true_var->kinetic_variables());
            true_var->aqueous_concentration(node, component, true_var->velocity()) += (
                    //true_var->aqueous_concentration(node, component, true_var->transport_rate())
                    + true_var->aqueous_concentration(node, component, true_var->chemistry_rate())
                    //- true_var->solid_concentration(node, component, true_var->chemistry_rate())
                    );


            true_var->aqueous_concentration(node, component, true_var->displacement()) =
                    true_var->aqueous_concentration(node, component, true_var->predictor()) +
                    m_dt*true_var->aqueous_concentration(node, component, true_var->velocity());
        }
    }
}

//! \brief Solve the equation for the timestep
solver::StaggerReturnCode KineticStagger::restart_timestep(
        VariablesBase * const var)
{
    TrueConstPtr true_var = cast_to_var(var);
    int failed_chemistry = 0;
#ifdef SPECMICP_USE_OPENMP
#pragma omp parallel  default(none) shared(failed_chemistry)
    // node true_var being const is shared by default, can't be mentionned again
    {
#pragma omp for schedule(dynamic, 5)
        for (index_t node=0; node<true_var->nb_nodes(); ++node)
        {
            // only solve if necessary
            if (true_var->is_fixed_composition(node) or failed_chemistry > 0) continue;
            const auto retcode = solve_one_node(node, true_var);
            if (retcode > 0)
            {
                ++failed_chemistry;
            }
        }
    }
#else
    {
        for (index_t node=0; node<true_var->nb_nodes(); ++node)
        {
            if (true_var->is_fixed_composition(node)) continue;
            const auto retcode = solve_one_node(node, true_var);
            if (retcode > 0)
            {
                ++failed_chemistry;
                break;
            }
        }
    }
#endif // SPECMICP_USE_OPENMP
    if (failed_chemistry > 0)
        return solver::StaggerReturnCode::UnknownError;
    return solver::StaggerReturnCode::ResidualMinimized;
}

//!
int KineticStagger::solve_one_node(
        index_t node,
        SaturatedVariables * const true_var
        )
{
    AdimensionalSystemConstraints constraints(get_constraints(node));

    constraints.total_concentrations = true_var->total_concentrations(node);
    constraints.set_inert_volume_fraction(m_model->get_volume_fraction_kinetic(node, true_var->kinetic_variables()));

    AdimensionalSystemSolver adim_solver(true_var->get_database(),
                                         constraints,
                                         true_var->equilibrium_solution(node),
                                         m_options);
    Vector variables(true_var->equilibrium_solution(node).main_variables);
    micpsolver::MiCPPerformance perf =  adim_solver.solve(variables);

    micpsolver::MiCPSolverReturnCode retcode = perf.return_code;
    if (retcode <= micpsolver::MiCPSolverReturnCode::NotConvergedYet)
    {

        ERROR << "Failed to solve chemistry problem at node " << node
              << ", return code = " << static_cast<int>(retcode)
              << ", residual = " << perf.current_residual;
        ERROR << "Total concentration : \n" << constraints.total_concentrations;
        return  1;
    }

    true_var->equilibrium_solution(node) = adim_solver.get_raw_solution(variables);
    AdimensionalSystemSolutionExtractor extractor(true_var->equilibrium_solution(node),
                                                  true_var->get_database(),
                                                  m_options.units_set);

    for (index_t component=0; component<true_var->nb_component(); ++component)
    {
        const scalar_t c_aq = extractor.density_water()*extractor.total_aqueous_concentration(component);
        true_var->aqueous_concentration(node, component, true_var->displacement()) = c_aq;

        const scalar_t c_aq_0 = true_var->aqueous_concentration(node, component, true_var->predictor());
        const scalar_t vel_aq = (c_aq - c_aq_0)/m_dt;
        true_var->aqueous_concentration(node, component, true_var->velocity()) = vel_aq;

        const scalar_t c_sol = extractor.total_immobile_concentration(component);
        true_var->solid_concentration(node, component, true_var->displacement()) = c_sol;

        const scalar_t c_sol_0 = true_var->solid_concentration(node, component, true_var->predictor());
        const scalar_t vel_sol = (c_sol - c_sol_0)/m_dt;
        true_var->solid_concentration(node, component, true_var->velocity()) = vel_sol;
        true_var->solid_concentration(node, component, true_var->chemistry_rate()) =  vel_sol;
    }

    return 0;
}

} // end namespace satdiff
} // end namespace systems
} // end namespace reactmicp
} // end namespace specmicp
