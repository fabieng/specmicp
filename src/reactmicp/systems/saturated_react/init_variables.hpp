/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_SYSTEMS_SATURATED_INITVARIABLES_HPP
#define SPECMICP_REACTMICP_SYSTEMS_SATURATED_INITVARIABLES_HPP

#include "variablesfwd.hpp"
#include "dfpm/meshes/mesh1dfwd.hpp"
#include "specmicp_database/database_fwd.hpp"
#include "specmicp_common/physics/units.hpp"
#include "specmicp/adimensional/adimensional_system_solution.hpp"

#include <vector>

namespace specmicp {
namespace reactmicp {
namespace systems {
namespace satdiff {

//! \brief Initialize an instance of Saturated Variables
class SaturatedVariablesFactory
{
public:
    //! \brief Constructor
    //!
    //! \param the_mesh the mesh
    //! \param the_database SpecMiCP database
    //! \param the_units units of the variables and used in computation
    //! \param list_fixed_nodes list of nodes where variables are fixed
    //! \param list_initial_states a list of the different initial conditions
    //! \param index_initial_state a map combining nodes (index) to the initial conditions (value)
    SaturatedVariablesFactory(
            mesh::Mesh1DPtr the_mesh,
            RawDatabasePtr the_database,
            units::UnitsSet the_units,
            const std::vector<index_t>& list_fixed_nodes,
            const std::vector<AdimensionalSystemSolution>& list_initial_states,
            const std::vector<int>& index_initial_state
            );
    //! \brief Initialize the main vectors
    void init_size();
    //! \brief Initialize the BC
    void set_fixed_nodes(const std::vector<index_t>& list_fixed_nodes);
    //! \brief Initialize the chemistry informations
    void init_chemistry(
            units::UnitsSet the_units,
            const std::vector<int>& index_initial_state,
            const std::vector<AdimensionalSystemSolution>& list_initial_states);
    //! \brief Return the variables
    SaturatedVariablesPtr get_variable() {return m_variable;}

private:
    SaturatedVariablesPtr m_variable;
    RawDatabasePtr m_database;
    index_t nb_component;
    index_t nb_nodes;
};

//! \brief Initialise an instance of SaturatedVariables
SaturatedVariablesPtr SPECMICP_DLL_PUBLIC init_variables(
        mesh::Mesh1DPtr the_mesh,
        RawDatabasePtr the_database,
        units::UnitsSet the_units,
        const std::vector<index_t>& list_fixed_nodes,
        const std::vector<AdimensionalSystemSolution>& list_initial_states,
        const std::vector<int>& index_initial_state
        );

} // end namespace satdiff
} // end namespace systems
} // end namespace reactmicp
} // end namespace specmicp

#endif // SPECMICP_REACTMICP_SYSTEMS_SATURATED_INITVARIABLES_HPP

