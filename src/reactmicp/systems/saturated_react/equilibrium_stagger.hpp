/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_SYSTEMS_SATURATED_EQUILIBRIUMSTAGGER_HPP
#define SPECMICP_REACTMICP_SYSTEMS_SATURATED_EQUILIBRIUMSTAGGER_HPP

#include "reactmicp/solver/staggers_base/chemistry_stagger_base.hpp"
#include "variablesfwd.hpp"

#include "specmicp/adimensional/adimensional_system_solver_structs.hpp"

namespace specmicp {
namespace reactmicp {
namespace systems {
namespace satdiff {

using VariablesBase = solver::VariablesBase; //!< Type of the variables


//! \brief Solve the equilibrium problem
class SPECMICP_DLL_PUBLIC EquilibriumStagger:
        public solver::ChemistryStaggerBase
{
public:
    //! \brief Constructor with uniform chemistry
    EquilibriumStagger(index_t nb_nodes,
                       AdimensionalSystemConstraints constraints,
                       AdimensionalSystemSolverOptions options):
        m_id_constraints(nb_nodes, 0),
        m_list_constraints({constraints,}),
        m_options(options)
    {}
    //! \brief Constructor with non-uniform chemistry
    EquilibriumStagger(
            std::vector<AdimensionalSystemConstraints> list_constraints,
            std::vector<int> index_constraints,
            AdimensionalSystemSolverOptions options):
        m_id_constraints(index_constraints),
        m_list_constraints(list_constraints),
        m_options(options)
    {}

    //! \brief Initialize the stagger at the beginning of the computation
    virtual void initialize(VariablesBase * const var) override {}

    //! \brief Initialize the stagger at the beginning of the computation
    void initialize(std::shared_ptr<SaturatedVariables>& var) {}

    //! \brief Initialize the stagger at the beginning of an iteration
    virtual void initialize_timestep(
            scalar_t dt,
            VariablesBase * const var
            ) override;

    //! \brief Solve the equation for the timestep
    virtual solver::StaggerReturnCode restart_timestep(
            VariablesBase * const var
            ) override;

    //! \brief Solve the speciation problem at one node
    int solve_one_node(index_t node, SaturatedVariables * const var);

    //! \brief Return the constraints for 'node'
    //!
    //! \param node Index of the node
    AdimensionalSystemConstraints& get_constraints(index_t node) {
        return m_list_constraints[m_id_constraints[node]];
    }

private:
    scalar_t m_dt; //!< The timestep

    std::vector<int> m_id_constraints; //!< id of constraints
    std::vector<AdimensionalSystemConstraints> m_list_constraints; //!< List of constraints
    AdimensionalSystemSolverOptions m_options; //!< The SpecMiCP options
};

} // end namespace satdiff
} // end namespace systems
} // end namespace reactmicp
} // end namespace specmicp

#endif // SPECMICP_REACTMICP_SYSTEMS_SATURATED_EQUILIBRIUMSTAGGER_HPP
