/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_UNSATURATED_VARIABLESINTERFACE_HPP
#define SPECMICP_REACTMICP_UNSATURATED_VARIABLESINTERFACE_HPP

#include "types_fwd.hpp"
#include "dfpm/meshes/mesh1dfwd.hpp"
#include "specmicp_database/database_fwd.hpp"

#include <vector>
#include <memory>


//! \file variables_interface.hpp
//! \brief Interface to initialize and store variables

namespace specmicp {

class AdimensionalSystemSolutionExtractor;

namespace units {

struct UnitsSet;

} //end namespace units

namespace reactmicp {
namespace systems {
namespace unsaturated {

//! \brief Return code to check if the variables are ok
enum class VariablesValidity {
    good,
    warning,
    error,
    critical
};

class UnsaturatedVariables;

//! \brief Initialize safely a set of variables
class SPECMICP_DLL_PUBLIC VariablesInterface
{
public:
    //! \brief Create a new set of variables
    //!
    //! Use default units
    VariablesInterface(mesh::Mesh1DPtr the_mesh,
                       database::RawDatabasePtr the_database,
                       const std::vector<index_t>& component_with_gas);
    //! \brief Create a new set of variables
    VariablesInterface(mesh::Mesh1DPtr the_mesh,
                       database::RawDatabasePtr the_database,
                       const std::vector<index_t>& component_with_gas,
                       const units::UnitsSet& units_set
                       );
    //! \brief Attach variables 'var' to an interface
    VariablesInterface(std::shared_ptr<UnsaturatedVariables> vars
                       );

    ~VariablesInterface();

    //! \brief Return the variables to pass to the reactive transport solver
    solver::VariablesBasePtr get_base_variables();

    //! \brief Return the variables
    UnsaturatedVariablesPtr get_variables();

    //! \brief Return the variables
    UnsaturatedVariables* get_raw_variables();

    //! \brief Check the variables
    VariablesValidity check_variables();
    // Variables

    // Main Variables
    // ==============

    // Saturation
    // ----------

    //! \name Liquid saturation
    //! \brief The saturation of the liquid phase
    //! @{

    //! \brief Return the liquid saturation
    const Vector& get_liquid_saturation() const;
    //! \brief Set the liquid saturation at 'node'
    void set_liquid_saturation(index_t node, scalar_t value);
    //! \brief Copy 'values' to the liquid saturation vector
    //!
    //! \param values a column vector containing the values,
    //!     it's size is the number of nodes
    void set_liquid_saturation(const Vector& values);
    //! \brief Set the saturation to be constant accross the mesh
    void set_liquid_saturation(scalar_t value);
    //! @}

    // Aqueous concentration
    // ---------------------

    //! \name Aqueous concentration
    //! \brief Concentration (mol/volume) of the aqueous component
    //! @{

    //! \brief Return the aqueous concentration of 'component'
    const Vector& get_aqueous_concentration(index_t component) const;
    //! \brief Set the aqueous concentration of 'component' at 'node'
    void set_aqueous_concentration(
            index_t component,
            index_t node,
            scalar_t value
            );
    //! \brief Copy 'values' into the aqueous concentration vector
    //!  of 'component'
    //!
    //! \param component the component
    //! \param values a column vector containing the values,
    //!     it's size is the number of nodes
    void set_aqueous_concentration(index_t component, const Vector& values);
    //! \brief Set a constant aqueous concentration for 'component'
    void set_aqueous_concentration(index_t component, scalar_t value);

    //! @}

    // Partial pressure
    // --------------

    //! \name Partial pressure
    //! \brief Partial pressure for a gas
    //! @{

    //! \brief Return the partial pressure for 'component'
    const Vector& get_partial_pressure(index_t component) const;
    //! \brief Set the partial pressure of 'component' at 'node'
    void set_partial_pressure(index_t component, index_t node, scalar_t value);
    //! \brief Copy 'values' into the 'component' partial pressure vector
    void set_partial_pressure(index_t component, const Vector& values);
    //! \brief Set a constant partial pressure for 'component'
    void set_partial_pressure(index_t component, scalar_t value);

    // @}

    // Solid concentration
    // -------------------

    //! \name Solid concentration
    //! \brief Solid concentration (moles/volume)
    //! @{

    //! \brief Return the solid concentration of 'component'
    const Vector& get_solid_concentration(index_t component) const;
    //! \brief Set the solid concentration of 'component' at 'node'
    void set_solid_concentration(index_t component, index_t node, scalar_t value);
    //! \brief Copy 'values' to the 'component' solid concentration vector
    void set_solid_concentration(index_t component, const Vector& values);
    //! \brief Set a constant solid concentration for 'component'
    void set_solid_concentration(index_t component, scalar_t value);

    //! @}

    // Chemistry
    // ---------
    //! \brief initialize specmicp vars of node 'node' using 'extractor'
    void initialize_variables(
            index_t node,
            const AdimensionalSystemSolutionExtractor& extractor
            );

    // Secondary Variables
    // ===================

    // Porosity
    // --------

    //! \name Porosity
    //! \brief The porosity of the porous media
    //! @{

    //! \brief Return the porosity vector
    const Vector& get_porosity() const;
    //! \brief Set the porosity at 'node'
    void set_porosity(index_t node, scalar_t value);
    //! \brief Copy 'values' into the porosity vector
    void set_porosity(const Vector& values);
    //! \brief Set a constant porosity accross the mesh
    void set_porosity(scalar_t value);

    //! @}

    // Water aqueous concentration
    // ---------------------------

    //! \name Water aqueous concentration
    //! \brief Aqueous concentration (moles/volume) for the water

    //! \brief Return the water aqueous concentration
    const Vector& get_water_aqueous_concentration() const;
    //! \brief Set the water aqueous concentration at 'node'
    void set_water_aqueous_concentration(index_t node, scalar_t value);
    //! \brief Copy values into the water aqueous concentration vector
    void set_water_aqueous_concentration(const Vector& values);
    //! \brief Set the water aqueous concentration to be constant accross
    //! the mesh
    void set_water_aqueous_concentration(scalar_t value);

    // Liquid diffusivity
    // ------------------

    //! \name Liquid diffusion coefficient
    //! \brief Intrinsic diffusion coefficient in the liquid phase
    //!
    //! This value only depends on the microstructure, not the saturation
    //! @{

    //! \brief Return the liquid diffusion coefficient
    const Vector& get_liquid_diffusivity() const;
    //! \brief Set the liquid diffusion coefficient at 'node'
    void set_liquid_diffusivity(index_t node, scalar_t value);
    //! \brief Copy 'values' into the liquid diffusion coefficient vector
    void set_liquid_diffusivity(const Vector& values);
    //! \brief Set the liquid diffusion coefficient to be constant accross
    //!  the mesh
    void set_liquid_diffusivity(scalar_t value);

    //! @}

    // Liquid permeability
    // -------------------

    //! \name Liquid permeability
    //! \brief Intrinsic permeability of the liquid phase
    //!
    //! This value only depends on the microstructure, not the saturation
    //! @{

    //! \brief Return the liquid permeability
    const Vector& get_liquid_permeability() const;
    //! \brief Set the liquid permeability at 'node'
    void set_liquid_permeability(index_t node, scalar_t value);
    //! \brief Copy 'values' into the liquid permeability vector
    void set_liquid_permeability(const Vector& values);
    //! \brief Set the liquid permeability to be constant accross the mesh
    void set_liquid_permeability(scalar_t value);

    //! @}


    // Gas diffusivity : binary diffusion coefficient
    // ----------------------------------------------

    //! \name Gas binary diffusion coefficient
    //! \brief Binary (Gas-Air) diffusion coefficient
    //!
    //! This value is independant on the microstructure and the saturation,
    //! only dependant of the gas
    //! @{

    //! \brief Return the binary gas diffusion coefficient for the gas of
    //! 'component'
    scalar_t get_binary_gas_diffusivity(index_t component) const;
    //! \brief Set the binary gas diffusion coefficient for the gass of
    //! 'component'
    void set_binary_gas_diffusivity(index_t component, scalar_t value);

    //! @}

    // Gas diffusivity : resistance factor
    // -----------------------------------

    //! \name Gas resistance factor
    //! \brief Resistance factor for the diffusion coefficient of a gas
    //!
    //! This value only depends on the microstructure, not the saturation
    //! @{

    //! \brief Return the gas diffusion coefficient vector
    const Vector& get_resistance_gas_diffusivity() const;
    //! \brief Set the gas diffusion coefficient at 'node'
    void set_resistance_gas_diffusivity(index_t node, scalar_t value);
    //! \brief Copy 'values' into the gas diffusion coefficient vector
    void set_resistance_gas_diffusivity(const Vector& values);
    //! \brief Set the gas diffusion coefficient to be constant accross the mesh
    void set_resistance_gas_diffusivity(scalar_t value);

    //! @}

    // Advection flux
    // ---------------

    //! \name Liquid phase velocity
    //! \brief The liquid phase velocity due to the capillary pressure
    //! @{

    //! \brief Return the liquid phase velocity vector
    const Vector& get_advection_flux() const;
    //! \brief Set the liquid phase velocity at 'node'
    void set_advection_flux(index_t node, scalar_t value);
    //! \brief Copy 'values' into the liquid phase velocity vector
    void set_advection_flux(const Vector& value);
    //! \brief Set the liquid phase velocity to be constant accross the mesh
    void set_advection_flux(scalar_t value);

    //! @}

    // User models
    // -----------

    //! \name Capillary pressure
    //! \brief The macroscopic capillary pressure
    //! @{

    //! \brief Return the capillary pressure values
    const Vector& get_capillary_pressure() const;
    //! \brief Set the capillary pressure model
    void set_capillary_pressure_model(
            user_model_saturation_f capillary_pressure_model
            );

    //! @}

    //! \name Vapor pressure
    //! \brief The vapor pressure as function of the saturation
    //! @{

    //! \brief Return the water vapor pressure values
    const Vector& get_vapor_pressure() const;
    //! \brief Set the vapor pressure model
    void set_vapor_pressure_model(
            user_model_saturation_f vapor_pressure_model
            );

    //! @}

    //! \name Relative liquid diffusivity
    //! \brief The relative diffusion coefficient of the liquid phase
    //! @{

    //! \brief Return the relative liquid diffusion coefficient values
    const Vector& get_relative_liquid_diffusivity() const;
    //! \brief Set the relative liquid diffusion coefficient model
    void set_relative_liquid_diffusivity_model(
            user_model_saturation_f relative_liquid_diffusivity_model
            );

    //! @}

    //! \name Relative liquid permeability
    //! \brief The relative permeability of the liquid phase
    //! @{

    //! \brief Return the relative liquid permeability values
    const Vector& get_relative_liquid_permeability() const;
    //! \brief Set the relative liquid permeability model
    void set_relative_liquid_permeability_model(
            user_model_saturation_f relative_liquid_permeability_model
            );

    //! @}

    //! \name Relative gas diffusivity
    //! \brief The relative diffusion coefficient of the gas phase
    //!
    //! @{

    //! \brief Return the relative gas diffusion coefficient values
    const Vector& get_relative_gas_diffusivity() const;
    //! \brief Set the relative gas diffusion coefficient model
    void set_relative_gas_diffusivity_model(
            user_model_saturation_f relative_gas_diffusivity_model
            );

    //! @}

private:

    //! \brief The implementation class
    //! \internal
    struct VariablesInterfaceImpl;
    //! \brief The implementation details
    //! \internal
    std::unique_ptr<VariablesInterfaceImpl> m_impl;
};

} //end namespace unsaturated
} //end namespace systems
} //end namespace reactmicp
} //end namespace specmicp


#endif // SPECMICP_REACTMICP_UNSATURATED_VARIABLESINTERFACE_HPP
