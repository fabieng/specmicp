/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "aqueous_pressure_equation.hpp"
#include "variables_box.hpp"
#include "boundary_conditions.hpp"

#include "dfpm/meshes/mesh1d.hpp"
#include "specmicp_common/compat.hpp"
#include "specmicp_common/physics/constants.hpp"
#include "specmicp_common/physics/maths.hpp"

#include "dfpm/solver/parabolic_driver.hpp"

namespace specmicp {

namespace dfpmsolver {

// explicit template instanciation
template class ParabolicDriver<reactmicp::systems::unsaturated::AqueousGasTransportEquation>;

} //end namespace dfpmsolver

namespace reactmicp {
namespace systems {
namespace unsaturated {

static constexpr index_t no_equation     {-1};
static constexpr index_t no_eq_no_var    {-2};
static constexpr index_t not_initialized {-5};

struct SPECMICP_DLL_LOCAL AqueousGasTransportEquation::AqueousGasTransportEquationImpl
{
    uindex_t m_id_component;
    mesh::Mesh1DPtr m_mesh;
    LiquidGasAqueousVariableBox m_vars;
    std::vector<index_t> m_ideq;
    std::shared_ptr<BoundaryConditions> m_bcs;

    mesh::Mesh1D* mesh() {return m_mesh.get();}
    LiquidAqueousComponentVariableBox& vars() {return m_vars;}

    bool node_has_equation(index_t node) {
        return m_ideq[node] > no_equation;
    }

    bool node_can_flux(index_t node) {
        return m_ideq[node] > no_eq_no_var;
    }

    index_t id_equation(index_t node) {
        return m_ideq[node];
    }

    void fix_node(index_t node) {
        m_ideq[node] = no_equation;
    }

    void gas_node(index_t node) {
        m_ideq[node] = no_eq_no_var;
    }

    AqueousGasTransportEquationImpl(
            uindex_t id_component,
            mesh::Mesh1DPtr the_mesh,
            LiquidGasAqueousVariableBox the_vars,
            std::shared_ptr<BoundaryConditions> bcs
            ):
        m_id_component(id_component),
        m_mesh(the_mesh),
        m_vars(the_vars),
        m_ideq(the_mesh->nb_nodes(), not_initialized),
        m_bcs(bcs)
    {}

    void compute_transport_rate(scalar_t dt, const Vector& displacement);

    scalar_t get_bc_liquid_flux(uindex_t node) {
        return m_bcs->get_flux_liquid_dof(node, m_id_component);
    }
    scalar_t get_bc_gas_flux(uindex_t node) {
        return m_bcs->get_flux_gas_dof(node, m_id_component);
    }

    scalar_t get_diff(
            uindex_t node_0,
            uindex_t node_1,
            const scalar_t& adv
            ) const;

    uindex_t number_equations();

    scalar_t get_diff_gas(
            uindex_t node_0,
            uindex_t node_1,
            const scalar_t& adv
            ) const;

};

AqueousGasTransportEquation::AqueousGasTransportEquation(
        uindex_t id_component,
        mesh::Mesh1DPtr the_mesh,
        LiquidGasAqueousVariableBox& variables,
        std::shared_ptr<BoundaryConditions> bcs
        ):
    base(the_mesh->nb_nodes()),
    m_impl(make_unique<AqueousGasTransportEquationImpl>(
               id_component, the_mesh, variables, bcs))
{
    number_equations();
}

AqueousGasTransportEquation::~AqueousGasTransportEquation() = default;


index_t AqueousGasTransportEquation::id_equation_impl(index_t id_dof)
{
    const auto id_eq = m_impl->id_equation(id_dof);
    return (id_eq>no_equation)?id_eq:no_equation;
}

mesh::Mesh1D* AqueousGasTransportEquation::get_mesh_impl()
{
    return m_impl->mesh();
}


void AqueousGasTransportEquation::pre_nodal_residual_hook_impl(
        index_t node, const Vector& displacement)
{}
void AqueousGasTransportEquation::pre_residual_hook_impl(const Vector& displacement)
{}
void AqueousGasTransportEquation::post_residual_hook_impl(
        const Vector& displacement,
        const Vector& residuals)
{}

scalar_t AqueousGasTransportEquation::AqueousGasTransportEquationImpl::get_diff(
        uindex_t node_0,
        uindex_t node_1,
        const scalar_t& adv
        ) const
{
    scalar_t diff;
    if (adv > 0 ) {
        diff = m_vars.liquid_diffusivity(node_0)
                * m_vars.relative_liquid_diffusivity(node_0);
    } else if (adv < 0) {
        diff = m_vars.liquid_diffusivity(node_1)
                * m_vars.relative_liquid_diffusivity(node_1);
    } else {
        const scalar_t diff_0 = m_vars.liquid_diffusivity(node_0)
                * m_vars.relative_liquid_diffusivity(node_0);
        const scalar_t diff_1 = m_vars.liquid_diffusivity(node_1)
                * m_vars.relative_liquid_diffusivity(node_1);
       diff = average<Average::harmonic>(diff_0, diff_1);
    }
    return diff;
}


scalar_t AqueousGasTransportEquation::AqueousGasTransportEquationImpl::get_diff_gas(
        uindex_t node_0,
        uindex_t node_1,
        const scalar_t& adv
        ) const
{
    scalar_t diff;
    if (adv > 0) {
        diff = m_vars.resistance_gas_diffusivity(node_0)
                * m_vars.relative_gas_diffusivity(node_0);
    } else if (adv < 0) {
        diff = m_vars.resistance_gas_diffusivity(node_1)
                * m_vars.relative_gas_diffusivity(node_1);
    } else {
//        if (m_bcs->is_gas_node(node_0)) {
//            diff = m_vars.resistance_gas_diffusivity(node_0)
//                    * m_vars.relative_gas_diffusivity(node_0);
//        } else if (m_bcs->is_gas_node(node_1)) {
//            diff = m_vars.resistance_gas_diffusivity(node_1)
//                    * m_vars.relative_gas_diffusivity(node_1);
//        } else {
            const scalar_t coeff_diff_0 = m_vars.resistance_gas_diffusivity(node_0)
                * m_vars.relative_gas_diffusivity(node_0);
            const scalar_t coeff_diff_1 = m_vars.resistance_gas_diffusivity(node_1)
                * m_vars.relative_gas_diffusivity(node_1);

            diff = average<Average::harmonic>(
                    coeff_diff_0, coeff_diff_1);
//        }
    }
    return diff * m_vars.binary_diffusion_coefficient;

}

//! \brief Compute the residuals inside 'element'
void AqueousGasTransportEquation::residuals_element_impl(
        index_t element,
        const Vector& displacement,
        const Vector& velocity,
        Eigen::Vector2d& element_residual,
        bool use_chemistry_rate
        )
{
    element_residual.setZero();

    mesh::Mesh1D* m_mesh = m_impl->mesh();
    LiquidGasAqueousVariableBox& vars = m_impl->m_vars;
    const scalar_t& rt = vars.constants.rt;

    const scalar_t mass_coeff_0 = m_mesh->get_volume_cell_element(element, 0);
    const scalar_t mass_coeff_1 = m_mesh->get_volume_cell_element(element, 1);

    const index_t node_0 = m_mesh->get_node(element, 0);
    const index_t node_1 = m_mesh->get_node(element, 1);

    scalar_t flux_0 = 0.0;
    scalar_t flux_1 = 0.0;

    const scalar_t adv_vel = vars.advection_flux(element);

    const scalar_t dx = m_mesh->get_dx(element);
    const scalar_t section = m_mesh->get_face_area(element);

    if (m_impl->node_can_flux(node_0) and m_impl->node_can_flux(node_1))
    {
        // Diffusion Cw
        const scalar_t coeff_diff = m_impl->get_diff(node_0, node_1, adv_vel);
        const scalar_t diff_aq = (  displacement(node_1)
                                  - displacement(node_0)
                                 );
        const scalar_t diff_flux = coeff_diff * diff_aq / dx;

        // advection
        if (adv_vel < 0)
        {
            flux_0 = -adv_vel*diff_aq;
        }
        else if (adv_vel > 0)
        {
            flux_1 = -adv_vel*diff_aq;
        }
        flux_0 += diff_flux;
        flux_1 -= diff_flux;
    }

    const scalar_t coeff_diff_gas = m_impl->get_diff_gas(node_0, node_1, adv_vel);

    const scalar_t diff_flux_gas = coeff_diff_gas*(
                vars.partial_pressure(node_1) - vars.partial_pressure(node_0))
            / dx
            / rt;


    flux_0 += diff_flux_gas;
    flux_0 += m_impl->get_bc_gas_flux(node_0) / rt;
    flux_0 += m_impl->get_bc_liquid_flux(node_0);
    flux_0 *= section;

    flux_1 -= diff_flux_gas;
    flux_1 += m_impl->get_bc_gas_flux(node_1) / rt;
    flux_1 += m_impl->get_bc_liquid_flux(node_1);
    flux_1 *= section;

    // transient
    if (m_impl->node_has_equation(node_0))
    {
        const scalar_t porosity_0    = vars.porosity(node_0);
        const scalar_t aq_tot_conc_0 = displacement(node_0);
        const scalar_t saturation_0  = vars.saturation(node_0);

        const scalar_t pv_0 = vars.partial_pressure(node_0);

        const scalar_t transient_0 =
                  porosity_0   * aq_tot_conc_0 * vars.saturation.velocity(node_0)
                + saturation_0 * aq_tot_conc_0 * vars.porosity.velocity(node_0)
                + porosity_0   * saturation_0  * velocity(node_0)
                ;
        const scalar_t transient_p_0 =
              (
              -  porosity_0            * pv_0       * vars.saturation.velocity(node_0)
              + ( 1.0 - saturation_0 ) * pv_0       * vars.porosity.velocity(node_0)
              + ( 1.0 - saturation_0 ) * porosity_0 * vars.partial_pressure.velocity(node_0)
              ) / rt;

        scalar_t res = mass_coeff_0*(transient_0+transient_p_0) - flux_0;
        //scalar_t res = mass_coeff_0*(transient_0) - flux_0;

        if (use_chemistry_rate)
        {
            const scalar_t chemistry_0 =
                  vars.aqueous_concentration.chemistry_rate(node_0)
                + vars.solid_concentration.chemistry_rate(node_0)
                    ;
            res += mass_coeff_0*(- chemistry_0);
        }
        element_residual(0) = res/get_scaling();
    }

    if (m_impl->node_has_equation(node_1))
    {
        const scalar_t porosity_1    = vars.porosity(node_1);
        const scalar_t aq_tot_conc_1 = displacement(node_1);
        const scalar_t saturation_1  = vars.saturation(node_1);

        const scalar_t pv_1 = vars.partial_pressure(node_1);

        const scalar_t transient_1 =
                  porosity_1   * aq_tot_conc_1 * vars.saturation.velocity(node_1)
                + saturation_1 * aq_tot_conc_1 * vars.porosity.velocity(node_1)
                + porosity_1   * saturation_1  * velocity(node_1);
        const scalar_t transient_p_1 =
              (
              - porosity_1             * pv_1       * vars.saturation.velocity(node_1)
              + ( 1.0 - saturation_1 ) * pv_1       * vars.porosity.velocity(node_1)
              + ( 1.0 - saturation_1 ) * porosity_1 * vars.partial_pressure.velocity(node_1)
              ) / rt;

        scalar_t res =  mass_coeff_1*(transient_1+transient_p_1) - flux_1;
        //scalar_t res =  mass_coeff_1*(transient_1) - flux_1;

        if (use_chemistry_rate)
        {
            const scalar_t chemistry_1 =
                  vars.aqueous_concentration.chemistry_rate(node_1)
                + vars.solid_concentration.chemistry_rate(node_1)
                    ;
            res += mass_coeff_1*(- chemistry_1);
        }
        element_residual(1) = res/get_scaling();
    }
}


uindex_t AqueousGasTransportEquation::AqueousGasTransportEquationImpl::number_equations()
{
    uindex_t neq = 0;
    for (index_t node: m_mesh->range_nodes()) {
        IdBCs bc_l = m_bcs->get_bcs_liquid_dof(node, m_id_component);
        switch (bc_l) {
        case IdBCs::FixedDof:
            m_ideq[node] = no_equation;
            break;
        case IdBCs::NoEquation:
            m_ideq[node] = no_eq_no_var;
            break;
        default:
            m_ideq[node] = neq;
            ++neq;
        }
    }
    return neq;
}

void AqueousGasTransportEquation::number_equations()
{
    uindex_t neq = m_impl->number_equations();
    register_number_equations(neq);
}

void AqueousGasTransportEquation::compute_transport_rate(
        scalar_t dt,
        const Vector& displacement
        )
{
    m_impl->compute_transport_rate(dt, displacement);
}

void AqueousGasTransportEquation::AqueousGasTransportEquationImpl::compute_transport_rate(
        scalar_t dt,
        const Vector& displacement)
{
    MainVariable& aqueous_concentration = m_vars.aqueous_concentration;
    const MainVariable& saturation = m_vars.saturation;
    const MainVariable& solid_conc = m_vars.solid_concentration;
    const MainVariable& pressure = m_vars.partial_pressure;
    const SecondaryTransientVariable& porosity = m_vars.porosity;

    for (index_t node: m_mesh->range_nodes())
    {
        if (! node_has_equation(node)) continue;

        const scalar_t transient = (
                    (  porosity(node)
                     * saturation(node)
                     * displacement(node))
                    - (  porosity.predictor(node)
                       * aqueous_concentration.predictor(node)
                       * saturation.predictor(node))
                    ) / dt;

        const scalar_t chem_rates = (
                      saturation.chemistry_rate(node)
                    + solid_conc.chemistry_rate(node)
                    + pressure.chemistry_rate(node)
                    );

        aqueous_concentration.transport_fluxes(node) = transient - chem_rates;
    }
}


} //end namespace unsaturated
} //end namespace systems
} //end namespace reactmicp
} //end namespace specmicp
