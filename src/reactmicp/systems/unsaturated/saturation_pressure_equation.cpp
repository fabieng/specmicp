/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "saturation_pressure_equation.hpp"

#include "variables_box.hpp"
#include "boundary_conditions.hpp"

#include "dfpm/meshes/mesh1d.hpp"
#include "dfpm/solver/parabolic_driver.hpp"

#include "specmicp_common/compat.hpp"
#include "specmicp_common/physics/maths.hpp"

#include <vector>
#include <iostream>

namespace specmicp {
namespace dfpmsolver {

// explicit template instanciation
template class ParabolicDriver<reactmicp::systems::unsaturated::SaturationPressureEquation>;

} //end namespace dfpmsolver
} //end namespace specmicp

namespace specmicp {
namespace reactmicp {
namespace systems {
namespace unsaturated {

static constexpr index_t no_equation     {-1};
static constexpr index_t no_eq_no_var    {-2};
//static constexpr index_t no_eq_flux_gas  {-3};
static constexpr index_t not_initialized {-5};

struct SPECMICP_DLL_LOCAL
        SaturationPressureEquation::SaturationPressureEquationImpl
{

    mesh::Mesh1DPtr m_mesh;
    SaturationPressureVariableBox m_vars;
    std::vector<index_t> m_ideq;

    std::shared_ptr<BoundaryConditions> m_bcs;

    bool m_store_residual_info;

    SaturationPressureEquationImpl(
            mesh::Mesh1DPtr the_mesh,
            SaturationPressureVariableBox vars,
            std::shared_ptr<BoundaryConditions> bcs
            ):
        m_mesh(the_mesh),
        m_vars(vars),
        m_ideq(the_mesh->nb_nodes(), not_initialized),
        m_bcs(bcs)
    {}

    uindex_t number_equations();

    index_t& id_equation(index_t node) {return m_ideq[node];}
    bool node_can_flux(index_t node)   {return m_ideq[node] != no_eq_no_var;}
    bool node_has_eq(index_t node)     {return m_ideq[node] > no_equation;}

    void set_store_residual_info() {
        m_store_residual_info = true;
    }
    void reset_store_residual_info() {
        m_store_residual_info = false;
    }

    bool store_residual_info() {
        return m_store_residual_info;
    }

    //! \brief Return a pointer to the mesh
    mesh::Mesh1D* mesh() {return m_mesh.get();}
    range_t range_nodes() {return m_mesh->range_nodes();}

    void set_relative_variables(const Vector& displacement);
    void set_relative_variables(index_t node, const Vector& displacement);

    void compute_transport_rate(scalar_t dt, const Vector& displacement);


    // diffusion coefficient and all
    scalar_t get_perm(
            index_t node_0,
            index_t node_1,
            index_t advection_direction
            ) const;

    scalar_t get_diff(
            index_t node_0,
            index_t node_1,
            index_t advection_direction
            ) const;

    scalar_t get_diff_gas(
            index_t node_0,
            index_t node_1,
            index_t advection_direction
            ) const;

     scalar_t get_bc_liquid_flux(uindex_t node) {
         return m_bcs->get_flux_liquid_dof(node, 0);
     }
     scalar_t get_bc_gas_flux(uindex_t node) {
         return m_bcs->get_flux_gas_dof(node, 0);
     }
};



SaturationPressureEquation::~SaturationPressureEquation() = default;

SaturationPressureEquation::SaturationPressureEquation(
        uindex_t id_component,
        mesh::Mesh1DPtr the_mesh,
        SaturationPressureVariableBox& variables,
        std::shared_ptr<BoundaryConditions> bcs
       ):
    base(the_mesh->nb_nodes()),
    m_impl(make_unique<SaturationPressureEquationImpl>(the_mesh, variables, bcs))
{
    number_equations();
}


uindex_t
SaturationPressureEquation::SaturationPressureEquationImpl::number_equations()
{
    uindex_t neq = 0;
    for (index_t node: m_mesh->range_nodes()) {
        IdBCs bc_l = m_bcs->get_bcs_liquid_dof(node, 0);

        switch (bc_l) {
        case IdBCs::FixedDof:
            m_ideq[node] = no_equation;
            break;
        case IdBCs::NoEquation:
            m_ideq[node] = no_eq_no_var;
            break;
        default:
            m_ideq[node] = neq;
            ++neq;
        }
    }
    return neq;
}

void SaturationPressureEquation::number_equations()
{
    auto neq = m_impl->number_equations();
    register_number_equations(neq);
}

mesh::Mesh1D* SaturationPressureEquation::get_mesh_impl() {
    return m_impl->mesh();
}

index_t SaturationPressureEquation::id_equation_impl(index_t id_dof)
{
    return m_impl->id_equation(id_dof)<0?no_equation:m_impl->id_equation(id_dof);
}

void SaturationPressureEquation::pre_nodal_residual_hook_impl(
        index_t node,
        const Vector& displacement
        )
{
    m_impl->set_relative_variables(node, displacement);
}
void SaturationPressureEquation::pre_residual_hook_impl(
        const Vector& displacement
        )
{
    m_impl->set_relative_variables(displacement);
    m_impl->set_store_residual_info();
}
void SaturationPressureEquation::post_residual_hook_impl(
        const Vector& displacement,
        const Vector& residuals)
{
    m_impl->reset_store_residual_info();
}


// Residuals
// =========

scalar_t SaturationPressureEquation::SaturationPressureEquationImpl::get_perm(
        index_t node_0,
        index_t node_1,
        index_t advection_direction
        ) const
{
    scalar_t perm = 0.0;
    if (advection_direction == +1) {
        perm = m_vars.liquid_permeability(node_0)
                * m_vars.relative_liquid_permeability(node_0);
    }
    else if (advection_direction == -1) {
        perm = m_vars.liquid_permeability(node_1)
                * m_vars.relative_liquid_permeability(node_1);
    }
    else {
        const scalar_t perm_0 = m_vars.liquid_permeability(node_0)
                * m_vars.relative_liquid_permeability(node_0);
        const scalar_t perm_1 = m_vars.liquid_permeability(node_1)
                * m_vars.relative_liquid_permeability(node_1);
        perm = average<Average::harmonic>(perm_0, perm_1);
    }
    return perm;
}

scalar_t SaturationPressureEquation::SaturationPressureEquationImpl::get_diff(
        index_t node_0,
        index_t node_1,
        index_t advection_direction
        ) const
{
    scalar_t diff = 0.0;
    if (advection_direction == +1) {
        diff = m_vars.liquid_diffusivity(node_0)
                * m_vars.relative_liquid_diffusivity(node_0);
    }
    else if (advection_direction == -1) {
        diff = m_vars.liquid_diffusivity(node_1)
                * m_vars.relative_liquid_diffusivity(node_1);
    }
    else {
        const scalar_t coeff_diff_0 = m_vars.liquid_diffusivity(node_0)
                * m_vars.relative_liquid_diffusivity(node_0);
        const scalar_t coeff_diff_1 = m_vars.liquid_diffusivity(node_1)
                * m_vars.relative_liquid_diffusivity(node_1);

        diff = average<Average::harmonic>(coeff_diff_0, coeff_diff_1);
    }
    return diff;
}

scalar_t SaturationPressureEquation::SaturationPressureEquationImpl::get_diff_gas(
        index_t node_0,
        index_t node_1,
        index_t advection_direction
        ) const
{
    scalar_t diff = 0.0;
    if (advection_direction == +1) {
        diff = m_vars.resistance_gas_diffusivity(node_0)
                * m_vars.relative_gas_diffusivity(node_0);
    }
    else if (advection_direction == -1) {
        diff = m_vars.resistance_gas_diffusivity(node_1)
                * m_vars.relative_gas_diffusivity(node_1);
    }
    else {
        if (m_bcs->is_gas_node(node_0)) {
            diff = m_vars.resistance_gas_diffusivity(node_0)
                    * m_vars.relative_gas_diffusivity(node_0);
        }
        else if (m_bcs->is_gas_node(node_1)) {
            diff = m_vars.resistance_gas_diffusivity(node_1)
                    * m_vars.relative_gas_diffusivity(node_1);
        }
        else {
            const scalar_t coeff_diff_0 = m_vars.resistance_gas_diffusivity(node_0)
                * m_vars.relative_gas_diffusivity(node_0);
            const scalar_t coeff_diff_1 = m_vars.resistance_gas_diffusivity(node_1)
                * m_vars.relative_gas_diffusivity(node_1);

            diff = average<Average::harmonic>(coeff_diff_0, coeff_diff_1);
        }
    }
    return diff * m_vars.binary_diffusion_coefficient;
}

void SaturationPressureEquation::residuals_element_impl(
        index_t element,
        const Vector& displacement,
        const Vector& velocity,
        Eigen::Vector2d& element_residual,
        bool use_chemistry_rate
        )
{
    element_residual.setZero();

    mesh::Mesh1D* m_mesh = m_impl->mesh();
    SaturationPressureVariableBox& vars = m_impl->m_vars;
    const scalar_t& rt = vars.constants.rt;

    const scalar_t mass_coeff_0 = m_mesh->get_volume_cell_element(element, 0);
    const scalar_t mass_coeff_1 = m_mesh->get_volume_cell_element(element, 1);

    const scalar_t section = m_mesh->get_face_area(element);
    const scalar_t dx = m_mesh->get_dx(element);

    const index_t node_0 = m_mesh->get_node(element, 0);
    const index_t node_1 = m_mesh->get_node(element, 1);

    scalar_t flux_0 = 0.0;
    scalar_t flux_1 = 0.0;
    scalar_t tot_flux = 0.0;
    scalar_t advection_flux = 0.0;

    index_t advection_direction = 0;

    if (m_impl->node_can_flux(node_0) and m_impl->node_can_flux(node_1))
    {
        // advection -> Pc(S)
        scalar_t pc0 = vars.capillary_pressure(node_0);
        scalar_t pc1 = vars.capillary_pressure(node_1);

        if      (pc1 > pc0) advection_direction = +1;
        else if (pc0 > pc1) advection_direction = -1;

        const scalar_t perm = m_impl->get_perm(
                    node_0, node_1, advection_direction);

        const scalar_t aq_coefficient = average<Average::arithmetic>(
                    vars.aqueous_concentration(node_0),
                    vars.aqueous_concentration(node_1)
                    );

        const scalar_t cap_pressure_gradient = (  pc1 - pc0
                                               ) / m_mesh->get_dx(element);
        advection_flux = ( perm / vars.constants.viscosity_liquid_water
                         ) * cap_pressure_gradient;
        const scalar_t cappres_flux = -aq_coefficient*advection_flux;

        // diffusion Cw
        const scalar_t coeff_diff = m_impl->get_diff(node_0, node_1, advection_direction);

        const scalar_t aq_flux = coeff_diff*(
                      vars.aqueous_concentration(node_1)
                    - vars.aqueous_concentration(node_0)
                    ) / dx;

        tot_flux = (cappres_flux + aq_flux);
    }
    // Diffusion pressure pv
    const scalar_t coeff_diff_gas = m_impl->get_diff_gas(
                node_0, node_1, advection_direction);

    const scalar_t diff_flux_gas = coeff_diff_gas*(
                  vars.partial_pressure(node_1)
                - vars.partial_pressure(node_0)
                )
            / dx
            / rt;

    // Tot flux
    tot_flux += diff_flux_gas;

    flux_0  =  tot_flux;
    flux_1  = -tot_flux;

    // liquid and gas fluxes (0 if no fluxes)
    flux_0 += m_impl->get_bc_liquid_flux(node_0);
    flux_0 += m_impl->get_bc_gas_flux(   node_0) / rt;
    flux_1 += m_impl->get_bc_liquid_flux(node_1);
    flux_1 += m_impl->get_bc_gas_flux(   node_1) / rt;

//    if (m_impl->m_bcs->is_gas_node(node_0)) {
//        flux_1 -= 1e3*2.2e-4*(
//                    vars.partial_pressure(node_1)
//                  - vars.partial_pressure(node_0)
//                  ) / rt;
//    }

    flux_0 *= section;
    flux_1 *= section;

    // Storage
    if (m_impl->store_residual_info())
    {
        // advective flux stored by element
        vars.advection_flux(element) = advection_flux;

        //  fluxes to compute exchange term
        vars.liquid_saturation.transport_fluxes(node_0) += flux_0;
        vars.liquid_saturation.transport_fluxes(node_1) += flux_1;
    }

    // transient
    if (m_impl->node_has_eq(node_0))
    {
        const scalar_t porosity_0    = vars.porosity(node_0);
        const scalar_t aq_tot_conc_0 = vars.aqueous_concentration(node_0);
        const scalar_t saturation_0  = displacement(node_0);
        const scalar_t pv_0 = vars.partial_pressure(node_0);

        scalar_t transient_0 = (
                  porosity_0   * aq_tot_conc_0 * velocity(node_0)
                + saturation_0 * aq_tot_conc_0 * vars.porosity.velocity(node_0)
                + porosity_0   * saturation_0  * vars.aqueous_concentration.velocity(node_0)
                    );

        const scalar_t transient_p_0 =
              (
              -  porosity_0            * pv_0       * velocity(node_0)
              + ( 1.0 - saturation_0 ) * pv_0       * vars.porosity.velocity(node_0)
              + ( 1.0 - saturation_0 ) * porosity_0 * vars.partial_pressure.velocity(node_0)
              ) / rt;

        auto res = mass_coeff_0*(transient_0+transient_p_0) - flux_0;
        if (use_chemistry_rate)
        {
            const scalar_t chemistry_0 =
                  vars.liquid_saturation.chemistry_rate(node_0)
                + vars.solid_concentration.chemistry_rate(node_0)
                    ;
            res -= mass_coeff_0*chemistry_0;
        }
        element_residual(0) = res / get_scaling();
    }
    if (m_impl->node_has_eq(node_1))
    {
        const scalar_t porosity_1    = vars.porosity(node_1);
        const scalar_t aq_tot_conc_1 = vars.aqueous_concentration(node_1);
        const scalar_t saturation_1  = displacement(node_1);
        const scalar_t pv_1 = vars.partial_pressure(node_0);

        scalar_t transient_1 = (
                  porosity_1   * aq_tot_conc_1 * velocity(node_1)
                + saturation_1 * aq_tot_conc_1 * vars.porosity.velocity(node_1)
                + porosity_1   * saturation_1  * vars.aqueous_concentration.velocity(node_1)
                    );
        const scalar_t transient_p_1 =
              (
              - porosity_1             * pv_1       * velocity(node_1)
              + ( 1.0 - saturation_1 ) * pv_1       * vars.porosity.velocity(node_1)
              + ( 1.0 - saturation_1 ) * porosity_1 * vars.partial_pressure.velocity(node_1)
              ) / rt;

        auto res = mass_coeff_1*(transient_p_1 + transient_1) - flux_1;
        if (use_chemistry_rate)
        {
            const scalar_t chemistry_1 =
                  vars.liquid_saturation.chemistry_rate(node_1)
                + vars.solid_concentration.chemistry_rate(node_1)
                    ;
            res -= mass_coeff_1*chemistry_1;
        }
        element_residual(1) = res / get_scaling();
    }
}

void SaturationPressureEquation::set_relative_variables(const Vector& displacement)
{
    return m_impl->set_relative_variables(displacement);
}

void SaturationPressureEquation::SaturationPressureEquationImpl::set_relative_variables(
        index_t node,
        const Vector& displacement
        )
{
    if (not node_can_flux(node)) return;

    const scalar_t saturation = displacement(node);
    m_vars.relative_liquid_diffusivity(node) = m_vars.relative_liquid_diffusivity_f(node, saturation);
    m_vars.relative_liquid_permeability(node) = m_vars.relative_liquid_permeability_f(node, saturation);
    m_vars.capillary_pressure(node) = m_vars.capillary_pressure_f(node, saturation);
    m_vars.partial_pressure(node) = m_vars.partial_pressure_f(node, saturation);
}

void SaturationPressureEquation::SaturationPressureEquationImpl::set_relative_variables(
        const Vector& displacement
        )
{
    for (index_t node: m_mesh->range_nodes())
    {
        set_relative_variables(node, displacement);
    }
}

void SaturationPressureEquation::compute_transport_rate(
        scalar_t dt,
        const Vector& displacement
        )
{
    m_impl->compute_transport_rate(dt, displacement);
}

void SaturationPressureEquation::SaturationPressureEquationImpl::compute_transport_rate(
        scalar_t dt,
        const Vector& displacement)
{
    MainVariable& saturation = m_vars.liquid_saturation;
    const MainVariable& solid_conc = m_vars.solid_concentration;
    const MainVariable& pressure = m_vars.partial_pressure;
    const SecondaryTransientVariable& porosity = m_vars.porosity;
    const SecondaryTransientVariable& aqueous_concentration = m_vars.aqueous_concentration;

    for (index_t node: m_mesh->range_nodes())
    {
        if (! node_has_eq(node)) continue;

        const scalar_t transient = (
                    (porosity(node)*aqueous_concentration(node)*displacement(node))
                    - (porosity.predictor(node)*aqueous_concentration.predictor(node)*saturation.predictor(node))
                    ) / dt;

        const scalar_t chem_rates = (
                      saturation.chemistry_rate(node)
                    + solid_conc.chemistry_rate(node)
                    + pressure.chemistry_rate(node)
                    );

        saturation.transport_fluxes(node) = transient - chem_rates;
    }
}


} //end namespace unsaturated
} //end namespace systems
} //end namespace reactmicp
} //end namespace specmicp
