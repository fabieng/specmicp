/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "pressure_equation.hpp"
#include "boundary_conditions.hpp"
#include "variables_box.hpp"

#include "dfpm/solver/parabolic_driver.hpp"
#include "dfpm/meshes/mesh1d.hpp"

#include "specmicp_common/physics/constants.hpp"
#include "specmicp_common/physics/maths.hpp"
#include "specmicp_common/compat.hpp"

#include <iostream>

namespace specmicp {

namespace dfpmsolver {

// explicit template instanciation
template class ParabolicDriver<reactmicp::systems::unsaturated::PressureEquation>;

} //end namespace dfpmsolver

namespace reactmicp {
namespace systems {
namespace unsaturated {

struct SPECMICP_DLL_LOCAL PressureEquation::PressureEquationImpl
{
    uindex_t m_id_component;
    PressureVariableBox m_vars;
    std::vector<index_t> m_ideq;
    mesh::Mesh1DPtr m_mesh;
    std::shared_ptr<BoundaryConditions> m_bcs;


    mesh::Mesh1D* mesh() {return m_mesh.get();}
    PressureVariableBox& vars() {return m_vars;}

    bool node_has_equation(uindex_t node) {
        return m_ideq[node] != no_equation;
    }

    bool is_gas_node(uindex_t node) {
        return m_bcs->is_gas_node(node);
    }

    index_t id_equation(uindex_t node) {
        return m_ideq[node];
    }

    void fix_node(uindex_t node) {
        m_ideq[node] = no_equation;
    }

    scalar_t get_bc_gas_flux(uindex_t node, scalar_t var) {
        if (m_bcs->get_bcs_gas_dof(node, m_id_component) == IdBCs::ImplicitFixedFlux) {
            return m_bcs->get_implicit_flux_gas_dof(node, m_id_component, var);
        }
        else
        {
            return m_bcs->get_flux_gas_dof(node, m_id_component);
        }
    }

    PressureEquationImpl(
            uindex_t id_component,
            mesh::Mesh1DPtr the_mesh,
            PressureVariableBox& the_vars,
            std::shared_ptr<BoundaryConditions> bcs
            ):
        m_id_component(id_component),
        m_vars(the_vars),
        m_ideq(the_mesh->nb_nodes(), -5),
        m_mesh(the_mesh),
        m_bcs(bcs)
    {}


    scalar_t get_diff(
            index_t node_0,
            index_t node_1
            );

    uindex_t number_equations();

    void compute_transport_rate(scalar_t dt, const Vector& displacement);
};

PressureEquation::PressureEquation(
        uindex_t id_component,
        mesh::Mesh1DPtr the_mesh,
        PressureVariableBox& variables,
        std::shared_ptr<BoundaryConditions> bcs
        ):
    base(the_mesh->nb_nodes()),
    m_impl(make_unique<PressureEquationImpl>(
               id_component, the_mesh, variables, bcs))
{
    number_equations();
}

PressureEquation::~PressureEquation() = default;


index_t PressureEquation::id_equation_impl(index_t id_dof)
{
    return m_impl->id_equation(id_dof);
}

mesh::Mesh1D* PressureEquation::get_mesh_impl() {
    return m_impl->mesh();
}


void PressureEquation::pre_nodal_residual_hook_impl(
        index_t node, const Vector& displacement)
{}

void PressureEquation::pre_residual_hook_impl(const Vector& displacement)
{
}
void PressureEquation::post_residual_hook_impl(
        const Vector& displacement,
        const Vector& residuals)
{}

scalar_t PressureEquation::PressureEquationImpl::get_diff(
        index_t node_0,
        index_t node_1
        )
{
    scalar_t coeff_diff = 0;

//    if (is_gas_node(node_0)) {
//        coeff_diff = m_vars.resistance_gas_diffusivity(node_0)
//                * m_vars.relative_gas_diffusivity(node_0);
//    } else if (is_gas_node(node_1)) {
//        coeff_diff = m_vars.resistance_gas_diffusivity(node_1)
//                * m_vars.relative_gas_diffusivity(node_1);
//    }
//    else {
        const scalar_t coeff_diff_0 = m_vars.resistance_gas_diffusivity(node_0)
                * m_vars.relative_gas_diffusivity(node_0);
        const scalar_t coeff_diff_1 = m_vars.resistance_gas_diffusivity(node_1)
                * m_vars.relative_gas_diffusivity(node_1);

        coeff_diff = average<Average::harmonic>(coeff_diff_0, coeff_diff_1);
//    }
    coeff_diff *=  m_vars.binary_diffusion_coefficient;
    return coeff_diff;
}

//! \brief Compute the residuals inside 'element'
void PressureEquation::residuals_element_impl(
        index_t element,
        const Vector& displacement,
        const Vector& velocity,
        Eigen::Vector2d& element_residual,
        bool use_chemistry_rate
        )
{

    element_residual.setZero();

    mesh::Mesh1D* m_mesh = m_impl->mesh();
    PressureVariableBox& vars = m_impl->m_vars;
    const scalar_t& rt = vars.constants.rt;

    const scalar_t mass_coeff_0 = m_mesh->get_volume_cell_element(element, 0);
    const scalar_t mass_coeff_1 = m_mesh->get_volume_cell_element(element, 1);

    const index_t node_0 = m_mesh->get_node(element, 0);
    const index_t node_1 = m_mesh->get_node(element, 1);

    // Diffusion Cw
    const scalar_t coeff_diff = m_impl->get_diff(node_0, node_1);
    const scalar_t diff_flux = coeff_diff*(
                displacement(node_1) - displacement(node_0))
            / m_mesh->get_dx(element)
            / rt;


    // Tot flux
    scalar_t flux_0 =  diff_flux;
    scalar_t flux_1 = -diff_flux;
    flux_0 += m_impl->get_bc_gas_flux(node_0, displacement(node_0)) / rt;
    flux_1 += m_impl->get_bc_gas_flux(node_1, displacement(node_1)) / rt;

    const scalar_t section = m_mesh->get_face_area(element);
    flux_0 *= section;
    flux_1 *= section;

    // transient
    if (m_impl->node_has_equation(node_0))
    {
        const scalar_t porosity_0 = vars.porosity(node_0);
        const scalar_t pressure_0 = displacement(node_0);
        const scalar_t saturation_0 = 1.0 - vars.liquid_saturation(node_0);

        const scalar_t transient_0 =  mass_coeff_0/rt * (
                  porosity_0   * saturation_0 * velocity(node_0)
                + saturation_0 * pressure_0   * vars.porosity.velocity(node_0)
                - porosity_0   * pressure_0   * vars.liquid_saturation.velocity(node_0)
                    );

        auto res = transient_0 - flux_0;
        if (use_chemistry_rate)
        {
            res += mass_coeff_0*vars.partial_pressure.chemistry_rate(node_0);
        }
        element_residual(0) = res/get_scaling();
    }

    if (m_impl->node_has_equation(node_1))
    {
        const scalar_t porosity_1 = vars.porosity(node_1);
        const scalar_t pressure_1 = displacement(node_1);
        const scalar_t saturation_1 = 1.0 - vars.liquid_saturation(node_1);

        const scalar_t transient_1 = mass_coeff_1/rt * (
                  porosity_1   * saturation_1 * velocity(node_1)
                + saturation_1 * pressure_1   * vars.porosity.velocity(node_1)
                - porosity_1   * pressure_1   * vars.liquid_saturation.velocity(node_1)
                    );

        auto res = transient_1 - flux_1;
        if (use_chemistry_rate)
        {
            res +=  mass_coeff_1*vars.partial_pressure.chemistry_rate(node_1);
        }
        element_residual(1) = res/get_scaling();
    }
}

uindex_t PressureEquation::PressureEquationImpl::number_equations()
{
    uindex_t neq = 0;
    for (index_t node: m_mesh->range_nodes())
    {
        switch (m_bcs->get_bcs_gas_dof(node, m_id_component)){
        case IdBCs::FixedDof:
            fix_node(node);
            break;
        default:
            m_ideq[node] = neq;
            ++neq;
        }
    }
    return neq;
}

void PressureEquation::number_equations()
{
    auto neq = m_impl->number_equations();
    register_number_equations(neq);
}


void PressureEquation::compute_transport_rate(
        scalar_t dt,
        const Vector& displacement)
{
    m_impl->compute_transport_rate(dt, displacement);
}

void PressureEquation::PressureEquationImpl::compute_transport_rate(
        scalar_t dt,
        const Vector& displacement)
{
    const scalar_t& rt = m_vars.constants.rt;

    const MainVariable& saturation = m_vars.liquid_saturation;
    MainVariable& pressure = m_vars.partial_pressure;
    const SecondaryTransientVariable& porosity = m_vars.porosity;

    for (index_t node: m_mesh->range_nodes())
    {
        if (! node_has_equation(node)) continue;

        const scalar_t transient = (
                    (porosity(node)*(1.0-saturation(node))*displacement(node))
                    - (porosity.predictor(node)*(1.0-saturation.predictor(node))*pressure.predictor(node))
                    ) / (rt*dt);

        const scalar_t chem_rates = (
                    - pressure.chemistry_rate(node)
                    );

        pressure.transport_fluxes(node) = transient - chem_rates;
    }
}

} //end namespace unsaturated
} //end namespace systems
} //end namespace reactmicp
} //end namespace specmicp
