/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_UNSATURATED_SATURATIONPRESSUREEQUATION_HPP
#define SPECMICP_REACTMICP_UNSATURATED_SATURATIONPRESSUREEQUATION_HPP

//! \file unsaturated/saturation_pressure_equation.hpp
//! \brief The saturation and pressure equation for the unsaturated system

#include "specmicp_common/types.hpp"
#include "fv_1dof_equation.hpp"

#include "dfpm/meshes/mesh1dfwd.hpp"

#include "variables.hpp"

namespace specmicp {
namespace reactmicp {
namespace systems {
namespace unsaturated {

struct SaturationPressureVariableBox;
class BoundaryConditions;

//! \brief The saturation equation
//!
//! Solve the transport of liquid water
class SPECMICP_DLL_LOCAL SaturationPressureEquation:
        public FV1DOFEquation<SaturationPressureEquation>
{
    using base = FV1DOFEquation<SaturationPressureEquation>;
    using base::get_scaling;
    using base::register_number_equations;

public:

    //! Constructor
    SaturationPressureEquation(
            uindex_t id_component,
            mesh::Mesh1DPtr the_mesh,
            SaturationPressureVariableBox& variables,
            std::shared_ptr<BoundaryConditions> bcs
            );

    //! Destructor
    ~SaturationPressureEquation();

    //! \brief Return the id of equation dof
    index_t id_equation_impl(index_t id_dof);
    //! \brief Return the mesh
    mesh::Mesh1D* get_mesh_impl();

    //! \brief Callback
    //! \internal
    void pre_nodal_residual_hook_impl(index_t node, const Vector& displacement);
    //! \brief Callback
    //! \internal
    void pre_residual_hook_impl(const Vector& displacement);
    //! \brief Callback
    //! \internal
    void post_residual_hook_impl(const Vector& displacement,
                                 const Vector& residuals);

    //! \brief Compute the residuals inside 'element' for 'component'
    void residuals_element_impl(
            index_t element,
            const Vector& displacement,
            const Vector& velocity,
            Eigen::Vector2d& element_residual,
            bool use_chemistry_rate
            );

    //! \brief Set the relative variables
    void set_relative_variables(const Vector& displacement);

    //! \brief Compute the transport rates
    void compute_transport_rate(scalar_t dt, const Vector& displacement);

    //! \brief Number the equations
    void number_equations();

private:

    struct SaturationPressureEquationImpl;
    std::unique_ptr<SaturationPressureEquationImpl> m_impl;
};


} //end namespace unsaturated
} //end namespace systems
} //end namespace reactmicp
} //end namespace specmicp

#endif // SPECMICP_REACTMICP_UNSATURATED_SATURATIONPRESSUREEQUATION_HPP
