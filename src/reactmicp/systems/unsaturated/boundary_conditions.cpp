
/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "boundary_conditions.hpp"
#include "specmicp/adimensional/adimensional_system_structs.hpp"

#include "specmicp_common/cached_vector.hpp"
#include "specmicp_common/compat.hpp"

#include "specmicp_common/eigen/incl_eigen_sparse_core.hpp"

namespace specmicp {
namespace reactmicp {
namespace systems {
namespace unsaturated {

enum DofType: index_t {
      Liquid = 0
    , Gas    = 1
};

struct BoundaryConditions::BoundaryConditionsImpl
{
    BoundaryConditionsImpl(uindex_t nb_nodes, uindex_t nb_components):
        m_nb_nodes(nb_nodes),
        m_nb_components(nb_components),
        m_id_bcs(2*nb_nodes*nb_components),
        m_flux_bcs(2*nb_nodes*nb_components),
        m_constraints(nb_nodes, "default", AdimensionalSystemConstraints())
    {
        m_id_bcs.setZero();
        m_flux_bcs.setZero();
    }

    //index_t cast_from_id(IdBCs the_id) {
    //    return reinterpret_cast<index_t>(the_id);
    //}
    //IdBCs cast_to_id(index_t val) {
    //    return reinterpret_cast<IdBCs>(val);
    //}
    const index_t& cast_from_id(const IdBCs& the_id) {
        return reinterpret_cast<const index_t&>(the_id);
    }
    const IdBCs& cast_to_id(const index_t& val) {
        return reinterpret_cast<const IdBCs&>(val);
    }


    index_t id_dof(uindex_t node, uindex_t component, DofType type) const {
        return 2*m_nb_components*node+2*component+type;
    }
    IdBCs& id_bcs(uindex_t node, uindex_t component, DofType type) {
        return reinterpret_cast<IdBCs&>(m_id_bcs(id_dof(node, component, type)));
    }
    IdBCs id_bcs(uindex_t node, uindex_t component, DofType type) const {
        return static_cast<IdBCs>(m_id_bcs(id_dof(node, component, type)));
    }
    scalar_t& flux_bcs(uindex_t node, uindex_t component, DofType type) {
        return m_flux_bcs(id_dof(node, component, type));
    }
    scalar_t flux_bcs(uindex_t node, uindex_t component, DofType type) const {
        return m_flux_bcs(id_dof(node, component, type));
    }

    // Attributes (public)
    // -------------------

    uindex_t m_nb_nodes;
    uindex_t m_nb_components;

    // Contains the
    Eigen::Matrix<index_t, Eigen::Dynamic, 1> m_id_bcs;
    // Contains the value of the fixed flux. For implicit flux, it contains
    Vector m_flux_bcs;

    std::vector<implicit_flux_f> m_implicit_fluxes;


    utils::NameCachedVector<AdimensionalSystemConstraints> m_constraints;
    std::vector<index_t> m_mineral_noprecip;
};

//BoundaryConditions::BoundaryConditions():
//    m_impl(nullptr)
//{}

BoundaryConditions::BoundaryConditions(uindex_t nb_nodes, uindex_t nb_components):
    m_impl(utils::make_pimpl<BoundaryConditionsImpl>(nb_nodes, nb_components))
{}

BoundaryConditions::~BoundaryConditions() = default;

// tranport

IdBCs BoundaryConditions::get_bcs_liquid_dof(
        uindex_t node,
        uindex_t component
        ) {
    return m_impl->id_bcs(node, component, DofType::Liquid);
}
IdBCs BoundaryConditions::get_bcs_gas_dof(
        uindex_t node,
        uindex_t component
        ) {
    return m_impl->id_bcs(node, component, DofType::Gas);
}

scalar_t BoundaryConditions::get_flux_liquid_dof(
        uindex_t node,
        uindex_t component
        ) const {
    return m_impl->flux_bcs(node, component, DofType::Liquid);
}
scalar_t BoundaryConditions::get_flux_gas_dof(
        uindex_t node,
        uindex_t component
        ) const {
    return m_impl->flux_bcs(node, component, DofType::Gas);
}

void BoundaryConditions::reset_flux_liquid_dof(
        uindex_t node,
        uindex_t component
        ) {
    m_impl->id_bcs(  node, component, DofType::Liquid) = IdBCs::NormalNode;
    m_impl->flux_bcs(node, component, DofType::Liquid) = 0;
}
void BoundaryConditions::reset_flux_gas_dof(
        uindex_t node,
        uindex_t component
        ) {
    m_impl->id_bcs(  node, component, DofType::Gas) = IdBCs::NormalNode;
    m_impl->flux_bcs(node, component, DofType::Gas) = 0;
}


void BoundaryConditions::add_gas_node(uindex_t node)
{
    for (auto component: range(m_impl->m_nb_components)) {
        m_impl->id_bcs(node, component, DofType::Liquid) = IdBCs::NoEquation;
    }
}
void BoundaryConditions::add_gas_nodes(const std::vector<uindex_t>& nodes) {
    for (auto node: nodes)
        add_gas_node(node);
}
bool BoundaryConditions::is_gas_node(uindex_t node) const
{
    return (m_impl->id_bcs(node, 0, DofType::Liquid) == IdBCs::NoEquation);
}

void BoundaryConditions::add_fixed_node(uindex_t node) {

    for (auto component: RangeIterator<index_t>(m_impl->m_nb_components)) {
        m_impl->id_bcs(node, component, DofType::Liquid) = IdBCs::FixedDof;
        m_impl->id_bcs(node, component, DofType::Gas   ) = IdBCs::FixedDof;
    }
}
void BoundaryConditions::add_fixed_nodes(const std::vector<uindex_t>& nodes) {
    for (auto node: nodes)
        add_fixed_node(node);
}

void BoundaryConditions::add_fixed_liquid_dof(
        uindex_t node,
        uindex_t component) {
    m_impl->id_bcs(node, component, DofType::Liquid) = IdBCs::FixedDof;
}
void BoundaryConditions::add_fixed_gas_dof(
        uindex_t node,
        uindex_t component) {
    m_impl->id_bcs(node, component, DofType::Gas) = IdBCs::FixedDof;
}

void BoundaryConditions::add_fixed_flux_liquid_dof(
        uindex_t node,
        uindex_t component) {
    m_impl->id_bcs(node, component, DofType::Liquid) = IdBCs::FixedFlux;
}
void BoundaryConditions::add_fixed_flux_gas_dof(
        uindex_t node,
        uindex_t component) {
    m_impl->id_bcs(node, component, DofType::Gas) = IdBCs::FixedFlux;
}
void BoundaryConditions::set_fixed_flux_liquid_dof(
        uindex_t node,
        uindex_t component,
        scalar_t value
        ) {
    m_impl->id_bcs(node, component, DofType::Liquid) = IdBCs::FixedFlux;
    m_impl->flux_bcs(node, component, DofType::Liquid) = value;
}
void BoundaryConditions::set_fixed_flux_gas_dof(
        uindex_t node,
        uindex_t component,
        scalar_t value
        ) {
    m_impl->id_bcs(node, component, DofType::Gas) = IdBCs::FixedFlux;
    m_impl->flux_bcs(node, component, DofType::Gas) = value;
}

void BoundaryConditions::set_fixed_flux_implicit_gas(
        uindex_t node,
        uindex_t component,
        std::function<scalar_t (scalar_t)> function
        ) {
    if (m_impl->id_bcs(node, component, DofType::Gas) != IdBCs::ImplicitFixedFlux)
    {
        m_impl->id_bcs(node, component, DofType::Gas) = IdBCs::ImplicitFixedFlux;
        m_impl->flux_bcs(node, component, DofType::Gas) = static_cast<scalar_t>(m_impl->m_implicit_fluxes.size());
        // ok the cast is not pretty, feels dangerous, look dangerous and probably is
        // the entire thing may need to be implemented in a new way : good thing : API doesn't have to change
        m_impl->m_implicit_fluxes.push_back(function);
    }
    else
    {
        m_impl->m_implicit_fluxes[m_impl->flux_bcs(node, component, DofType::Gas)]= function;
    }
}

scalar_t BoundaryConditions::get_implicit_flux_gas_dof(uindex_t node, uindex_t component, scalar_t variable) const
{
    const auto& f = m_impl->m_implicit_fluxes[static_cast<index_t>(m_impl->flux_bcs(node, component, DofType::Gas)+0.5)];
    specmicp_assert(f != nullptr);
    return f(variable);
}
// chemistry

bool BoundaryConditions::has_constraint(const std::string &name) const {
    return m_impl->m_constraints.has_value(name);
}

const AdimensionalSystemConstraints&
BoundaryConditions::get_constraint(uindex_t node) const {
    return m_impl->m_constraints[node];
}
AdimensionalSystemConstraints&
BoundaryConditions::get_constraint(uindex_t node) {
    return m_impl->m_constraints[node];
}

const AdimensionalSystemConstraints&
BoundaryConditions::get_constraint(const std::string& name) const {
    return m_impl->m_constraints.get(name);
}
AdimensionalSystemConstraints&
BoundaryConditions::get_constraint(const std::string& name) {
    return m_impl->m_constraints.get(name);
}

AdimensionalSystemConstraints& BoundaryConditions::fork_constraint(
        uindex_t node,
        const std::string& name
        ) {
    return m_impl->m_constraints.fork(node, name);
}


AdimensionalSystemConstraints& BoundaryConditions::fork_constraint(
        const std::string& old_name,
        const std::string& new_name
        ) {
    m_impl->m_constraints.fork(old_name, new_name);
    return m_impl->m_constraints.get(new_name);
}

AdimensionalSystemConstraints& BoundaryConditions::add_constraint(
        const std::string& name
        ) {
    m_impl->m_constraints.emplace_back_cache(name);
    return m_impl->m_constraints.get(name);
}

void BoundaryConditions::set_constraint(uindex_t node, const std::string& name) {
    return m_impl->m_constraints.set(node, name);
}

void BoundaryConditions::add_mineral_no_precipitation(index_t id_mineral) {
    if (std::find(m_impl->m_mineral_noprecip.begin(),
                  m_impl->m_mineral_noprecip.end(),
                  id_mineral
                  ) == m_impl->m_mineral_noprecip.end()) {
        m_impl->m_mineral_noprecip.push_back(id_mineral);
    }
}

const std::vector<index_t>& BoundaryConditions::get_minerals_no_precipitation()
{
    return m_impl->m_mineral_noprecip;
}


} //end namespace unsaturated
} //end namespace systems
} //end namespace reactmicp
} //end namespace specmicp
