/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_SYSTEMS_UNSATURATED_EQUILIBRIUMSTAGGER_HPP
#define SPECMICP_REACTMICP_SYSTEMS_UNSATURATED_EQUILIBRIUMSTAGGER_HPP

#include "reactmicp/solver/staggers_base/chemistry_stagger_base.hpp"
#include "specmicp_common/pimpl_ptr.hpp"

#include "specmicp/adimensional/adimensional_system_solver_structs.hpp"
#include "boundary_conditions.hpp"
#include "specmicp_common/cached_vector.hpp"


//! \file reactmicp/systems/unsaturated/equilibrium_stagger.hpp
//! \brief The default chemistry stagger for unsaturated systems
//!
//! Only compute chemical equilibrium

namespace specmicp {
namespace reactmicp {
namespace systems {
namespace unsaturated {

class UnsaturatedVariables;

//! \brief The vector of options for SpecMiCP solver
//!
//! Same set of options can be used for several cells
class SPECMICP_DLL_PUBLIC EquilibriumOptionsVector:
        public utils::NameCachedVector<AdimensionalSystemSolverOptions>
{
    // It is defined as its own class to simplify the use of the API
public:
    //! \brief Default constructor
    //!
    //! \deprecated
    EquilibriumOptionsVector();
    //! \brief Constructor with default value
    EquilibriumOptionsVector(
            size_type size,
            std::string name,
            AdimensionalSystemSolverOptions& value
            );
    //! \brief Constructor with no value
    EquilibriumOptionsVector(
            size_type size,
            std::string name
            );

    //! \brief Build a default EquilibriumOptionsVector
    static std::shared_ptr<EquilibriumOptionsVector>
    make(index_t nb_nodes, std::string name="default") {
        return std::make_shared<EquilibriumOptionsVector>(nb_nodes, name);
    }
};

//! \brief The default Chemistry Stagger for unsaturated system
class SPECMICP_DLL_PUBLIC EquilibriumStagger:
        public solver::ChemistryStaggerBase
{
public:

    using StaggerReturnCode = solver::StaggerReturnCode; //!< Type of the return codes
    using VariablesBase = solver::VariablesBase; //!< Type of the variables

    //! \brief Constructor
    EquilibriumStagger(
            std::shared_ptr<UnsaturatedVariables> variables,
            std::shared_ptr<BoundaryConditions>   boundary_conditions,
            std::shared_ptr<EquilibriumOptionsVector> options
            );
    ~EquilibriumStagger();



    //! \brief Return an equilibrium stagger
    static std::shared_ptr<EquilibriumStagger> make(
            std::shared_ptr<UnsaturatedVariables> variables,
            std::shared_ptr<BoundaryConditions>   boundary_conditions,
            std::shared_ptr<EquilibriumOptionsVector>  options
            )
    {
        return std::make_shared<EquilibriumStagger>(
                    variables,boundary_conditions, options);
    }

    //! \brief Return the equilibrium options
    std::shared_ptr<EquilibriumOptionsVector> get_options();

    //! \brief Initialize the stagger at the beginning of the computation
    //!
    //! \param var a shared_ptr to the variables
    void initialize(VariablesBase * const var);

    //! \brief Initialize the stagger at the beginning of an iteration
    //!
    //! This is where the predictor can be saved, the first trivial iteration done, ...
    //!
    //! \param dt the duration of the timestep
    //! \param var a shared_ptr to the variables
    void initialize_timestep(scalar_t dt, VariablesBase * const var);

    //! \brief Solve the equation for the timestep
    //!
    //! \param var a shared_ptr to the variables
    StaggerReturnCode restart_timestep(VariablesBase * const var);

private:

    struct SPECMICP_DLL_LOCAL  EquilibriumStaggerImpl;
    utils::pimpl_ptr<EquilibriumStaggerImpl> m_impl; //!< The implementation
};



} //end namespace unsaturated
} //end namespace systems
} //end namespace reactmicp
} //end namespace specmicp

#endif // SPECMICP_REACTMICP_SYSTEMS_UNSATURATED_EQUILIBRIUMSTAGGER_HPP
