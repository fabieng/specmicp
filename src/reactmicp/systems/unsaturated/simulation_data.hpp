/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_UNSATURATED_SIMULATIONDATA_HPP
#define SPECMICP_REACTMICP_UNSATURATED_SIMULATIONDATA_HPP

//! \file unsaturated/simulation_data.hpp
//! \brief Simulation data for the unsaturated system

#include <memory>

namespace specmicp {

namespace database {
    struct DataContainer;
} //end namespace database

namespace mesh {
    class Mesh1D;
} //end namespace mesh

namespace units {
    struct UnitsSet;
} //end namespace units

namespace reactmicp {
namespace systems {
namespace unsaturated {

class UnsaturatedVariables;
class BoundaryConditions;

//! \brief Contain information to be given to the staggers
struct SimulationData {
    std::shared_ptr<database::DataContainer> raw_data; //!< The database
    std::shared_ptr<mesh::Mesh1D> mesh1d;              //!< The mesh
    std::shared_ptr<UnsaturatedVariables> variables;   //!< The variables
    std::shared_ptr<BoundaryConditions> bcs;           //!< The boundary conditions
    units::UnitsSet units;                             //!< The units
};

} //end namespace unsaturated
} //end namespace systems
} //end namespace reactmicp
} //end namespace specmicp

#endif // SPECMICP_REACTMICP_UNSATURATED_SIMULATIONDATA_HPP
