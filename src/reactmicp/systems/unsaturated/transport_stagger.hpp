/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_UNSATURATED_TRANSPORTSTAGGER_HPP
#define SPECMICP_REACTMICP_UNSATURATED_TRANSPORTSTAGGER_HPP

//! \file unsaturated/transport_stagger.hpp
//! \brief The unsaturated transport stagger

#include "reactmicp/solver/staggers_base/transport_stagger_base.hpp"
#include "specmicp_common/pimpl_ptr.hpp"

#include "boundary_conditions.hpp"

namespace specmicp {

namespace dfpmsolver {
struct ParabolicDriverOptions;
} //end namespace dfpmsolver

namespace reactmicp {
namespace systems {
namespace unsaturated {

class UnsaturatedVariables;
class TransportConstraints;

//! \brief Options to configure the transport stagger
struct SPECMICP_DLL_PUBLIC UnsaturatedTransportStaggerOptions
{
    bool merge_saturation_pressure {false}; //!< Merge liquid+gas equation for water
    bool merge_aqueous_pressure {false}; //!< Merge liquid+gas equation for aqueous components

    scalar_t cutoff_residuals {1e-12}; //!< if |R|_0^2 < cutoff
                                       //!< then |R|_0^2 = 1
                                       //!< remove "already almost solved equations

};


//! \brief The transport stagger for the unsaturated system
class SPECMICP_DLL_PUBLIC UnsaturatedTransportStagger:
        public solver::TransportStaggerBase
{
    using VariablesBase = solver::VariablesBase;
    using StaggerReturnCode = solver::StaggerReturnCode;

public:
    //! \brief Constructor
    //!
    //! \param variables shared_pointer to the unsaturated variables
    //! \param boundary_conditions Boundary conditions
    //! \param opts options for the dfpm solvers
    UnsaturatedTransportStagger(
            std::shared_ptr<UnsaturatedVariables> variables,
            std::shared_ptr<BoundaryConditions> boundary_conditions,
            const UnsaturatedTransportStaggerOptions& opts
            );


    //! \brief Constructor
    //!
    //! \param variables shared_pointer to the unsaturated variables
    //! \param boundary_conditions Boundary conditions
    //! \param opts options for the dfpm solvers
    static std::shared_ptr<UnsaturatedTransportStagger>
    make(
            std::shared_ptr<UnsaturatedVariables> variables,
            std::shared_ptr<BoundaryConditions> boundary_conditions,
            UnsaturatedTransportStaggerOptions opts=UnsaturatedTransportStaggerOptions()
            )
    {
        return std::make_shared<UnsaturatedTransportStagger>(
                    variables, boundary_conditions, opts
                    );
    }

    //! \brief Destructor
    ~UnsaturatedTransportStagger();

    //! \brief Initialize the stagger at the beginning of an iteration
    //!
    //! This is where the first residual may be computed, the predictor saved, ...
    //! \param dt the duration of the timestep
    //! \param var shared_ptr to the variables
    void initialize_timestep(scalar_t dt, VariablesBase * const var) override;

    //! \brief Solve the equation for the timetep
    //!
    //! \param var shared_ptr to the variables
    StaggerReturnCode restart_timestep(VariablesBase * const var) override;

    //! \brief Compute the residuals norm
    //!
    //! \param var shared_ptr to the variables
    scalar_t get_residual(VariablesBase * const var) override;
    //! \brief Compute the residuals norm
    //!
    //! \param var shared_ptr to the variables
    scalar_t get_residual_0(VariablesBase * const var) override;

    //! \brief Obtain the norm of the step size
    //!
    //! This is used to check if the algorithm has reach a stationary points.
    //! It should look like : return main_variables.norm()
    //!
    //! \param var shared_ptr to the variables
    scalar_t get_update(VariablesBase * const var) override;

    //! \brief Print debug information
    virtual void print_debug_information(
            VariablesBase * const var
            ) override;

    //! Return options of saturation equation
    //!
    //! \return the options or nullptr if the equation does not exist
    dfpmsolver::ParabolicDriverOptions* get_saturation_options();

    //! return options of aqueous transport equation
    //!
    //! \return the options or nullptr if the equation does not exist
    dfpmsolver::ParabolicDriverOptions* get_aqueous_options(index_t component);

    //! return options of gas transport equation
    //!
    //! \return the options or nullptr if the equation does not exist
    dfpmsolver::ParabolicDriverOptions* get_gas_options(index_t component);

private:
    // The implementation details
    class UnsaturatedTransportStaggerImpl;
    utils::pimpl_ptr<UnsaturatedTransportStaggerImpl> m_impl;
};

} //end namespace unsaturated
} //end namespace systems
} //end namespace reactmicp
} //end namespace specmicp

#endif // SPECMICP_REACTMICP_UNSATURATED_TRANSPORTSTAGGER_HPP
