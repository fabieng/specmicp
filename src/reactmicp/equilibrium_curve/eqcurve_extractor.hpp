/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_EQCURVEEXTRACTOR_HPP
#define SPECMICP_REACTMICP_EQCURVEEXTRACTOR_HPP

//! \file reactmicp/equilibrium_curve/eqcurve_extractor.hpp

#include "specmicp_common/types.hpp"

namespace specmicp {
namespace reactmicp {
namespace eqcurve {

//! \brief Extract information from the equilibrium curve
class SPECMICP_DLL_PUBLIC EquilibriumCurveExtractor
{
public:
    //! \brief Constructor
    //!
    //! \param eq_curve The equilibrium curve
    EquilibriumCurveExtractor(const Matrix& eq_curve):
        m_eqcurve(eq_curve)
    {
        m_is_increasing = (xs_offset(m_eqcurve.rows()) >= xs_offset(1));
    }
    //! \brief
    //!
    //! \param eq_curve The equilibrium curve
    EquilibriumCurveExtractor(Matrix&& eq_curve):
        m_eqcurve(eq_curve)
    {
        m_is_increasing = (xs_offset(m_eqcurve.rows()) >= xs_offset(1));
    }

    //! \brief Return the closest index to x
    //!
    //! \param x total solid concentration
    index_t find_point(scalar_t x);

    //! \brief Return the first index
    index_t first() {return 0;}
    //! \brief Return the last valid index
    index_t last() {return m_eqcurve.rows()-1;}

    //! \brief True if total concentration is increasing
    bool is_increasing() {return m_is_increasing;}
    //! \brief Return the solid concentration
    scalar_t totsolid_concentration(index_t index) {
        return m_eqcurve(index, 0);
    }
    //! \brief Return the aqueous concentration
    scalar_t totaq_concentration(index_t index) {
        return m_eqcurve(index, 1);
    }
    //! \brief Return the porosity
    scalar_t porosity(index_t index) {
        return m_eqcurve(index, 2);
    }
    //! \brief Return the diffusion coefficient
    scalar_t diffusion_coefficient(index_t index) {
        return m_eqcurve(index, 3);
    }
    //! \brief Interpolate
    //!
    //! \param index closest index to x
    //! \param x solid concentration
    //! \param col column to interpolate
    scalar_t interpolate(index_t index, scalar_t x, index_t col);
    //! \brief Return the slope of variable col at 'index'
    scalar_t slope(index_t index, index_t col);

private:
    //!< Return equilibrium curve value from offset index
    scalar_t SPECMICP_DLL_LOCAL xs_offset(index_t offset_index) {
        return m_eqcurve(offset_index-1, 0);
    }

    Matrix m_eqcurve; //!< The equilibrium curve

    bool m_is_increasing; //!< True if the concentration is increasing
};

} // end namespace eqcurve
} // end namespace reactmicp
} // end namespace specmicp

#endif // SPECMICP_REACTMICP_EQCURVEEXTRACTOR_HPP
