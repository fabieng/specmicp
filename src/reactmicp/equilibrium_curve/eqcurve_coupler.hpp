/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_EQCURVECOUPLER_HPP
#define SPECMICP_REACTMICP_EQCURVECOUPLER_HPP

#include "eqcurve_extractor.hpp"
#include "dfpm/meshes/mesh1dfwd.hpp"

#include "dfpm/1dtransport/diffusion.hpp"
#include "dfpm/solver/parabolic_driver.hpp"

//! \file reactmicp/equilibrium_curve/eqcurve_coupler.hpp
//! \brief Coupling algorithm for the equilibrium curve approcj
//!

namespace specmicp {

namespace dfpm {
struct SaturatedDiffusion1DParameters;
} // end namespace dfpm

namespace reactmicp {

//! \namespace specmicp::eqcurve
//! \brief Equilibrium approach to reactive transport
namespace eqcurve {

//! \brief Coupling algorithm for the Equilibrium curve approach
class SPECMICP_DLL_PUBLIC EquilibriumCurveCoupler
{
public:
    //! \brief Constructor
    EquilibriumCurveCoupler(Matrix& eq_curve,
                            mesh::Mesh1DPtr the_mesh,
                            dfpmsolver::ParabolicDriverOptions options
                            );

    //! \brief Run a timestep
    void run_step(scalar_t timestep);

    //! \brief Return the vector of solid concentrations
    Vector& solid_concentrations() {return m_solid_concentrations;}
    //! \brief Return the vector of aqueous concentrations
    Vector& aqueous_concentrations() {return m_aqueous_concentrations;}
    //! \brief Interpolate the equilibrium curve
    void chemistry_step();

private:
    EquilibriumCurveExtractor m_eqcurve; //!< The equilibrium curve

    mesh::Mesh1DPtr m_mesh; //!< The mesh

    std::shared_ptr<dfpm::SaturatedDiffusion1DParameters> m_param; //!< Porosity and diffusivity

    Vector m_aqueous_concentrations; //!< Aqueous concentrations
    Vector m_solid_concentrations;   //!< Solid concentration

    dfpm::SaturatedDiffusion1D m_transport_program; //!< Diffusion equations
    dfpmsolver::ParabolicDriver<dfpm::SaturatedDiffusion1D> m_transport_solver; //!< Diffusion solver
};

} // end namespace eqcurve
} // end namespace reactmicp
} // end namespace specmicp

#endif // SPECMICP_REACTMICP_EQCURVECOUPLER_HPP
