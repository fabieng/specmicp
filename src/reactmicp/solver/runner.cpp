/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "runner.hpp"

#include "timestepper.hpp"
#include "timestepper_structs.hpp"
#include "reactive_transport_solver.hpp"
#include "reactive_transport_solver_structs.hpp"

#include "reactmicp/io/reactive_transport.hpp"
#include  "staggers_base/variables_base.hpp"

#include "specmicp_common/timer.hpp"
#include "specmicp_common/io/csv_formatter.hpp"

#include "specmicp_common/filesystem.hpp"

#include <fstream>

namespace specmicp {
namespace reactmicp {
namespace solver {

struct ReactiveTransportRunner::ReactiveTransportRunnerImpl
{
    ReactiveTransportRunnerImpl(
            ReactiveTransportSolver& solver,
            scalar_t lower_dt_bound,
            scalar_t upper_dt_bound,
            const SimulationInformation& info
            ):
        m_solver(solver),
        m_timestepper(lower_dt_bound, upper_dt_bound, 0, 2.0),
        m_simulinfo(info)
    {

    }

    index_t m_cnt{0};
    ReactiveTransportSolver& m_solver;
    Timestepper m_timestepper;
    const SimulationInformation& m_simulinfo;
    output_f m_output_function{dummy_output};
    scalar_t m_output_target;
    std::string m_iter_path {};

    scalar_t run_until(scalar_t target, VariablesBasePtr variables);
};

ReactiveTransportRunner::ReactiveTransportRunner(
        ReactiveTransportSolver& solver,
        scalar_t lower_dt_bound,
        scalar_t upper_dt_bound,
        const SimulationInformation& info):
    m_impl(utils::make_pimpl<ReactiveTransportRunnerImpl>(
               solver, lower_dt_bound, upper_dt_bound, info)
           )
{}


ReactiveTransportRunner::~ReactiveTransportRunner() = default;


void ReactiveTransportRunner::set_output_policy(output_f output_policy) {
    m_impl->m_output_function = output_policy;
}

ReactiveTransportOptions& ReactiveTransportRunner::get_options() {
    return m_impl->m_solver.get_options();
}

TimestepperOptions& ReactiveTransportRunner::get_timestepper_options() {
    return m_impl->m_timestepper.get_options();
}

ReactiveTransportPerformance& ReactiveTransportRunner::get_perfs() {
    return m_impl->m_solver.get_perfs();
}

ReactiveTransportTimer& ReactiveTransportRunner::get_timer() {
    return m_impl->m_solver.get_timer();
}

scalar_t ReactiveTransportRunner::run_until(
        scalar_t target,
        VariablesBasePtr variables
        )
{
    return m_impl->run_until(target, variables);
}


std::string ReactiveTransportRunner::get_iter_file_path()
{
    return m_impl->m_iter_path;
}

// Implementation

scalar_t ReactiveTransportRunner::ReactiveTransportRunnerImpl::run_until(
        scalar_t target,
        VariablesBasePtr variables
        )
{

    Timer total_time;
    VariablesBase* vars  = variables.get();
    total_time.start();

    m_timestepper.set_total_target(target);
    m_output_target = m_timestepper.get_total()+m_simulinfo.output_step;

    auto filepath = m_simulinfo.complete_filepath(
                std::string("iter"), std::string("dat"));
    if (not m_simulinfo.working_dir.empty() and
        not utils::is_path_absolute(filepath))
    {
        filepath = utils::complete_path(m_simulinfo.working_dir, filepath);
    }
    m_iter_path = filepath;
    io::OutFile out_iter(filepath);
    io::print_reactmicp_header(out_iter);

    if (m_simulinfo.print_iter_info)
    {
        io::print_reactmicp_performance_long_header(out_iter);
    }

    scalar_t dt = m_timestepper.get_options().restart_timestep;
    auto retcode = ReactiveTransportReturnCode::NotConvergedYet;

    bool last_solved = true;

    while(retcode >= ReactiveTransportReturnCode::NotConvergedYet
          and m_timestepper.get_total() < m_timestepper.get_total_target())
    {
        if (m_timestepper.get_total() > 0) last_solved = false;

        Timer step_timer;
        step_timer.start();
        m_solver.set_clock(m_timestepper.get_total());
        retcode = m_solver.solve_timestep(dt, vars);
        step_timer.stop();

        if (m_simulinfo.print_iter_info)
            io::print_reactmicp_performance_long(
                        out_iter, m_cnt,
                        m_timestepper.get_total()+dt,
                        m_solver.get_perfs()
                        );

        dt = m_timestepper.next_timestep(
                    dt,
                    retcode,
                    m_solver.get_perfs().nb_iterations
                    );
        if (retcode <= reactmicp::solver::ReactiveTransportReturnCode::NotConvergedYet)
        {
            dt = m_timestepper.get_options().restart_timestep;
            vars->reset_main_variables();
            m_solver.set_clock(m_timestepper.get_total());
            retcode = m_solver.solve_timestep(dt, vars);
            if (m_simulinfo.print_iter_info)
                io::print_reactmicp_performance_long(
                            out_iter, m_cnt,
                            m_timestepper.get_total()+dt,
                            m_solver.get_perfs()
                            );
            if (retcode <= reactmicp::solver::ReactiveTransportReturnCode::NotConvergedYet)
                vars->reset_main_variables();
            dt = m_timestepper.next_timestep(
                        dt,
                        retcode,
                        m_solver.get_perfs().nb_iterations
                        );
        }
        ++m_cnt;

        // output
        if (m_timestepper.get_total() > m_output_target)
        {
            last_solved = true;
            m_output_function(m_timestepper.get_total(), variables);
            m_output_target += m_simulinfo.output_step;
        }

        if (retcode == reactmicp::solver::ReactiveTransportReturnCode::UserTermination)
            break;
    }

    if (not last_solved) {
        m_output_function(m_timestepper.get_total(), variables);
    }

    total_time.stop();

    io::print_reactmicp_end(out_iter, total_time, m_solver.get_timer());
    io::print_resource_usage(out_iter);

    return m_timestepper.get_total();
}

} // end namespace solver
} // end namespace reactmicp
} // end namespace specmicp
