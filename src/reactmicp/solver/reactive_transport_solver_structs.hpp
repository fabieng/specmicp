
/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_SOLVER_REACTIVETRANSPORTSOLVERSTRUCTS_HPP
#define SPECMICP_REACTMICP_SOLVER_REACTIVETRANSPORTSOLVERSTRUCTS_HPP

#include "specmicp_common/types.hpp"

//! \file reactive_transport_solver_structs.hpp
//! \brief  Structs used by the reactive transport solver

//! \def REACTMICP_DEFAULT_RES_TOL
//! \brief default relative tolerance for the residuals
#define REACTMICP_DEFAULT_RES_TOL 1e-4
//! \def REACTMICP_DEFAULT_ABS_TOL
//! \brief default absolute tolerance for the residuals
#define REACTMICP_DEFAULT_ABS_TOL 1e-14
//! \def REACTMICP_DEFAULT_STEP_TOL
//! \brief default tolerance for the update
#define REACTMICP_DEFAULT_STEP_TOL 1e-10
//! \def REACTMICP_DEFAULT_GOOD_ENOUGH_TOL
//! \brief default relative tolerance for the residuals when max iterations is reached
#define REACTMICP_DEFAULT_GOOD_ENOUGH_TOL 1e-2
//! \def REACTMICP_DEFAULT_MAX_ITER
//! \brief default relative tolerance for the residuals
#define REACTMICP_DEFAULT_MAX_ITER 100
//! \def REACTMICP_DEFAULT_IMPL_UPSCALING
//! \brief if true, include upscaling stagger in SIA
#define REACTMICP_DEFAULT_IMPL_UPSCALING true

namespace specmicp {
namespace reactmicp {
namespace solver {


//! \brief Return codes used by the reactive transport solver
enum class ReactiveTransportReturnCode
{
    UpscalingFailure         = -13, //!< Upscaling stagger has failed
    ChemistryFailure         = -12, //!< Chemistry stagger has failed
    TransportFailure         = -11, //!< Transport stagger has failed
    StaggerFailure           = -10, //!< A stagger has failed
    MaximumIterationsReached = - 2, //!< Maximum number of fixed-point iterations is reached
    StationaryPoint          = - 1, //!< The solver has reached a stationnary points
    NotConvergedYet          =   0, //!< The solver has not converged yet
    ResidualMinimized        =   1, //!< The residuals are minimized
    ErrorMinimized           =   2, //!< The error is minimized (may indicate a stationnary point)
    GoodEnough               =   3, //!< Good enough
    TransportBypass          =   5, //!< Transport is minimized, no need to do iterations
    UserTermination          =   6, //!< User requested termination through one of the stagger
};

//! \brief Options used by the reactive transport solver
struct SPECMICP_DLL_PUBLIC ReactiveTransportOptions
{
    //! Relative tolerance for the residuals
    scalar_t residuals_tolerance {REACTMICP_DEFAULT_RES_TOL};
    //! Absolute tolerance for the residuals
    scalar_t absolute_residuals_tolerance {REACTMICP_DEFAULT_ABS_TOL};
    //! Absolute tolerance for the step
    scalar_t step_tolerance {REACTMICP_DEFAULT_STEP_TOL};
    //! Relative tolerance to detect a stationnary point
    scalar_t good_enough_tolerance {REACTMICP_DEFAULT_GOOD_ENOUGH_TOL};
    //! Maximum number of iterations allowed
    index_t  maximum_iterations {REACTMICP_DEFAULT_MAX_ITER};
    //! When true, the upscaling problem is solved at each iteration
    bool     implicit_upscaling {REACTMICP_DEFAULT_IMPL_UPSCALING};

    //! \brief Use a Sequential Non-iterative Algorithm
    void set_snia() {maximum_iterations = 1;}
    //! \brief Return true if the problem is solved using a SNIA
    bool is_snia() {return maximum_iterations <= 1;}

    ReactiveTransportOptions()
    {}
};

//! \brief Struct containing performance information
//!
//! This is valid for one timestep.
struct SPECMICP_DLL_PUBLIC ReactiveTransportPerformance
{
    scalar_t timestep;     //!< Timestep used
    index_t nb_iterations; //!< The number of fixed-point iterations for this timestep
    ReactiveTransportReturnCode return_code; //!< The return code of the timestep
    scalar_t residuals;      //!< The norm of the residuals at the end of the timestep
    scalar_t update;         //!< Update of a step
    scalar_t total_time;     //!< Time spent solving one timestep
    scalar_t transport_time; //!< Time spent solving the transport problem
    scalar_t chemistry_time; //!< Time spent solving the chemistry problem
    scalar_t upscaling_time; //!< Time spent solving the upscaling problem

    ReactiveTransportPerformance():
        nb_iterations(0),
        return_code(ReactiveTransportReturnCode::NotConvergedYet),
        residuals(HUGE_VAL),
        update(0.0),
        total_time(0.0),
        transport_time(0.0),
        chemistry_time(0.0)
    {}
};

//! \brief Struct containing the execution time of the staggers
struct SPECMICP_DLL_PUBLIC ReactiveTransportTimer
{
    scalar_t transport_time; //!< Time spent in transport stagger
    scalar_t chemistry_time; //!< Time spent in chemistry stagger
    scalar_t upscaling_time; //!< Time spent in upscaling stagger
};


} // end namespace solver
} // end namespace reactmicp
} // end namespace specmicp

#endif // SPECMICP_REACTMICP_SOLVER_REACTIVETRANSPORTSOLVERSTRUCTS_HPP
