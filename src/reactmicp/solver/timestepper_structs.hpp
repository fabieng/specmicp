/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_SOLVER_TIMESTEPPER_STRUCTS_HPP
#define SPECMICP_REACTMICP_SOLVER_TIMESTEPPER_STRUCTS_HPP

//! \file reactmicp/solver/timestepper_structs.hpp
//! \brief Options for the timestepper

#include "specmicp_common/types.hpp"

namespace specmicp {
namespace reactmicp {
namespace solver {

//! \brief Options for the timestepper
//!
//! The main options are the lower and upper bound for the timestep and the
//! iterations. The restart timestep should usually be set to the lower bound
//! value.
struct SPECMICP_DLL_PUBLIC TimestepperOptions
{
    scalar_t lower_bound;      //!< Lower bound for the timestep
    scalar_t upper_bound;      //!< Upper bound for the timestep
    scalar_t restart_timestep; //!< Value used when restarting the problem

    scalar_t iteration_lower_target{1.01}; //!< Lower target for the number of iterations
    scalar_t iteration_upper_target{15.0}; //!< Upper target for the number of iterations

    scalar_t alpha_average{0.5}; //!< Parameter for the exponential moving average, between 0 and 1

    scalar_t decrease_failure{0.5};  //!< Reduction factor in case of failure
    scalar_t increase_error_minimization{1.3}; //!< Increase factor in case of error minimization
    scalar_t decrease_factor{0.75};  //!< Reduction factor to get the number of iterations inside the target
    scalar_t increase_factor{1.25};  //!< Increase factor to get the number of iterations inside the target

    //! \brief Constructor
    TimestepperOptions(scalar_t dt_lower_bound, scalar_t dt_upper_bound):
        lower_bound(dt_lower_bound),
        upper_bound(dt_upper_bound),
        restart_timestep(dt_lower_bound)
    {}
};

} // end namespace solver
} // end namespace reactmicp
} // end namespace specmicp

#endif // SPECMICP_REACTMICP_SOLVER_TIMESTEPPER_STRUCTS_HPP
