/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_SOLVER_RUNNER_HPP
#define SPECMICP_REACTMICP_SOLVER_RUNNER_HPP


/*!
\file reactmicp/solver/runner.hpp
\brief Run the reactive transport solver for several timestep
*/

#include "specmicp_common/types.hpp"
#include "specmicp_common/pimpl_ptr.hpp"

#include <string>
#include <functional>
#include <memory>

namespace specmicp {
namespace reactmicp {
namespace solver {

// forward declarations
class ReactiveTransportSolver;
struct ReactiveTransportOptions;
struct ReactiveTransportPerformance;
struct ReactiveTransportTimer;
struct TimestepperOptions;

class VariablesBase;
using VariablesBasePtr = std::shared_ptr<VariablesBase>;

//! \struct SimulationInformation
//! \brief Contains some basic information about the simulation (name, output dir,...)
//!
struct SPECMICP_DLL_PUBLIC SimulationInformation
{
    std::string name;              //!< Name of the simulation
    std::string output_prefix;     //!< prefix for the output files
    std::string working_dir {""};  //!< Working directory (to complete filepath)

    bool print_iter_info{true}; //!< If true, print the iteration informations
    scalar_t output_step; //!< Output step, runner will output every 'output_step' seconds

    //! \brief Constructor
    //!
    //! \param name_simul name of the simulation
    //! \param outputstep time (in s) between too save in outputs files
    SimulationInformation(std::string name_simul, scalar_t outputstep):
        name(name_simul),
        output_prefix(name_simul+"_"),
        output_step(outputstep)
    {}

    //! \brief Complete the name of an output file
    //!
    //! \param name base name of the file
    //! \param suffix the suffix for the name
    //! \return the complete file name
    std::string complete_filepath(
            const std::string& name,
            const std::string& suffix
            ) const {
        return output_prefix+name+"."+suffix;
    }

    //! \brief Complete the name of an output file
    //!
    //! \param name base name of the file
    //! \param suffix the suffix for the name
    //! \return the complete file name
    std::string complete_filepath(
            std::string&& name,
            std::string&& suffix
            ) const {
        return output_prefix+name+"."+suffix;
    }
};

//! \brief Signature of an output function
using output_f = std::function<void (scalar_t time, VariablesBasePtr variables)>;
//! \brief default = no output
inline void dummy_output(scalar_t _, VariablesBasePtr __) {}

//! \brief Run the reactive transport solver until a time target is reached
//!
//! \sa specmicp::reactmicp::solver::ReactiveTransportSolver
class SPECMICP_DLL_PUBLIC ReactiveTransportRunner
{
public:
    //!
    //! \param solver the reactive transport solver
    //! \param lower_dt_bound the timestep lower bound
    //! \param upper_dt_bound the timestep upper bound
    //! \param info information about the simulation
    ReactiveTransportRunner(ReactiveTransportSolver& solver,
                            scalar_t lower_dt_bound,
                            scalar_t upper_dt_bound,
                            const SimulationInformation& info);
    ~ReactiveTransportRunner();

    //! \brief Run the solver until a certain target
    //!
    //! \param target the target to reach
    //! \param variables the variables to use
    scalar_t run_until(scalar_t target, VariablesBasePtr variables);

    //! \brief Set the output function
    //!
    //! The output is every 'SimulationInformation.output_step' second
    void set_output_policy(output_f output_policy);

    //! \brief Returns the options of the reactive transport solver
    ReactiveTransportOptions& get_options();

    //! \brief Returns the options of the timestepper
    TimestepperOptions& get_timestepper_options();

    //! \brief Returns the performance struct of the reactive transport solver
    ReactiveTransportPerformance& get_perfs();

    //! \brief Returns the timer of the reactive transport solver
    ReactiveTransportTimer& get_timer();

    //! \brief Return the path to the iter file
    //!
    //! Empty string if no such file
    std::string get_iter_file_path();

private:
    struct SPECMICP_DLL_LOCAL ReactiveTransportRunnerImpl;
    utils::pimpl_ptr<ReactiveTransportRunnerImpl> m_impl; //!< implementation
};

} // end namespace solver
} // end namespace reactmicp
} // end namespace specmicp

#endif // SPECMICP_REACTMICP_SOLVER_RUNNER_HPP
