/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_SOLVER_REACTIVETRANSPORTSOLVER_HPP
#define SPECMICP_REACTMICP_SOLVER_REACTIVETRANSPORTSOLVER_HPP

//! \file reactive_transport_solver.hpp The reactive transport solver

//! \namespace specmicp::reactmicp::solver Namespace containing the algorithms for the reactive transport solver

#include <memory>

#include "reactive_transport_solver_structs.hpp"

#include "specmicp_common/types.hpp"
#include "specmicp_common/options_handler.hpp"
#include "specmicp_common/perfs_handler.hpp"

// forward declarations
// ====================
namespace specmicp {
namespace reactmicp {
namespace solver {

class VariablesBase;
//! \brief Shared pointer to the variables
using VariablesBasePtr = std::shared_ptr<VariablesBase>;

class TransportStaggerBase;
class ChemistryStaggerBase;
class UpscalingStaggerBase;
//! \brief A shared pointer to the transport stagger
//!
//! \sa TransportStaggerBase
using TransportStaggerPtr = std::shared_ptr<TransportStaggerBase>;
//! \brief A shared pointer to the chemistry stagger
//!
//! \sa ChemistryStaggerBase
using ChemistryStaggerPtr = std::shared_ptr<ChemistryStaggerBase>;
//! \brief A shared pointer to the upscaling stagger
//!
//! \sa UpscalingStaggerBase
using UpscalingStaggerPtr = std::shared_ptr<UpscalingStaggerBase>;

namespace internal {
struct ReactiveTransportResiduals;
} // end namespace internal

} // end namespace solver
} // end namespace reactmicp
} // end namespace specmicp


// Reactive Transport Solver
// =========================

namespace specmicp {
//! \namespace specmicp::reactmicp
//! \brief The ReactMiCP solver and systems
namespace reactmicp {
//! \namespace specmicp::reactmicp::solver
//! \brief The ReactMiCP solver
namespace solver {

//! \brief The reactive transport solver
//!
//! This class solves a reactive transport problem.
//! The details of the problem are implemented in the staggers.
//!
//! There are three staggers :
//!     - The transport stagger
//!     - The chemistry stagger
//!     - The upscaling stagger
//!
//! The transport stagger also implements the residuals used to checked the convergence.
//!
//! This algorithm do not directly update, nor modify the variables.
//! The details must be implemented in the staggers.
//! The variables shared by the algorithm is a shared_ptr to the abstract base class specmicp::reactmicp::solver::VariablesBase
//! To be useful, this variable must be casted to the true class in the staggers.
//!
//! This class only calls the staggers in the right order to run a timestep.
//! How to use :
//!     - Setup the class by providing the staggers
//!
//! This class is usually used with the runner specmicp::reactmicp::solver::ReactiveTransportRunner
//!
//! \sa specmicp::reactmicp::solver::ReactiveTransportRunner
class SPECMICP_DLL_PUBLIC ReactiveTransportSolver:
        public OptionsHandler<ReactiveTransportOptions>,
        public PerformanceHandler<ReactiveTransportPerformance>
{
public:
    //! \brief Build a reactive transport problem
    //!
    //!
    //!
    //! \param transport_stagger shared_ptr to a transport stagger
    //! \param chemistry_stagger shared_ptr to a chemistry stagger
    //! \param upscaling_stagger shared_ptr to an upscaling stagger
    ReactiveTransportSolver(
            TransportStaggerPtr transport_stagger,
            ChemistryStaggerPtr chemistry_stagger,
            UpscalingStaggerPtr upscaling_stagger
            ):
        m_transport_stagger(transport_stagger),
        m_chemistry_stagger(chemistry_stagger),
        m_upscaling_stagger(upscaling_stagger)
    {}

    //! \brief Solve a timestep
    //!
    //! \param timestep The duration of the timestep
    //! \param variables shared_ptr to the variables
    ReactiveTransportReturnCode solve_timestep(
            scalar_t timestep,
            VariablesBasePtr variables
            );

    //! \brief Solve a timestep
    //!
    //! \param timestep The duration of the timestep
    //! \param variables pointer to the variables
    ReactiveTransportReturnCode solve_timestep(
            scalar_t timestep,
            VariablesBase* variables
            );

    //! \brief Return the time spent in each staggers
    ReactiveTransportTimer& get_timer() {return m_timer;}

    //! \brief Set the simulation clock
    //!
    //! This information is passed to the upscaling stagger.
    //! It can be used to change boundary conditions during the simulations
    //! or adapt the model as function of time.
    void set_clock(scalar_t clock_time);

private: // members
    //! \brief One iteration inside the timestep
    //!
    //! \param variables shared_ptr to the variables
    //! \param residuals struct containing the residuals information
    ReactiveTransportReturnCode SPECMICP_DLL_LOCAL one_iteration(
            VariablesBasePtr variables,
            internal::ReactiveTransportResiduals& residuals
            );
    //! \brief One iteration inside the timestep
    //!
    //! \param variables raw ptr to the variables
    //! \param residuals struct containing the residuals information
    ReactiveTransportReturnCode SPECMICP_DLL_LOCAL one_iteration(
            VariablesBase* variables,
            internal::ReactiveTransportResiduals& residuals
            );
    //! \brief Check the convergence
    //!
    //! \param variables shared_ptr to the variables
    //! \param residuals struct containing the residuals information
    ReactiveTransportReturnCode SPECMICP_DLL_LOCAL check_convergence(
            VariablesBase* variables,
            const internal::ReactiveTransportResiduals& residuals,
            ReactiveTransportReturnCode iteration_return_code
            );

private: // attributes
    TransportStaggerPtr m_transport_stagger; //!< The transport stagger
    ChemistryStaggerPtr m_chemistry_stagger; //!< The chemistry stagger
    UpscalingStaggerPtr m_upscaling_stagger; //!< The upscaling stagger

    ReactiveTransportTimer m_timer; //!< Time spent in each stagger
};

} // end namespace solver
} // end namespace reactmicp
} // end namespace specmicp

#endif // SPECMICP_REACTMICP_SOLVER_REACTIVETRANSPORTSOLVER_HPP
