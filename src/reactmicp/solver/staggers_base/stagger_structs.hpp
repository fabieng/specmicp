/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_SOLVER_STAGGERSTRUCTS_HPP
#define SPECMICP_REACTMICP_SOLVER_STAGGERSTRUCTS_HPP

//! \file stagger_structs.hpp common structs for the staggers

namespace specmicp {
namespace reactmicp {
namespace solver {

//! \brief Return code error used by a stagger
//!
//! Success of the stagger should be tested as :
//!     return code > NotConvergedYet
enum class StaggerReturnCode
{
    LolThatsNotSupposedToHappen = -5, //!< Mainly for debugging purposes
    UnknownError                = -4, //!< Generic error, when used, an entry in the log is necessary
    MaximumIterationsReached    = -3, //!< Maximum number of iterations reached in the code
    StationaryPoint             = -2, //!< Stagger is stuck in a Stationary point
    NotConvergedYet             = -1, //!< The stagger has not converged yet
    // -----------------------------
    UserTermination             =  1, //!< The simulation should be stopped
    ResidualMinimized           =  2, //!< The residuals are minimized
    ErrorMinimized              =  3  //!< The error is minimized
};

} // end namespace solver
} // end namespace reactmicp
} // end namespace specmicp

#endif // SPECMICP_REACTMICP_SOLVER_STAGGERSTRUCTS_HPP
