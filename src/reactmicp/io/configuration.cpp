/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "configuration.hpp"

#include "specmicp_common/io/yaml.hpp"
#include "specmicp_common/io/safe_config.hpp"

#include "dfpm/solver/parabolic_structs.hpp"
#include "reactmicp/solver/runner.hpp"
#include "reactmicp/solver/reactive_transport_solver_structs.hpp"
#include "reactmicp/solver/timestepper_structs.hpp"

#include "saturated_react.hpp"

#include "specmicp_database/database.hpp"

#include "specmicp_common/io/config_yaml_sections.h"

#include <string>
#include <algorithm>


namespace specmicp {
namespace io {

//! \brief Simplify a label
std::string clean_label(std::string label);


void configure_reactmicp_options(
        reactmicp::solver::ReactiveTransportOptions& options,
        io::YAMLConfigHandle&& conf
        )
{
    conf.set_if_attribute_exists<scalar_t>(options.residuals_tolerance,
                                           SPC_CF_S_REACTMICP_A_RES_TOL, 0.0, 1.0);
    conf.set_if_attribute_exists<scalar_t>(options.absolute_residuals_tolerance,
                                           SPC_CF_S_REACTMICP_A_ABS_TOL, 0.0);
    conf.set_if_attribute_exists<scalar_t>(options.step_tolerance,
                                           SPC_CF_S_REACTMICP_A_STEP_TOL, 0.0, 1.0);
    conf.set_if_attribute_exists<scalar_t>(options.good_enough_tolerance,
                                           SPC_CF_S_REACTMICP_A_GOOD_ENOUGH_TOL, 0.0);
    conf.set_if_attribute_exists<bool>(options.implicit_upscaling,
                                       SPC_CF_S_REACTMICP_A_IMPL_UPSCALING);
    conf.set_if_attribute_exists<index_t>(options.maximum_iterations,
                                          SPC_CF_S_REACTMICP_A_MAX_ITER, 0);
}


void SPECMICP_DLL_PUBLIC print_reactmicp_options(
        std::ostream& output,
        const reactmicp::solver::ReactiveTransportOptions& options
        )
{
    output << "   - residuals tolerance   " << options.residuals_tolerance
           << "\n   - absolute tolerance    " << options.absolute_residuals_tolerance
           << "\n   - step tolerance        " << options.step_tolerance
           << "\n   - good enough tolerance " << options.good_enough_tolerance
           << "\n   - maximum iterations    " << options.maximum_iterations
           << "\n   - implicit upscaling    " << options.implicit_upscaling
           << std::endl;
}

void configure_reactmicp_csv_output(
        io::OutputNodalVariables& output_policy,
        const reactmicp::solver::SimulationInformation& simul_info,
        YAMLConfigHandle& conf
        )
{
    database::Database db_manager(output_policy.get_database());
    if (conf.get_optional_attribute<bool>(SPC_CF_S_REACTOUTPUT_A_PH, false))
    {
        output_policy.register_pH(simul_info.complete_filepath("ph", "dat"));
    }
    if (conf.get_optional_attribute<bool>(SPC_CF_S_REACTOUTPUT_A_POROSITY, false))
    {
        output_policy.register_porosity(
                    simul_info.complete_filepath("porosity", "dat"));
    }
    if (conf.get_optional_attribute<bool>(SPC_CF_S_REACTOUTPUT_A_DIFFUSIVITY, false))
    {
        output_policy.register_diffusion_coefficient(
                    simul_info.complete_filepath("diffusivity", "dat"));
    }
    if (conf.has_node(SPC_CF_S_REACTOUTPUT_A_VOLFRAC_MINERAL))
    {
        auto labels = conf.list_to_vector<std::string>(
                    SPC_CF_S_REACTOUTPUT_A_VOLFRAC_MINERAL);
        for (auto label: labels)
        {
            index_t id = db_manager.safe_mineral_label_to_id(label);
            auto simple_label = clean_label(label);
            output_policy.register_volume_fraction_mineral(
                        id, simul_info.complete_filepath("phi_"+simple_label, "dat"));
        }
    }
    if (conf.has_node(SPC_CF_S_REACTOUTPUT_A_TOT_CONC))
    {
        auto labels = conf.list_to_vector<std::string>(
                    SPC_CF_S_REACTOUTPUT_A_TOT_CONC);
        for (auto label: labels)
        {
            index_t id = db_manager.safe_component_label_to_id(label);
            auto simple_label = clean_label(label);
            output_policy.register_total_concentration(
                        id, simul_info.complete_filepath("t_"+simple_label, "dat"));
        }
    }
    if (conf.has_node(SPC_CF_S_REACTOUTPUT_A_TOT_AQ_CONC))
    {
        auto labels = conf.list_to_vector<std::string>(
                    SPC_CF_S_REACTOUTPUT_A_TOT_AQ_CONC);
        for (auto label: labels)
        {
            index_t id = db_manager.safe_component_label_to_id(label);
            auto simple_label = clean_label(label);
            output_policy.register_total_aqueous_concentration(
                        id, simul_info.complete_filepath("c_"+simple_label, "dat"));
        }
    }
    if (conf.has_node(SPC_CF_S_REACTOUTPUT_A_TOT_S_CONC))
    {
        auto labels = conf.list_to_vector<std::string>(
                    SPC_CF_S_REACTOUTPUT_A_TOT_S_CONC);
        for (auto label: labels)
        {
            index_t id = db_manager.safe_component_label_to_id(label);
            auto simple_label = clean_label(label);
            output_policy.register_total_solid_concentration(
                        id, simul_info.complete_filepath("s_"+simple_label, "dat"));
        }
    }
    if (conf.has_node(SPC_CF_S_REACTOUTPUT_A_SATINDEX))
    {
        auto labels = conf.list_to_vector<std::string>(
                    SPC_CF_S_REACTOUTPUT_A_SATINDEX);
        for (auto label: labels)
        {
            index_t id = db_manager.safe_mineral_kinetic_label_to_id(label);
            auto simple_label = clean_label(label);
            output_policy.register_saturation_index_mineral_kinetic(
                        id, simul_info.complete_filepath("si_"+simple_label, "dat"));

        }
    }
}

reactmicp::solver::SimulationInformation SPECMICP_DLL_PUBLIC
configure_simulation_information(
        io::YAMLConfigHandle&& conf
        )
{
    reactmicp::solver::SimulationInformation simul_info(
                conf.get_required_attribute<std::string>(SPC_CF_S_SIMULINFO_A_NAME),
                conf.get_required_attribute<scalar_t>(SPC_CF_S_SIMULINFO_A_OUTPUTSTEP)
                );
    conf.set_if_attribute_exists<std::string>(simul_info.output_prefix, SPC_CF_S_SIMULINFO_A_OUTPUTPREFIX);
    conf.set_if_attribute_exists<bool>(simul_info.print_iter_info,
                                       SPC_CF_S_SIMULINFO_A_PRINTITER);
    return simul_info;
}


std::string clean_label(std::string label)
{
    static std::vector<char> orig {']', '[', '(', ')', '+', '-', ','};
    static std::vector<char> repl {'_', '_', '_', '_', 'p', 'm', '_'};

    for (auto it = label.begin(); it!=label.end(); ++it)
    {
        auto itf = std::find(orig.begin(), orig.end(), *it);
        if (itf != orig.end())
        {
            *it = repl[itf - orig.begin()];
        }
    }
    std::transform(label.begin(), label.end(), label.begin(), ::tolower);
    return label;
}


void configure_reactmicp_timestepper(
        reactmicp::solver::TimestepperOptions& options,
        io::YAMLConfigHandle&& conf
        )
{
    options.lower_bound = conf.get_required_attribute<scalar_t>(
                SPC_CF_S_TIMESTEP_A_LOWER_BOUND, 0.0);
    options.upper_bound = conf.get_required_attribute<scalar_t>(
                SPC_CF_S_TIMESTEP_A_UPPER_BOUND, 0.0);

    if (conf.has_attribute(SPC_CF_S_TIMESTEP_A_RESTART_DT)) {
        options.restart_timestep = conf.get_attribute<scalar_t>(
                    SPC_CF_S_TIMESTEP_A_RESTART_DT, 0.0);
    } else {
        options.restart_timestep = options.lower_bound;
    }

    conf.set_if_attribute_exists<scalar_t>(options.iteration_lower_target,
                                           SPC_CF_S_TIMESTEP_A_LOWER_TARGET, 0.0);
    conf.set_if_attribute_exists<scalar_t>(options.iteration_upper_target,
                                           SPC_CF_S_TIMESTEP_A_UPPER_TARGET, 0.0);
    if (options.iteration_upper_target < options.iteration_lower_target) {
        conf.report_error(
                    YAMLConfigError::InvalidArgument,
                    "Iterations lower target is greater than the upper target.");
    }

    conf.set_if_attribute_exists<scalar_t>(options.alpha_average,
                                           SPC_CF_S_TIMESTEP_A_AVERAGE_PARAMETER,
                                           0.0, 1.0);
    conf.set_if_attribute_exists<scalar_t>(options.decrease_factor,
                                           SPC_CF_S_TIMESTEP_A_FACTOR_IF_DECREASE,
                                           0.0, 1.0);
    conf.set_if_attribute_exists<scalar_t>(options.increase_factor,
                                           SPC_CF_S_TIMESTEP_A_FACTOR_IF_INCREASE,
                                           1.0);
    conf.set_if_attribute_exists<scalar_t>(options.increase_error_minimization,
                                           SPC_CF_S_TIMESTEP_A_FACTOR_IF_MINIMUM,
                                           0.0);
    conf.set_if_attribute_exists<scalar_t>(options.decrease_failure,
                                           SPC_CF_S_TIMESTEP_A_FACTOR_IF_FAILURE,
                                           0.0);



}

} //end namespace io
} //end namespace specmicp
