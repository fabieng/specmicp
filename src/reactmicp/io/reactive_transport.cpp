/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "reactive_transport.hpp"

#include <iostream>

#include "reactmicp/solver/reactive_transport_solver_structs.hpp"

#include "specmicp_common/timer.hpp"
#include "specmicp_common/io/csv_formatter.hpp"

#include "specmicp_common/filesystem.hpp"

namespace specmicp {
namespace io {

//! \brief Print the ReactMiCP header
void print_reactmicp_header(
        OutFile& output
        )
{
    output << "//  ================================== //\n"
              << "//                                     //\n"
              << "//           ReactMiCP                 //\n"
              << "//                                     //\n"
              << "// ==================================  //\n\n"
              << "A reactive transport solver based on SpecMiCP.\n"
              << "------------------------------------------- \n\n";
    output.flush();
}

//! \brief Print the performance information from the ReactMiCP solver
void print_reactmicp_performance_short(
        OutFile& output,
        const reactmicp::solver::ReactiveTransportPerformance& perf
        )
{
    output    << "------------------------------------------- \n"
              << " Performance \n "
              << "------------------------------------------- \n\t"
              << " - Return code : " << reactmicp_return_code_to_string(perf.return_code) << "\n\t"
              << " - Residuals : " << perf.residuals << "\n\t"
              << " - Number of iterations : " << perf.nb_iterations << "\n"
              << "------------------------------------------- \n";
}

//! \brief Print the time spent in the different staggers
void print_reactmicp_timer(
        OutFile& output,
        const reactmicp::solver::ReactiveTransportTimer& timer
        )
{
    scalar_t tot = timer.transport_time + timer.chemistry_time + timer.upscaling_time;
    output    << "------------------------------------------- \n"
              << "Time spent in each stagger \n"
              << "------------------------------------------- \n\t"
              << "- Transport : " << timer.transport_time << " s    ("
                                  << timer.transport_time/tot*100 << " %)\n\t"
              << "- Chemistry : " << timer.chemistry_time << " s    ("
                                  << timer.chemistry_time/tot*100 << " %)\n\t"
              << "- Upscaling : " << timer.upscaling_time << " s    ("
                                  << timer.upscaling_time/tot*100 << " %)\n\t"
              << "----------------------------------------- \n";
}

//! \brief Print time and timer at the end of the computation
void print_reactmicp_end(
        OutFile& output,
        const Timer& total_timer,
        const reactmicp::solver::ReactiveTransportTimer& timer
        )
{
    scalar_t elapsed_s = total_timer.elapsed_time();
    index_t hours =  static_cast<index_t>(elapsed_s) / 3600;
    index_t elapsed_minutes = elapsed_s - hours*3600;
    index_t minute = elapsed_minutes / 60;
    index_t seconds = elapsed_minutes - 60*minute;

    output << " ====================================================== \n";
    output << "computation finished at "
           << total_timer.get_ctime_stop();
    output << " Duration of the computation : " << total_timer.elapsed_time() << " s"
              << "( " << hours << "h " << minute << "min " << seconds << "s )\n";
    print_reactmicp_timer(output, timer);

    output.flush();
}

//! \brief Print the header for the performance table
//!
//! \sa print_reactmicp_performance_long
void print_reactmicp_performance_long_header(
        OutFile& output
        )
{
    output << "Id\t"
           << "T\t"
           << "dt\t"
           << "Return_code\t"
           << "Iterations\t"
           << "Residuals\t"
           << "Update\t"
           << "Time\t"
           << "Transport_time\t"
           << "Chemistry_time\n";
}

//! \brief Print a row in the performance table
//!
//! The performance table contains information about an iteration of the reactmicp solver
//!
//! \sa print_reactmicp_performance_long_header
void print_reactmicp_performance_long(
        OutFile& output,
        index_t cnt,
        scalar_t total,
        const reactmicp::solver::ReactiveTransportPerformance& perfs
        )
{
    output  << cnt << "\t"
              << total << "\t"
              << perfs.timestep << "\t"
              << (int) perfs.return_code << "\t"
              << perfs.nb_iterations << "\t"
              << perfs.residuals << "\t"
              << perfs.update << "\t"
              << perfs.total_time << "\t"
              << perfs.transport_time << "\t"
              << perfs.chemistry_time << "\t"
              << perfs.upscaling_time
              << "\n";
}

//! \brief Transform a ReactiveTransportreturnCode into a string
std::string reactmicp_return_code_to_string(
        reactmicp::solver::ReactiveTransportReturnCode retcode)
{
    using RetCode = reactmicp::solver::ReactiveTransportReturnCode;
    switch (retcode) {
    case RetCode::ResidualMinimized:
        return "ResidualMinimized";
    case RetCode::ErrorMinimized:
        return "ErrorMinimized";
    default:
        switch (retcode) {
        case RetCode::StationaryPoint:
            return "StationaryPoint";
        case RetCode::MaximumIterationsReached:
            return "MaximumIterationsReached";
        case RetCode::GoodEnough:
            return "Good enough...";
        case RetCode::TransportBypass:
            return "Transport Bypass";
        case RetCode::TransportFailure:
            return "TransportFailure";
        case RetCode::ChemistryFailure:
            return "ChemistryFailure";
        case RetCode::UpscalingFailure:
            return "UpscalingFailure";
        default:
            return "Unknow error code !";
        }
    }
}

void SPECMICP_DLL_PUBLIC print_resource_usage(
        OutFile& output
        )
{
    utils::resource_usage resource = utils::get_resource_usage();
    output << "----------- Resource Usage -----------\n"
           << "User CPU Time      : " << resource.user_cpu_time   << " (s) \n"
           << "System CPU Time    : " << resource.system_cpu_time << " (s) \n"
           << "Max. Resident size : " << resource.max_resident_set_size << " (kb) \n";
}

} // end namespace io
} // end namespace specmicp
