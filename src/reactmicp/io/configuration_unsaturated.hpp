/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_IO_CONFIGURATIONUNSATURATED_HPP
#define SPECMICP_REACTMICP_IO_CONFIGURATIONUNSATURATED_HPP

//! \file io/configuration_unsaturated.hpp
//! \brief YAML configuration for the unsaturated system

#include "specmicp_common/types.hpp"

#include <memory>

namespace specmicp {

namespace units {
    struct UnitsSet;
} //end namespace units

namespace database {
    struct DataContainer;
} //end namespace database

namespace reactmicp {
namespace systems {
namespace unsaturated {

class EquilibriumOptionsVector;
class BoundaryConditions;

} //end namespace unsaturated
} //end namespace systems
} //end namespace reactmicp

namespace io {

class RORichYAMLNode;
class YAMLConfigHandle;

//! \brief Configure the boundary condition
std::shared_ptr<reactmicp::systems::unsaturated::BoundaryConditions>
SPECMICP_DLL_PUBLIC
configure_unsaturated_boundary_conditions(
        uindex_t nb_nodes,
        const database::DataContainer * const raw_data,
        YAMLConfigHandle&& configuration
        );

//! \brief Configure the SpecMiCP options for the unsaturated system
std::shared_ptr<reactmicp::systems::unsaturated::EquilibriumOptionsVector>
SPECMICP_DLL_PUBLIC
configure_unsaturated_equilibrium_options(
        uindex_t nb_nodes,
        const units::UnitsSet& the_units,
        YAMLConfigHandle&& configuration
        );

} //end namespace io
} //end namespace specmicp

#endif // SPECMICP_REACTMICP_IO_CONFIGURATIONUNSATURATED_HPP
