/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_IO_CONFIGURATION_HPP
#define SPECMICP_REACTMICP_IO_CONFIGURATION_HPP

//! \file reactmicp/io/configuration.hpp
//! \brief Configure reactmicp

#include "specmicp_common/types.hpp"
#include <iosfwd>

namespace YAML {
class Node;
} //end namespace YAML

namespace specmicp {

struct AdimensionalSystemSolverOptions;

namespace reactmicp {
namespace solver {

struct ReactiveTransportOptions;
struct SimulationInformation;
struct TimestepperOptions;

} //end namespace solver
} //end namespace reactmicp

namespace io {

class YAMLConfigHandle;
class OutputNodalVariables;

//! \brief Configure options for the reactive transport solver
void SPECMICP_DLL_PUBLIC configure_reactmicp_options(
        reactmicp::solver::ReactiveTransportOptions& options,
        io::YAMLConfigHandle&& configuration
        );

//! \brief Print the options of the reactive transport solver
void SPECMICP_DLL_PUBLIC print_reactmicp_options(
        std::ostream& output,
        const reactmicp::solver::ReactiveTransportOptions& options
        );

//! \brief Configure output of the saturated system
void SPECMICP_DLL_PUBLIC configure_reactmicp_csv_output(
        io::OutputNodalVariables& output_policy,
        const reactmicp::solver::SimulationInformation& simul_info,
        YAMLConfigHandle& conf
        );

//! \brief Configure main information for a simulation
reactmicp::solver::SimulationInformation SPECMICP_DLL_PUBLIC
configure_simulation_information(
        io::YAMLConfigHandle&& configuration
        );

//! \brief Configure the timestepper options
void SPECMICP_DLL_PUBLIC
configure_reactmicp_timestepper(
        reactmicp::solver::TimestepperOptions& options,
        io::YAMLConfigHandle&& configuration
        );
} //end namespace io
} //end namespace specmicp

#endif // SPECMICP_REACTMICP_IO_CONFIGURATION_HPP
