/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_IO_REACTMICP_HPP
#define SPECMICP_IO_REACTMICP_HPP

//! \file reactmicp/io/reactive_transport.hpp
//! \brief Print information from the reactive transport module


#include "specmicp_common/types.hpp"
#include <string>


namespace specmicp {

// forward declaration
class Timer;

namespace reactmicp {
namespace solver {

struct ReactiveTransportPerformance;
struct ReactiveTransportTimer;
enum class ReactiveTransportReturnCode;

} //end namespace solver
} //end namespace reactmicp

namespace io {

class OutFile;

//! \brief Print the ReactMiCP header
void SPECMICP_DLL_PUBLIC print_reactmicp_header(
        OutFile& output
        );

//! \brief Print the performance information from the ReactMiCP solver
void SPECMICP_DLL_PUBLIC print_reactmicp_performance_short(
        OutFile& output,
        const reactmicp::solver::ReactiveTransportPerformance& perf
        );

//! \brief Print the time spent in the different staggers
void SPECMICP_DLL_PUBLIC print_reactmicp_timer(
        OutFile& output,
        const reactmicp::solver::ReactiveTransportTimer& timer
        );

//! \brief Print time and timer at the end of the computation
void SPECMICP_DLL_PUBLIC print_reactmicp_end(
        OutFile& output,
        const Timer& total_timer,
        const reactmicp::solver::ReactiveTransportTimer& timer
        );

//! \brief Print the header for the performance table
//!
//! \sa print_reactmicp_performance_long
void SPECMICP_DLL_PUBLIC print_reactmicp_performance_long_header(
        OutFile& output
        );

//! \brief Print a row in the performance table
//!
//! The performance table contains information about an iteration of the reactmicp solver
//!
//! \sa print_reactmicp_performance_long_header
void SPECMICP_DLL_PUBLIC print_reactmicp_performance_long(
        OutFile& output,
        index_t cnt,
        scalar_t total,
        const reactmicp::solver::ReactiveTransportPerformance& perfs
        );

//! \brief Transform a ReactiveTransportreturnCode into a string
std::string SPECMICP_DLL_PUBLIC reactmicp_return_code_to_string(
        reactmicp::solver::ReactiveTransportReturnCode retcode);

//! \brief Return the resource usage
void SPECMICP_DLL_PUBLIC print_resource_usage(
        OutFile& output
        );

} // end namespace io
} // end namespace specmicp


#endif // SPECMICP_IO_REACTMICP_HPP
