/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

//! \file reactmicp_core.hpp
//! \brief Core files of the reactmicp solver - not standalone
//!
//! This file includes the main headers from the ReactMiCP solver.

#ifndef SPECMICP_REACTMICP_CORE_HPP
#define SPECMICP_REACTMICP_CORE_HPP

#include "specmicp_common/types.hpp"

#include "specmicp_database/database.hpp"
#include "specmicp/specmicp.hpp"
#include "dfpm/mesh.hpp"

// solver

#include "reactmicp/solver/reactive_transport_solver.hpp"
#include "reactmicp/solver/reactive_transport_solver_structs.hpp"
#include "reactmicp/solver/staggers_base/upscaling_stagger_base.hpp"
#include "reactmicp/solver/staggers_base/stagger_structs.hpp"
#include "reactmicp/solver/timestepper.hpp"
#include "reactmicp/solver/timestepper_structs.hpp"
#include "reactmicp/solver/runner.hpp"

// output

#include "specmicp_common/io/csv_formatter.hpp"
#include "dfpm/io/meshes.hpp"
#include "reactmicp/io/reactive_transport.hpp"
#include "specmicp_common/physics/io/units.hpp"

// configuration

#include "specmicp_common/io/yaml.hpp"

#include "specmicp_database/io/configuration.hpp"
#include "dfpm/io/configuration.hpp"
#include "reactmicp/io/configuration.hpp"

#include "specmicp_common/timer.hpp"

#endif // SPECMICP_REACTMICP_CORE_HPP
