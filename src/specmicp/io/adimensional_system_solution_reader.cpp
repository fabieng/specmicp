/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */


#include <memory>
#include <string>

#include "specmicp_common/log.hpp"
#include "adimensional_system_solution_reader.hpp"
#include "specmicp/adimensional/adimensional_system_numbering.hpp"
#include "specmicp/adimensional/adimensional_system_solution.hpp"
#include "specmicp_common/io/yaml.hpp"
#include "specmicp_common/compat.hpp"
#include "specmicp_database/database.hpp"
#include "config_solution_output_format.h"

namespace specmicp {
namespace io {

struct AdimensionalSystemSolutionReader::AdimensionalSystemSolutionReaderImpl
{
public:

    // from existing db
    AdimensionalSystemSolutionReaderImpl(std::string solution_path, RawDatabasePtr db);

    // from new db
    AdimensionalSystemSolutionReaderImpl(std::string solution_path);

    RawDatabasePtr m_data;
    std::unique_ptr<AdimemsionalSystemNumbering> m_n;
    std::unique_ptr<YAML::Node> m_file;

    void read_main_variables(
            const YAML::Node& solution_node,
            AdimensionalSystemSolution& solution
            );
     void read_loggamma(
            const YAML::Node& solution_node,
            AdimensionalSystemSolution& solution
            );
     void read_aqueous_molalities(
             const YAML::Node& solution_node,
             AdimensionalSystemSolution& solution
             );
     void read_gas_fugacities(
             const YAML::Node& solution_node,
             AdimensionalSystemSolution& solution
             );
     void read_sorbed_molalities(
             const YAML::Node& solution_node,
             AdimensionalSystemSolution& solution
             );

     std::vector<AdimensionalSystemSolution> parse_solutions();
     void parse_one_solution(
             const YAML::Node& solution_node,
             int id_solution,
             AdimensionalSystemSolution& solution);

private:
    YAML::Node& get_root() {return *m_file;}
    void load_file(std::string solution_path);
    void load_database();
    int check_db_version(database::DataContainer* db);
};

//-------------------------------------

// database read from solution file
AdimensionalSystemSolutionReader::AdimensionalSystemSolutionReader(
        std::string filepath
        ):
    m_impl(utils::make_pimpl<AdimensionalSystemSolutionReaderImpl>(filepath))
{}

// database provided
AdimensionalSystemSolutionReader::AdimensionalSystemSolutionReader(
        std::string filepath,
        RawDatabasePtr valid_database
        ):
    m_impl(utils::make_pimpl<AdimensionalSystemSolutionReaderImpl>(filepath, valid_database))
{}

AdimensionalSystemSolutionReader::~AdimensionalSystemSolutionReader() = default;

std::vector<AdimensionalSystemSolution> AdimensionalSystemSolutionReader::parse_solutions()
{
    return m_impl->parse_solutions();
}

RawDatabasePtr AdimensionalSystemSolutionReader::get_database()
{
    return m_impl->m_data;
}

//-------------------------------------


std::vector<AdimensionalSystemSolution> parse_solutions_yaml(
        std::string filepath,
        RawDatabasePtr& valid_or_not_database
        )
{
    std::vector<AdimensionalSystemSolution> sols;
    if (valid_or_not_database != nullptr and valid_or_not_database->is_valid()) {
        WARNING << "db is valid";
        AdimensionalSystemSolutionReader reader(filepath, valid_or_not_database);
        sols = reader.parse_solutions();
    } else {
        AdimensionalSystemSolutionReader reader(filepath);
        sols = reader.parse_solutions();
        valid_or_not_database = reader.get_database();
    }
    return sols;
}

AdimensionalSystemSolution parse_solution_yaml(
        std::string filepath,
        RawDatabasePtr& valid_or_not_database
        )
{
    auto sols = parse_solutions_yaml(filepath, valid_or_not_database);
    if (sols.size() == 0) {
        throw std::runtime_error("No solutions saved in " + filepath + ".");
    }
    return sols[0];
}



//-------------------------------------

AdimensionalSystemSolutionReader::AdimensionalSystemSolutionReaderImpl::AdimensionalSystemSolutionReaderImpl(
        std::string solution_path,
        RawDatabasePtr db
        )
{
    load_file(solution_path);
    check_db_version(db.get());
    m_data = db;
    m_n = make_unique<AdimemsionalSystemNumbering>(AdimemsionalSystemNumbering(m_data));
}

AdimensionalSystemSolutionReader::AdimensionalSystemSolutionReaderImpl::AdimensionalSystemSolutionReaderImpl(
        std::string solution_path
        )
{
    load_file(solution_path);
    load_database();
    m_n = make_unique<AdimemsionalSystemNumbering>(AdimemsionalSystemNumbering(m_data));
}


void AdimensionalSystemSolutionReader::AdimensionalSystemSolutionReaderImpl::load_file(
        std::string solution_path)
{
    m_file = make_unique<YAML::Node>(io::parse_yaml_file(solution_path));
}

void AdimensionalSystemSolutionReader::AdimensionalSystemSolutionReaderImpl::load_database()
{
    //io::check_mandatory_yaml_node(get_root(), VALUE_META_DATABASE_PATH, "__main__");
    auto db_path = io::get_yaml_mandatory<std::string>(
                       get_root(), VALUE_META_DATABASE_PATH, "__main__");

    auto db_manager = database::Database(db_path, false);
    m_data = db_manager.get_database();
}

int
AdimensionalSystemSolutionReader::AdimensionalSystemSolutionReaderImpl::check_db_version(
        database::DataContainer* db
        )
{
    assert(db != nullptr);
    io::check_mandatory_yaml_node(get_root(), VALUE_META_DATABASE, "__main__");
    if (get_root()[VALUE_META_DATABASE].as<std::string>() != db->metadata.name)
    {
        WARNING << "SolutionSaver : The name of the database does not match the record in the solution !";
        return 1;
    }
    io::check_mandatory_yaml_node(get_root(), VALUE_META_DATABASE_VERSION, "__main__");
    if (get_root()[VALUE_META_DATABASE_VERSION].as<std::string>() != db->metadata.version)
    {
        WARNING << "SolutionSaver : The version of the databases does not match the record in the solution !";
        return 2;
    }
    return 0;
}

std::vector<AdimensionalSystemSolution>
AdimensionalSystemSolutionReader::AdimensionalSystemSolutionReaderImpl::parse_solutions()
{

    const YAML::Node& sols_section = get_root()[SECTION_VALUES];
    std::size_t nb_sol = sols_section.size();
    std::vector<AdimensionalSystemSolution> sols(nb_sol);
    for (std::size_t i=0; i<nb_sol; ++i)
    {
        parse_one_solution(sols_section[i], i, sols[i]);
    }
    return sols;
}

void AdimensionalSystemSolutionReader::AdimensionalSystemSolutionReaderImpl::parse_one_solution(
        const YAML::Node& solution_node,
        int id_solution,
        AdimensionalSystemSolution& solution
        )
{
    read_main_variables(solution_node, solution);
    read_aqueous_molalities(solution_node, solution);
    read_loggamma(solution_node, solution);
    read_gas_fugacities(solution_node, solution);
    read_sorbed_molalities(solution_node, solution);


    solution.ionic_strength = get_yaml_mandatory<scalar_t>(
        solution_node, VALUE_IONIC_STRENGTH, std::to_string(id_solution));
    solution.inert_volume_fraction = get_yaml_mandatory<scalar_t>(
        solution_node, VALUE_INERT, std::to_string(id_solution));
}


void AdimensionalSystemSolutionReader::AdimensionalSystemSolutionReaderImpl::read_main_variables(
        const YAML::Node& solution_node,
        AdimensionalSystemSolution& solution
        )
{
    // Initialization
    // --------------
    solution.main_variables = Vector(m_n->total_dofs());
    solution.main_variables(m_n->dof_water()) = 0;
    solution.main_variables(m_n->dof_electron()) = - INFINITY;
    for (auto id: m_data->range_aqueous_component())
    {
        solution.main_variables(m_n->dof_component(id)) = - INFINITY;
    }
    solution.main_variables(m_n->dof_surface()) = - INFINITY;
    for (auto mineral: m_data->range_mineral())
    {
        solution.main_variables(m_n->dof_mineral(mineral)) = 0;
    }
    // Parsing values
    // --------------
    io::check_mandatory_yaml_node(solution_node, SECTION_MAIN, SECTION_VALUES);
    const YAML::Node& main = solution_node[SECTION_MAIN];
    // components
    io::check_mandatory_yaml_node(main, VALUE_COMPONENT, SECTION_VALUES);
    const YAML::Node& components = main[VALUE_COMPONENT];
    for (auto& it: components)
    {
        const std::string label = it.first.as<std::string>();
        const index_t id = m_data->get_id_component(label);
        if (id == no_species)
        {
            throw std::invalid_argument("Component '" + label + "' does not exist in the database !");
        }
        try
        {
            solution.main_variables(m_n->dof_component(id)) = it.second.as<scalar_t>();
        }
        catch (YAML::BadConversion)
        {
            if (it.second.as<std::string>() == "-inf")
            {
                solution.main_variables(m_n->dof_component(id)) = - INFINITY;
            }
        }
    }
    // sorption
    solution.main_variables(m_n->dof_surface()) =
            io::get_yaml_optional<scalar_t>(main, VALUE_FREE_SURFACE, SECTION_MAIN, -INFINITY);
    // minerals
    io::check_mandatory_yaml_node(main, VALUE_MINERAL, SECTION_VALUES);
    const YAML::Node& minerals = main[VALUE_MINERAL];
    for (auto& it: minerals)
    {
        const std::string label = it.first.as<std::string>();
        const index_t id = m_data->get_id_mineral(label);
        if (id == no_species)
        {
            throw std::invalid_argument("Mineral '" + label + "' does not exist in the database !");
        }
        solution.main_variables(m_n->dof_mineral(id)) = it.second.as<scalar_t>();
    }
}

void AdimensionalSystemSolutionReader::AdimensionalSystemSolutionReaderImpl::read_loggamma(
        const YAML::Node& solution_node,
        AdimensionalSystemSolution& solution
        )
{
    solution.log_gamma = Vector::Zero(m_data->nb_component()+m_data->nb_aqueous());
    io::check_mandatory_yaml_node(solution_node, SECTION_LOGGAMMA, SECTION_VALUES);
    const YAML::Node& loggamma = solution_node[SECTION_LOGGAMMA];
    // components
    io::check_mandatory_yaml_node(loggamma, VALUE_COMPONENT, SECTION_LOGGAMMA);
    const YAML::Node& components = loggamma[VALUE_COMPONENT];
    for (auto& it: components)
    {
        const std::string label = it.first.as<std::string>();
        const index_t id = m_data->get_id_component(label);
        if (id == no_species)
        {
            throw std::invalid_argument("Component '" + label + "' does not exist in the database !");
        }
        solution.log_gamma(m_n->dof_component_gamma(id)) = it.second.as<scalar_t>();
    }
    // aqueous
    io::check_mandatory_yaml_node(loggamma, VALUE_AQUEOUS, SECTION_LOGGAMMA);
    const YAML::Node& aqueous = loggamma[VALUE_AQUEOUS];
    for (auto& it: aqueous)
    {
        const std::string label = it.first.as<std::string>();
        const index_t id = m_data->get_id_aqueous(label);
        if (id == no_species)
        {
            throw std::invalid_argument(
                        "Aqueous '" + label +
                        "' does not exist in the database !");
        }
        solution.log_gamma(m_n->dof_aqueous_gamma(id)) = it.second.as<scalar_t>();
    }
}

void AdimensionalSystemSolutionReader::AdimensionalSystemSolutionReaderImpl::read_aqueous_molalities(
        const YAML::Node& solution_node,
        AdimensionalSystemSolution& solution
        )
{
    solution.secondary_molalities = Vector::Zero(m_data->nb_aqueous());
    io::check_mandatory_yaml_node(solution_node, SECTION_AQUEOUS, SECTION_VALUES);
    const YAML::Node& aqueous_section = solution_node[SECTION_AQUEOUS];
    for (auto& it: aqueous_section)
    {
        const std::string label = it.first.as<std::string>();
        const index_t id = m_data->get_id_aqueous(label);
        if (id == no_species)
        {
            throw std::invalid_argument(
                        "Secondary aqueous species '" + label +
                        "' does not exist in the database !");
        }
        solution.secondary_molalities(id) = it.second.as<scalar_t>();
    }
}

void AdimensionalSystemSolutionReader::AdimensionalSystemSolutionReaderImpl::read_gas_fugacities(
        const YAML::Node& solution_node,
        AdimensionalSystemSolution& solution
        )
{
    solution.gas_fugacities = Vector::Zero(m_data->nb_gas());
    io::check_mandatory_yaml_node(solution_node, SECTION_GAS, SECTION_VALUES);
    const YAML::Node& gas_section = solution_node[SECTION_GAS];
    for (auto& it: gas_section)
    {
        const std::string label = it.first.as<std::string>();
        const index_t id = m_data->get_id_gas(label);
        if (id == no_species)
        {
            throw std::invalid_argument("Gas '" + label +
                                        "' does not exist in the database !");
        }
        solution.gas_fugacities(id) = it.second.as<scalar_t>();
    }
}

void AdimensionalSystemSolutionReader::AdimensionalSystemSolutionReaderImpl::read_sorbed_molalities(
        const YAML::Node& solution_node,
        AdimensionalSystemSolution& solution
        )
{
    solution.sorbed_molalities = Vector::Zero(m_data->nb_sorbed());
    io::check_mandatory_yaml_node(solution_node, SECTION_SORBED, SECTION_VALUES);
    const YAML::Node& sorbed_section = solution_node[SECTION_SORBED];
    for (auto& it: sorbed_section)
    {
        const std::string label = it.first.as<std::string>();
        const index_t id = m_data->get_id_sorbed(label);
        if (id == no_species)
        {
            throw std::invalid_argument("sorbed species '" + label +
                                        "' does not exist in the database !");
        }
        solution.sorbed_molalities(id) = it.second.as<scalar_t>();
    }
}


} // end namespace io
} // end namespace specmicp
