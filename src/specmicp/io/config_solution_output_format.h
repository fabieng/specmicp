/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

//! \file config_solution_output_format.h
//! \internal
//! \brief Names of the sections, subsections, keys sed in the yaml output of a solution

#ifndef SPEMCICP_ADIM_SOLUTION_OUTPUTFORMAT_HPP
#define SPEMCICP_ADIM_SOLUTION_OUTPUTFORMAT_HPP

#ifndef SPC_DOXYGEN_SHOULD_SKIP_THIS

#define SECTION_VALUES "solutions"

// metatada
#define VALUE_META_NAME "name"
#define VALUE_META_DATE "date"
#define VALUE_META_DATABASE "database"
#define VALUE_META_DATABASE_VERSION "database_version"
#define VALUE_META_DATABASE_PATH "database_path"

// main sections
#define SECTION_MAIN "main_variables"
#define SECTION_LOGGAMMA "log_gamma"
#define SECTION_GAS "gas_fugacities"
#define SECTION_SORBED "sorbed_molalities"
#define SECTION_AQUEOUS "aqueous_molalities"

// individual values
#define VALUE_FREE_SURFACE "free_surface"
#define VALUE_IONIC_STRENGTH "ionic_strength"
#define VALUE_INERT "inert_vol_fraction"
#define VALUE_COMPONENT "components"
#define VALUE_AQUEOUS "aqueous"
#define VALUE_MINERAL "minerals"

#endif // SPC_DOXYGEN_SHOULD_SKIP_THIS

#endif // SPEMCICP_ADIM_SOLUTION_OUTPUTFORMAT_HPP
