/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "hdf5_adimensional.hpp"

#include "specmicp/adimensional/adimensional_system_solution.hpp"

#include "specmicp_common/io/hdf5/path.hpp"
#include "specmicp_common/io/hdf5/group.hpp"
#include "specmicp_common/io/hdf5/dataspace.hpp"
#include "specmicp_common/io/hdf5/attribute.hpp"
#include "specmicp_common/io/hdf5/dataset.hpp"

namespace specmicp {
namespace io {

#define MAIN_VARIABLES_DS "main_variables"
#define SECONDARY_MOLALITIES_DS "secondary_molalities"
#define LOG_GAMMA_DS "log_gamma"
#define GAS_FUGACITIES_DS "gas_fugacities"
#define SORBED_MOLALITIES_DS "sorbed_molalities"
#define SCALAR_VARS_ATT "scalar_vars"

//! \brief Implementation class to save a HDF5 file
//!
//! \internal
class SPECMICP_DLL_LOCAL AdimensionalSystemSolutionHDF5Saver
{
public:
    AdimensionalSystemSolutionHDF5Saver(
            const AdimensionalSystemSolution& solution):
        m_solution(solution)
    {}

    //! \brief Save the solution into the current location
    //!
    //! Create a new group named 'name' to store the datasets
    void save(
            hdf5::GroupPath& location,
            const std::string& name
            );
private:
    void save_main_variables(    hdf5::GroupPath& location);
    void save_molalities(        hdf5::GroupPath& location);
    void save_loggamma(          hdf5::GroupPath& location);
    void save_gas_fugacities(    hdf5::GroupPath& location);
    void save_sorbed_molalities( hdf5::GroupPath& location);

    const AdimensionalSystemSolution& m_solution;
};

//! \brief Implementation : read a HDF5 file
//!
//! \internal
namespace AdimensionalSystemSolutionHDF5Reader {

//! \brief Save the solution into file (in subgroup section/name)
static AdimensionalSystemSolution read(
        const hdf5::GroupPath& location
        );

static void read_main_variables(
            const hdf5::GroupPath& location,
            AdimensionalSystemSolution& solution);
static void read_molalities(
            const hdf5::GroupPath& location,
            AdimensionalSystemSolution& solution);
static void read_loggamma(
            const hdf5::GroupPath& location,
            AdimensionalSystemSolution& solution);
static void read_gas_fugacities(
            const hdf5::GroupPath& location,
            AdimensionalSystemSolution& solution);
static void read_sorbed_molalities(
            const hdf5::GroupPath& location,
            AdimensionalSystemSolution& solution);
} // end namespace AdimensionalSystemSolutionHDF5Reader

void save_adimensional_system_solution(
        hdf5::GroupPath& location,
        const std::string& name,
        const AdimensionalSystemSolution& solution
        )
{
    if (not solution.is_valid) {
        throw std::runtime_error("AdimSolution to save is not valid !"
                                 " Solution " + name + " to be saved in "
                                 + location.get_path() + ".");
    }
    AdimensionalSystemSolutionHDF5Saver saver(solution);
    saver.save(location, name);
}

//! \brief Read a solution from an HDF5 file
AdimensionalSystemSolution SPECMICP_DLL_PUBLIC
read_adimensional_system_solution(
        const hdf5::GroupPath& location,
        const std::string& name
        )
{
    if (not location.has_link(name)) {
        throw std::runtime_error("The path " + location.add_to_path(name)
                                 + "does not exist");
    }
    hdf5::Group sol_loc = location.open_group(name);
    return AdimensionalSystemSolutionHDF5Reader::read(sol_loc);
}

// Implementation
// ==============

// Save
// -----

void AdimensionalSystemSolutionHDF5Saver::save(
        hdf5::GroupPath& location,
       const std::string& name
        )
{
    auto grp = location.create_group(name);

    save_main_variables(grp);
    save_molalities(grp);
    save_loggamma(grp);

    if (m_solution.gas_fugacities.rows() > 0) {
        save_gas_fugacities(grp);
    }
    if (m_solution.sorbed_molalities.rows() > 0) {
        save_sorbed_molalities(grp);
    }
}

void AdimensionalSystemSolutionHDF5Saver::save_main_variables(
        hdf5::GroupPath& location
        )
{
    hdf5::Dataset dataset = location.create_vector_dataset(
                MAIN_VARIABLES_DS,
                m_solution.main_variables
    );
    // save scalar values as attributes
    std::array<double, 2> attribute_values = {
        m_solution.inert_volume_fraction,
        m_solution.ionic_strength
    };

    hdf5::Attribute attr = dataset.create_scalar_attribute(
                               SCALAR_VARS_ATT, attribute_values);
}

void AdimensionalSystemSolutionHDF5Saver::save_molalities(
        hdf5::GroupPath& location
        )
{
    location.create_vector_dataset(
                SECONDARY_MOLALITIES_DS,
                m_solution.secondary_molalities
    );
}

void AdimensionalSystemSolutionHDF5Saver::save_loggamma(
        hdf5::GroupPath& location
        )
{
    location.create_vector_dataset(
                LOG_GAMMA_DS,
                m_solution.log_gamma
                );
}

void AdimensionalSystemSolutionHDF5Saver::save_gas_fugacities(
        hdf5::GroupPath& location
        )
{
    location.create_vector_dataset(
                GAS_FUGACITIES_DS,
                m_solution.gas_fugacities
                );
}

void AdimensionalSystemSolutionHDF5Saver::save_sorbed_molalities(
        hdf5::GroupPath& location
        )
{
    location.create_vector_dataset(
                SORBED_MOLALITIES_DS,
                m_solution.sorbed_molalities
                );
}


// read
// ----
namespace AdimensionalSystemSolutionHDF5Reader {

AdimensionalSystemSolution read(
        const hdf5::GroupPath& location
        )
{
    AdimensionalSystemSolution solution;

    read_main_variables(location, solution);
    read_molalities(location, solution);
    read_loggamma(location, solution);

    read_gas_fugacities(location, solution);
    read_sorbed_molalities(location, solution);
    solution.is_valid = true;
    return solution;
}

void read_main_variables(
        const hdf5::GroupPath& location,
        AdimensionalSystemSolution &solution
        )
{
    solution.main_variables = location.read_vector_dataset(MAIN_VARIABLES_DS);

    // read scalar variables
    hdf5::Dataset dset = location.open_dataset(MAIN_VARIABLES_DS);
    auto attributes = dset.read_scalar_attribute<2>(SCALAR_VARS_ATT);

    solution.inert_volume_fraction = attributes[0];
    solution.ionic_strength = attributes[1];
}

void read_molalities(
        const hdf5::GroupPath& location,
        AdimensionalSystemSolution &solution
        )
{
    solution.secondary_molalities =
            location.read_vector_dataset(SECONDARY_MOLALITIES_DS);
}

void read_loggamma(
        const hdf5::GroupPath& location,
        AdimensionalSystemSolution &solution
        )
{
    solution.log_gamma = location.read_vector_dataset(LOG_GAMMA_DS);
}

void read_gas_fugacities(
        const hdf5::GroupPath& location,
        AdimensionalSystemSolution &solution
        )
{
    if (location.has_link(GAS_FUGACITIES_DS)) {
        solution.gas_fugacities =
                location.read_vector_dataset(GAS_FUGACITIES_DS);
    }
}

void read_sorbed_molalities(
        const hdf5::GroupPath& location,
        AdimensionalSystemSolution &solution
        )
{
    if (location.has_link(SORBED_MOLALITIES_DS)) {
        solution.sorbed_molalities =
                location.read_vector_dataset(SORBED_MOLALITIES_DS);
    }
}

} // end namespace AdimensionalSystemSolutionHDF5Reader

} //end namespace io
} //end namespace specmicp
