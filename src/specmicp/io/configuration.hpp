/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_SPECMICP_IO_HPP
#define SPECMICP_SPECMICP_IO_HPP

#include "specmicp_common/types.hpp"

#include <memory>

//! \file specmicp/io/configuration.hpp
//! \brief configuration for specmicp adim system

namespace specmicp {

struct AdimensionalSystemSolverOptions;
struct Formulation;
struct AdimensionalSystemConstraints;
class ReactantBox;
class SmartAdimSolver;

namespace database {
    struct DataContainer;
}

using RawDatabasePtr = std::shared_ptr<database::DataContainer>;

namespace units {
    struct UnitsSet;
} //end namespace units

namespace io {

class YAMLConfigHandle;

/*! \brief Configure specmicp solver options

Conserve previous values if not given.

Configuration must be of the form :
\code
    opts1: val1
    opts2: val2
    ....
\endcode

\warning units must be set elsewhere !
*/
void SPECMICP_DLL_PUBLIC configure_specmicp_options(
        AdimensionalSystemSolverOptions& options,
        const units::UnitsSet& the_units,
        YAMLConfigHandle&& configuration
        );

//! \brief Configure a reactant box from the configuration
ReactantBox SPECMICP_DLL_PUBLIC configure_specmicp_reactant_box(
        RawDatabasePtr raw_db,
        const units::UnitsSet& the_units,
        YAMLConfigHandle&& configuration
        );

//! \brief Configure a reactant box from the configuration
void SPECMICP_DLL_PUBLIC configure_specmicp_reactant_box(
        ReactantBox& reactant_box,
        YAMLConfigHandle&& configuration
        );

//! \brief Configure specmicp constraints
void SPECMICP_DLL_PUBLIC configure_specmicp_constraints(
        AdimensionalSystemConstraints& constraints,
        const database::DataContainer* const raw_db,
        YAMLConfigHandle&& conf_constraints
        );

//! \brief Configure AdimSmartSolver initialization
void SPECMICP_DLL_PUBLIC configure_smart_solver_initialization(
        SmartAdimSolver& solver,
        YAMLConfigHandle&& conf_init
        );

} //end namespace io
} //end namespace specmicp

#endif // SPECMICP_SPECMICP_IO_HPP
