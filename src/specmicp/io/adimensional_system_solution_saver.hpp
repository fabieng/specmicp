/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_SPECMICP_ADIMENSIONALSYSTEMSOLUTIONSAVER_HPP
#define SPECMICP_SPECMICP_ADIMENSIONALSYSTEMSOLUTIONSAVER_HPP

#include "specmicp_common/types.hpp"
#include "specmicp_database/database_fwd.hpp"

#include <string>
#include <memory>
#include <vector>
#include "specmicp_common/pimpl_ptr.hpp"

namespace YAML {
    class Node;
    class Emitter;
} //end namespace YAML

namespace specmicp {

struct AdimensionalSystemSolution;

namespace io {


//! \brief This is a serializer for the AdimensionalSystemSolution
//!
//! This class format the solution is a JSON-formatted string.
class SPECMICP_DLL_PUBLIC AdimensionalSystemSolutionSaver
{
public:
    //! \brief Constructor
    AdimensionalSystemSolutionSaver(RawDatabasePtr the_database);
    ~AdimensionalSystemSolutionSaver();

    //! \brief Format the solution as a YAML-formatted string
    std::string format_solution(const AdimensionalSystemSolution& solution);

    //! \brief Save the solution as a YAML file
    //!
    //! \param filepath path the file where to solve the solution
    //! \param solution the solution to save
    //! \param db_path path to the database
    void save_solution(
            const std::string& filepath,
            const AdimensionalSystemSolution& solution,
            std::string db_path=""
            );
    //! \brief Save a set of solutions as a YAML file
    //!
    //! \param filepath path to the file where to solve the solutions
    //! \param solutions vector of solutions
    //! \param db_path file path to the database
    void save_solutions(
            const std::string& filepath,
            const std::vector<AdimensionalSystemSolution>& solutions,
            std::string db_path=""
            );

private:
    //! \brief Implementation of the saver
    struct SPECMICP_DLL_LOCAL AdimensionalSystemSolutionSaverImpl;
    utils::pimpl_ptr<AdimensionalSystemSolutionSaverImpl> m_impl; //!< Implementation

};

//! \brief Serialize a solution as a JSON-formatted string
inline std::string format_solution_yaml(
    RawDatabasePtr the_database,
    const AdimensionalSystemSolution& the_solution
    )
{
    return AdimensionalSystemSolutionSaver(the_database).format_solution(the_solution);
}

//! \brief Serialize a solution and save it in a file
inline void save_solution_yaml(
    const std::string& filename,
    RawDatabasePtr the_database,
    const AdimensionalSystemSolution& the_solution,
    std::string db_path=""
    )
{
    return AdimensionalSystemSolutionSaver(the_database).save_solution(filename, the_solution, db_path);
}

//! \brief Save a set of solutions to a file
inline void save_solutions_yaml(
    const std::string& filename,
    RawDatabasePtr the_database,
    const std::vector<AdimensionalSystemSolution>& solutions,
    std::string db_path=""
    )
{
    return AdimensionalSystemSolutionSaver(the_database).save_solutions(filename, solutions, db_path);
}

} //end namespace io
} //end namespace specmicp

#endif // SPECMICP_SPECMICP_ADIMENSIONALSYSTEMSOLUTIONSAVER_HPP
