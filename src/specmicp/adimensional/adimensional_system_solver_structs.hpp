/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_SPECMICP_ADIMENSIONALSYSTEMSOLVERSTRUCTS_HPP
#define SPECMICP_SPECMICP_ADIMENSIONALSYSTEMSOLVERSTRUCTS_HPP

#include "adimensional_system_structs.hpp"

#include "specmicp_common/types.hpp"
#include "specmicp_common/micpsolver/micpsolver_structs.hpp"
#include "specmicp_common/physics/units.hpp"

//! \file adimensional_system_solver_structs.hpp Options and conditions for AdimensionalSystemSolver

namespace specmicp {


//! \struct AdimensionalSystemSolverOptions
//! \brief Options for the Equilibrium solver
//!
//! Most of the options are contained in the MiCP solver options or the AdimensionalSystem options
struct SPECMICP_DLL_PUBLIC AdimensionalSystemSolverOptions
{
    //! Allow the restarting if the problem failed the first part
    bool allow_restart;
    //! If true use the pcfm method to initialize the problem
    bool use_pcfm;
    //! Use pcfm before every trial
    bool force_pcfm;
    //! Set of units
    units::UnitsSet units_set;
    //! Options of the MiCP solver
    micpsolver::MiCPSolverOptions solver_options {};
    //! Options of the system
    AdimensionalSystemOptions     system_options {};

    AdimensionalSystemSolverOptions();
};


} // end namespace specmicp

#endif //SPECMICP_SPECMICP_ADIMENSIONALSYSTEMSOLVERSTRUCTS_HPP
