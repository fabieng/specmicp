/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_ADIMENSIONALSYSTEM_PCFM_HPP
#define SPECMICP_ADIMENSIONALSYSTEM_PCFM_HPP

#include "adimensional_system_pcfm_structs.hpp"

#include "specmicp_database/database.hpp"
#include "specmicp_common/options_handler.hpp"

//! \file adimensional_system_pcfm.hpp The positive continuous fraction method

namespace specmicp {

// forward declaration
class AdimensionalSystem;

//! \brief The positive continuous fraction method
//!
//! It is particularly well adapted for aqueous only systems
//!
//!  References :
//!     - Carrayrou (2002)
class SPECMICP_DLL_LOCAL AdimensionalSystemPCFM:
        public OptionsHandler<PCFMOptions>
{
public:
    //! \brief Constructor, default options
    AdimensionalSystemPCFM(std::shared_ptr<AdimensionalSystem> program);

    //! \brief Constructor, custom options
    AdimensionalSystemPCFM(
            std::shared_ptr<AdimensionalSystem> program,
            const PCFMOptions& options);

    //! \brief Pre-solve the problem
    //! \param x Vector of the main variable (should be initialized)
    PCFMReturnCode solve(Vector& x);
    //! \brief Run one iteration of the PCFM method
    void one_iteration(Vector& x);
    //! \brief Run the PCFM method for one component
    void solve_component(index_t component, Vector& x);
    //! \brief Run the PCFM method for the surface equation
    void solve_surface(Vector& x);
    //! \brief Check the validity of the problem; for now just print error message
    //! \return Return true if the problem is true
    bool check_validity();

private:
    RawDatabasePtr m_data; //!< The database
    std::shared_ptr<AdimensionalSystem> m_program; //!< The program
    Vector m_errors {}; //!< Store convergence criterion
};

//! \brief Initialize a system using the positive continuous fraction method
//!
//! \param program shared_ptr to the program
//! \param x Vector of the main variables (should be initialized)
//!
//! \sa AdimensionalSystemPCFM
inline PCFMReturnCode positive_continuous_fraction_method(
        std::shared_ptr<AdimensionalSystem> program,
        Vector& x
        )
{
    return AdimensionalSystemPCFM(program).solve(x);
}



} // end namespace specmicp

#endif // SPECMICP_ADIMENSIONALSYSTEM_PCFM_HPP
