/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_ADIMENSIONALSYSTEMNUMBERING
#define SPECMICP_ADIMENSIONALSYSTEMNUMBERING

#include "specmicp_common/types.hpp"
#ifndef SPECMICP_DATABASE_HPP
#include "specmicp_database/database.hpp"
#endif

namespace specmicp {

//! \brief The equation numbering scheme for the adimensional system
//!
//! This is the numbering of the main variables (the non-reduced vector)
class AdimemsionalSystemNumbering
{
public:

    //! \brief Initialize the numbering scheme
    //! \param ptr_database shared_ptr to the database container
    AdimemsionalSystemNumbering(RawDatabasePtr ptr_database):
        m_data(ptr_database)
    {}

    //! \brief Return the dof for water conservation
    index_t dof_water() const noexcept {return m_data->water_index();}
    //! \brief Return the dof corresponding to the component conservation
    //! \param component Index of the component
    index_t dof_component(index_t component) const NOEXCEPT {
        specmicp_assert_component_bounds(component, m_data);
        return component;
    }
    //! \brief Return the dof corresponding to the electron activity
    index_t dof_electron() const noexcept {
        return m_data->electron_index();
    }
    //! \brief Return the dof corresponding to the sorption sites conservation
    index_t dof_surface() const noexcept {
        return m_data->nb_component();
    }
    //! \brief Return the offset for the dof of minerals
    //! \sa dof_mineral
    index_t offset_minerals() const noexcept {
        return m_data->nb_component()+1;
    }
    //! \brief Return the dof for mineral 'mineral'
    //! \param mineral Index of the mineral (in the database)
    index_t dof_mineral(index_t mineral) const NOEXCEPT {
        specmicp_assert_mineral_bounds(mineral, m_data);
        return offset_minerals()+mineral;
    }

    //! \brief Return the number of dofs
    index_t total_dofs() const noexcept {
        return m_data->nb_component()+m_data->nb_mineral()+1;
    }

    // secondary species
    //! \brief dof of the activitiy coefficient for 'component'
    //! \param component Index of the component (in the database)
    index_t dof_component_gamma(index_t component) const {
        specmicp_assert_component_bounds(component, m_data);
        return component;
    }
    //! \brief dof of a secondary aqueous species
    //! \param aqueous Index of the aqueous species (in the database)
    index_t dof_aqueous(index_t aqueous) const {
        specmicp_assert(aqueous < m_data->nb_aqueous());
        return aqueous;
    }
    //! \brief dof of the activity coefficient for 'aqueous'
    //! \param aqueous Index of the aqueous species (in the database)
    index_t dof_aqueous_gamma(index_t aqueous) const {
        specmicp_assert_aqueous_bounds(aqueous, m_data);
        return m_data->nb_component()+aqueous;
    }

    //! \brief Return the database
    RawDatabasePtr get_database() {
        return m_data;
    }

protected:
    RawDatabasePtr m_data; //!< The database
};

} // end namespace specmicp

#endif // SPECMICP_ADIMENSIONALSYSTEMNUMBERING
