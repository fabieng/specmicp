/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_SPECMICP_ADIMENSIONALSYSTEMSTRUCTS_HPP
#define SPECMICP_SPECMICP_ADIMENSIONALSYSTEMSTRUCTS_HPP

#include "specmicp_common/types.hpp"
#include <vector>

//! \file adimensional_system_structs.hpp
//! \brief Options and constraints for the AdimensionalSystem


namespace specmicp {

//! \struct AdimensionalSystemOptions
//! \brief Options for the Adimensional Systems
//!
//! It is mainly about the secondary variables fixed-point iterations
struct SPECMICP_DLL_PUBLIC AdimensionalSystemOptions
{
    //! Solve for non ideality
    bool non_ideality;
    //! Max iterations for the non ideality model
    index_t non_ideality_max_iter;
    //! Scaling the electron equation
    scalar_t scaling_electron;
    //! Tolerance for non ideality
    scalar_t non_ideality_tolerance;
    //! Under relaxation factor for the conservation of water
    scalar_t under_relaxation_factor;
    //! Log of the molality used to restart the computation
    scalar_t restart_concentration;
    //! Liquid water volume fraction to restart the computation
    scalar_t restart_water_volume_fraction;
    //! Log_10 of the molality for a new component
    scalar_t new_component_concentration;
    //! Factor to start the non-ideality computation
    scalar_t start_non_ideality_computation;
    //! Cutoff for including components in the computation
    scalar_t cutoff_total_concentration;

    AdimensionalSystemOptions(); // in adimensional_system_solver_structs.cpp
};

//! \enum AqueousComponentEquationType
//! \brief Type of an aqueous component equation
enum class AqueousComponentEquationType
{
    NoEquation = no_equation, //!< Not an equation, component is not present in the system
    MassConservation,   //!< Mass balance
    ChargeBalance,      //!< M.B. replaced by charge balance
    FixedFugacity,      //!< M.B. replaced by a fixed fugacity equation
    FixedActivity,      //!< M.B. replaced by a fixed activity equation
    FixedMolality       //!< The molality of the species is fixed
};

//! \enum WaterEquationType
//! \brief The type of the equation solved for the water
enum class WaterEquationType
{
    NoEquation = no_equation,  //!< Amount of water is not solved
    MassConservation,          //!< Water is conserved
    SaturatedSystem,           //!< System is saturated
    FixedSaturation            //!< Saturation is fixed
};

//! \struct FixedFugacityConstraint
//! \brief Struct to contain information needed to solve a fix fugacity problem
struct FixedFugacityConstraint
{
    index_t id_gas;       //!< Index of the fixed-fugacity gas
    index_t id_component; //!< Index of the corresponding component
    scalar_t log_value;   //!< Log_10 of the fugacity

    //! \brief Constructor with initialization
    FixedFugacityConstraint(index_t gas, index_t component, scalar_t logvalue):
        id_gas(gas),
        id_component(component),
        log_value(logvalue)
    {}
};

//! \struct FixedActivityConstraint
//! \brief Struct to contain information needed to solve a fix activity problem.
struct FixedActivityConstraint
{
    index_t id_component; //!< Index of the fixed-activity component
    scalar_t log_value;   //!< Log_10 of the activity

    //! \brief Constructor with initialization
    FixedActivityConstraint(index_t component, scalar_t logvalue):
        id_component(component),
        log_value(logvalue)
    {}
};

//! \struct FixedMolalityConstraint
//! \brief Contain information about a fixed molality constraint
struct FixedMolalityConstraint
{
    index_t id_component;   //!< Index of the component
    scalar_t log_value;     //!< Log_10 of the molality

    //! \brief Constructor with initialization
    FixedMolalityConstraint(index_t component, scalar_t logvalue):
        id_component(component),
        log_value(logvalue)
    {}
};

//! \enum SurfaceEquationType
//! \brief The model for surface sorption
enum class SurfaceEquationType
{
    NoEquation = no_equation,     //!< Do not include surface sorption
    Equilibrium     //!< Equilibrium model
};

//! \struct SurfaceConstraint
//! This struct contains the information to set-up the surface sorption model
struct SurfaceConstraint
{
    SurfaceEquationType model_type; //!< The model to use
    scalar_t concentration;         //!< The total concentration of sorption sites

    //! \brief By default, we don't include surface sorption in the computation
    SurfaceConstraint() noexcept:
        model_type(SurfaceEquationType::NoEquation),
        concentration(0.0)
    {}

    //! \brief When a concentration is supplied, the surface sorption model is equilibrium
    SurfaceConstraint(scalar_t surface_concentration) noexcept:
        model_type(SurfaceEquationType::Equilibrium),
        concentration(surface_concentration)
    {}
};

//! \enum ElectronEquationType the type of the equation for the electron
enum class ElectronEquationType
{
    NoEquation = no_equation,     //!< Do not compute the concentration equation of the electron
    Equilibrium,    //!< Set the concentration of electron to be 0
    FixedpE         //!< Activity of the electron is fixed
};


//! \enum MineralConstraintType
//! \brief Solid phase constraint type
enum class MineralConstraintType
{
    Equilibrium,      //!< Default - equilibrium phase
    NoPrecipitation   //!< No precipitation allowed
};

//! \brief Solid phase constraint
struct MineralConstraint
{
    index_t id_mineral {no_species};     //!< Solid phase to be constrained
    MineralConstraintType equation_type; //!< Type of the constraint
    scalar_t param {0.0};                //!< Numerical value attached to the constraint

    //! \brief Default constructor, no initialization
    MineralConstraint() {}

    //! \brief Constructor with initialization
    MineralConstraint(
            index_t mineral,
            MineralConstraintType constraint,
            scalar_t parameter
            ):
        id_mineral(mineral),
        equation_type(constraint),
        param(parameter)
    {}
};

//! \struct ElectronConstraint
//! \brief the constraint for the electron
struct ElectronConstraint
{
    ElectronEquationType equation_type{ElectronEquationType::NoEquation}; //!< The equation type
    scalar_t fixed_value;         //!< The fixed value of pE if needed
    index_t species {no_species}; //!< In case of fixed pE, this is the reaction to use

    //! \brief By default we assume equilibrium
    ElectronConstraint():
        equation_type(ElectronEquationType::Equilibrium),
        fixed_value(0.0)
    {}

    //! \brief When a value is provided, we assume that the pE is fixed
    ElectronConstraint(scalar_t pe_value, scalar_t aqueous_species):
        equation_type(ElectronEquationType::FixedpE),
        fixed_value(pe_value),
        species(aqueous_species)

    {}
};

//! \brief Return the partial pressure as function of the liquid saturation
//!
//! This function must return the pressure in Pa
using water_partial_pressure_f = std::function<scalar_t (scalar_t)>;

//! \brief Constraints for the partial pressure model
struct WaterPartialPressureConstraint
{
    bool use_partial_pressure_model; //!< True if we use partial pressure model
    water_partial_pressure_f partial_pressure_model; //!< The model given by the user

    //! \brief By default the partial pressure constraint is disabled
    WaterPartialPressureConstraint():
        use_partial_pressure_model(false),
        partial_pressure_model(nullptr)
    {}

    //! \brief Enble the water partial pressure model
    WaterPartialPressureConstraint(water_partial_pressure_f& model):
        WaterPartialPressureConstraint()
    {
        if (model != nullptr)
        {
            use_partial_pressure_model = true;
            partial_pressure_model = model;
        }
    }
};


//! \struct AdimensionalSystemConstraints
//! \brief Struct to contains the "Boundary conditions" for the AdimensionalSystem
//!
//! \ingroup specmicp_api
struct AdimensionalSystemConstraints
{
    //! Total concentrations
    Vector total_concentrations;

    // Water

    //! Water equation
    WaterEquationType water_equation {WaterEquationType::MassConservation};
    //! The partial pressure of water (if any)
    WaterPartialPressureConstraint water_partial_pressure {};
    //! \brief A parameter for the water equation
    //!
    //! It is the saturation if the system has a fixed saturation,
    //! or invalid otherwise
    scalar_t water_parameter {-1.0};

    //! The equation for this component is replace by the charge balance
    index_t charge_keeper{no_species};

    //! Contains information about fixed fugacity constraints
    std::vector<FixedFugacityConstraint> fixed_fugacity_cs {};
    //! Contains information about fixed activity constraints
    std::vector<FixedActivityConstraint> fixed_activity_cs {};
    //! Contains information about fixed molality constraints
    std::vector<FixedMolalityConstraint> fixed_molality_cs {};

    //! Additional constraints for solid phases
    std::vector<MineralConstraint> mineral_constraints {};

    //! \brief Volume fraction of inert solid
    //!
    //! (inert in the equilibrium computation)
    scalar_t inert_volume_fraction{ 0.0 };
    SurfaceConstraint surface_model{};         //!< Surface sorption model
    ElectronConstraint electron_constraint{};  //!< constraint for the electron

    //! \brief Default constructor
    AdimensionalSystemConstraints():
        total_concentrations()
    {}

    //! \brief Constructor, initialize total concentrations
   AdimensionalSystemConstraints(const Vector& total_concs):
       total_concentrations(total_concs)
   {}

    //! \brief Set the total concentrations
    void set_total_concentrations(const Vector& total_concs) {total_concentrations = total_concs;}

    //! \brief Enable the conservation of water
    void enable_conservation_water() noexcept {water_equation = WaterEquationType::MassConservation;}
    //! \brief Disable the conservation of water
    void disable_conservation_water() noexcept {water_equation = WaterEquationType::NoEquation;}
    //! \brief The system is saturated
    void set_saturated_system() noexcept {water_equation = WaterEquationType::SaturatedSystem;}
    //! \brief Fixed the saturation in the system
    void set_fixed_saturation(scalar_t saturation) noexcept {
        water_equation = WaterEquationType::FixedSaturation;
        water_parameter = saturation;
    }

    //! \brief Disable the surface sorption model
    void disable_surface_model() noexcept {surface_model.model_type = SurfaceEquationType::NoEquation;}
    //! \brief Enable the surface sorption model
    //! \param surface_sorption_model_concentration concentration of the surface sorption sites
    void enable_surface_model(scalar_t surface_sorption_model_concentration) noexcept {
        surface_model.model_type = SurfaceEquationType::Equilibrium;
        surface_model.concentration = surface_sorption_model_concentration;
    }

    //! \brief Set the charge keeper to 'component'
    //!
    //! \param component Index of the component (in the database)
    void set_charge_keeper(index_t component) noexcept {
        charge_keeper = component;
    }

    //! \brief Add a fixed fugacity gas condition
    //!
    //! \param constraint struct containing the information about a fixed-fugacity constraint
    void add_fixed_fugacity_gas(const FixedFugacityConstraint& constraint) {
        fixed_fugacity_cs.push_back(constraint);
    }
    //! \brief Add a fixed fugacity gas condition
    //!
    //! \param gas Index of the gas (in the database)
    //! \param component Index of the corresponding component (in the database)
    //! \param logvalue Log_10 of the fugacity
    void add_fixed_fugacity_gas(index_t gas, index_t component, scalar_t logvalue) {
        fixed_fugacity_cs.emplace_back(FixedFugacityConstraint(gas, component, logvalue));
    }
    //! \brief Add a fixed activity component condition
    //!
    //! \param constraint struct containing the information about a fixed-activity constraint
    void add_fixed_activity_component(const FixedActivityConstraint& constraint) {
        fixed_activity_cs.push_back(constraint);
    }
    //! \brief Add a fixed activity condition for a component
    //!
    //! \param component Index of the corresponding component (in the database)
    //! \param log_value Log_10 of the activity
    void add_fixed_activity_component(index_t component, scalar_t log_value) {
        fixed_activity_cs.emplace_back(FixedActivityConstraint(component, log_value));
    }

    //! \brief Add a fixed molality condition for a component
    //!
    //! \param component Index of the corresponding component (in the database)
    //! \param log_value Log_10 of the molality
    void add_fixed_molality_component(index_t component, scalar_t log_value) {
        fixed_molality_cs.emplace_back(FixedMolalityConstraint(component, log_value));
    }

    //! \brief Set the inert volume fraction
    //!
    //! The volume fraction of the inert phase is used to offset the saturation.
    //! This inert phase may correspond to aggregates or solid phases governed by kinetics.
    //!
    //! \param value volume fraction of the inert phase
    void set_inert_volume_fraction(scalar_t value) noexcept {
        inert_volume_fraction = value;
    }

    //! \brief Set a partial pressure model for water
    //!
    //! The partial pressure model for water is a function taking the saturation
    //!  and returning the partial pressure of water
    void set_water_partial_pressure_model(water_partial_pressure_f model)
    {
        water_partial_pressure.use_partial_pressure_model = true;
        water_partial_pressure.partial_pressure_model = model;
    }

    //! \brief Set an upper bound for the volume fraction of the mineral
    //!
    //! Emulate a no precipitation condition
    void set_mineral_upper_bound(index_t mineral, scalar_t upper_volume_fraction)
    {
        mineral_constraints.emplace_back(mineral,
                                         MineralConstraintType::NoPrecipitation,
                                         upper_volume_fraction);
    }

//    //! \brief Set the system at a fixed pE
//    void set_fixed_pe(scalar_t pe_value, index_t aqueous_species) noexcept {
//        electron_constraint = ElectronConstraint(pe_value, aqueous_species);
//    }
};

} // end namespace specmicp

#endif // SPECMICP_SPECMICP_ADIMENSIONALSYSTEMSTRUCTS_HPP
