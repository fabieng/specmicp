/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_SPECMICP_ADIMENSIONALSYSTEMSOLUTIONEXTRACTOR_HPP
#define SPECMICP_SPECMICP_ADIMENSIONALSYSTEMSOLUTIONEXTRACTOR_HPP

#include "adimensional_system_solution.hpp"
#include "adimensional_system_numbering.hpp"

#include "specmicp_common/types.hpp"
#include "specmicp_database/database.hpp"

#include "specmicp_common/physics/units.hpp"
#include "specmicp_common/physics/constants.hpp"

#include "specmicp_common/pimpl_ptr.hpp"

//! \file adimensional_system_solution_extractor.hpp Obtain data from AdimensionalSystemSolution

namespace specmicp {

//! \class AdimensionalSystemSolutionExtractor
//! \brief Obtain physical variables from AdimensionalSystemSolution
//!
//! This class can't modify the solution, use specmicp::AdimensionalSystemSolutionModificator for this.
//!
//! \ingroup specmicp_api
//! \sa specmicp::AdimensionalSystemSolutionModificator
class SPECMICP_DLL_PUBLIC AdimensionalSystemSolutionExtractor:
        public AdimemsionalSystemNumbering,
        public units::UnitBaseClass

{
public:
    // public members
    // ###############

    //! \brief Constructor
    //!
    //! \param solution Reference to a solution from the AdimensionalSystemSolution
    //! \param thedatabase shared_ptr to the database
    //! \param units_set Units used by the solver
    //!
    //! \warning This is just a reader, it does not store the information
    AdimensionalSystemSolutionExtractor(
            const AdimensionalSystemSolution& solution,
            RawDatabasePtr thedatabase,
            units::UnitsSet units_set);

    //! \brief Move constructor
    AdimensionalSystemSolutionExtractor(AdimensionalSystemSolutionExtractor&& other);
    //! \brief Destructor
    ~AdimensionalSystemSolutionExtractor();

    // Primary variables
    // =================

    // water
    // ------

    //! \brief Return the volume fraction of water
    scalar_t volume_fraction_water() const {
        return m_solution.main_variables(dof_water());}

    //! \brief Return the density of water
    scalar_t density_water() const;

    // electron
    // ---------

    //! \brief Return the log of the activity electron
    scalar_t log_activity_electron() const {
        return m_solution.main_variables(dof_electron());
    }
    //! \brief Return the activity of the electron
    scalar_t activity_electron() const {
        return pow10(log_activity_electron());
    }
    //! \brief Return the pE
    scalar_t pE() const {
        return -log_activity_electron();
    }
    //! \brief The electric potential
    scalar_t Eh() const {
        return std::log(10.0)*constants::gas_constant*units::celsius(25.0)/constants::faraday_constant*pE();
    }

    // component
    // ---------

    //! \brief Return the log_10 of the molality of 'component'
    scalar_t log_molality_component(index_t component) const;

    //! \brief Return the molality of 'component'
    scalar_t molality_component(index_t component) const;

    // solid phases
    // ------------

    //! \brief Return the volume fraction of 'mineral'
    scalar_t volume_fraction_mineral(index_t mineral) const {
        return m_solution.main_variables(dof_mineral(mineral));
    }

    // surface
    // -------

    //! \brief Return the concentration of free surface
    scalar_t log_free_surface_concentration() const {
        return m_solution.main_variables(dof_surface());
    }
    //! \brief Return the concentration of free surface
    scalar_t free_surface_concentration() const {
        return pow10(m_solution.main_variables(dof_surface()));
    }

    // Secondary variables
    // ===================

    //! \brief Return the ionic strength of the solution
    scalar_t ionic_strength() const {return m_solution.ionic_strength;}

    // component
    // ---------

    //! \brief Return the log_10 of the activity coefficient of 'component'
    scalar_t log_activity_coefficient_component(index_t component) const {
        return m_solution.log_gamma(dof_component_gamma(component));
    }
    //! \brief Return the activity coefficient of 'component'
    scalar_t activity_coefficient_component(index_t component) const {
        return pow10(log_activity_coefficient_component(component));
    }
    //! \brief Return the log_10 of the activity of 'component
    scalar_t log_activity_component(index_t component) const {
        return log_molality_component(component) + log_activity_coefficient_component(component);
    }
    //! \brief Return the activity of 'component'
    scalar_t activity_component(index_t component) const {
        return pow10(log_activity_component(component));
    }

    // aqueous species
    // ---------------
    //! \brief Return the molality of secondary specis 'aqueous'
    scalar_t molality_aqueous(index_t aqueous) const;
    //! \brief Return the log10 of the activity ocefficient of secondary species 'aqueous'
    scalar_t log_activity_coefficient_aqueous(index_t aqueous) const {
        return m_solution.log_gamma(dof_aqueous_gamma(aqueous));
    }
    //! \brief Return the activity coefficient of secondary species 'aqueous'
    scalar_t activity_coefficient_aqueous(index_t aqueous) const {
        return pow10(log_activity_coefficient_aqueous(aqueous));
    }
    //! \brief Return the activity of secondary species 'aqueous'
    scalar_t activity_aqueous(index_t aqueous) const {
        return activity_coefficient_aqueous(aqueous)*molality_aqueous(aqueous);
    }

    // gas fugacity
    // -------------
    //! \brief Return fugacity for 'gas'
    scalar_t fugacity_gas(index_t gas) const {
        return m_solution.gas_fugacities(gas);
    }

    // Sorbed species
    // --------------
    //! \brief Return sorbed species molalities for 'sorbed'
    scalar_t molality_sorbed_species(index_t sorbed) const {
        return m_solution.sorbed_molalities(sorbed);
    }

    // Tertiary variables
    // ==================

    // Water
    // -----

    //! \brief Return the saturation of water
    scalar_t saturation_water() const {return volume_fraction_water()/porosity();}

    //! \brief Return the mass concentration of water
    scalar_t mass_concentration_water() const;

    //! \brief Return the pH of the solution
    scalar_t pH() const;

    // Component
    // ---------

    //! \brief Return the total aqueous concentration w.r.t solution (x*mol/kg)
    scalar_t total_aqueous_concentration(index_t component) const;
    //! \brief Return the total solid concentration w.r.t to REV(x*mol/m^3)
    scalar_t total_solid_concentration(index_t component) const;
    //! \brief Return the total immobile concentration w.r.t ro REV (x*mol/m^3)
    scalar_t total_immobile_concentration(index_t component) const;
    //! \brief Return the total gaseous concentration w.r.t to gas phase (x*mol/m^3)
    scalar_t total_gaseous_concentration(index_t component) const;
    //! \brief Return the total concentration vector (x*mol/m^3)
    scalar_t total_concentration(index_t component) const;
    //! \brief Return the vector of total concentration (x*mol/m^3)
    Vector total_concentrations() const;

    // Gas phase
    // ---------

    //! \brief Return the saturation of the gas phase
    scalar_t saturation_gas_phase() const {return 1-saturation_water();}

    //! \brief Return the volume fraction occupied by the gas phase
    scalar_t volume_fraction_gas_phase() const {
        return porosity() - volume_fraction_water();
    }

    // Component
    // ---------

    // Solid phases
    // ------------

    //! \brief Return the concentration of 'mineral'
    //!
    //! \param mineral Index of the mineral (in the database)
    scalar_t mole_concentration_mineral(index_t mineral) const;

    //! \brief Return the mass concentration of 'mineral'
    //!
    //! \param mineral Index of the mineral (in the database)
    scalar_t mass_concentration_mineral(index_t mineral) const;

    //! \brief Return the total saturation of all minerals
    //! Deprecated, use the volume fraction function instead
    scalar_t total_saturation_minerals() const;
    //! \brief Return the volume fraction occupied by the minerals
    scalar_t volume_fraction_minerals() const {return total_saturation_minerals();}

    //! \brief Return the inert volume fraction
    scalar_t volume_fraction_inert() const {return m_solution.inert_volume_fraction;}

    //! \brief Return the porosity
    scalar_t porosity() const {return 1 - total_saturation_minerals() - volume_fraction_inert();}

    //! \brief Return the saturation index for 'mineral'
    //!
    //! \param mineral Index of the mineral (in the database)
    scalar_t saturation_index(index_t mineral) const;

    //! \brief Return the saturation index for 'mineral_kinetic'
    //!
    //! \param mineral_kinetic Index of the mineral governed by the kinetic (in the database)
    scalar_t saturation_index_kinetic(index_t mineral_kinetic) const;

    //! \brief Return the total mass concentration of the system (liquid + solid at equilibrium)
    scalar_t total_mass_concentration() const;

    //! \brief Return the total mass concentration of the solids at equilibrium)
    scalar_t total_solid_mass_concentration() const;

    //! \brief Return the vector of main variables, can be used to initialize the solver
    Vector get_main_variables() const {return m_solution.main_variables;}

    //! \brief Return a copy of the solution
    AdimensionalSystemSolution get_solution() const {return m_solution;}



protected:
    // private attributes
    // ##################
    const AdimensionalSystemSolution& m_solution; //!< Reference to the solution

    //! \brief A cache to store some computations
    struct SPECMICP_DLL_LOCAL ExtractorCache;
    utils::pimpl_ptr<ExtractorCache> m_cache; //!< Cache to avoid recomputing stuff

};

//! \class AdimensionalSystemSolutionModificator
//! \brief Special class to modify (scale) the solution
//!
//! The set of modification are limited to straightforward modification that does not modify the equilibrium
//!
//! \sa AdimensionalSystemExtractor
//! \ingroup specmicp_api
class SPECMICP_DLL_PUBLIC AdimensionalSystemSolutionModificator: public AdimensionalSystemSolutionExtractor
{
public:
    //! \brief Constructor
    AdimensionalSystemSolutionModificator(
            AdimensionalSystemSolution& solution,
            RawDatabasePtr thedatabase,
            units::UnitsSet units_set):
        AdimensionalSystemSolutionExtractor(solution, thedatabase, units_set),
        m_nonconst_solution(solution)
    {}

    //! \brief Scale the solid phases with respect to the total solid concentration of a component
    //!
    //! \param component Index of the component (in the database)
    //! \param new_value New value of the total solid concentration
    void scale_total_concentration(index_t component, scalar_t new_value);

    //! \brief Remove the solid phases
    void remove_solids();

    //! \brief Set some species to be considered as governed by kinetics
    //!
    //! \param[in,out] list_species list of minerals to flag as kinetics,
    //!         The list will contain the new indexes of the corresponding solid phases;
    //! \return the vector of saturation
    //!
    //! This function modify the databse
    Vector set_minerals_kinetics(std::vector<index_t>& list_species);

private:
    AdimensionalSystemSolution& m_nonconst_solution; //!< Brief Reference to the solution
};

} // end namespace specmicp

#endif // SPECMICP_SPECMICP_ADIMENSIONALSYSTEMSOLUTIONEXTRACTOR_HPP
