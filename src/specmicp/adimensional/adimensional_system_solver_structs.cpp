/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "adimensional_system_solver_structs.hpp"
#include "config_default_options_solver.h"

namespace specmicp {


AdimensionalSystemOptions::AdimensionalSystemOptions():
    non_ideality(SPECMICP_DEFAULT_ENABLE_NONIDEAL),
    non_ideality_max_iter(SPECMICP_DEFAULT_NONIDEAL_MAX_ITER),
    scaling_electron(SPECMICP_DEFAULT_SCALING_ELECTRON),
    non_ideality_tolerance(SPECMICP_DEFAULT_NONIDEAL_TOL),
    under_relaxation_factor(SPECMICP_DEFAULT_UNDERRELAX_FACTOR),
    restart_concentration(SPECMICP_DEFAULT_RESTART_CONC),
    restart_water_volume_fraction(SPECMICP_DEFAULT_RESTART_WATER),
    new_component_concentration(SPECMICP_DEFAULT_NEW_COMPONENT_CONC),
    start_non_ideality_computation(SPECMICP_DEFAULT_TRHSOLD_START_NONIDEAL),
    cutoff_total_concentration(SPECMICP_DEFAULT_CUTOFF_TOT_CONC)
{}

AdimensionalSystemSolverOptions::AdimensionalSystemSolverOptions():
    allow_restart(SPECMICP_DEFAULT_ALLOW_RESTART),
    use_pcfm(SPECMICP_DEFAULT_USE_PCFM),
    force_pcfm(SPECMICP_DEFAULT_FORCE_PCFM)
    {
        units_set.length = SPECMICP_DEFAULT_LENGTH_UNIT;
        // disable the descent condition check
        solver_options.maxstep = SPECMICP_DEFAULT_MAX_STEP;
        solver_options.factor_descent_condition = SPECMICP_DEFAULT_FACTOR_DESC_COND;
    }

} //end namespace spemicp
