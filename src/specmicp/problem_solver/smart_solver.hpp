/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_SPECMICP_SMARTSOLVER_HPP
#define SPECMICP_SPECMICP_SMARTSOLVER_HPP

//! \file specmicp/problem_solver/smart_solver.hpp
//! \brief 'smart solver' for a speciation system
//!
//! This solver is 'smart' only in the sense that it tries very hard
//! to find a solution by seeking subproblem easier to solve in case
//! of failure

#include "specmicp_common/types.hpp"
#include "specmicp_common/pimpl_ptr.hpp"

#include <memory>
#include <unordered_map>

// forward declaration
namespace specmicp {
struct AdimensionalSystemConstraints;
struct AdimensionalSystemSolution;
struct AdimensionalSystemSolverOptions;
class ReactantBox;

namespace database {
    struct DataContainer;
} // end namespace database

namespace micpsolver {
struct MiCPPerformance;
} // end namespace micpsolver
} // end namespace specmicp


// interface
namespace specmicp {

//! \brief A 'smart' speciation solver
//!
//! This solver tries several strategy to solve a problem.
//!
//! The first strategy is always to try to solve the user input as it is given
class SPECMICP_DLL_PUBLIC SmartAdimSolver
{
public:
    //! \brief Initialize the solver with constraints and options
    SmartAdimSolver(
            std::shared_ptr<database::DataContainer> raw_data,
            const AdimensionalSystemConstraints& constraints,
            const AdimensionalSystemSolverOptions& options
            );
    //! \brief Initialize the solver with reactant box and options
    SmartAdimSolver(
            std::shared_ptr<database::DataContainer> raw_data,
            const ReactantBox& reactant_box,
            const AdimensionalSystemSolverOptions& options,
            bool modify_db = true
            );
    //! \brief Initialize the solver with reactant box
    SmartAdimSolver(
            std::shared_ptr<database::DataContainer> raw_data,
            const ReactantBox& reactant_box,
            bool modify_db = true
            );
    //! \brief Destructor
    ~SmartAdimSolver();

    //! \brief Solve the problem
    bool solve();
    //! \brief Return true if the problem is solved
    bool is_solved();

    //! \brief Set the seed used to initialize the solution
    void set_warmstart(const AdimensionalSystemSolution& previous_solution);
    //! \brief Set the seed used to initialize the solution
    void set_warmstart(AdimensionalSystemSolution&& previous_solution);
    //! \brief Return the solution
    AdimensionalSystemSolution get_solution();

    //! \brief Return the options
    AdimensionalSystemSolverOptions& get_options();

    //! \brief Give an initial value for the volume fraction of water
    void set_init_volfrac_water(scalar_t volume_fraction);
    //! \brief Give an initial value for the molality of aqueous components
    void set_init_molality(scalar_t molality);
    //! \brief Give an initial value for the molality of components
    void set_init_molality(std::unordered_map<std::string, scalar_t> molalities);
    //! \brief Give an initial value for the volume fraction of minerals
    void set_init_volfrac_mineral(std::unordered_map<std::string, scalar_t> volume_fraction);


private:
    //! \brief Implementation of the smart solver
    struct SPECMICP_DLL_LOCAL SmartAdimSolverImpl;
    utils::pimpl_ptr<SmartAdimSolverImpl> m_impl; //!< the implementation
};

} // end namespace specmicp

#endif // SPECMICP_SPECMICP_SMARTSOLVER_HPP
