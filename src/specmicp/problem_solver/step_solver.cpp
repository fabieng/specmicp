/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "step_solver.hpp"

#include "specmicp/adimensional/adimensional_system_solution_extractor.hpp"
#include "specmicp/problem_solver/smart_solver.hpp"
#include "specmicp/adimensional/adimensional_system_solver.hpp"

#include "specmicp/problem_solver/reactant_box.hpp"

#include "specmicp_common/log.hpp"

#include <limits>
#include <iostream>

namespace specmicp {

// =========== //
// StepProblem //
// =========== //

struct StepProblem::StepProblemImpl
{
    StepProblemImpl() {}

    stepper_next_step_f m_constraints_updater;
    stepper_stop_condition_f m_stopper;

    RawDatabasePtr m_data;
    units::UnitsSet m_units;

    AdimensionalSystemConstraints m_constraints;

    AdimensionalSystemSolutionExtractor get_extractor(
            const AdimensionalSystemSolution * const solution
            ) {
        return AdimensionalSystemSolutionExtractor(*solution, m_data, m_units);
    }
};

StepProblem::StepProblem():
    m_impl(utils::make_pimpl<StepProblemImpl>())
{}


StepProblem::StepProblem(
        stepper_next_step_f updater,
        stepper_stop_condition_f stopper,
        AdimensionalSystemConstraints&& constraints
        ):
    m_impl(utils::make_pimpl<StepProblemImpl>())
{
    m_impl->m_constraints_updater = updater;
    m_impl->m_stopper = stopper;

    m_impl->m_constraints = std::move(constraints);
}


StepProblem::StepProblem(StepProblem &&other):
    m_impl(std::move(other.m_impl))
{

}

StepProblem::~StepProblem() = default;


void StepProblem::set_constraint_updater(stepper_next_step_f updater)
{
    m_impl->m_constraints_updater = updater;
}

void StepProblem::set_stop_condition(stepper_stop_condition_f stopper)
{
    m_impl->m_stopper = stopper;
}

void StepProblem::set_stop_condition_step(uindex_t stop_step)
{
    set_stop_condition([stop_step](
        specmicp::uindex_t cnt,
        const specmicp::AdimensionalSystemSolutionExtractor& _)
        {
            auto status = specmicp::StepProblemStatus::OK;
            if (cnt >= stop_step) {status = specmicp::StepProblemStatus::Stop;}
            return status;
        });
}

void StepProblem::set_initial_constraints(
        ReactantBox& reactant_box,
        bool modify_db
        )
{
    m_impl->m_constraints = reactant_box.get_constraints(modify_db);
    m_impl->m_data  = reactant_box.get_database();
    m_impl->m_units = reactant_box.get_units();
}

bool StepProblem::is_valid()
{
    return (   m_impl->m_constraints_updater != nullptr
           and m_impl->m_stopper != nullptr);
}

AdimensionalSystemConstraints StepProblem::get_constraints() const
{
    return m_impl->m_constraints;
}

StepProblemStatus StepProblem::update_constraints(
        uindex_t cnt,
        const AdimensionalSystemSolution * const solution
        )
{
    if (m_impl->m_constraints_updater == nullptr)
    {
        ERROR << "Bad initialization : no constraints updater in step problem.";
    }
    auto status = m_impl->m_constraints_updater(
                cnt,
                m_impl->get_extractor(solution),
                m_impl->m_constraints
                );
    return status;
}

StepProblemStatus StepProblem::check_stop_condition(
        uindex_t cnt,
        const AdimensionalSystemSolution * const solution)
{
    if (m_impl->m_stopper == nullptr)
    {
        ERROR << "Bad initialization : no stopper in step problem.";
    }
    return m_impl->m_stopper(cnt,
                             m_impl->get_extractor(solution)
                             );
}

std::shared_ptr<database::DataContainer> StepProblem::get_database()
{
    return m_impl->m_data;
}

const units::UnitsSet& StepProblem::get_units()
{
    return m_impl->m_units;
}

// ================= //
// StepProblemRunner //
// ================= //

struct StepProblemRunner::StepProblemRunnerImpl
{
    AdimensionalSystemSolverOptions m_opts;
    AdimensionalSystemSolution m_solution;

    stepper_output_f m_output {nullptr};
};


StepProblemRunner::StepProblemRunner():
    m_impl(utils::make_pimpl<StepProblemRunnerImpl>())
{

}

StepProblemRunner::StepProblemRunner(StepProblemRunner&& other):
    m_impl(std::move(other.m_impl))
{

}

StepProblemRunner::~StepProblemRunner() = default;

void StepProblemRunner::set_warmstart_solution(const AdimensionalSystemSolution& solution)
{
    m_impl->m_solution = solution;
}

void StepProblemRunner::set_warmstart_solution(AdimensionalSystemSolution&& solution)
{
    m_impl->m_solution = std::move(solution);
}

void StepProblemRunner::set_solver_options(AdimensionalSystemSolverOptions& opts)
{
    m_impl->m_opts = opts;
}

AdimensionalSystemSolverOptions& StepProblemRunner::get_solver_options()
{
    return m_impl->m_opts;
}

const AdimensionalSystemSolution& StepProblemRunner::get_solution() const
{
    return m_impl->m_solution;
}

const AdimensionalSystemSolution * const  StepProblemRunner::get_ptr_solution() const
{
    return &m_impl->m_solution;
}

void StepProblemRunner::set_step_output(stepper_output_f output)
{
    m_impl->m_output = output;
}

StepProblemStatus StepProblemRunner::run(StepProblem& problem)
{
    uindex_t cnt = 0;
    StepProblemStatus status;
    while (cnt < std::numeric_limits<uindex_t>::max())
    {
        SmartAdimSolver solver(problem.get_database(),
                               problem.get_constraints(),
                               get_solver_options()
                               );
        if (get_solution().is_valid)
        {
            solver.set_warmstart(get_solution());
        }
        bool ret = solver.solve();
        if (not ret)
        {
            status = StepProblemStatus::Error;
            ERROR << "Failed to solve step " << cnt << " in step problem";
            break;
        }
        // save solution
        m_impl->m_solution = solver.get_solution();
        // output if asked
        if (m_impl->m_output != nullptr)
        {
            const AdimensionalSystemSolution& solref = m_impl->m_solution;
            status = m_impl->m_output(
                         cnt,
                         AdimensionalSystemSolutionExtractor(
                             solref,
                             problem.get_database(),
                             problem.get_units())
                         );
            if (status == StepProblemStatus::Error)
            {
                ERROR << "User detected problem in step " << cnt
                      << " while updating contraints ";
                break;

            }
        }

        ++cnt;

        status = problem.check_stop_condition(cnt, get_ptr_solution());
        if (status == StepProblemStatus::Stop) break;
        else if (status == StepProblemStatus::Error) {
            ERROR << "User detected problem in step " << cnt;
            break;
        }

        status = problem.update_constraints(cnt, get_ptr_solution());
        if (status == StepProblemStatus::Error)
        {
                    ERROR << "User detected problem in step " << cnt
                          << "while updating contraints ";
                    break;
        }
    }

    if (status == StepProblemStatus::Stop) status = StepProblemStatus::OK;
    else status = StepProblemStatus::Error;

    return status;
}

// Functors
// =========


StepProblemStatus StepTotalConcentration::operator() (
        uindex_t cnt,
        const AdimensionalSystemSolutionExtractor& _,
        AdimensionalSystemConstraints& constraints)
{
    constraints.total_concentrations += m_update;
    return StepProblemStatus::OK;
}

} // end namespace specmicp
