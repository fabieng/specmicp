/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_SPECMICP_ADIMKINETICS_KINETICVARIABLES_HPP
#define SPECMICP_SPECMICP_ADIMKINETICS_KINETICVARIABLES_HPP

#include "specmicp_common/types.hpp"
#include "specmicp_database/database.hpp"
#include "specmicp/adimensional/adimensional_system_solution.hpp"

namespace specmicp {
namespace kinetics {

//! \brief Variables used in a kinetic computation
class AdimKineticVariables
{
public:
    //! \brief Constructor, no initial equilibrium solution
    AdimKineticVariables(
            const Vector& total_concentrations,
            const Vector& mineral_concentration
            ):
        m_mineral_kinetics(mineral_concentration),
        m_total_concentrations_initial(total_concentrations),
        m_total_concentrations(total_concentrations),
        m_rate(Vector::Zero(total_concentrations.rows()))
    {}
    //! \brief Constructor with initial equilibrium condition
    AdimKineticVariables(
            const Vector& total_concentrations,
            const Vector& mineral_concentration,
            const AdimensionalSystemSolution& equilibrium_solution
            ):
        m_mineral_kinetics(mineral_concentration),
        m_total_concentrations_initial(total_concentrations),
        m_total_concentrations(total_concentrations),
        m_rate(Vector::Zero(total_concentrations.rows())),
        m_equilibrium_solution(equilibrium_solution)
    {}

    //! \brief Return the number of species considered in the kinetics computation
    index_t get_neq() {return m_mineral_kinetics.rows();}

    // Concentration of minerals
    // ---------------------------

    //! \brief Return mole number of mineral "mineral kinetic"
    scalar_t concentration_mineral(index_t mineral_kinetic) const {return m_mineral_kinetics(mineral_kinetic);}
    //! \brief Return a const reference to the vector of moles number
    const Vector& concentration_minerals() const {return m_mineral_kinetics;}
    //! \brief Return a reference to the vector of moles number
    Vector& concentration_minerals() {return m_mineral_kinetics;}

    // Total concentrations
    // --------------------

    //! \brief Return the total concentration (at equilibrium) of 'component'
    scalar_t total_concentration(index_t component) const {return m_total_concentrations(component);}
    //! \brief Return the total concentration (at equilibrium) of 'component'
    scalar_t& total_concentration(index_t component) {return m_total_concentrations(component);}
    //! \brief Return a const reference to the total concentration (at equilibrium) vector
    const Vector& total_concentrations() const {return m_total_concentrations;}
    //! \brief Return a reference to the total concentration at equilibrium vector
    Vector& total_concentrations() {return m_total_concentrations; }

    // Initial total concentrations
    // ----------------------------

    //! \brief Return the total concentration (at equilibrium) of 'component'
    scalar_t total_concentration_initial(index_t component) const {
        return m_total_concentrations_initial(component);}
    //! \brief Return a const reference to the total concentration (at equilibrium) vector
    const Vector& total_concentrations_initial() const {return m_total_concentrations_initial;}
    //! \brief Return a reference to the total concentration at equilibrium vector
    Vector& total_concentrations_initial() {return m_total_concentrations_initial; }

    // Rates
    // -----

    //! \brief Return the value of the flux for 'component'
    scalar_t rate_component(index_t component) const {
        return m_rate(component);
    }
    //! \brief Return a const reference to the flux vector
    const Vector& rate_components() const {return m_rate;}
    //! \brief Return a reference to the flux vector
    Vector& rate_components() {return m_rate;}
    //! \brief reset the rates
    void reset_rate() {m_rate.setZero();}

    // Equilibrium
    // ------------

    //! \brief Const Reference to the equilibrium state
    const AdimensionalSystemSolution& equilibrium_solution() const {return m_equilibrium_solution;}
    //! \brief Reference to the equilibrium State
    AdimensionalSystemSolution& equilibrium_solution() {return m_equilibrium_solution;}
    //! \brief Update the equilibrium state
    void update_equilibrium_solution(const AdimensionalSystemSolution& other) {m_equilibrium_solution = other;}
private:
    Vector m_mineral_kinetics;             //!< Number of moles of minerals at equilibrium
    Vector m_total_concentrations_initial; //!< Initial total concentrations at equilibrium
    Vector m_total_concentrations;         //!< total concentration at equilibrium,
    Vector m_rate;                         //!< Kinetic rates
    AdimensionalSystemSolution m_equilibrium_solution;        //!< Current equilibrium state
};

} // end namespace kinetics
} // end namespace specmicp

#endif //  SPECMICP_SPECMICP_KINETICS_KINETICVARIABLES_HPP
