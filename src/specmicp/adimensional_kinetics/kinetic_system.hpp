/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_SPECMICP_ADIMKINETICS_KINETICSYSTEM_HPP
#define SPECMICP_SPECMICP_ADIMKINETICS_KINETICSYSTEM_HPP


#include "kinetic_variables.hpp"

#include "specmicp_common/types.hpp"
#include "specmicp_database/database.hpp"

#include "specmicp/adimensional/adimensional_system_solver_structs.hpp"

#include <memory>

namespace specmicp {

//! \namespace specmicp::kinetics
//! \brief Solver for kinetics systems
namespace kinetics {

class AdimKineticModel;

//! \brief Kinetics System,
//!
//! Wrapper for a kinetic model
class AdimKineticSystem
{
public:
    //! \brief Constructor, with default SpecMiCP options
    AdimKineticSystem(
            std::shared_ptr<AdimKineticModel> model,
            const Vector& total_concentrations,
            const Vector& mineral_concentrations,
            AdimensionalSystemConstraints& constraints,
            RawDatabasePtr database
            ):
        m_data(database),
        m_model(model),
        m_constraints(constraints),
        m_variables(total_concentrations, mineral_concentrations)
    {}

    //! \brief Constructor, with custom SpecMiCP options
    AdimKineticSystem(
            std::shared_ptr<AdimKineticModel> model,
            const Vector& total_concentrations,
            const Vector& mineral_concentrations,
            AdimensionalSystemConstraints& constraints,
            const AdimensionalSystemSolution& equilibrium_solution,
            RawDatabasePtr database
            ):
        m_data(database),
        m_model(model),
        m_constraints(constraints),
        m_variables(total_concentrations, mineral_concentrations, equilibrium_solution)
    {}

    //! \brief Compute the kinetics rates to be solved
    //!
    //! Use the kinetic model provided by the user
    void compute_rates(scalar_t x, const Vector& y, Vector& dydt);

    //! \brief Compute the equilibrium state of the solution
    void compute_equilibrium(
            AdimensionalSystemConstraints& constraints,
            AdimensionalSystemSolverOptions& options
            );

    //! \brief Update the total concentrations
    //!
    //! \param y vector of variables (mols of minerals)
    void update_total_concentrations(const Vector& y);

    //! \brief Update the problem to the new initial condition
    void update_to_new_initial_condition(const Vector& y, scalar_t dt);

    //! \brief Right Hand side function for the integrator
    void rhs(scalar_t x, const Vector& y, Vector& dydt, AdimensionalSystemSolverOptions& options)
    {
        update_total_concentrations(y);
        compute_equilibrium(m_constraints, options);
        compute_rates(x, y, dydt);
    }

    //! \brief Return a reference to the variables
    AdimKineticVariables& variables() {return m_variables;}
    //! \brief Return a reference to SpecMiCP constraints
    AdimensionalSystemConstraints& constraints() {return m_constraints;}
private:
    RawDatabasePtr m_data; //!< The database
    std::shared_ptr<AdimKineticModel> m_model; //!< The model
    AdimensionalSystemConstraints m_constraints; //!< SpecMiCP constraints
    AdimKineticVariables m_variables; //!< The variables
};

} // end namespace kinetics
} // end namespace specmicp


#endif //SPECMICP_SPECMICP_ADIMKINETICS_KINETICSYSTEM_HPP
