/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "print.hpp"

#include "dfpm/solver/parabolic_structs.hpp"

namespace specmicp {
namespace io {


void print_transport_options(
        dfpmsolver::ParabolicDriverOptions& options,
        std::ostream& output
        )
{
    output << "  - residual tolerance    "  << options.residuals_tolerance
           << "\n  - absolute tolerance    " << options.absolute_tolerance
           << "\n  - step tolerance        " << options.step_tolerance
           << "\n  - max. iterations       " << options.maximum_iterations
           << "\n  - max. step length      " << options.maximum_step_length
           << "\n  - # iter. at max length " << options.max_iterations_at_max_length
           << "\n  - threshold stationary  " << options.threshold_stationary_point
           << "\n  - quasi newton iter.    " << options.quasi_newton
           << "\n  - coeff. Newton step    " << options.coeff_accept_newton_step
           << "\n";
}


std::string to_string(dfpmsolver::ParabolicDriverReturnCode retcode)
{
    using RetCode = dfpmsolver::ParabolicDriverReturnCode;
    std::string str;
    switch (retcode) {
    case RetCode::ResidualMinimized:
        str = "ResidualMinimized";
        break;
    case RetCode::ErrorMinimized:
        str = "ErrorMinimized";
        break;
    case RetCode::NotConvergedYet:
        str = "NotConvergedYet";
        break;
    case RetCode::StationaryPoint:
        str = "StationaryPoint";
        break;
    case RetCode::ErrorLinearSystem:
        str = "ErrorLinearSystem";
        break;
    case RetCode::MaxStepTakenTooManyTimes:
        str = "MaxStepTakenTooManyTimes";
        break;
    case RetCode::MaxIterations:
        str = "MaxIterations";
        break;
    case RetCode::LinesearchFailed:
        str = "LinesearchFailed";
    default:
        str = "Unknown return code";
        break;
    }
    return str;
}

} // end namespace io
} // end namespace specmicp
