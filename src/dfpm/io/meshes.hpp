/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DFPM_IO_MESHES_HPP
#define SPECMICP_DFPM_IO_MESHES_HPP

//! \file dfpm/io/meshes.hpp
//! \brief Print the meshes

#include "specmicp_common/types.hpp"
#include "dfpm/meshes/mesh1dfwd.hpp"
#include "specmicp_common/physics/units.hpp"

#include <iosfwd>
#include <functional>

namespace specmicp {
namespace io {

class CSVFile;

//! \brief Print 'the_mesh' informations in 'output'
void SPECMICP_DLL_PUBLIC
print_mesh(std::ostream* output, mesh::Mesh1DPtr the_mesh, units::LengthUnit lenght_unit);

//! \brief Print 'the_mesh' in the file 'filepath'
void SPECMICP_DLL_PUBLIC
print_mesh(std::string filepath, mesh::Mesh1DPtr the_mesh, units::LengthUnit lenght_unit);

//! \brief Signature of a function use to obtain information about a node in order to print it
using to_print_each_node_f = std::function<scalar_t (index_t)>;

//! \brief The header for the #print_for_each_node function
//!
//!
void SPECMICP_DLL_PUBLIC
print_header_for_each_node(CSVFile& output, mesh::Mesh1DPtr the_mesh, const std::string& x_label);


//! \brief Output the value of func in a CSV file for every node
//!
//!  A row looks like : x,func(0),func(1),...,func(n)
//!
void SPECMICP_DLL_PUBLIC
print_for_each_node(CSVFile& output, mesh::Mesh1DPtr the_mesh, scalar_t x, to_print_each_node_f& func);

} // end namespace io
} // end namespace specmicp

#endif // SPECMICP_DFPM_IO_MESHES_HPP
