/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "hdf5_mesh.hpp"

#include "dfpm/meshes/mesh1d.hpp"

#include "specmicp_common/io/hdf5/path.hpp"
#include "specmicp_common/io/hdf5/group.hpp"
#include "specmicp_common/io/hdf5/dataset.hpp"

#define MESH_COORDINATES_DSET "coordinates"
#define MESH_FACE_AREA_DSET   "face_area"
#define MESH_CELL_VOL_LEFT    "cell_vol_left"
#define MESH_CELL_VOL_RIGHT   "cell_vol_right"

namespace specmicp {
namespace io {

namespace internal {

//! \brief Implementation of mesh saver in HDF5 format
//!
//! \internal
class SPECMICP_DLL_PUBLIC MeshHDF5Saver
{
public:
    MeshHDF5Saver(mesh::Mesh1DPtr the_mesh):
        m_mesh(the_mesh)
    {}

    void save_mesh_coordinates(
            hdf5::GroupPath& location
            );

    void save_extra_info(
            hdf5::GroupPath& location
            );

private:
    mesh::Mesh1DPtr m_mesh;
};

} //end namespace internal


void save_mesh_coordinates(
            hdf5::GroupPath& location,
            const std::string& name,
            mesh::Mesh1DPtr the_mesh
        )
{
    internal::MeshHDF5Saver saver(the_mesh);
    hdf5::Group mesh_group = location.create_group(name);
    saver.save_mesh_coordinates(mesh_group);
}


void save_mesh(
        hdf5::GroupPath& location,
        const std::string& name,
        mesh::Mesh1DPtr the_mesh
        )
{
    internal::MeshHDF5Saver saver(the_mesh);
    hdf5::Group mesh_group = location.create_group(name);
    saver.save_mesh_coordinates(mesh_group);
    saver.save_extra_info(mesh_group);
}


mesh::Mesh1DPtr read_mesh(
        hdf5::GroupPath &location,
        const std::string& grp_name)
{
    auto grp = location.open_group(grp_name);
    return read_mesh(grp);
}

mesh::Mesh1DPtr read_mesh(hdf5::GroupPath &location)
{
    Vector coordinates = location.read_vector_dataset(MESH_COORDINATES_DSET);

    mesh::Mesh1DStorage store(coordinates.rows());
    store.set_node_coords(coordinates);
    store.set_face_sections(location.read_vector_dataset(MESH_FACE_AREA_DSET));
    store.set_cell_volumes_left(location.read_vector_dataset(MESH_CELL_VOL_LEFT));
    store.set_cell_volumes_right(location.read_vector_dataset(MESH_CELL_VOL_RIGHT));

    return std::make_shared<mesh::Mesh1D>(std::move(store));
}

// Implementation
// ==============
namespace internal {

void MeshHDF5Saver::save_mesh_coordinates(
            hdf5::GroupPath& mesh_group
            )
{
    auto store = m_mesh->get_storage();
    mesh_group.create_vector_dataset(MESH_COORDINATES_DSET,
                                     store.m_data.col(store.id_node_coord));
}

void MeshHDF5Saver::save_extra_info(
            hdf5::GroupPath& mesh_group
            )
{
    auto store = m_mesh->get_storage();
    mesh_group.create_vector_dataset(MESH_FACE_AREA_DSET,
                                     store.m_data.col(store.id_face_section));
    mesh_group.create_vector_dataset(MESH_CELL_VOL_LEFT,
                                     store.m_data.col(store.id_cell_vol_0));
    mesh_group.create_vector_dataset(MESH_CELL_VOL_RIGHT,
                                     store.m_data.col(store.id_cell_vol_1));
}


} //end namespace internal

} //end namespace io
} //end namespace specmicp
