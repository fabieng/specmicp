/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DFPM_IO_CONFIGURATION_HPP
#define SPECMICP_DFPM_IO_CONFIGURATION_HPP

#include "specmicp_common/macros.hpp"
#include "dfpm/meshes/mesh1dfwd.hpp"


namespace YAML {
class Node;
} //end namespace YAML

namespace specmicp {

namespace dfpmsolver {

struct ParabolicDriverOptions;

} //end namespace dfpmsolver

namespace io {

class YAMLConfigHandle;

//! \brief Configure the mesh
mesh::Mesh1DPtr SPECMICP_DLL_PUBLIC
configure_mesh(YAMLConfigHandle&& mesh_section);

//! \brief Configure an uniform mesh
mesh::Mesh1DPtr SPECMICP_DLL_PUBLIC
configure_uniform_mesh1D(YAMLConfigHandle&& uniform_section);

//! \brief Configure a 'ramp' mesh
mesh::Mesh1DPtr SPECMICP_DLL_PUBLIC
configure_ramp_mesh1D(YAMLConfigHandle&& ramp_section);

//! \brief Configure an uniform axisymmetric mesh
mesh::Mesh1DPtr SPECMICP_DLL_PUBLIC
configure_uniform_axisymmetric_mesh1D(YAMLConfigHandle&& axisymmetric_section);

// parabolic driver
// ----------------

//! \brief Configure parabolic driver options
void SPECMICP_DLL_PUBLIC configure_transport_options(
        dfpmsolver::ParabolicDriverOptions& options,
        io::YAMLConfigHandle&& configuration
        );


} //end namespace io
} //end namespace specmicp

#endif // SPECMICP_DFPM_IO_CONFIGURATION_HPP
