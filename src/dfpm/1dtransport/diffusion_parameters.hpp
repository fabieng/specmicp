/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DFPM_1DTRANSPORT_DIFFUSIONPARAMETERS_HPP
#define SPECMICP_DFPM_1DTRANSPORT_DIFFUSIONPARAMETERS_HPP

#include "specmicp_common/types.hpp"

namespace specmicp {
namespace dfpm {

//! \brief Parameter for the saturated diffusion equation
//!
//! \sa specmicp::dfpm::SaturatedDiffusion
struct SaturatedDiffusion1DParameters
{
    Vector porosity; //!< The porosity (one per node)
    Vector diffusion_coefficient; //!< The diffusion coefficient (one per node)

    //! \brief Constructor, initialize structures
    //!
    //! Parameters values must be set independantly
    SaturatedDiffusion1DParameters(index_t nb_nodes):
        porosity(nb_nodes),
        diffusion_coefficient(nb_nodes)
    {}


};

} // end namespace dfpm
} // end namespace specmicp

#endif // SPECMICP_DFPM_1DTRANSPORT_DIFFUSIONPARAMETERS_HPP
