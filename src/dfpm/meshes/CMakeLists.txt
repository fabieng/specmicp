set(dfpm_meshes_srcs
    mesh1d_storage.cpp
)

set(dfpm_meshes_headers
    mesh1dfwd.hpp
    mesh1d.hpp
    mesh1d_storage.hpp
)

add_to_dfpm_srcs_list(dfpm_meshes_srcs)
add_to_dfpm_headers_list(dfpm_meshes_headers)

INSTALL(FILES ${dfpm_meshes_headers}
    DESTINATION ${INCLUDE_INSTALL_DIR}/dfpm/meshes
)
