/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "mesh1d_storage.hpp"
#include <vector>

namespace specmicp {
namespace mesh {


Mesh1DStorage uniform_1d_storage(const Uniform1DMeshGeometry& geom)
{
    Mesh1DStorage store(geom.nb_nodes, Mesh1DType::uniform);


    // first cell
    store.node_coord(0) = 0;
    // middle
    for (auto i = 1; i < geom.nb_nodes; ++i)
    {
        store.node_coord(i) = i*geom.dx;
    }
    // last cell
    store.node_coord((geom.nb_nodes-1)*geom.dx);

    store.set_face_sections(geom.section);


    auto half_cell_vol = geom.dx / 2.0 * geom.section;
    Vector half_cell_vols(Vector::Constant(geom.nb_nodes, half_cell_vol));
    half_cell_vols(0) = 0;
    store.set_cell_volumes_left(half_cell_vols);

    half_cell_vols(0) = half_cell_vol;
    half_cell_vols(geom.nb_nodes - 1) = 0;
    store.set_cell_volumes_right(half_cell_vols);

    return store;
}


Mesh1DStorage rampmesh_1d_storage(const Ramp1DMeshGeometry& geom)
{
    // first compute node coordinates

    // create a vector to hold them => easier to resize
    std::vector<scalar_t> coords;
    // estimation of the number of nodes
    coords.reserve( std::ceil(  geom.length_plateau/geom.dx_max
                   + 2 * geom.length_ramp/(geom.dx_max+geom.dx_min)
                   ) + 5 );


    const scalar_t b = geom.dx_min;
    const scalar_t a = (geom.dx_max - geom.dx_min)/geom.length_ramp;

    scalar_t x = 0;
    coords.push_back(x);
    while (x < geom.length_ramp)
    {
        scalar_t hbar = a*x+b; // average value (function of the distance)
        scalar_t mult = std::floor(hbar/geom.dx_min);
        scalar_t rem = hbar - mult*geom.dx_min;
        if (rem/geom.dx_min < 0.5)
            x += mult*geom.dx_min;
        else
            x += (mult+1)*geom.dx_min;
        coords.push_back(x);
    }
    while(x < geom.length_plateau+geom.length_ramp)
    {
        x += geom.dx_max;
        coords.push_back(x);
    }

    index_t nb_nodes = coords.size();
    Mesh1DStorage store(nb_nodes, Mesh1DType::ramp);

    store.set_node_coords(Eigen::Map<Vector>(coords.data(), nb_nodes));
    // set section
    store.set_face_sections(geom.section);

    // then compute volumes
    Vector left_vols;
    Vector right_vols;

    for (auto e=0; e<(nb_nodes-1); ++e)
    {
        auto dx  = store.node_coord(e+1) - store.node_coord(e);
        auto vol = dx/2*geom.section;

        store.cell_volume_right(e)  = vol;
        store.cell_volume_left(e+1) = vol;
    }
    store.cell_volume_left(0)           = 0;
    store.cell_volume_right(nb_nodes-1) = 0;

    return store;
}

static scalar_t volume_ring(scalar_t r_out, scalar_t r_in, scalar_t height)
{
    return M_PI*height*(std::pow(r_out, 2) - std::pow(r_in, 2));
}

Mesh1DStorage axisymmetric_mesh_storage(
        const Eigen::Ref<Vector>& coords,
        scalar_t height
        )
{
    index_t nb_nodes = coords.rows();

    Mesh1DStorage store(nb_nodes, Mesh1DType::axisymmetric);

    store.set_node_coords(coords);

    for (index_t e=0; e<(nb_nodes-1); ++e)
    {
        auto coord_0   = coords(e);
        auto coord_1   = coords(e+1);
        auto coord_mid = (coord_0 + coord_1)/2.0;

        store.face_section(e)       = 2*M_PI*coord_mid*height;
        store.cell_volume_right(e)  = volume_ring(coord_0, coord_mid, height);
        store.cell_volume_left(e+1) = volume_ring(coord_mid, coord_1, height);
    }
    store.cell_volume_left(0)           = 0;
    store.cell_volume_right(nb_nodes-1) = 0;

    return store;
}

Mesh1DStorage uniform_axisymmetric_mesh_storage(
        const UniformAxisymmetricMesh1DGeometry& geom
        )
{
    // compute needed informations if necessary
    index_t nb_nodes = geom.nb_nodes;
    scalar_t dx      = geom.dx;
    if (nb_nodes <= 0)
    {
        if (dx <= 0 or geom.radius <= 0)
        {
            throw std::invalid_argument("Missing information when creating"
              " a uniform axisymmetric 1D mesh. Two of (nb_nodes, dx, radius)"
              " must be provided.");
        }
        nb_nodes = std::ceil(geom.radius / dx) + 1;
        dx = geom.radius / (nb_nodes-1);
    }
    else if (dx <= 0)
    {
        if (geom.radius <= 0)
        {
            throw std::invalid_argument("Missing information when creating"
              " a uniform axisymmetric 1D mesh. Two of (nb_nodes, dx, radius)"
              " must be provided.");
        }
        dx = geom.radius / (nb_nodes - 1);
    }

    // coordinates
    Vector radius(nb_nodes);
    for (index_t i=0; i<nb_nodes; ++i)
    {
        radius(i) = geom.radius-i*dx;
    }
    return axisymmetric_mesh_storage(radius, geom.height);
}



} // end namespace mesh
} // end namespace specmicp
