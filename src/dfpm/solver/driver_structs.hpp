/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DFPMSOLVER_DRIVERSTRUCTS_HPP
#define SPECMICP_DFPMSOLVER_DRIVERSTRUCTS_HPP

#include "specmicp_common/types.hpp"
#include "specmicp_common/sparse_solvers/sparse_solver_structs.hpp"

#define DFPM_DEFAULT_RES_TOL 1e-5
#define DFPM_DEFAULT_ABS_TOL 1e-12
#define DFPM_DEFAULT_STEP_TOL 1e-10
#define DFPM_DEFAULT_TRSHOLD_STATIONARY 1e-4
#define DFPM_DEFAULT_MAX_ITER 200
#define DFPM_DEFAULT_MAX_STEP_LENGTH 1e3
#define DFPM_DEFAULT_MAX_ITER_MAX_LENGTH 50
#define DFPM_DEFAULT_COEFF_ACCEPT_NEWTON 0.9
#define DFPM_DEFAULT_SPARSE_SOLVER specmicp::sparse_solvers::SparseSolver::SparseQR
#define DFPM_DEFAULT_PIVOTS_TRSHOLD -1 // -1 means it is find automatically

namespace specmicp {
namespace dfpmsolver {


//! \brief Options of a driver
//!
struct DriverOptions
{
    //! Maximum iterations allowed
    int maximum_iterations {DFPM_DEFAULT_MAX_ITER };
    //! Maximum number of iterations at maximum step length
    int max_iterations_at_max_length {DFPM_DEFAULT_MAX_ITER_MAX_LENGTH};
    //! The sparse solver to use
    sparse_solvers::SparseSolver sparse_solver {DFPM_DEFAULT_SPARSE_SOLVER};
    //! The threshold for the pivots in the decomposition
    scalar_t sparse_solver_pivots_threshold {DFPM_DEFAULT_PIVOTS_TRSHOLD};
    //! Tolerance for the residual
    scalar_t residuals_tolerance {DFPM_DEFAULT_RES_TOL};
    //! Absolute tolerance for the residual
    scalar_t absolute_tolerance {DFPM_DEFAULT_ABS_TOL};
    //! Tolerance for the minimum step length
    scalar_t step_tolerance {DFPM_DEFAULT_STEP_TOL};
    //! if ||R||>threshold, the point is classified as stationary
    scalar_t threshold_stationary_point {DFPM_DEFAULT_TRSHOLD_STATIONARY };
    //! Maximum step length allowed
    scalar_t maximum_step_length {DFPM_DEFAULT_MAX_STEP_LENGTH};
    //! Accept Newton step if enough progress is made
    scalar_t coeff_accept_newton_step {DFPM_DEFAULT_COEFF_ACCEPT_NEWTON};

    DriverOptions() {}
};

//! \brief Performance of a driver
struct DriverPerformance
{
    int nb_call_residuals; //!< Number of times the residuals were computed
    int nb_call_jacobian;  //!< Number of time the Jacobian was computed
    int nb_iterations;     //!< Number of iterations
    int nb_consecutive_max_step_taken; //!< Number of consecutive time a maximum step length was taken
    int nb_max_step_taken;      //!< Number of maximum step
    bool maximum_step_taken;    //!< True if the maximum step length was taken
    scalar_t current_residual;  //!< Current value of the residual
    scalar_t absolute_residual; //!< Current value of the absolute residuals
    scalar_t current_update;    //!< Current value of the step length

    //! \brief Default constructor
    DriverPerformance():
        nb_call_residuals(0),
        nb_call_jacobian(0),
        nb_iterations(0),
        nb_consecutive_max_step_taken(0),
        nb_max_step_taken(0),
        maximum_step_taken(0),
        current_residual(0.0),
        absolute_residual(0.0),
        current_update(0.0)
    {}
};

} // end namespace dfpmsolver
} // end namespace specmicp

#endif // SPECMICP_DFPMSOLVER_DRIVERSTRUCTS_HPP
