/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DFPMSOLVER_PARABOLICSTRUCTS_HPP
#define SPECMICP_DFPMSOLVER_PARABOLICSTRUCTS_HPP

#include "driver_structs.hpp"

#define DFPM_PARABOLIC_DEFAULT_ALPHA 1.0
#define DFPM_PARABOLIC_DEFAULT_LINESEARCH specmicp::dfpmsolver::ParabolicLinesearch::Backtracking
#define DFPM_PARABOLIC_DEFAULT_QUASI_NEWTON 1

namespace specmicp {
namespace dfpmsolver {

//! \brief The linesearch to use in the parabolic driver
enum class ParabolicLinesearch
{
    Backtracking, //!< Backtracking linesearch
    Strang       //!< Strang Linesearch
};

//! \brief The return code of the parabolic linesearch
enum class ParabolicLinesearchReturnCode
{
    NotSupposedToHappen, //!< For debugging purpose
    MaximumIterations,   //!< Error : maximum iteration reached
    LambdaTooSmall,      //!< Error : step tolerance reached
    Divergence,          //!< Error : divergence
    Success              //!< Success !

};

//! \brief Options of a parabolic driver
//!
struct ParabolicDriverOptions: public DriverOptions
{
    //! The linesearch to use
    ParabolicLinesearch linesearch {DFPM_PARABOLIC_DEFAULT_LINESEARCH};
    //! Number of iterations without reforming the jacobian
    int quasi_newton {DFPM_PARABOLIC_DEFAULT_QUASI_NEWTON};
    //! Implicit/Cranck-Nicholson parameter (between  0 and 1)
    scalar_t alpha {DFPM_PARABOLIC_DEFAULT_ALPHA};

    ParabolicDriverOptions() {}
};

//! \brief Return codes of the parabolic solver
//!
//! A value greater than NotconvergedYet indicates a succes
enum class ParabolicDriverReturnCode
{
    LinesearchFailed = -5,         //!< Linesearch has failed (usually indicates a bad system)
    MaxIterations = -4,            //!< Maximum number of iterations reached
    MaxStepTakenTooManyTimes = -3, //!< Maximum step taken too many times (divergence)
    ErrorLinearSystem = -2,        //!< Error when solving the linear system
    StationaryPoint = -1,          //!< Stationnary points are detected
    NotConvergedYet = 0,           //!< Problem is not converged
    ResidualMinimized = 1,         //!< The residual is minimized (Success)
    ErrorMinimized = 2             //!< Error is minimized (may be good)
};

//! \brief Return true if 'retcode' corresponds to a failure
inline bool has_failed(ParabolicDriverReturnCode retcode) {
    return (retcode <= ParabolicDriverReturnCode::NotConvergedYet);
}

//! \brief Performance of the parabolic driver
struct ParabolicDriverPerformance: public DriverPerformance
{
    ParabolicDriverReturnCode return_code; //!< Return code of the solver

    //! \brief Default constructor
    ParabolicDriverPerformance():
        DriverPerformance(),
        return_code(ParabolicDriverReturnCode::NotConvergedYet)
    {}
};

} // end namespace dfpmsolver
} // end namespace specmicp

#endif // SPECMICP_DFPMSOLVER_PARABOLICSTRUCTS_HPP
