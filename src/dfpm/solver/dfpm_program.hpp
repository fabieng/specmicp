/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DFPMSOLVER_DFPMPROGRAM_HPP
#define SPECMICP_DFPMSOLVER_DFPMPROGRAM_HPP

#include "specmicp_common/types.hpp"

namespace specmicp {
namespace dfpmsolver {

//! \brief Base class for a program
template <class Derived>
class DFPMProgram
{
public:
    //! \brief Return a pointer to the true object
    Derived* derived() {static_cast<Derived*>(this);}

    //! \brief Return the number of equations
    index_t get_neq() const {return derived()->get_neq();}

    //! \brief Return the number of degrees of freedom per node
    index_t get_ndf() const {return derived()->get_ndf();}

    //! \brief Return the total number of degrees of freedom
    index_t get_tot_ndf() const {return derived()->get_tot_ndf();}

    //! \brief Return the id of the equation corresponding to the degree of freedom 'id_dof'
    //!
    //! Return 'no_equation' if no equation exist
    index_t id_equation(index_t id_dof) const {return derived()->id_equation(id_dof);}

};

} // end namespace dfpmsolver
} // end namespace specmicp

#endif // SPECMICP_DFPMSOLVER_DFPMPROGRAM_HPP
