/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DATABASE_UNKNOWNCLASS_HPP
#define SPECMICP_DATABASE_UNKNOWNCLASS_HPP


//! \file unknown_class.hpp
//! \brief Find properties of species in an unknow class

#include "specmicp_common/types.hpp"
#include "database_fwd.hpp"
#include "module.hpp"

namespace specmicp {
namespace database {

//! \brief Class of an aqueous species
enum class AqueousSpeciesClass
{
    Component, //!< The aqeuous species is in the basis
    Aqueous,   //!< The aqueous species is a secondary species
    Compound,  //!< The aqueous is a species is a compound
    Invalid    //!< This is not an aqueous species
};

//! \brief Identification of a generic aqueous species
struct GenericAqueousSpecies
{
    AqueousSpeciesClass type; //!< The class of the aqueous species
    index_t id;               //!< Its id in the database
};

//! \brief Class of a solid phase
enum class SolidPhaseClass
{
    EquilibriumMineral, //!< Solid phase at equilibrium
    MineralKinetics,    //!< Solid phase governed by kinetics
    Invalid             //!< Invalid solid phase
};

//! \brief Identification of a generic solid phase
struct GenericSolidPhase
{
    SolidPhaseClass type; //!< The class of the solid phase
    index_t id;           //!< Its id in the database
};


//! \brief Find the type of a species and it's properties
class SPECMICP_DLL_PUBLIC SpeciesTypeFinder:
        private DatabaseModule
{
public:
    //! \brief Constructor
    SpeciesTypeFinder(RawDatabasePtr thedata):
        DatabaseModule(thedata)
    {}

    //! \brief Return the class of an aqueous species
    AqueousSpeciesClass find_aqueous_species_class(
            const std::string& label
            ) const;

    //! \brief Find an aqueous species in the database
    GenericAqueousSpecies find_aqueous_species(
            const std::string& label
            ) const;

    //! \brief Return the molar class of an aqueous of unknown class
    scalar_t molar_mass_aqueous(
            const std::string& label
            ) const;

    //! \brief Return the class of an solid phase
    SolidPhaseClass find_solid_phase_class(
            const std::string& label
            ) const;

    //! \brief Find a solid phase in the database
    GenericSolidPhase find_solid_phase(
            const std::string& label
            ) const;

    //! \brief Return the molar mass (in SI units) of a solid phase
    scalar_t molar_mass_solid_phase(
            const std::string& label
            ) const;

    //! \brief Return the molar volume (in SI units) of a solid phase
    scalar_t molar_volume_solid_phase(
            const std::string& label
            ) const;

    //! \brief Return the database
    RawDatabasePtr get_database() const {
        return data;
    }

};

} //end namespace database
} //end namespace specmicp

#endif // SPECMICP_DATABASE_UNKNOWNCLASS_HPP
