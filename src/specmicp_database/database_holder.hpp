/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DATABASE_DATABASEHOLDER_HPP
#define SPECMICP_DATABASE_DATABASEHOLDER_HPP

//! \file database_holder.hpp
//! \brief Base class for a class using the database

#include "data_container.hpp"

namespace specmicp {
namespace database {

//! \brief Base class for a class using a database
class DatabaseHolder
{
public:
    //! \brief Construct from a database
    DatabaseHolder(RawDatabasePtr& the_database):
        m_data(the_database)
    {}

    //! \brief Move constructor
    DatabaseHolder(DatabaseHolder&& other):
        m_data(other.m_data)
    {}

    //! \brief Copy constructor
    DatabaseHolder& operator=(DatabaseHolder&& other)
    {
        m_data = other.m_data;
        return *this;
    }

    //! \brief return the database
    RawDatabasePtr& get_database() {return m_data;}

    //! \brief return a const reference to the database
    const RawDatabasePtr& get_database() const {return m_data;}

protected:
    RawDatabasePtr m_data; //!< The database

};


} //end namespace database
} //end namespace specmicp

#endif // SPECMICP_DATABASE_DATABASEHOLDER_HPP
