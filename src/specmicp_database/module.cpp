/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "module.hpp"

namespace specmicp {
namespace database {


DatabaseModule::DatabaseModule(RawDatabasePtr& thedata)
{
    if (thedata == nullptr)
    {
        throw std::invalid_argument("The database was not initialized");
    }
    set_database(thedata);
}

index_t DatabaseModule::safe_component_label_to_id(const std::string& label)
{
    auto id = component_label_to_id(label);
    if (id == no_species)
    {
        throw InvalidComponent(label);
    }
    return id;
}

index_t DatabaseModule::safe_aqueous_label_to_id(const std::string& label)
{
    auto id = aqueous_label_to_id(label);
    if (id == no_species)
    {
        throw InvalidAqueous(label);
    }
    return id;
}

index_t DatabaseModule::safe_mineral_label_to_id(const std::string& label)
{
    auto id = mineral_label_to_id(label);
    if (id == no_species)
    {
        throw InvalidMineral(label);
    }
    return id;
}

index_t DatabaseModule::safe_mineral_kinetic_label_to_id(const std::string& label)
{
    auto id = mineral_kinetic_label_to_id(label);
    if (id == no_species)
    {
        throw InvalidMineralKinetics(label);
    }
    return id;
}

index_t DatabaseModule::safe_gas_label_to_id(const std::string& label)
{
    auto id = gas_label_to_id(label);
    if (id == no_species)
    {
        throw InvalidGas(label);
    }
    return id;
}

index_t DatabaseModule::safe_sorbed_label_to_id(const std::string& label)
{
    auto id = sorbed_label_to_id(label);
    if (id == no_species)
    {
        throw InvalidSorbed(label);
    }
    return id;
}

} //end namespace database
} //end namespace specmicp
