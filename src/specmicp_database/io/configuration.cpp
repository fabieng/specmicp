/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "configuration.hpp"

#include "specmicp_database/database.hpp"

#include "specmicp_common/io/yaml.hpp"
#include "specmicp_common/io/safe_config.hpp"
#include "specmicp_common/filesystem.hpp"
#include "specmicp_common/string_algorithms.hpp"

#include "specmicp_common/log.hpp"

#include <iostream>

#include "specmicp_common/io/config_yaml_sections.h"

namespace specmicp {
namespace io {


//! \brief Read SPECMICP_DATABASE_DIR
void add_db_dirs_from_env(std::vector<std::string>& db_dirs)
{
    const auto env = utils::get_env("SPECMICP_DATABASE_PATH");
    if (not env.empty()) {
         for(auto dir: utils::split(env, ';')) {
             db_dirs.push_back(dir);
         }
    }
}

// new interface
// -------------
//! \brief Swap components in the database
void swap_components(
        io::YAMLConfigHandle&& swap_section,
        database::Database& db_manager
        );
//! \brief Configure solid and gas phases
void phases_conf(
        io::YAMLConfigHandle& db_section,
        database::Database& db_manager
        );

RawDatabasePtr configure_database(
        YAMLConfigHandle&& db_conf
        )
{
    return configure_database(std::move(db_conf), {});
}


RawDatabasePtr configure_database(
        YAMLConfigHandle&& db_conf,
        const std::vector<std::string>& database_dirs
        )
{
    const auto path = db_conf.get_required_attribute<std::string>(
                SPC_CF_S_DATABASE_A_PATH
                );
    auto filepath = utils::find_path(path, database_dirs);
    SPC_CONF_LOG << "Reading database from : " << filepath;
    if (filepath == "") {
        auto msg = "Database file '" + path + "' not found.";
        ERROR << msg;
        throw std::runtime_error(msg);
    }

    auto check_compo = db_conf.get_optional_attribute<bool>(
                           SPC_CF_S_DATABASE_A_CHECKCOMPO, true);
    SPC_CONF_LOG << "Check composition is " << std::boolalpha << check_compo;

    database::Database db_manager(filepath, check_compo);

    if (db_conf.has_section(SPC_CF_S_DATABASE_A_SWAP)) {
        swap_components(db_conf.get_section(SPC_CF_S_DATABASE_A_SWAP), db_manager);
    }

    phases_conf(db_conf, db_manager);

    if (db_conf.has_node(SPC_CF_S_DATABASE_A_REMOVE_HALF_CELLS))
    {
        if (db_conf.is_sequence(SPC_CF_S_DATABASE_A_REMOVE_HALF_CELLS))
        {
            const std::vector<std::string> comp_to_remove =
                    db_conf.list_to_vector<std::string>(SPC_CF_S_DATABASE_A_REMOVE_HALF_CELLS);
            db_manager.remove_half_cell_reactions(comp_to_remove);
        }
        else if (db_conf.get_attribute<bool>(SPC_CF_S_DATABASE_A_REMOVE_HALF_CELLS))
        {
            db_manager.remove_half_cell_reactions();
        }
    }
    return db_manager.get_database();
}


void swap_components(
        io::YAMLConfigHandle&& swap_section,
        database::Database& db_manager
        )
{
    std::map<std::string, std::string> switch_map;
    const uindex_t size = swap_section.size();
    for (uindex_t ind=0; ind<size; ++ind)
    {
        auto node = swap_section.get_section(ind);
        switch_map.insert({
            node.get_required_attribute<std::string>(SPC_CF_S_DATABASE_A_SWAP_K_OUT),
            node.get_required_attribute<std::string>(SPC_CF_S_DATABASE_A_SWAP_K_IN)
            });
    }

    db_manager.swap_components(switch_map);
}

void phases_conf(
        io::YAMLConfigHandle& db_section,
        database::Database& db_manager
        )
{
    // gas
    if (db_section.get_optional_attribute(SPC_CF_S_DATABASE_A_REMOVE_GAS, false)) {
        db_manager.remove_gas_phases();
    }
    if (db_section.has_attribute(SPC_CF_S_DATABASE_A_EXTRA_GAS)) {
        db_manager.add_gas_phases(
              db_section.get_attribute<std::string>(SPC_CF_S_DATABASE_A_EXTRA_GAS)
              );
    }
    // solid phases
    if (db_section.has_node(SPC_CF_S_DATABASE_A_LIST_SOLIDS_TOKEEP))
    {
        auto list = db_section.list_to_vector<std::string>(
                        SPC_CF_S_DATABASE_A_LIST_SOLIDS_TOKEEP
                    );
        db_manager.minerals_keep_only(list);
    }
    if (db_section.get_optional_attribute(SPC_CF_S_DATABASE_A_REMOVE_SOLIDS, false)) {
        db_manager.remove_solid_phases();
    }
    if (db_section.has_attribute(SPC_CF_S_DATABASE_A_EXTRA_SOLIDS)) {
        db_manager.add_solid_phases(
            db_section.get_attribute<std::string>(SPC_CF_S_DATABASE_A_EXTRA_SOLIDS)
        );
    }
    // sorbed species
    if (db_section.get_optional_attribute(SPC_CF_S_DATABASE_A_REMOVE_SORBED, false)) {
        db_manager.remove_sorbed_species();
    }
    if (db_section.has_attribute(SPC_CF_S_DATABASE_A_EXTRA_SORBED)) {
        db_manager.add_sorbed_species(
            db_section.get_attribute<std::string>(SPC_CF_S_DATABASE_A_EXTRA_SORBED)
        );
    }
}


// old interface
// -------------

//! \brief Swap components
//! \deprecated
void swap_components(const YAML::Node& db_conf, database::Database& db_manager);
//! \brief Configure solid and gas phases
//! \deprecated
void phases_conf(const YAML::Node& db_conf, database::Database& db_manager);


RawDatabasePtr configure_database(const YAML::Node& conf)
{
    check_mandatory_yaml_node(conf, SPC_CF_S_DATABASE, "__main__");
    const YAML::Node& db_conf = conf[SPC_CF_S_DATABASE];

    const std::string path = get_yaml_mandatory<std::string>(
                db_conf, SPC_CF_S_DATABASE_A_PATH, SPC_CF_S_DATABASE);
    database::Database db_manager(path);

    if (db_conf[SPC_CF_S_DATABASE_A_SWAP])
        swap_components(db_conf, db_manager);

    phases_conf(db_conf, db_manager);


    if (db_conf[SPC_CF_S_DATABASE_A_REMOVE_HALF_CELLS])
    {

        if (db_conf[SPC_CF_S_DATABASE_A_REMOVE_HALF_CELLS].IsSequence())
        {
            std::vector<std::string> comp_to_remove;
            for (auto node: db_conf[SPC_CF_S_DATABASE_A_REMOVE_HALF_CELLS])
                comp_to_remove.push_back(node.as<std::string>());
            db_manager.remove_half_cell_reactions(comp_to_remove);
        }
        else if (db_conf[SPC_CF_S_DATABASE_A_REMOVE_HALF_CELLS].as<bool>())
        {
            db_manager.remove_half_cell_reactions();
        }
    }
    return db_manager.get_database();
}


void swap_components(const YAML::Node &db_conf, database::Database &db_manager)
{
    const YAML::Node& swapper = db_conf[SPC_CF_S_DATABASE_A_SWAP];
    std::map<std::string, std::string> switch_map;

    for (auto it: swapper)
    {
        switch_map[it[SPC_CF_S_DATABASE_A_SWAP_K_OUT].as<std::string>()]
                = it[SPC_CF_S_DATABASE_A_SWAP_K_IN].as<std::string>();
    }
    db_manager.swap_components(switch_map);
}



void phases_conf(const YAML::Node& db_conf, database::Database& db_manager)
{
    if (     db_conf[SPC_CF_S_DATABASE_A_REMOVE_GAS]
         and db_conf[SPC_CF_S_DATABASE_A_REMOVE_GAS].as<bool>())
        db_manager.remove_gas_phases();

    if (db_conf[SPC_CF_S_DATABASE_A_EXTRA_GAS])
        db_manager.add_gas_phases(
                    db_conf[SPC_CF_S_DATABASE_A_EXTRA_GAS].as<std::string>());

    if (db_conf[SPC_CF_S_DATABASE_A_LIST_SOLIDS_TOKEEP])
    {
        std::vector<std::string> list;
        list.reserve(db_conf[SPC_CF_S_DATABASE_A_LIST_SOLIDS_TOKEEP].size());
        for (auto it: db_conf[SPC_CF_S_DATABASE_A_LIST_SOLIDS_TOKEEP])
        {
            list.push_back(it.as<std::string>());
        }
        db_manager.minerals_keep_only(list);
    }

    if (    db_conf[SPC_CF_S_DATABASE_A_REMOVE_SOLIDS]
        and db_conf[SPC_CF_S_DATABASE_A_REMOVE_SOLIDS].as<bool>())
        db_manager.remove_solid_phases();

    if (db_conf[SPC_CF_S_DATABASE_A_EXTRA_SOLIDS])
        db_manager.add_solid_phases(
                    db_conf[SPC_CF_S_DATABASE_A_EXTRA_SOLIDS].as<std::string>());

    if (    db_conf[SPC_CF_S_DATABASE_A_REMOVE_SORBED]
        and db_conf[SPC_CF_S_DATABASE_A_REMOVE_SORBED].as<bool>())
        db_manager.remove_gas_phases();

    if (db_conf[SPC_CF_S_DATABASE_A_EXTRA_SORBED])
        db_manager.add_sorbed_species(
                    db_conf[SPC_CF_S_DATABASE_A_EXTRA_SORBED].as<std::string>());
}

} //end namespace io
} //end namespace specmicp
