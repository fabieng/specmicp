/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "switch_basis.hpp"

#include <Eigen/Eigen>
#include <stdexcept>

namespace specmicp {
namespace database {

index_t BasisSwitcher::get_true_aqueous_index(index_t ind)
{
    return ind - data->nb_component();
}

void BasisSwitcher::swap_components(const std::map<std::string, std::string>& swap_to_make)
{
    std::vector<index_t> id_swap;
    id_swap.reserve(data->nb_component());
    for (index_t i: data->range_component())
    {
        id_swap.push_back(i);
    }
    for (const auto& it: swap_to_make)
    {
        if (it.first == water_label) {
            throw std::invalid_argument("Basis switch : water cannot be swapped from basis");
        }
        if (it.first == electron_label) {
            throw std::invalid_argument("Basis switch : electron cannot be swapped from basis");
        }
        const index_t idc = safe_component_label_to_id(it.first);
        const index_t idaq = data->nb_component() + safe_aqueous_label_to_id(it.second);
        id_swap[idc] = idaq;
    }
    switch_basis(id_swap);
}

void BasisSwitcher::switch_basis(std::vector<index_t>& new_basis)
{
    Eigen::MatrixXd beta = Eigen::MatrixXd::Zero(
                data->nb_component(), data->nb_component());
    Eigen::VectorXd kswap(data->nb_component());
    // swap coefficients, build beta matrix and kswap
    for (index_t i: data->range_component())
    {
        if (new_basis[i] >= data->nb_component())
        {
            index_t j = get_true_aqueous_index(new_basis[i]);
            beta.row(i).segment(0, data->nb_component()) = data->aqueous.get_nu_row(j);
            data->aqueous.reset_nu_row(j);
            data->aqueous.set_nu_ji(j, i, 1.0);
            kswap(i) = data->logk_aqueous(j);
            data->aqueous.set_logk(j, 0.0);
        }
        else
        {
            beta(i, i) = 1.0;
            kswap(i) = 0.0;
        }
    }
    Matrix betainv = beta.inverse();
    data->components.molar_mass_transform(beta);
    data->aqueous.switch_basis(betainv, kswap);
    swap_aq_param(new_basis);
    swap_labels(new_basis);
    // now do the minerals
    data->minerals.switch_basis(betainv, kswap);
    data->minerals_kinetic.switch_basis(betainv, kswap);
    //  and the gases
    if (data->nb_gas() > 0)
    {
        data->gas.switch_basis(betainv, kswap);
    }
    // and sorbed species
    if (data->nb_sorbed() > 0)
    {
        data->sorbed.switch_basis(betainv, kswap);
    }
    if (data->nb_compounds() > 0)
    {
        data->compounds.switch_basis(betainv, kswap);
    }
}

void BasisSwitcher::swap_aq_param(std::vector<index_t>& new_basis)
{
    for (index_t i: data->range_component())
    {
        if(new_basis[i] >= data->nb_component())
        {
            const index_t j = get_true_aqueous_index(new_basis[i]);
            const auto tmp = data->components.ionic_values(i);
            data->components.set_ionic_values(i, data->aqueous.ionic_values(j));
            data->aqueous.set_ionic_values(j, tmp);
        }
    }
}

void BasisSwitcher::swap_labels(std::vector<index_t>& new_basis)
{
    for (index_t i: data->range_component())
    {
        if (new_basis[i] >= data->nb_component())
        {
            const index_t j = get_true_aqueous_index(new_basis[i]);
            const auto tmp = data->components.get_label(i);
            data->components.set_label(i, data->aqueous.get_label(j));
            data->aqueous.set_label(j, tmp);
        }
    }
}

} // end namespace database
} // end namespace specmicp
