/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "data_container.hpp"
#include "errors.hpp"
#include "config_database.hpp"

namespace std {
template <>
struct hash<specmicp::database::Metadata> {
    size_t operator() (const specmicp::database::Metadata& meta_data) const
    {
        auto hasher =  hash<string>();
        return hasher(meta_data.path)+hasher(meta_data.name)+hasher(meta_data.version);
    }
};
}

namespace specmicp {
namespace database {

const std::string water_label{LABEL_WATER};     //!< The water label in the database
const std::string electron_label{LABEL_ELECTRON}; //!< The electron label in the database

units::UnitsSet const database_units {units::CMG_units};

//! \brief conversion from g/mol to (mass_unit)/(mol_unit)
scalar_t SPECMICP_CONST_F scaling_factor_molar_mass(const units::UnitsSet& units_set)
{
    scalar_t factor = 1.0;
    if (units_set.mass == units::MassUnit::kilogram) factor = 1e-3;
    if (units_set.quantity  == units::QuantityUnit::millimoles) factor *= 1e-3;
    return factor;
}

scalar_t DataContainer::molar_volume_mineral(index_t m, const units::UnitsSet& units_set) const
{
    const auto& molar_volume = minerals.molar_volume(m);
    if (molar_volume < 0) {
        throw db_noninitialized_value("molar volume", minerals.get_label(m));
    }
    return scaling_molar_volume(units_set)*molar_volume;
}

scalar_t DataContainer::molar_volume_mineral_kinetic(index_t m, const units::UnitsSet& units_set) const
{
    const auto& molar_volume = minerals_kinetic.molar_volume(m);
    if (molar_volume < 0) {
        throw db_noninitialized_value("molar volume", minerals_kinetic.get_label(m));
    }
    return scaling_molar_volume(units_set)*molar_volume;
}

scalar_t DataContainer::scaling_molar_volume(const units::UnitsSet& units_set) const
{
    scalar_t scaling = 1.0; // value stored in cm^3/mol
    if (units_set.length == units::LengthUnit::meter)
        scaling = 1e-6;
    else if (units_set.length == units::LengthUnit::decimeter)
        scaling = 1e-3;
    if (units_set.quantity == units::QuantityUnit::millimoles)
        scaling *= 1e-3;
    return scaling;
}

//! \brief Return the molar volume (m^3/mol) of a mineral
scalar_t DataContainer::molar_volume_mineral(index_t m) const
{
    const auto& molar_volume = minerals.molar_volume(m);
    if (molar_volume < 0) {
        throw db_noninitialized_value("molar volume", minerals.get_label(m));
    }
    return 1e-6*molar_volume;
}
//! \brief Return the molar volume (m^3/mol) of a mineral governed by kinetic
scalar_t DataContainer::molar_volume_mineral_kinetic(index_t m) const
{
    const auto& molar_volume = minerals_kinetic.molar_volume(m);
    if (molar_volume < 0) {
        throw db_noninitialized_value("molar volume", minerals_kinetic.get_label(m));
    }
    return 1e-6*molar_volume;
}

//! Return the molar mass of 'component' in 'mass_unit'/mol
scalar_t DataContainer::molar_mass_basis(index_t component, const units::UnitsSet& units_set) const NOEXCEPT
{
    const scalar_t scaling = scaling_factor_molar_mass(units_set);
    return components.molar_mass(component)*scaling;
}

//! \brief Return the molar mass (kg/mol) of a mineral
scalar_t DataContainer::molar_mass_mineral(index_t mineral) const {
    return 1e-3*components.molar_mass_compounds(minerals.get_nu_row(mineral));
}
//! \brief Return the molar mass (kg/mol) of a mineral governed by kinetic
scalar_t DataContainer::molar_mass_mineral_kinetic(index_t mineral) const {
    return 1e-3*components.molar_mass_compounds(minerals_kinetic.get_nu_row(mineral));
}

//! Return the molar mass of 'mineral' in 'mass_unit'/'mol_unit'
scalar_t DataContainer::molar_mass_mineral(index_t mineral, const units::UnitsSet& units_set) const
{
    scalar_t scaling = scaling_factor_molar_mass(units_set);
    return scaling*components.molar_mass_compounds(minerals.get_nu_row(mineral));
}

//! Return the molar mass of 'mineral_kinetic' in 'mass_unit'/'mol_unit'
scalar_t DataContainer::molar_mass_mineral_kinetic(index_t mineral_kinetic, const units::UnitsSet& units_set) const
{
    scalar_t scaling = scaling_factor_molar_mass(units_set);
    return scaling*components.molar_mass_compounds(minerals_kinetic.get_nu_row(mineral_kinetic));
}

//! Return the molar mass (kg/mol) of a compound
scalar_t DataContainer::molar_mass_compound(index_t j) const
{
    return 1e-3*components.molar_mass_compounds(compounds.get_nu_row(j));
}
//! Return the molar mass of a compound
scalar_t DataContainer::molar_mass_compound(index_t j, const units::UnitsSet& units_set) const
{
    scalar_t scaling = scaling_factor_molar_mass(units_set);
    return scaling*components.molar_mass_compounds(compounds.get_nu_row(j));
}

//! Return the molar mass (kg/mol) of an aqueous species
scalar_t DataContainer::molar_mass_aqueous(index_t j) const
{
    return 1e-3*components.molar_mass_compounds(aqueous.get_nu_row(j));
}
//! Return the molar mass of an aqueous species
scalar_t DataContainer::molar_mass_aqueous(index_t j, const units::UnitsSet& units_set) const
{
    scalar_t scaling = scaling_factor_molar_mass(units_set);
    return scaling*components.molar_mass_compounds(aqueous.get_nu_row(j));
}

//! Return the molar mass (kg/mol) of a sorbed species
scalar_t DataContainer::molar_mass_sorbed(index_t j) const
{
    return 1e-3*components.molar_mass_compounds(sorbed.get_nu_row(j));
}
//! Return the molar mass of a sorbed species
scalar_t DataContainer::molar_mass_sorbed(index_t j, const units::UnitsSet& units_set) const
{
    scalar_t scaling = scaling_factor_molar_mass(units_set);
    return scaling*components.molar_mass_compounds(sorbed.get_nu_row(j));
}

bool DataContainer::is_valid() const
{
    bool is_valid = (components.is_valid()
                and aqueous.is_valid()
                and minerals.is_valid()
                and minerals_kinetic.is_valid()
                and gas.is_valid()
                and sorbed.is_valid()
                and compounds.is_valid()
                and elements.size() == nb_component()
                );
    if (m_fixed_hash > 0)
            is_valid = is_valid and (get_hash() == m_fixed_hash);
    return is_valid;
}

size_t DataContainer::get_hash() const
{
    size_t hash_db = 0; // doesn't make sense to use metadata to set hash
    // the factors 2 are to distinguish cases were species are simply moved to another list
    hash_db += (  components.get_hash()
                  + 2*aqueous.get_hash()
                  + minerals.get_hash()
                  + 2*minerals_kinetic.get_hash()
                  + gas.get_hash()
                  + sorbed.get_hash()
                  + compounds.get_hash()
                  );
    return hash_db;
}


} // end namespace database
} // end namespace specmicp
