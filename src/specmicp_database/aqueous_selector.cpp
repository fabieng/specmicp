/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "aqueous_selector.hpp"

namespace specmicp {
namespace database {

//! \brief Remove some specific aqueous species
void AqueousSelector::remove_aqueous(const std::vector<index_t>& id_aqueous)
{
   const index_t new_nb_aqueous = data->nb_aqueous() - id_aqueous.size();

   index_t new_id = 0;
   for (index_t aqueous: data->range_aqueous())
   {
       auto it = std::find(id_aqueous.cbegin(), id_aqueous.cend(), aqueous);
       if (it == id_aqueous.end()) // if we need to keep the species
       {
           if (*it == aqueous)
           {
               ++new_id;
               continue; // nothing to change
           }
           data->aqueous.move_erase(aqueous, new_id);
           ++new_id;
       }
   }
   specmicp_assert(new_id == new_nb_aqueous);
   data->aqueous.resize(new_nb_aqueous);
   data->aqueous.set_valid();
}

} // end namespace database
} // end namespace specmicp
