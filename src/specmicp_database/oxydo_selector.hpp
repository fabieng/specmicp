/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DATABASE_OXYDOSELECTOR_HPP
#define SPECMICP_DATABASE_OXYDOSELECTOR_HPP

//! \file oxydo_selector.hpp
//! \brief Remove half-cells reactions

#include "module.hpp"

namespace specmicp {
namespace database {

//! \class OxydoSelector
//! \brief Remove half cells reaction for for given components
//!
class OxydoSelector: public DatabaseModule
{
public:
    //! \brief Constructor
    OxydoSelector(RawDatabasePtr thedata):
        DatabaseModule(thedata)
    {}
    //! \brief Remove all half-cells reaction
    void remove_half_cells();
    //! \brief Remove the halfs-cells reaction for components in 'list_components'
    void remove_half_cells(const std::vector<std::string>& list_components);
    //! \brief Remove the halfs-cells reaction for components in 'list_components'
    void remove_half_cells(const std::vector<index_t>& list_components);

};

} // end namespace database
} // end namespace specmicp


#endif // SPECMICP_DATABASE_MINERALSELECTOR_HPP
