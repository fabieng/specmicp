/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DATABASE_MINERALSELECTOR_HPP
#define SPECMICP_DATABASE_MINERALSELECTOR_HPP

//! \file mineral_selector.hpp select minerals in the database

#include "module.hpp"

namespace specmicp {
namespace database {

//! \brief Select equilibrum minerals in the database
//!
//! Keep only the solid phases that the user wants
class MineralSelector: public DatabaseModule
{
public:
    //! \brief Constructor
    MineralSelector(RawDatabasePtr thedata):
        DatabaseModule(thedata)
    {}

    //! \brief Keep only minerals with id in "minerals to keep"
    void keep_only(const std::vector<index_t>& minerals_to_keep);

    //! \brief Keep only minerals in "minerals_to_keep"
    void keep_only(const std::vector<std::string>& minerals_to_keep);

    //! \brief Remove all minerals from the databse
    void remove_all_minerals();

};

} // end namespace database
} // end namespace specmicp


#endif // SPECMICP_DATABASE_MINERALSELECTOR_HPP
