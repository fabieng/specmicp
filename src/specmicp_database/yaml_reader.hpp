/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPEMCICP_DATABASE_YAMLREADER_HPP
#define SPEMCICP_DATABASE_YAMLREADER_HPP

//! \file database/yaml_reader.hpp
//! \brief A reader for a database in yaml format

#include "module.hpp"

namespace YAML
{
    class Node;
}

namespace specmicp {
namespace database {


//! \brief Read a database from a YAML-formatted file
class SPECMICP_DLL_LOCAL DataReaderYaml: public DatabaseModule
{
public:
    //! \brief Empty constructor
    DataReaderYaml();
    ~DataReaderYaml();

    //! \brief Constructor
    DataReaderYaml(RawDatabasePtr data, bool check_compo=true);
    //! \brief Constructor
    //!
    //! \param filepath string containing the path to the database
    //! \param check_compo if true, reader will check the composition using the formula field
    DataReaderYaml(const std::string& filepath, bool check_compo=true);

    //! \brief Constructor
    //!
    //! \param input input stream that contains the database
    //! \param check_compo if true, reader will check the composition using the formula field
    DataReaderYaml(std::istream& input, bool check_compo=true);

    //! Return the databes
    RawDatabasePtr get_database() {return data;}

    //! \brief Parse the basis section
    //!
    //! Contains the list of primary species
    void parse_basis(const YAML::Node& basis_root);
    //! \brief Parse the aqueous section
    //!
    //! Contains the list of secondary species
    void parse_aqueous(const YAML::Node& aqueous_root, AqueousList& alist);
    //! \brief Parse the mineral section
    //!
    //! Contains the list of minerals
    void parse_minerals(
            const YAML::Node& minerals,
            MineralList &minerals_list,
            MineralList &minerals_kinetic_list
            );
    //! \brief Parse the gas section
    void parse_gas(const YAML::Node& gas_root, GasList& glist);
    //! \brief Parse the sorbed species section
    void parse_sorbed(const YAML::Node& sorbed_root, SorbedList& slist);
    //! \brief Parse the compounds
    void parse_compounds(const YAML::Node& compounds, CompoundList& clist);
    //! \brief Parse the elements
    void parse_elements(const YAML::Node& elements, ElementList& elist);
private:
    //! \brief Parse database
    //!
    //! Throw db_invalid_syntax if a syntax error was detected.
    //! Throws std::invalid_argument if the path to the database is not correct
    void parse(std::istream& input);

    //! \brief Parse database
    //!
    //! Throw db_invalid_syntax if a syntax error was detected.
    //! Throws std::invalid_argument if the path to the database is not correct
    void parse(const std::string& filepath);

    //! \brief Parse the metadata section
    //!
    //! we don't do much with them for now....
    void parse_metadata(const YAML::Node& root);

    //! \brief Initialize the data to check the composition
    void init_elemental_composition();
    //! \brief Check initial composition
    //!
    //! \throw db_invalid_data if the composition is not consistent with the formula
    void check_composition(const std::string& formula, ReactiveSpeciesList& rlist, index_t id);

    DataReaderYaml(const DataReaderYaml&) = delete;
    DataReaderYaml& operator =(const DataReaderYaml&) = delete;

    struct DataReaderYamlElementData; //!< Metadata to check composition
    std::unique_ptr<DataReaderYamlElementData> m_elem_data; //!< Contains metadata

};

} //end namespace database
} //end namespace specmicp

#endif // SPEMCICP_DATABASE_YAMLREADER_HPP
