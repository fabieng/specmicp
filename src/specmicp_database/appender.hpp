/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DATABASE_APPENDER_HPP
#define SPECMICP_DATABASE_APPENDER_HPP

//! \file appender.hpp
//! \brief Add species to a list

#include "module.hpp"

namespace specmicp {
namespace database {

//! \brief Add Species to the database from yaml inputs
class DataAppender: public DatabaseModule
{
public:
    //! \brief Constructor
    DataAppender(RawDatabasePtr thedata):
        DatabaseModule(thedata)
    {}

    //! \brief Add gases to the database
    void add_gas(const std::string& yaml_input, bool check_compo=true);
    //! \brief Add gases to the database
    void add_gas(std::istream& yaml_input, bool check_compo=true);


    //! \brief Add solid phases to the database
    void add_minerals(const std::string& yaml_input, bool check_compo=true);
    //! \brief Add solid phases to the database
    void add_minerals(std::istream& yaml_input, bool check_compo=true);

    //! \brief Add sorbed species to the database
    void add_sorbed(const std::string& yaml_input, bool check_compo=true);
    //! \brief Add sorbed species to the database
    void add_sorbed(std::istream& yaml_input, bool check_compo=true);

    //! \brief Add compounds species to the database
    void add_compounds(const std::string& yaml_input, bool check_compo=true);
    //! \brief Add sorbed species to the database
    void add_compounds(std::istream& yaml_input, bool check_compo=true);

};

} // end namespace database
} // end namespace specmicp


#endif // SPECMICP_DATABASE_AQUEOUSSELECTOR_HPP
