/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DATABASE_SPECIES_ELEMENTLIST_HPP
#define SPECMICP_DATABASE_SPECIES_ELEMENTLIST_HPP

//! \file element_list.hpp
//! \brief List of elements

#include "specmicp_common/types.hpp"
#include <memory>
#include <vector>

namespace specmicp {
namespace database {

//! \brief List of Element
class SPECMICP_DLL_PUBLIC ElementList
{
public:
    //! \brief Default constructor
    ElementList();
    //! \brief Destructor
    ~ElementList();
    //! \brief Move constructor
    ElementList(ElementList&& other);
    //! \brief Move operator
    ElementList& operator=(ElementList&& other);
    //! \brief Add element 'label_element' that corresponds to component
    void add_element(const std::string& label_element, index_t component);
    //! \brief Return the id of component corresponding to element 'label'
    index_t get_id_component(const std::string& label) const;
    //! \brief return the size of the map (should be equal to the number of components)
    index_t size() const;
    //! \brief Remove components
    void remove_components(const std::vector<index_t>& is_component_to_remove);
    //! \brief Return the label of an element given a component index
    std::string get_label_element(index_t id_component);
private:
    class Impl;
    std::unique_ptr<Impl> m_impl;
};

} //end namespace database
} //end namespace specmicp

#endif // SPECMICP_DATABASE_SPECIES_ELEMENTLIST_HPP
