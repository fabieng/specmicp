/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "element_list.hpp"

#include "specmicp_common/compat.hpp"

#include <map>

namespace specmicp {
namespace database {

class ElementList::Impl
{
public:
    Impl() {}

    //! \brief Add element 'label_element' that corresponds to component
    void add_element(const std::string& label_element, index_t component){
        m_map[label_element] = component;
    }
    //! \brief Return the id of component corresponding to element 'label'
    index_t get_id_component(const std::string& label) const {
        auto it = m_map.find(label);
        if (it != m_map.end()) return it->second;
        else return no_species;
    }
    //! \brief return the size of the map (should be equal to the number of components)
    index_t size() const {return m_map.size();}
    //! \brief return the element corresponding to the component
    std::string get_element_from_component(index_t component);

    void remove_components(const std::vector<index_t>& is_component_to_remove);

private:
   std::map<std::string, index_t> m_map;
};


std::string ElementList::Impl::get_element_from_component(index_t component)
{
    for (auto it: m_map)
    {
        if (it.second == component)
        {
            return it.first;
        }
    }
    throw std::invalid_argument("'"+std::to_string(component)+"' is not a valid index for a component");
}

void ElementList::Impl::remove_components(const std::vector<index_t>& is_component_to_remove)
{
    std::vector<std::string> to_erase;
    // Check if the component still exist
    // if it does copy its new location
    for (auto it=m_map.begin(); it!=m_map.end(); ++it)
    {
        const index_t new_value = is_component_to_remove[static_cast<int>(it->second)];
        if (new_value == no_species)
            to_erase.push_back(it->first);
        else
            it->second = new_value;
    }
    // Remove the element that does not exist anymore
    for (auto it: to_erase)
    {
        m_map.erase(it);
    }
}

void ElementList::remove_components(const std::vector<index_t> &is_component_to_remove)
{
    m_impl->remove_components(is_component_to_remove);
}

void ElementList::add_element(const std::string& label_element, index_t component)
{
    m_impl->add_element(label_element, component);
}

index_t ElementList::get_id_component(const std::string& label) const
{
    return m_impl->get_id_component(label);
}

index_t ElementList::size() const
{
    return m_impl->size();
}

ElementList::ElementList():
    m_impl(make_unique<Impl>())
{}

std::string ElementList::get_label_element(index_t id_component)
{
    return m_impl->get_element_from_component(id_component);
}

// syntax hilighter does not like that byt this is correct
// see pg 53 of Effective modern C++ by Scott Meyer
ElementList::~ElementList() = default;
ElementList::ElementList(ElementList&& other) = default;
ElementList& ElementList::operator=(ElementList&& other) = default;


} //end namespace data
} //end namespace specmicp
