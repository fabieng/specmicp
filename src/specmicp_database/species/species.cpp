/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "species.hpp"

#include "specmicp_database/errors.hpp"
#include <numeric>

namespace specmicp {
namespace database {

static std::hash<std::string> hash_fn;                       //!< The hash function

void SpeciesList::set_label(index_t ind, const std::string& label) {
    m_labels[ind] = label;
    m_hashes[ind] = hash_fn(label);
}
void SpeciesList::set_label(index_t ind, std::string&& label) {
    m_labels.emplace(m_labels.begin()+ind, std::move(label));
    m_hashes[ind] = hash_fn(m_labels[ind]);
}

index_t SpeciesList::get_id(const std::string& label) const
{
    if (label == "") return no_species;
    auto hash = hash_fn(label);

    auto searchit = std::find(m_hashes.cbegin(), m_hashes.cend(), hash);
    if (searchit == m_hashes.cend())
        return no_species; // the species does not exist in the list
    return static_cast<index_t>(searchit - m_hashes.cbegin());
}

size_t SpeciesList::get_hash() const {
    return std::accumulate(m_hashes.cbegin(), m_hashes.cend(), 0);
}

//! \brief Move species 'ind' to the list 'other' at 'other_ind'
void ReactiveSpeciesList::move_erase_to(index_t ind, ReactiveSpeciesList& other, index_t other_ind)
{
    if (other.get_id(get_label(ind)) != no_species)
    {
        throw db_species_already_exist(get_label(ind), "the destination");
    }
    SpeciesList::move_erase_to(ind, other, other_ind);
    m_nu_coeffs.move_erase_to(ind, other.m_nu_coeffs, other_ind);
    m_log_k.move_erase_to(ind, other.m_log_k, other_ind);
}


} // end namespace database
} // end namespace specmicp

