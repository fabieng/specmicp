/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DATABASE_COMPONENTLIST_HPP
#define SPECMICP_DATABASE_COMPONENTLIST_HPP

#include "specmicp_common/types.hpp"
#ifndef SPECMICP_DATABASE_SPECIES_HPP
#include "species.hpp"
#endif
#ifndef SPECMICP_DATABASE_IONICPARAMETERS_HPP
#include "ionic_parameters.hpp"
#endif

//! \file component_list.hpp A list of components, i.e the basis

namespace specmicp {

class AdimensionalSystem;

namespace database {

//! \struct ComponentValues
//! \brief Used for the initialization of a component
//!
//! \ingroup database_species
struct ComponentValues {
    std::string label;
    scalar_t molar_mass;
    IonicModelValues ionic_values;
};

//! \class ComponentList
//! \brief The basis
//!
//! This is the list of components in the system
//! It extends the species list by managing the ionic
//! model parameters and the molar masses.
//!
//! \ingroup database_species
class ComponentList: public SpeciesList
{
public:
    //! \brief Default constructor
    ComponentList() {}
    //! \brief Constructor
    //!
    //! \param size size of the list
    ComponentList(index_t size):
        SpeciesList(size),
        m_molar_mass(size),
        m_ionic_param(size)
    {}

    // Resize
    // ------

    //! \brief Resize the basis
    void resize(index_t size) override {
        SpeciesList::resize(size);
        m_molar_mass.resize(size);
        m_ionic_param.resize(size);
    }
    // Getter
    // ======

    //! \brief Return the molar mass of component k
    const scalar_t& molar_mass(index_t k) const {
        return m_molar_mass(k);
    }

    //! \brief Return the charge of component 'k'
    const scalar_t& charge(index_t k) const {
        return m_ionic_param.charge(k);
    }
    //! \brief Return the ionic size of component 'k'
    const scalar_t& a_debye(index_t k) const {
        return m_ionic_param.a_debye(k);
    }
    //! \brief Return the 'b-dot' parameter of component 'k'
    const scalar_t& b_debye(index_t k) const {
        return m_ionic_param.b_debye(k);
    }
    //! \brief Return the ionic model values of component 'k'
    IonicModelValues ionic_values(index_t k) const {
        return m_ionic_param.get_values(k);
    }

    // Setter
    // ------

    //! \brief set the values for component 'k'
    void set_values(index_t k, const ComponentValues& values) {
        set_label(k, values.label);
        m_molar_mass.set_value(k, values.molar_mass);
        m_ionic_param.set_values(k, values.ionic_values);
    }
    //! \brief set the values for component 'k'
    void set_values(index_t k, ComponentValues&& values) {
        set_label(k, std::move(values.label));
        m_molar_mass.set_value(k, values.molar_mass);
        m_ionic_param.set_values(k, values.ionic_values);
    }

    //! \brief Set the parameters for the ionic model
    void set_ionic_values(index_t k, const IonicModelValues& values) {
        m_ionic_param.set_values(k, values);
    }

    //! \brief Return the molar mass of a compounts
    //! \param stoech a vector containing the stoichiometric coefficients
    template <typename derived>
    scalar_t molar_mass_compounds(const Eigen::MatrixBase<derived>& stoech) const {
        return m_molar_mass.dot(stoech);
    }
    //! \brief Transform the molar mass of the basis
    //! \param transform_matrix the linear operator to apply
    template <typename derived>
    void molar_mass_transform(const Eigen::MatrixBase<derived>& transform_matrix) {
        m_molar_mass.transform(transform_matrix);
    }

    // Move
    // -----

    //! \brief Move component 'old_ind' to 'new_ind'
    void move_erase(index_t old_ind, index_t new_ind) override
    {
        m_molar_mass.move_erase(old_ind, new_ind);
        m_ionic_param.move_erase(old_ind, new_ind);
        SpeciesList::move_erase(old_ind, new_ind);
    }

     friend class specmicp::AdimensionalSystem;

private:
    VectorSpeciesWrapper m_molar_mass;
    IonicModelParameters m_ionic_param;
};

} // end namespace database
} // end namespace specmicp

#endif //SPECMICP_DATABASE_COMPONENTLIST_HPP
