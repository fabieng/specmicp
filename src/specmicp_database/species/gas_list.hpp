/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DATABASE_GASLIST_HPP
#define SPECMICP_DATABASE_GASLIST_HPP

#include "specmicp_common/types.hpp"
#ifndef SPECMICP_DATABASE_SPECIES_HPP
#include "species.hpp"
#endif
#include "aqueous_list.hpp"

//! \file gas_list.hpp
//! \brief The list of gas phases in the database.

namespace specmicp {
namespace database {


//! \struct GasValues
//! \brief Struct to initialize a gas in the gas list
//!
//! \ingroup database_species
struct GasValues {
    std::string label; //!< Label of the gas
    scalar_t logk;     //!< Log_10 of the equilibrium constant
};

//! \class GasList
//! \brief The gas list
//!
//! This is the list of gas in the system
//!
//! \ingroup database_species
class GasList: public ReactiveSpeciesList
{
public:
    //! Initialize an empty list
    GasList() {}
    //! Initialize a list of 'siz' aqueous species with 'nb_components' components
    GasList(index_t size, index_t nb_components):
        ReactiveSpeciesList(size, nb_components)
    {}

    //! \name Setter
    //! \brief Set values
    // -------------
    //! @{
    //! \brief Set the values
    //! \warning Do no set stoichiometric coefficients
    void set_values(index_t k, const GasValues& values) {
        set_label(k, values.label);
        set_logk(k, values.logk);
    }
    //! \brief Set the values
    //! \warning Do no set stoichiometric coefficients
    void set_values(index_t k, GasValues&& values) {
        set_label(k, std::move(values.label));
        set_logk(k, values.logk);
    }
    //! @}

    //! \brief Add the aqueous species 'other_species' to 'k', to obtain a canonical system
    void canonicalize(
            index_t ind,
            const AqueousList& aqueous,
            index_t aqueous_ind,
            scalar_t coeff
            )
    {
        add_alien_species_to(ind, aqueous, aqueous_ind, coeff);
    }


    //! \brief Append the phases in this list to the other list
    void append_to(GasList& other);
};

} // end namespace database
} // end namespace specmicp

#endif //SPECMICP_DATABASE_GASLIST_HPP
