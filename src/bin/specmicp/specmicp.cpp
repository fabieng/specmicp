/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "specmicp_common/io/config_yaml_sections.h"
#include "specmicp_common/io/safe_config.hpp"

#include "specmicp_database/database.hpp"
#include "specmicp_database/io/configuration.hpp"

#include "specmicp_common/physics/io/configuration.hpp"

#include "specmicp/problem_solver/reactant_box.hpp"
#include "specmicp/problem_solver/smart_solver.hpp"


#include "specmicp/adimensional/adimensional_system_solver.hpp"
#include "specmicp/adimensional/adimensional_system_solution.hpp"
#include "specmicp/adimensional/adimensional_system_solver_structs.hpp"
#include "specmicp/adimensional/adimensional_system_structs.hpp"

#include "specmicp/io/configuration.hpp"
#include "specmicp/io/print.hpp"

#include "specmicp_common/log.hpp"
#include "specmicp_common/io/configuration.hpp"
#include "specmicp/io/adimensional_system_solution_saver.hpp"

#include "specmicp_common/cli/parser.hpp"
#include "specmicp_common/io/all_io_files.hpp"
#include "specmicp_common/filesystem.hpp"
#include "specmicp_common/compat.hpp"

#include <iostream>

#define SECTION_MAIN "__main__"
#define SECTION_SPECIATION "speciation"
#define SECTION_FORMULATION "formulation"
#define SECTION_CONSTRAINTS "constraints"

#define ATTRIBUTE_NAME "name"
#define ATTRIBUTE_OUTPUT "output"
#define ATTRIBUTE_OUTPUT_DB "output_db"

namespace specmicp
{
    //! \brief Check the database
    void check_db(const std::string& filepath);

    //! \brief Run one formulation
    void run_formulation(
            io::YAMLConfigHandle&& conf_formulation,
            const RawDatabasePtr& the_database,
            const AdimensionalSystemSolverOptions& the_options,
            io::AllIOFiles* all_io_files
           );
}

const static char* header = "SpecMiCP: Speciation solver based on the complementarity condition\n"
        "(c) Copyright 2014-2016 fabien Georget <fabieng@princeton.edu> \n";

using namespace::specmicp;

using specmicp::cli::ValueType;

int main( int argc, char* argv[])
{
    specmicp::init_logger(&std::cerr, specmicp::logger::Warning);

    std::cout << header << std::endl;


    specmicp::cli::CommandLineParser parser;
    parser.add_option('i', "input",
                      ValueType::string,
                      "input_file (yaml format)");
    parser.add_option('d', "database_dir",
                      std::string(""),
                      "directory where database are stored");
    parser.add_option('x', "check-db", false,
                     "Check the database provided as input");
    parser.add_option('w', "workdir",
                      specmicp::utils::get_current_directory(),
                      "working directory");
    parser.add_option('o', "overwrite", false,
                      "If true, allow overwrite of previous simulation");
    parser.add_option('c', "clean", false,
                      "Clean previous output files");
    parser.add_option('p', "pretend", false,
                      "Dry-run, do not run simulation");
    parser.set_help_message("SpecMiCP : speciation solver");

    const auto ret_parse = parser.parse(argc, argv);
    if (ret_parse > 0) {
        return EXIT_SUCCESS;
    }

    const auto input_file = parser.get_option<cli::ValueType::string>("input");

    // check_db
    // ========
    if (parser.get_option<ValueType::boolean>("check-db")) {
        specmicp::check_db(input_file);
        return EXIT_SUCCESS;
    }


    std::string database_dir =
            parser.get_option<cli::ValueType::string>("database_dir");
    std::string working_dir =
            parser.get_option<cli::ValueType::string>("workdir");
    bool allow_overwrite = parser.get_option<cli::ValueType::boolean>("overwrite");
    bool clean_outputs = parser.get_option<cli::ValueType::boolean>("clean");
    bool only_pretend = parser.get_option<cli::ValueType::boolean>("pretend");


    // database directories
    std::vector<std::string> db_dirs;
    if (not database_dir.empty()) {db_dirs.push_back(database_dir);}
    db_dirs.push_back(utils::get_current_directory());
    io::add_db_dirs_from_env(db_dirs);
    if (not working_dir.empty()) {db_dirs.push_back(working_dir);}

    // All io files
    io::AllIOFilesMode mode_flag = io::AllIOFilesMode::ErrorIfExist;
    if (allow_overwrite) {
        mode_flag = io::AllIOFilesMode::Write;
    }

    const auto splitted_input = utils::split_filepath(input_file);
    std::string all_io_path = splitted_input[1] + "_io.yml";
    if (not working_dir.empty()) {
        utils::complete_path(working_dir, all_io_path);
    } else if (not splitted_input[0].empty()) {
        utils::complete_path(splitted_input[0], all_io_path);
    }

    std::unique_ptr<io::AllIOFiles> all_io_files =
            make_unique<io::AllIOFiles>(all_io_path, mode_flag);

    auto conf = io::YAMLConfigFile::load(input_file);
    all_io_files->add_configuration_file(io::input_file("Input", input_file));

    if (clean_outputs) {
        if (only_pretend) {
            return EXIT_SUCCESS;
        }
        all_io_files->clean_output_files();
        all_io_files.reset(nullptr);

        return EXIT_SUCCESS; // just clean, nothing to run
    }

    std::unique_ptr<std::ostream> out_log;
    std::unique_ptr<std::ostream> conf_log;
    if (conf.has_section(SPC_CF_S_LOGS))
    {
        out_log = specmicp::io::configure_log(
                      conf.get_section(SPC_CF_S_LOGS), all_io_files.get());
    }
    if (conf.has_section(SPC_CF_S_CONF_LOGS))
    {
        conf_log = specmicp::io::configure_conf_log(
                       conf.get_section(SPC_CF_S_CONF_LOGS), all_io_files.get());
    }

    RawDatabasePtr the_database = io::configure_database(
           conf.get_section(SPC_CF_S_DATABASE), db_dirs);
    all_io_files->add_database(io::input_file("database", the_database->metadata.path));
    units::UnitsSet the_units = io::configure_units(conf.get_section(SPC_CF_S_UNITS));

    AdimensionalSystemSolverOptions the_options;
    io::configure_specmicp_options(the_options, the_units,
                                   conf.get_section(SPC_CF_S_SPECMICP));

    if (not conf.has_section(SECTION_SPECIATION))
    {
        conf.report_error(io::YAMLConfigError::MissingRequiredSection,
                                   "No system to solve !");
    }
    auto conf_formulation = conf.get_section(SECTION_SPECIATION);

    if (conf_formulation.is_sequence())
    {
        uindex_t size = conf_formulation.size();
        for (uindex_t ind=0; ind<size; ++ind)
        {
            run_formulation(
                        conf_formulation.get_section(ind),
                        the_database, the_options, all_io_files.get()
                        );
        }
    }
    else
    {
        run_formulation(std::move(conf_formulation),
                        the_database, the_options, all_io_files.get());
    }


    all_io_files->sync();
    return EXIT_SUCCESS;
}


namespace specmicp {

void run_formulation(
        io::YAMLConfigHandle&& conf,
        const RawDatabasePtr& the_database,
        const AdimensionalSystemSolverOptions& the_options,
        io::AllIOFiles* all_io_files
       )
{
    auto name = conf.get_required_attribute<std::string>(ATTRIBUTE_NAME);
    std::string save_db = "";
    if (conf.has_attribute(ATTRIBUTE_OUTPUT_DB))
    {
        save_db = conf.get_attribute<std::string>(ATTRIBUTE_OUTPUT_DB);
    }
    std::string save_solution = "";
    if (conf.has_attribute(ATTRIBUTE_OUTPUT))
    {
        save_solution = conf.get_attribute<std::string>(ATTRIBUTE_OUTPUT);
    }
    else
    {
        WARNING << "No output file set, the solution will be lost in the void.";
    }


    auto rbox = io::configure_specmicp_reactant_box(
                    the_database, the_options.units_set,
                    std::move(conf)
                    );

    auto constraints = rbox.get_constraints(true);

    if (not save_db.empty())
    {
        database::Database(the_database).save(save_db);
        all_io_files->add_database(io::output_file(name, save_db));
    }


    SmartAdimSolver solver(the_database, constraints, the_options);

    // initialization
    if (conf.has_section(SPC_CF_S_INITIALIZATION))
    {
        io::configure_smart_solver_initialization(
                    solver,
                    conf.get_section(SPC_CF_S_INITIALIZATION)
                    );
    }
    // solve the problen


    auto is_solved = solver.solve();
    if (not is_solved) {
        CRITICAL << "Failed to solve formulation : " << name << ".";
        return;
    }

    if (not save_solution.empty())
    {
        io::save_solution_yaml(save_solution, the_database,
                               solver.get_solution(), save_db);
        all_io_files->add_solution(io::output_file(name, save_solution));
    }

}

void check_db(const std::string& filepath)
{
    std::cout << "Checking database : " << filepath << "\n ------ \n";

    database::Database the_database(filepath);
    if (not the_database.is_valid())
    {
        throw std::invalid_argument("The database is not valid !");
    }
    std::cout << "Database is valid !" << std::endl;
}

} //end namespace specmicp
