/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */


#include "specmicp_common/log.hpp"
#include "specmicp/adimensional/adimensional_system_solver.hpp"
#include "specmicp/adimensional/adimensional_system_solution.hpp"
#include "specmicp/problem_solver/formulation.hpp"
#include "specmicp/problem_solver/dissolver.hpp"
#include "specmicp/adimensional/adimensional_system_solution_extractor.hpp"

#include "specmicp/adimensional/equilibrium_curve.hpp"

#include "specmicp/adimensional/adimensional_system_solver.hpp"

#include "specmicp/adimensional_kinetics/kinetic_variables.hpp"
#include "specmicp/adimensional_kinetics/kinetic_model.hpp"
#include "specmicp/adimensional_kinetics/kinetic_system_euler_solver.hpp"
#include "specmicp/adimensional_kinetics/kinetic_system_solver.hpp"

#include "specmicp_database/database.hpp"
#include "specmicp_database/aqueous_selector.hpp"

#include <iostream>

specmicp::RawDatabasePtr get_momas_db()
{
    specmicp::database::Database thedatabase("../data/momas_benchmark.yaml");
    thedatabase.remove_components({"X5",});
    specmicp::database::AqueousSelector selector(thedatabase.get_database());
    selector.remove_aqueous({selector.aqueous_label_to_id("C6"), selector.aqueous_label_to_id("C7")});
    thedatabase.save("out_momas.db");
    return thedatabase.get_database();
}

class AutomaticReactionPath: public specmicp::EquilibriumCurve
{
public:

    AutomaticReactionPath()
    {
        specmicp::RawDatabasePtr raw_data =  get_momas_db();
        set_database(raw_data);

        specmicp::Vector total_concentrations(raw_data->nb_component());
        total_concentrations << 1, 0.0, -3.0, 0.0, 1.0;

        constraints() = specmicp::AdimensionalSystemConstraints(total_concentrations);
        constraints().disable_conservation_water();

        constraints().surface_model.model_type = specmicp::SurfaceEquationType::Equilibrium;
        constraints().surface_model.concentration = 1.0;

        specmicp::AdimensionalSystemSolverOptions& options = solver_options();
        options.solver_options.maxstep = 1.0;
        options.solver_options.max_iter = 200;
        options.solver_options.maxiter_maxstep = 200;
        options.solver_options.use_crashing = false;
        options.solver_options.use_scaling = true;
        options.solver_options.non_monotone_linesearch = false;
        options.solver_options.factor_descent_condition = -1;
        options.solver_options.factor_gradient_search_direction = 100;
        options.solver_options.projection_min_variable = 1e-9;

        options.solver_options.fvectol = 1e-6;
        options.solver_options.steptol = 1e-14;
        options.system_options.non_ideality_tolerance = 1e-8;
        options.system_options.non_ideality = false;
        options.use_pcfm = true;
        //solve_first_problem();


        specmicp::Vector variables;
        specmicp::AdimensionalSystemSolver solver(raw_data, constraints(), solver_options());

        solver.initialize_variables(variables,
                                    0.8,
                                    {{"X2", -3}, {"X4", -11}},
                                    std::unordered_map<std::string, specmicp::scalar_t>(),
                                    0.5
                                    );
        solver.run_pcfm(variables);

        specmicp::micpsolver::MiCPPerformance current_perf = solver.solve(variables, false);
        std::cout << (int) current_perf.return_code << std::endl;

        solution_vector() = variables;
        initialize_solution(solver.get_raw_solution(variables));

        std::cout << variables << std::endl;

        std::cout << "Buffering \t";
        for (specmicp::index_t mineral: raw_data->range_mineral())
        {
            std::cout << "\t" << raw_data->get_label_mineral(mineral);
        }
        std::cout << "\t S";
        for (specmicp::index_t sorbed: raw_data->range_sorbed())
        {
            std::cout << "\t" << raw_data->get_label_sorbed(sorbed);
        }
        std::cout << std::endl;

        output();

    }

    void output()
    {
        specmicp::AdimensionalSystemSolutionExtractor sol(current_solution(), database(), solver_options().units_set);
        std::cout <<  constraints().total_concentrations(1);
        for (specmicp::index_t mineral: database()->range_mineral())
        {
            std::cout << "\t" << sol.mole_concentration_mineral(mineral);
        }
        std::cout << "\t" << sol.free_surface_concentration();
        for (specmicp::index_t sorbed: database()->range_sorbed())
        {
            std::cout << "\t" << sol.molality_sorbed_species(sorbed);
        }
        std::cout << std::endl;
    }

    void update_problem()
    {
        constraints().total_concentrations(1) += 0.1;
        constraints().total_concentrations(2) += 0.1;
        constraints().total_concentrations(3) += 0.1;
    }

private:
    specmicp::index_t id_h2o;
    specmicp::index_t id_ho;
    specmicp::index_t id_hco3;
    specmicp::index_t id_co2g;
    specmicp::index_t id_ca;
};

class MomasKinetics: public specmicp::kinetics::AdimKineticModel
{
public:

    MomasKinetics():
        specmicp::kinetics::AdimKineticModel({0}),
        m_id_cc(0),
        m_id_c3(2),
        m_id_x4(4)
    {}

    //! \brief Compute the kinetic rates and store them in dydt
    void compute_rate(
            specmicp::scalar_t t,
            const specmicp::Vector& y,
            specmicp::kinetics::AdimKineticVariables& variables,
            specmicp::Vector& dydt
            )
    {
        dydt.resizeLike(y);

        specmicp::AdimensionalSystemSolution& solution = variables.equilibrium_solution();
        specmicp::scalar_t factor = std::pow(1e3*0.5*solution.secondary_molalities(m_id_c3),3)/
                          std::pow(1e3*0.5*pow10(solution.main_variables(m_id_x4)), 2);
        factor = 0.2*factor-1.0;
        specmicp::scalar_t k_constant = 10.0;
        if (factor >= 0)
            k_constant = 1e-2;
        dydt(0) = k_constant*factor;

    }


private:
    specmicp::index_t m_id_cc;
    specmicp::index_t m_id_c3;
    specmicp::index_t m_id_x4;
};

void solve_kinetics_problem()
{
    specmicp::RawDatabasePtr raw_data =  get_momas_db();

    specmicp::Vector total_concentrations(6);
    total_concentrations << 1, 0.6, -2.4, 0.6, 1.0, 1.0;

    specmicp::AdimensionalSystemConstraints constraints(total_concentrations);
    constraints.disable_conservation_water();


    specmicp::AdimensionalSystemSolverOptions options;
    options.solver_options.maxstep = 10.0;
    options.solver_options.max_iter = 200;
    options.solver_options.maxiter_maxstep = 200;
    options.solver_options.use_crashing = false;
    options.solver_options.use_scaling = true;
    options.solver_options.non_monotone_linesearch = true;
    options.solver_options.factor_descent_condition = -1;
    options.solver_options.factor_gradient_search_direction = 50;
    options.solver_options.projection_min_variable = 1e-9;
    options.solver_options.fvectol = 1e-8;
    options.solver_options.steptol = 1e-10;
    options.system_options.non_ideality_tolerance = 1e-10;
    options.system_options.non_ideality = false;

    specmicp::Vector equil_variables;

    specmicp::AdimensionalSystemSolver solver(raw_data, constraints, options);
    solver.initialize_variables(equil_variables, 0.8, -3);
    specmicp::micpsolver::MiCPPerformance current_perf = solver.solve(equil_variables, false);
    std::cout << (int) current_perf.return_code << std::endl;

    specmicp::AdimensionalSystemSolution equil_solution = solver.get_raw_solution(equil_variables);

    std::shared_ptr<specmicp::kinetics::AdimKineticModel> model = std::make_shared<MomasKinetics>();

    specmicp::Vector mineral_concentrations(1);
    mineral_concentrations << 5.0;

    specmicp::kinetics::AdimKineticSystemEulerSolver kinetic_solver(
    //specmicp::kinetics::AdimKineticSystemSolver kinetic_solver(
                model,
                total_concentrations,
                mineral_concentrations,
                constraints,
                equil_solution,
                raw_data);
    options.solver_options.use_scaling = false;
    kinetic_solver.get_options().speciation_options = options;

    kinetic_solver.solve(0.005, 1.0);




    std::cout << "Final amount : " <<  kinetic_solver.variables().concentration_mineral(0) << std::endl;
}

int main()
{
    specmicp::logger::ErrFile::stream() = &std::cerr;
    specmicp::stdlog::ReportLevel() = specmicp::logger::Debug;

 //   solve_kinetics_problem();

    AutomaticReactionPath test_automatic;

    for (int i=0; i<20; ++i)
    {
        test_automatic.run_step();
    }


    return EXIT_SUCCESS;
}
